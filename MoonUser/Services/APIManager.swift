//
//  ServerManeger.swift
//  OrcasTask
//
//  Created by Hyper Design on 12/27/18.
//  Copyright © 2018 Hyper Design. All rights reserved.
//


import Foundation
import Alamofire
import MOLH


enum ErrorCode:Int {
    case Caneled = -999
    case NoInternet = -1009
    case UnKnown = 000
}

enum StatusCode:Int {
    case Success = 200
    case Failed = 401
    case ExpiredJWT = 403
}


typealias errorType = (ErrorCode, Any?) -> ()
typealias errorCompletionType = ((ErrorCode?)->())

class APIManager {
    
    func contectToApiWith(url : String , methodType : HTTPMethod ,params : [String:Any]?, success: @escaping (Any) -> (), errorHandler: @ escaping errorType ) {
        
        let HEADER = ["lang": MOLHLanguage.currentAppleLanguage(),
                      "Jwt": DEF.userData.jwt_token]

        print("$ HEADER ", HEADER)
        print("$ URL:", url)
        if params != nil {
            print("$Param:", params!)
        }
        
        Alamofire.request(url.fixedArabicURL,
                          method: methodType,
                          parameters: params,
                          encoding: URLEncoding.default, headers: HEADER).validate().responseJSON{ response in
                            if response.result.error != nil {
                                if let errorCodeValue = response.error?._code,
                                    let errorCode = ErrorCode(rawValue: errorCodeValue){
                                    errorHandler(errorCode, response.error)
                                } else {
                                    errorHandler(ErrorCode.UnKnown, response.error)
                                }
                                return
                            }
                            
                            if response.data?.count == 0 {
                                errorHandler(ErrorCode.UnKnown, "No Data Retrived")
                                return
                            }
                            if let responseValue = response.value {
                                success(responseValue)
                                print("Response:", responseValue)
                            }
        }
        
        
    }    
}

