//
//  AppConstants.swift
//  OrcasTask
//
//  Created by Hyper Design on 12/27/18.
//  Copyright © 2018 Hyper Design. All rights reserved.
//


import Foundation

//TODO:- API URLs
enum URLs {
    //make in-accissible properties private
    private static let BASE = "https://moon4online.com/api/"
    
    case firebase
    case countries
    case checkPhone
    case settings
    case register
    case changeForgetPassword
    case login
    
    case homeServices(Int)
    case allStories(Int)
    case shopsList
    case tags
    case serviceDiscover
    case allDiscovers
    case searchShop
    case shopDetails
    case getProductFromCategory
    case createFollow
    case userReviewShop
    case addReviewShop
    case getDetailsProduct
    case addOrUpdateCart
    case deleteCart
    case cart
    case getShippingCompany
    case confirmOrder
    case ordersList
    case orderDetails
    case deleteOrder(Int)
    
    case institutionsList
    case companiesList
    case categoriesFilter
    case adsDetails
    case advertisementsList
    case addAdvertise
    case editAdvertise
    case MyAds
    case deleteAdvertise
    
    case famous
    case famousDiscover
    case famousSearch
    case famousDetails
    case albumsImages
    case filterShopAds
    case filter_shop_ads
    
    case clinics
    case clinicDetails
    case doctorSchedules
    case reservationOrder
    case reservations
    case cancelAppointment
    case AppointmentDetails
    
    case updateProfile
    case notifications
    
    case createTrip
    case tripDetails
    case cancelTrip
    case rateTrip
    case rideHistory
    case allTrucks
    
    case allChats
    case chat
    case orderChat
    case sendMessage
    case sendOrderMessage
    case favorite
    case addFavourite

    case contactUs
    
    var url: String {
        switch self {
            case .firebase:                                         return URLs.BASE + "firebase"
            case .countries:                                        return URLs.BASE + "getCountries"
            case .checkPhone:                                       return URLs.BASE + "checkPhone"
            case .settings:                                         return URLs.BASE + "settings"
            case .register:                                         return URLs.BASE + "register"
            case .changeForgetPassword:                             return URLs.BASE + "changeForgetPassword"
            case .login:                                            return URLs.BASE + "login"
            case .homeServices(let id):                             return URLs.BASE + "services?country_id=\(id)"
            case .allStories(let id):                               return URLs.BASE + "stories?country_id=\(id)"
            case .shopsList:                                        return URLs.BASE + "shops_list"
            case .tags:                                             return URLs.BASE + "tags"
            case .allDiscovers:                                     return URLs.BASE + "famous/main-service-categories"
            case .serviceDiscover:                                  return URLs.BASE + "discover"
            case .searchShop:                                       return URLs.BASE + "searchShop"
            case .shopDetails:                                      return URLs.BASE + "shopDetails"
            case .getProductFromCategory:                           return URLs.BASE + "getProductFromCategory"
            case .createFollow:                                     return URLs.BASE + "create_follow"
            case .userReviewShop:                                   return URLs.BASE + "userReviewShop"
            case .addReviewShop:                                    return URLs.BASE + "addReviewShop"
            case .getDetailsProduct:                                return URLs.BASE + "getDetailsProduct"
            case .addOrUpdateCart:                                  return URLs.BASE + "user/cart/add_or_update"
            case .deleteCart:                                       return URLs.BASE + "user/cart/delete"
            case .cart:                                             return URLs.BASE + "user/cart"
            case .getShippingCompany:                               return URLs.BASE + "user/getShippingCompany"
            case .confirmOrder:                                     return URLs.BASE + "order/user/confirm"
            case .ordersList:                                       return URLs.BASE + "order/user/list"
            case .orderDetails:                                     return URLs.BASE + "order/user/orderDetails"
            case .deleteOrder(let id):                                     return URLs.BASE + "order/user/order-delete\(id)"

            case .institutionsList:                                 return URLs.BASE + "Institution/institutions_list"
            case .companiesList:                                    return URLs.BASE + "companies/companies_list"
            case .categoriesFilter:                                 return URLs.BASE + "companies/services_filter"
            case .adsDetails:                                       return URLs.BASE + "Advertise/advertiseDetails"
            case .advertisementsList:                               return URLs.BASE + "Advertise/advertisements_list"
            case .addAdvertise:                                     return URLs.BASE + "Advertise/add_advertise"
            case .MyAds:                                            return URLs.BASE + "Advertise/advertisements"
            case .deleteAdvertise:                                  return URLs.BASE + "Advertise/deleteAdvertise"
            case .editAdvertise:                                    return URLs.BASE + "Advertise/editAdvertise"
            case .famous:                                           return URLs.BASE + "famous"
            case .famousDiscover:                                   return URLs.BASE + "famous/discover"
            case .famousSearch:                                     return URLs.BASE + "famous/searchFamous"
            case .famousDetails:                                    return URLs.BASE + "famous/details"
            case .filterShopAds:                                    return URLs.BASE + "famous/filterShopAdds"
            case .filter_shop_ads:                                  return URLs.BASE + "filter-shop-ads"
            case .albumsImages:                                     return URLs.BASE + "famous/albumsImages"
            case .clinics:                                          return URLs.BASE + "clinics/clinics_list"
            case .clinicDetails:                                    return URLs.BASE + "clinics/clinicDetails"
            case .doctorSchedules:                                  return URLs.BASE + "clinics/doctorSchedules"
            case .reservationOrder:                                 return URLs.BASE + "clinics/reservation_order"
            case .reservations:                                     return URLs.BASE + "clinics/orders/list"
            case .cancelAppointment:                                return URLs.BASE + "clinics/confirmOrder"
            case .AppointmentDetails:                               return URLs.BASE + "clinics/orderDetails"
            case .updateProfile:                                    return URLs.BASE + "profile/edit"
            case .notifications:                                    return URLs.BASE + "notification"
            case .createTrip:                                       return URLs.BASE + "taxi/user/create"
            case .tripDetails:                                      return URLs.BASE + "taxi/trip_details"
            case .cancelTrip:                                       return URLs.BASE + "taxi/driver/accept_trip"
            case .rateTrip:                                         return URLs.BASE + "taxi/driver/rate"
            case .rideHistory:                                      return URLs.BASE + "taxi/trip_history"
            case .allTrucks:                                        return URLs.BASE + "taxi/trucks_list"
            case .allChats:                                         return URLs.BASE + "chat/all_chats"
            case .chat:                                             return URLs.BASE + "chat"
            case .orderChat:                                        return URLs.BASE + "order/user/chat"
            case .sendMessage:                                      return URLs.BASE + "chat/create_chat"
            case .sendOrderMessage:                                 return URLs.BASE + "order/user/orderChat"
            case .favorite:                                         return URLs.BASE + "user/favorite"
            case .addFavourite:                                     return URLs.BASE + "user/favorite/add_or_remove"
            case .contactUs:                                        return URLs.BASE + "contact-us"
        }
    }
    
}


