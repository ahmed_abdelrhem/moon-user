//
//  Languages.swift
//  Moon Shop
//
//  Created by apple on 1/21/20.
//  Copyright © 2020 2grand. All rights reserved.
//

import Foundation
class Languages {
    private (set) var id : Int?
    private (set) var name: String?
    private (set) var shortcut: String?
    
    init(_ id: Int,_ name:String,_ shortcut:String) {
        self.id = id
        self.name = name
        self.shortcut = shortcut
    }
}

class CreateLanguages {
    
    private let languageArray = ["English", "Arabic", "French", "German", "Italian", "Armenian", "Czech", "Indian", "Korean", "Polish", "Turkish", "Russian", "Andalusia", "Bangladeshi", "Thai", "Filipino", "Chinese"]
    
    private let shortcut = ["en", "ar", "fr", "de", "it", "hy", "cs", "hi", "ko", "pl", "tr", "ru", "id", "bn", "th", "fil", "zh"]
    
    private (set) var languages : [Languages] = []
    
    init() {
        for i in 0..<languageArray.count {
            languages.append(Languages.init(i, languageArray[i], shortcut[i]))
        }
    }
    
}





