//
//  LanguageVC.swift
//  Moon Shop
//
//  Created by apple on 1/21/20.
//  Copyright © 2020 2grand. All rights reserved.
//

import UIKit
import MOLH


class LanguageVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var submitBtn: UIButtonX!
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK:- Variables
    internal var languages =  CreateLanguages().languages
    var selectedLanguage: Languages?
    
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        SharedHandller.instance.sideMenus(self)
        setTableView()
        updateUI()
    }
    
    
    //MARK:- Functions
    private func setTableView()  {
        tableView.register(UINib(nibName: "MenuCell", bundle: nil), forCellReuseIdentifier: "MenuCell")
        tableView.allowsMultipleSelection = false
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
    }
    
    internal func  updateUI() {
        submitBtn.isHighlighted = selectedLanguage == nil
        submitBtn.isUserInteractionEnabled = selectedLanguage != nil
    }
    
    
    //MARK:- Actions
    @IBAction func submitBtnPressed(_ sender: UIButton) {
        Messages.instance.demoCentered(title: ATTENTION, body: LANG_MSG, buttonTitle: OK) { (success) in
            if success {
                MOLH.setLanguageTo(self.selectedLanguage?.shortcut ?? "")
                DEF.language = self.selectedLanguage?.name ?? ""
                self.changeCurrency(completion: { (success) in
                    if success {
                        MOLH.reset()
                        exit(0)
                    }
                })
            }
        }
    }
    
    
    func changeCurrency(completion: @escaping CompletionHandler) {

        let interactor = CountryVCInteractor()

        interactor.getCountries(didDataReady: { [weak self](model) in
            guard self != nil else { return }
            if model.status == StatusCode.Success.rawValue {
                DEF.allCountries = model.data ?? []
                guard !DEF.isFirstTime, let c = DEF.allCountries.first(where: {$0.id == DEF.country.id}) else {
                    completion(true)
                    return
                }
                DEF.country = c
                completion(true)
            } else {
                completion(false)
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            completion(false)
        }


    }

    
}
