//
//  TurnOnLocationVC.swift
//  MoonUser
//
//  Created by Grand iMac on 1/22/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import GoogleMaps


class TurnOnLocationVC: UIViewController {

    private let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func turnOnLocationBtnClicked(_ sender: UIButton) {
        self.locationAuth()
    }
    


}


extension TurnOnLocationVC: CLLocationManagerDelegate {
    
    private func locationAuth() {
        
        switch CLLocationManager.authorizationStatus() {
        case  .restricted, .denied:
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)! as URL, options: [:], completionHandler: nil)
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        case .notDetermined:
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
        @unknown default:
            fatalError()
        }
        
    }
    
    private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if (status == .authorizedAlways) || (status == .authorizedWhenInUse) {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        DEF.currentLat = "\(locValue.latitude)"
        DEF.currentLng = "\(locValue.longitude)"
        self.getAddressFromLocation(lat: Double(locValue.latitude), long: Double(locValue.longitude))
        locationManager.stopUpdatingLocation()
        self.PresentIntroScreens()
    }
    
    
    func PresentIntroScreens() {
        let mainStorybord: UIStoryboard = UIStoryboard(name: Storyboard.Main.name, bundle: nil)
        let vc = mainStorybord.instantiateViewController(withIdentifier: Identifiers.Pages.id) as! PageViewController
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
    
    private func getAddressFromLocation(lat: Double, long: Double) {
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = long
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil) {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    print(addressString)
                    DEF.currentAdd = addressString
                }
        })
    }


}
