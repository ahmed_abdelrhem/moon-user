//
//  ThirdViewController.swift
//  MoonUser
//
//  Created by Grand iMac on 1/22/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController {

    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    //MARK:- Functions
    private func StartHomeScreen() {
        let storyboard = UIStoryboard(name: Storyboard.Tabs.name, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: Identifiers.SWReveal.id)
        UIApplication.shared.keyWindow?.rootViewController = vc
    }

    
    //MARK:- Actions
    @IBAction func startBtnClicked(_ sender: UIButton) {
        StartHomeScreen()
        DEF.isFirstTime = false
    }
    
    


}
