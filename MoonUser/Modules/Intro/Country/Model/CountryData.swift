/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation

struct CountryData : Codable {
    var id : Int = 0
    var country_name : String = ""
	var country_image : String = ""
	var code : String = ""
    var currency : String = "$"
	var cities : [Cities] = []

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case country_name = "country_name"
		case country_image = "country_image"
		case code = "code"
        case currency = "currency"
		case cities = "cities"
	}
    
    init() {
        self.id = 0
        self.country_name = ""
        self.country_image = ""
        self.code = ""
        self.currency = "$"
        self.cities = []
    }

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try (values.decodeIfPresent(Int.self, forKey: .id) ?? 0)
        country_name = try (values.decodeIfPresent(String.self, forKey: .country_name) ?? "")
        country_image = try (values.decodeIfPresent(String.self, forKey: .country_image) ?? "")
        code = try (values.decodeIfPresent(String.self, forKey: .code) ?? "")
        currency = try (values.decodeIfPresent(String.self, forKey: .currency) ?? "")
        cities = try (values.decodeIfPresent([Cities].self, forKey: .cities) ?? [])
	}

}
