//
//  CountryVCInteractor.swift
//  Moon Shop
//
//  Created by apple on 1/21/20.
//  Copyright © 2020 2grand. All rights reserved.
//


import Foundation

class CountryVCInteractor {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias model = (Countries_Base)->()
    
    func getCountries( didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        apiManager.contectToApiWith(url: URLs.countries.url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(Countries_Base.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
}
