//
//  CountryVC.swift
//  Moon Shop
//
//  Created by apple on 1/20/20.
//  Copyright © 2020 2grand. All rights reserved.
//

import UIKit

class CountryVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var submitBtn: UIButton!
    
    
    //MARK:- Variables
    internal var presenter : CountryVCPresenter!

    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        SharedHandller.instance.sideMenus(self)
        setTableView()
        setPresenter()
        updateUI()
    }
    
    
    //MARK:- Functions
    private func setPresenter() {
        presenter = CountryVCPresenter(view: self)
        presenter.viewdidload()
    }
    
    private func setTableView()  {
        tableView.register(UINib(nibName: "MenuCell", bundle: nil), forCellReuseIdentifier: "MenuCell")
        tableView.allowsMultipleSelection = false
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
    }

    
    internal func  updateUI() {
        submitBtn.isHighlighted = presenter.country.id == 0
        submitBtn.isUserInteractionEnabled = presenter.country.id != 0
    }
    

    //MARK:- Actions
    @IBAction func submitBtnPressed(_ sender: UIButton) {
        presenter.submitBtnPressed()
    }
    
}
