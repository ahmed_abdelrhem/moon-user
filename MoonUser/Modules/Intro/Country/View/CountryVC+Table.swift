//
//  CountryVC+Table.swift
//  Moon Shop
//
//  Created by apple on 1/21/20.
//  Copyright © 2020 2grand. All rights reserved.
//

import UIKit
extension CountryVC:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.listCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuCell
        presenter.configureCell(cell: cell, at: indexPath.row)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return CHOOSE_COUNTRY
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didselect(at: indexPath.row)
        self.updateUI()
    }
    
}
