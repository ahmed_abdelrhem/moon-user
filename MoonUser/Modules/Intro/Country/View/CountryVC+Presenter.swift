//
//  CountryVC+Presenter.swift
//  Moon Shop
//
//  Created by apple on 1/21/20.
//  Copyright © 2020 2grand. All rights reserved.
//

import Foundation

extension CountryVC: CountryVCView {
    
    func showProgress() {
        startAnimating()
    }
    
    func hideProgress() {
        stopAnimating()
    }
        
    func onSuccess(_ msg: String) {
        tableView.reloadData()
    }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func onEmpty(_ img: UIImage,_ msg: String) {
        tableView.setEmptyView(title: msg, messageImage: img)
    }
    

    func PresentLocationScreens() {
        let mainStorybord: UIStoryboard = UIStoryboard(name: Storyboard.Main.name, bundle: nil)
        let vc = mainStorybord.instantiateViewController(withIdentifier: Identifiers.TurnLocation.id) as! TurnOnLocationVC
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
    
    func goToHomeScreen() {
        homeScreen("")
    }

    
}
