//
//  CountryVCPresenter.swift
//  Moon Shop
//
//  Created by apple on 1/21/20.
//  Copyright © 2020 2grand. All rights reserved.
//


import Foundation

protocol CountryVCView: LoaderDelegate {
    func PresentLocationScreens()
    func goToHomeScreen()
}

class CountryVCPresenter {
    
    private weak var view: CountryVCView?
    private let interactor = CountryVCInteractor()
    private (set) var country = CountryData()
    
    
    init(view: CountryVCView) {
        self.view = view
    }
    
    func viewdidload()  {
        if DEF.allCountries.count == 0 {
            view?.showProgress()
            
            interactor.getCountries(didDataReady: { [weak self](model) in
                guard self != nil else { return }
                
                if model.status == StatusCode.Success.rawValue {
                    DEF.allCountries = model.data ?? []
                    if DEF.allCountries.count == 0 {
                        self?.view?.onEmpty(.no_data_image, NO_DATA_FOUND)
                    } else {
                        self?.view?.onSuccess("")
                    }
                } else {
                    self?.view?.onFailure(model.message ?? "")
                }
                self?.view?.hideProgress()
            }) { [weak self](error) in
                guard self != nil else { return }
                self?.view?.hideProgress()
                self?.view?.onEmpty(.no_connection_image, CONNECTION_ERROR)
            }
        }
    }
    
    
    var listCount: Int {
        return DEF.allCountries.count
    }
    

    func configureCell(cell: MenuCellView,at row: Int)  {
        let item = DEF.allCountries[row]
        cell.setName(name: item.country_name)
        cell.setPhoto(with: item.country_image)
    }

    func didselect(at row: Int)  {
        let item = DEF.allCountries[row]
        self.country = item
    }
    
    func submitBtnPressed() {
        DEF.country = self.country
        if DEF.currentLat != "" {
            view?.goToHomeScreen()
        } else {
            view?.PresentLocationScreens()
        }
    }
    
}
