//
//  ProfileVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 3/3/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation


typealias CountryHandler = (_ country: CountryData) -> ()


protocol ProfileVCView: LoaderDelegate {
    func changeProfileImage(img: UIImage)
    func createCountryPickerView(_ completion: @escaping CountryHandler)
    func presentMap()
    func navigateTochangePasswordVC()
}

class ProfileVCPresenter {
    
    private weak var view: ProfileVCView?
    private let interactor = ProfileVCAPI()
    private var profileImage: UIImage?
    private var countryID = DEF.userData.country_id
    private var lat: String = DEF.userData.lat
    private var lng: String = DEF.userData.lng

    
    init(view: ProfileVCView) {
        self.view = view
    }
    
    
    func profileImageClicked(_ vc: UIViewController, _ sender: AnyObject) {
        AttachmentHandler.shared.showAttachmentImage(vc, sender)
        
        AttachmentHandler.shared.singleImagePickedBlock = { (image, _) in
            DispatchQueue.main.async {
                self.profileImage = image
                self.view?.changeProfileImage(img: image)
            }
        }
    }

    func countryTxtClicked() {
        view?.createCountryPickerView({ [weak self](country) in
            guard self != nil else { return}
            self?.countryID = country.id
        })
    }
    
    func addressBtnClicked() {
        view?.presentMap()
    }
    
    func RetriveLocation(lat: Double, lng: Double) {
        self.lat = "\(lat)"
        self.lng = "\(lng)"
    }


    func changePasswordBtnClicked() {
        view?.navigateTochangePasswordVC()
    }

    func saveBtnClicked(name: String?, email: String?, phone: String?, address: String) {
        
        guard name != "", email != "", phone != "" else {
            view?.warringMSG(FILL_ALL_DATA)
            return
        }
        
        view?.showProgress()
        
        interactor.updateProfileData(image: self.profileImage, name: name!, email: email!, phone: phone!, countryID: countryID, lat: lat, lng: lng, address: address, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                DEF.userData = response.data!
                self?.view?.onSuccess(response.message ?? "")
            } else if response.status == StatusCode.ExpiredJWT.rawValue {
                Messages.instance.loginMessage()
            } else {
                self?.view?.warringMSG(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
        
        
    }
}
