//
//  ProfileVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 3/3/20.
//  Copyright © 2020 Organization. All rights reserved.
//


import Foundation
import Alamofire
import MOLH

class ProfileVCAPI {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    
    typealias model = (LoginModel)->()

    
    func updateProfileData(image: UIImage?, name: String, email: String, phone: String, countryID: Int, lat: String, lng: String, address: String, didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param = [
            "name": name,
            "email": email,
            "phone": phone,
            "country_id": "\(countryID)",
            "lat": lat,
            "lng": lng,
            "address": address,
            "type": "13"
        ]
        
        let HEADER = ["lang": MOLHLanguage.currentAppleLanguage(),
                      "Jwt": DEF.userData.jwt_token]

        print("$$", HEADER, "\n", param)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if image != nil {
                multipartFormData.append(image!.jpegData(compressionQuality: 0.5)!, withName: "image", fileName: "profileImage\(Date.timeIntervalSinceReferenceDate).jpg", mimeType: "image/jpg")
            }

            for (key, value) in param {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: URLs.updateProfile.url, method: .post, headers: HEADER) { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { progress in
                    print("progress: \(progress.fractionCompleted)")
                    if progress.fractionCompleted == 1.0 {
                        print("upload complete")
                    }
                })
                upload.responseJSON { response in
                    print(response)
                    switch response.result {

                    case .success(_):
                        guard let data = response.data else { return }
                        do {
                            let json = try JSONDecoder().decode(LoginModel.self, from: data)
                            didDataReady(json)
                        } catch {
                            print("error : ", error)
                        }
                    case .failure(let error):
                        print("1:::\(error)")
                        errorCompletion(error as? ErrorCode)
                    }
                }

            case .failure(let error):
                print("2:::\(error)")
                errorCompletion(error as? ErrorCode)
            }
        }
    }

    
        
}
