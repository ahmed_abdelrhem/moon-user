//
//  ProfileVC.swift
//  MoonUser
//
//  Created by Grand iMac on 3/3/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import SDWebImage


class ProfileVC: BaseViewController {

    
    //MARK:- Outlets
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var dataScrollView: UIScrollView!
    @IBOutlet weak var userDataStack: UIStackView!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var phoneTxt: UITextField!
    @IBOutlet weak var countryTxt: UITextField!
    @IBOutlet weak var addressBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!

    
    //MARK:- Variables
    internal var presenter: ProfileVCPresenter!
    
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        SharedHandller.instance.sideMenus(self)
        setDelegates()
        addGestures()
        setPresenter()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setUI()
    }
    
    
    //MARK:- Functions
    private func setUI() {
        if DEF.isLogin {
            DispatchQueue.main.async {
                self.dataScrollView.isHidden = false
                self.loginView.isHidden = true
                let imgURL = URL(string: DEF.userData.image)
                self.profileImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
                self.profileImg.sd_setImage(with: imgURL, placeholderImage: .avatar_image)
                self.nameTxt.text = DEF.userData.name
                self.emailTxt.text = DEF.userData.email
                self.phoneTxt.text = DEF.userData.phone
                self.countryTxt.text = DEF.userData.country_name
                self.addressBtn.setTitle(DEF.userData.address, for: .normal)
                self.saveBtn.isUserInteractionEnabled = false
                self.saveBtn.isHighlighted = true
            }
        } else {
            self.loginView.isHidden = false
            self.dataScrollView.isHidden = true
        }
    }
    
    private func setDelegates() {
        nameTxt.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        emailTxt.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        phoneTxt.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
    }
    
    private func addGestures() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.profileImgClicked(_:)))
        profileImg.isUserInteractionEnabled = true
        profileImg.addGestureRecognizer(tap)

    }

    
    private func setPresenter() {
        presenter = ProfileVCPresenter(view: self)
    }
    
    
    
    //MARK:- Actions
    @objc func profileImgClicked(_ sende: UIImageView) {
        presenter.profileImageClicked(self, sende)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.saveBtn.isUserInteractionEnabled = true
        self.saveBtn.isHighlighted = false
    }
    
    @IBAction func countryTxtDidBeginEditing(_ sender: UITextField) {
        presenter.countryTxtClicked()
    }
    
    @IBAction func addressBtnClicked(_ sender: UIButton) {
        presenter.addressBtnClicked()
    }

    @IBAction func changePasswordBtnClicked(_ sender: UIButton) {
        presenter.changePasswordBtnClicked()
    }


    @IBAction func saveBtnClicked(_ sender: UIButton) {
        presenter.saveBtnClicked(name: nameTxt.text, email: emailTxt.text, phone: phoneTxt.text, address: addressBtn.title(for: .normal)!)
    }

}


//MARK:- Location Delegate
extension ProfileVC: LocationDelegate {
    
    func RetriveLocation(lat: Double, lng: Double, add: String) {
        presenter.RetriveLocation(lat: lat, lng: lng)
        addressBtn.setTitle(add, for: .normal)
        self.saveBtn.isUserInteractionEnabled = true
        self.saveBtn.isHighlighted = false
    }
    
    
}
