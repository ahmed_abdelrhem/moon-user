//
//  ProfileVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 3/3/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension ProfileVC: ProfileVCView {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .success, layout: .cardView, style: .center)
        self.saveBtn.isUserInteractionEnabled = false
        self.saveBtn.isHighlighted = true
    }
    
    func warringMSG(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .warning, layout: .statusLine, style: .top)
    }
        
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
        
    func changeProfileImage(img: UIImage) {
        profileImg.image = img
        self.saveBtn.isUserInteractionEnabled = true
        self.saveBtn.isHighlighted = false
    }
    
    func createCountryPickerView(_ completion: @escaping CountryHandler) {
        
        countryTxt.resignFirstResponder()
        
        var selectedIndex = 0
        
        let alert = UIAlertController(style: .actionSheet, title: "", message: "")
        
        let pickerViewValues: [[String]] = [DEF.allCountries.map { $0.country_name }]
        let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: 0)
        
        alert.addPickerView(values: pickerViewValues, initialSelection: pickerViewSelectedValue) { vc, picker, index, values in
                selectedIndex = index.row
        }
        
        alert.addAction(image: nil, title: DONE, color: .mainColor, style: .default, isEnabled: true) { [weak self] (action) in
            guard self != nil else { return }
            DispatchQueue.global(qos: .background).async {
                completion(DEF.allCountries[selectedIndex])
            }
            DispatchQueue.main.async {
                self?.countryTxt.text = DEF.allCountries[selectedIndex].country_name
                self?.saveBtn.isUserInteractionEnabled = true
                self?.saveBtn.isHighlighted = false
            }
        }
        
        alert.addAction(image: nil, title: CANCEL, color: .red, style: .cancel, isEnabled: true) { _ in
            alert.dismiss(animated: true, completion: nil)
        }
        
        present(alert, animated: true, completion: nil)
    }

    
    func presentMap() {
        let vc = MapVC()
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }

    func navigateTochangePasswordVC() {
        let vc = ChangePasswordVC()
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }



}
