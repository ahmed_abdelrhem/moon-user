//
//  ChangePasswordVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 3/3/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol ChangePasswordVCView: LoaderDelegate {
    func enableActionButton(_ isEnable: Bool)
}

class ChangePasswordVCPresenter {
    
    private weak var view: ChangePasswordVCView?
    private let interactor = NewPasswordVCAPI()
    
    init(view: ChangePasswordVCView) {
        self.view = view
    }
    
    func textFieldDidChange(old: String?, new: String?, confirm: String?) {
        guard old != "", new != "", confirm != "" else {
            view?.enableActionButton(false)
            return
        }

        if !UITextField.compare(testStr: new!, confirm: confirm!) {
            view?.enableActionButton(false)
            return
        }
        
        view?.enableActionButton(true)
        
    }
    
    func changePassword(old: String, new: String) {

        view?.showProgress()

        interactor.newPassword(oldPassword: old, newPassword: new, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.view?.onSuccess(response.message ?? "")
            } else if response.status == StatusCode.ExpiredJWT.rawValue {
                Messages.instance.loginMessage()
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
    
}
