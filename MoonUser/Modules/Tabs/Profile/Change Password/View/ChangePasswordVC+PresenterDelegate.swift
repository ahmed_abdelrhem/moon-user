//
//  ChangePasswordVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 3/3/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension ChangePasswordVC: ChangePasswordVCView {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .success, layout: .cardView, style: .center)
        navigationController?.popViewController(animated: true)
    }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }

    func enableActionButton(_ isEnable: Bool) {
        self.changeBtn.isUserInteractionEnabled = isEnable
        self.changeBtn.isHighlighted = !isEnable
    }


}
