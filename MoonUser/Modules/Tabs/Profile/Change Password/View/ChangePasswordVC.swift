//
//  ChangePasswordVC.swift
//  MoonUser
//
//  Created by Grand iMac on 3/3/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {

    
    //MARK:- Outlets
    @IBOutlet weak var changeBtn: UIButton!
    @IBOutlet weak var oldPasswordTxt: UITextField!
    @IBOutlet weak var newPasswordTxt: UITextField!
    @IBOutlet weak var confirmPasswordTxt: UITextField!
    @IBOutlet weak var viewOldPasswordBtn: UIButton!
    @IBOutlet weak var viewNewPasswordBtn: UIButton!

    
    
    //MARK:- Variables
    internal var presenter: ChangePasswordVCPresenter!
    
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
        setPresenter()
    }
    
    
    
    //MARK:- Functions
    private func updateUI() {
        self.changeBtn.isUserInteractionEnabled = false
        self.changeBtn.isHighlighted = true
        title = NEW_PASSWORD
        backButtonTitle("")
        
        oldPasswordTxt.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        newPasswordTxt.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        confirmPasswordTxt.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
    }

    private func setPresenter() {
        presenter = ChangePasswordVCPresenter(view: self)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        presenter.textFieldDidChange(old: oldPasswordTxt.text, new: newPasswordTxt.text, confirm: confirmPasswordTxt.text)
    }

    
    
    
    //MARK:- Actions
    @IBAction func viewOldPasswordBtnClicked(_ sender: UIButton) {
        oldPasswordTxt.isSecureTextEntry = !oldPasswordTxt.isSecureTextEntry
        viewOldPasswordBtn.isSelected = !viewOldPasswordBtn.isSelected
    }

    @IBAction func viewNewPasswordBtnClicked(_ sender: UIButton) {
        newPasswordTxt.isSecureTextEntry = !newPasswordTxt.isSecureTextEntry
        viewNewPasswordBtn.isSelected = !viewNewPasswordBtn.isSelected
    }

    @IBAction func changePasswordBrnClicked(_ sender: UIButton) {
        presenter.changePassword(old: oldPasswordTxt.text!, new: newPasswordTxt.text!)
    }
    

}
