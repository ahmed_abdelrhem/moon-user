//
//  AddReviewVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 2/4/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class AddReviewVCPresenter {
    
    private weak var view: LoaderDelegate?
    private let interactor = ReviewsVCAPI()
    
    init(view: LoaderDelegate) {
        self.view = view
    }
    
    func submitReviewBtnClicked(shopID: Int, comment: String, rate: Double) {
        
        guard comment != "" else {
            view?.warringMSG(FILL_ALL_DATA)
            return
        }
        
        view?.showProgress()
        
        interactor.addReviewShop(shopID: shopID, comment: comment, rate: rate, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.view?.onSuccess(REVIEW_DONE)
            } else if response.status == StatusCode.ExpiredJWT.rawValue {
                Messages.instance.loginMessage()
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
    
    
}
