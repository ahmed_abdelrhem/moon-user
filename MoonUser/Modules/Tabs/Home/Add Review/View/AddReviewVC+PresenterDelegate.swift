//
//  AddReviewVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 2/4/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension AddReviewVC: LoaderDelegate {
 
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        DispatchQueue.main.async {
            _NC.post(name: .addReview, object: nil)
        }
        Messages.instance.showConfigMessage(title: "", body: msg, state: .success, layout: .centeredView, style: .center)
        navigationController?.popViewController(animated: true)
    }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func warringMSG(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .warning, layout: .messageView, style: .top)
    }
    

}
