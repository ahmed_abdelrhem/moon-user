//
//  AddReviewVC.swift
//  MoonUser
//
//  Created by Grand iMac on 2/4/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class AddReviewVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var feedBackTxt: UITextView!
    
    
    //MARK: Variables
    internal var presenter: AddReviewVCPresenter!
    var shopID: Int!

    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setPresenter()
        title = Add_REVIEW
        backButtonTitle("")
    }

    
    //MARK:- Fucntions
    private func setPresenter() {
        presenter = AddReviewVCPresenter(view: self)
    }
    
    
    //MARK:- Actions
    @IBAction func submitBtnClicked(_ sender: UIButton) {
        presenter.submitReviewBtnClicked(shopID: self.shopID, comment: feedBackTxt.text, rate: rateView.rating)
    }
    

}
