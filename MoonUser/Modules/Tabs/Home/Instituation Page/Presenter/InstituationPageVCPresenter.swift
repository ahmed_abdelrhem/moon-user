//
//  InstituationPageVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 2/17/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol InstituationPageVCView: LoaderDelegate {
    func updateUI(_ details: ShopDetails?)
    func presentReviewsVC(_ shopID: Int)
    func changeFollowBtnSelected()
    func changeFollowCount(count: Int)
    func playVideo(_ url: String?)
    func showLocation(lat: Double, lng: Double)
    func navigateToChatPageVC(reciverType: Int, id: Int)
    
    func showDetails()
    func showBranches()
    func showGallary()
    func showAdds()
}

class InstituationPageVCPresenter {
    
    private weak var view: InstituationPageVCView?
    private let interactor = InstituationPageVCAPI()
    private let shopInteractor = ShopPageVCAPI()
    private var details: ShopDetails?
    private var categories = [CategoryService]()
    private var gallery = [DiscoverData]()
    private var adds = [DiscoverData]()
    private var branches = [Shop]()
    private var selectedIndex: Int = 0

    
    
    init(view: InstituationPageVCView) {
        self.view = view
    }
    
    func viewDidLoad(id: Int) {
        
        view?.showProgress()
        
        interactor.getInstituationData(id: id, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.details = response.data?.shop_details
                self?.categories = response.data?.categories ?? []
                self?.gallery = response.data?.gallery ?? []
                self?.adds = response.data?.adds ?? []
                self?.branches = response.data?.branches ?? []
                self?.view?.updateUI(self?.details)
                self?.view?.showDetails()
                self?.view?.onSuccess("")
            } else {
                self?.view?.onFailure(CONNECTION_ERROR)
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
    
    func reviewViewClicked() {
        view?.presentReviewsVC(self.details?.id ?? 0)
    }

    func followBtnClicked() {
        if DEF.isLogin {
            view?.changeFollowBtnSelected()
            if details?.isFollow ?? false {
                details?.isFollow = false
                let c = details?.followers_count ?? 0
                details?.followers_count = c - 1
                view?.changeFollowCount(count: (details?.followers_count!)!)
            } else {
                details?.isFollow = true
                let c = details?.followers_count ?? 0
                details?.followers_count = c + 1
                view?.changeFollowCount(count: (details?.followers_count!)!)
            }
            DispatchQueue.global(qos: .background).async {
                self.followShop(shopID: self.details?.id ?? 0)
            }
        } else {
            showLoginMessage()
        }
    }

    func tabsSegmentValueChanged(_ index: Int) {
        self.selectedIndex = index
        switch index {
        case 0:
            view?.showDetails()
        case 1:
            view?.showBranches()
        case 2:
            view?.showGallary()
        case 3:
            view?.showAdds()
        default:
            break
        }
    }
    
    func chatBtnClicked() {
        let id = details?.id ?? 0
        let type = DEF.type
        view?.navigateToChatPageVC(reciverType: type, id: id)
    }
    
    
    //MARK:- TableView Functions
    func serviceCell(cell: MenuCellView, section: Int, row: Int)  {
        switch section {
        case 0:
            cell.setName(name: details?.address ?? "")
            let img = UIImage(named: "location")!
            cell.setPhoto(with: img)
        case 1:
            cell.setName(name: details?.description ?? "")
            cell.setPhoto(with: "")
        case 2:
            if categories.count != 0 {
                let item = categories[row]
                cell.setName(name: item.name ?? "")
                cell.setPhoto(with: item.image ?? "")
            } else {
                cell.setName(name: NO_DATA_FOUND)
                cell.setPhoto(with: "")
            }
        default:
            break
        }
    }
    
    func instituationConfigure(cell: InstituationCellView, index: Int) {
        let inst = branches[index]
        cell.configure(image: inst.image, name: inst.name, address: inst.address, rate: inst.rate)
    }

    
    func numberOfRowsInSection(_ tableView: UITableView,_ section: Int) -> Int {
        if tableView.tag == 0 {
            switch section {
            case 0,1 :
                return 1
            case 2:
                return categories.count
            default:
                return 0
            }
        }
        return branches.count
    }

    func headerInSection(_ section: Int) -> String {
        switch section {
        case 0:
            return LOCATION
        case 1:
            return HOTEL_INFO
        case 2:
            return SERVICES
        default:
            return ""
        }
    }
    
    func didSelectRowAt(_ tableView: UITableView, section: Int, index: Int) {
        if tableView.tag == 0 && section == 0 && index == 0 {
            let lat = Double(details?.lat ?? "0")!
            let lng = Double(details?.lng ?? "0")!
            view?.showLocation(lat: lat, lng: lng)
        } else if tableView.tag == 1 {
            let branch = branches[index]
            let lat = Double(branch.lat ?? "0")!
            let lng = Double(branch.lng ?? "0")!
            view?.showLocation(lat: lat, lng: lng)
        }
    }
    
    
    //MARK:- CollectionView Functions
    var collectionViewCount: Int {
        if selectedIndex == 2 {
            return gallery.count
        }
        return adds.count
    }

    func cellConfigure(cell: DiscoverCellView, index: Int) {
        let dis = selectedIndex == 2 ? gallery[index] : adds[index]
        if dis.type == 1 {
            cell.configure(img: dis.image)
            cell.isVideo(false)
        } else {
            cell.configure(img: dis.capture)
            cell.isVideo(true)
        }
    }

    func didSelectItemAt(index: Int) {
        let dis = selectedIndex == 2 ? gallery[index] : adds[index]
        if dis.type == 2 {
            view?.playVideo(dis.image)
        }
    }

    
    
    //MARK:- Private Functions
    private func followShop(shopID: Int) {
        DispatchQueue.global(qos: .background).async {
            self.shopInteractor.followShop(shopID: shopID, type: DEF.type)
        }
    }

    
}
