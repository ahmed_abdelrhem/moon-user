//
//  InstituationPageVC+UITableView+UICollectionView.swift
//  MoonUser
//
//  Created by Grand iMac on 2/17/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation


extension InstituationPageVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == detailTableView {
            return 3
        }
        return 1
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRowsInSection(tableView, section)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerFrame = tableView.frame
        
        let label = UILabel()
        label.frame =  CGRect(x: 20, y: 5, width: headerFrame.size.width-40, height: 40)
        label.font = UIFont.myBoldSystemFont(ofSize: 18)
        label.text = presenter.headerInSection(section)
        label.textColor = .black
        
        let headerView:UIView = UIView(frame: CGRect(x: 0, y: 0,  width: headerFrame.size.width, height: headerFrame.size.height))
        headerView.backgroundColor = .white
        headerView.addSubview(label)

        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == detailTableView {
            return 40
        }
        return 0
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        // UIView with darkGray background for section-separators as Section Footer
        let v = UIView(frame: CGRect(x: 0, y:0, width: tableView.frame.width, height: 1))
        v.backgroundColor = .lightGray
        return v
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == detailTableView {
            guard let serviceCell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as? MenuCell else {
                return UITableViewCell()
            }
            presenter.serviceCell(cell: serviceCell, section: indexPath.section, row: indexPath.row)
            return serviceCell
        } else {
            guard let instituationCell = tableView.dequeueReusableCell(withIdentifier: "InstituationCell") as? InstituationCell else {
                return UITableViewCell()
            }
            presenter.instituationConfigure(cell: instituationCell, index: indexPath.row)
            return instituationCell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == detailTableView {
            return UITableView.automaticDimension
        }
        return 180
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRowAt(tableView, section: indexPath.section, index: indexPath.row)
    }
    
}


extension InstituationPageVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.collectionViewCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DiscoverCell", for: indexPath) as? DiscoverCell else {
            return UICollectionViewCell()
        }
        presenter.cellConfigure(cell: cell, index: indexPath.row)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = collectionView.frame.width
        return CGSize(width: w/3, height: 120)
    }
    

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.didSelectItemAt(index: indexPath.row)
    }

    
}
