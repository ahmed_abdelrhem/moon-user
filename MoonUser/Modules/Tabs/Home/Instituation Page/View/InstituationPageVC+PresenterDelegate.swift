//
//  InstituationPageVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 2/17/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import SDWebImage
import AVKit


extension InstituationPageVC: InstituationPageVCView {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        detailTableView.reloadData()
        branchesTableView.reloadData()
        collectionView.reloadData()
        self.navigationController?.navigationBar.tintColor = .white
        containerView.isHidden = false
     }
    
    func updateUI(_ details: ShopDetails?) {
        DispatchQueue.main.async {
            let imgURL = URL(string: details?.image ?? "")
            self.instituationImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.instituationImg.sd_setImage(with: imgURL, placeholderImage: .placeHolder_image)
            self.instituationNameLbl.text = details?.name
            self.reviewsCountLbl.text = "(\(details?.comment_count ?? 0))"
            self.followerCountLbl.text = "(\(details?.followers_count ?? 0))"
            self.rateView.rating = Double(details?.rate ?? "0")!
            self.followBtn.isSelected = details?.isFollow ?? false
        }
    }

    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func onEmpty(_ img: UIImage, _ msg: String) {
        detailTableView.reloadData()
        detailTableView.setEmptyView(title: msg, messageImage: img)
    }
    
    func changeFollowBtnSelected() {
        followBtn.isSelected = !followBtn.isSelected
    }
    
    func changeFollowCount(count: Int) {
        DispatchQueue.main.async {
            self.followerCountLbl.text = "(\(count))"
        }
    }

    
    func presentReviewsVC(_ shopID: Int) {
        let vc = ReviewsVC()
        vc.shopID = shopID
        navigationController?.pushViewController(vc, animated: true)
    }

    func playVideo(_ url: String?) {
        let videoURL = URL(string: url ?? "")!
        let player = AVPlayer(url: videoURL)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player

        present(playerViewController, animated: true, completion: {
            playerViewController.player!.play()
        })
    }

    func showDetails() {
        detailTableView.isHidden = false
        branchesTableView.isHidden = true
        collectionView.isHidden = true
    }
    
    func showBranches() {
        detailTableView.isHidden = true
        branchesTableView.isHidden = false
        collectionView.isHidden = true
        if branchesTableView.numberOfRows(inSection: 0) == 0 {
            branchesTableView.setEmptyView(title: NO_DATA_FOUND, messageImage: .no_data_image)
        }
    }
    
    func showGallary() {
        collectionView.reloadData()
        detailTableView.isHidden = true
        branchesTableView.isHidden = true
        collectionView.isHidden = false
        if collectionView.numberOfItems(inSection: 0) == 0 {
            collectionView.setEmptyView(title: NO_DATA_FOUND, messageImage: .no_data_image)
        }
    }
    
    func showAdds() {
        collectionView.reloadData()
        detailTableView.isHidden = true
        branchesTableView.isHidden = true
        collectionView.isHidden = false
        if collectionView.numberOfItems(inSection: 0) == 0 {
            collectionView.setEmptyView(title: NO_DATA_FOUND, messageImage: .no_data_image)
        }
    }
    
    func showLocation(lat: Double, lng: Double) {
        let vc = ShowLocationVC()
        vc.lat = lat
        vc.lng = lng
        present(vc, animated: true, completion: nil)
    }
    
    func navigateToChatPageVC(reciverType: Int, id: Int) {
        let vc = ChatPageVC()
        vc.reciverType = reciverType
        vc.id = id
        navigationController?.pushViewController(vc, animated: true)
    }



}
