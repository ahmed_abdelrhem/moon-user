//
//  InstituationPageVC.swift
//  MoonUser
//
//  Created by Grand iMac on 2/17/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class InstituationPageVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var segmentBackgroundView: UIView!
    @IBOutlet weak var scrollTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var instituationImg: UIImageView!
    @IBOutlet weak var followBtn: UIButton!
    @IBOutlet weak var instituationNameLbl: UILabel!
    @IBOutlet weak var reviewsCountLbl: UILabel!
    @IBOutlet weak var followerCountLbl: UILabel!
    @IBOutlet weak var rateView: CosmosView!

    @IBOutlet weak var collectionView: UICollectionView!{
        didSet {
            collectionView.tag = 1
            collectionView.delegate = self
            collectionView.dataSource = self
            collectionView.register(UINib(nibName: "DiscoverCell", bundle: nil), forCellWithReuseIdentifier: "DiscoverCell")
        }
    }
    @IBOutlet weak var detailTableView: UITableView! {
        didSet {
            detailTableView.tag = 0
            detailTableView.register(UINib(nibName: "MenuCell", bundle: nil), forCellReuseIdentifier: "MenuCell")
            detailTableView.tableFooterView = UIView(frame: .zero)
            detailTableView.dataSource = self
            detailTableView.delegate = self
            detailTableView.separatorStyle = .none
            detailTableView.rowHeight = UITableView.automaticDimension
            detailTableView.estimatedRowHeight = 40
        }
    }
    @IBOutlet weak var branchesTableView: UITableView! {
          didSet {
              branchesTableView.tableFooterView = UIView(frame: .zero)
              branchesTableView.tag = 1
              branchesTableView.delegate = self
              branchesTableView.dataSource = self
              branchesTableView.separatorStyle = .none
              branchesTableView.register(UINib(nibName: "InstituationCell", bundle: nil), forCellReuseIdentifier: "InstituationCell")
          }
      }
    
    //MARK:- Variables
    internal var presenter: InstituationPageVCPresenter!
    var numberOfSegments : CGFloat = 4
    var id: Int!
    
        
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setPresenter()
        setUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.tintColor = .mainColor
    }

    
    //MARK:- Functions
    private func setPresenter() {
        presenter = InstituationPageVCPresenter(view: self)
        presenter.viewDidLoad(id: id)
    }
    
    private func setUI() {
        setNavigationBarIsTransparent()
        containerView.isHidden = true
        backButtonTitle("")
        SharedHandller.instance.setupSegment(view: self.segmentBackgroundView, segmentControl: segmentControl, numberOfSegments: numberOfSegments)
        scrollTopConstraint.constant = -90
        instituationImg.setAlpha(0.5)
        rateView.updateOnTouch = false
        instituationImg.fullScreenWhenTapped()
        rateView.didFinishTouchingCosmos = { _ in
            self.presenter.reviewViewClicked()
        }
    }


    
    //MARK:- Actions
    @IBAction func chatBtnClicked(_ sender: UIButton) {
        presenter.chatBtnClicked()
    }
    
    @IBAction func followBtnClicked(_ sender: UIButton) {
        presenter.followBtnClicked()
    }

    @IBAction func tabsSegmentValueChanged(_ sender: UISegmentedControl) {
        presenter.tabsSegmentValueChanged(sender.selectedSegmentIndex)
    }
    
}
