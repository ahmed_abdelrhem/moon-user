//
//  InstituationPageVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 2/17/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
 

class InstituationPageVCAPI {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias model = (InstitutionDetailsModel)->()
    
    func getInstituationData(id: Int, didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param = [
            "id": id,
            "type": 8,
            "flag": 1
        ]
        
        let url =  URLs.shopDetails.url.concatURL(param)
        
        apiManager.contectToApiWith(url: url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(InstitutionDetailsModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
}
