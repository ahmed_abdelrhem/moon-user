//
//  ReviewsVC.swift
//  MoonUser
//
//  Created by Grand iMac on 2/3/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class ReviewsVC: UITableViewController {
        
    //MARK:- Variables
    internal var presenter: ReviewsVCPresenter!
    var shopID: Int!
    
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setTableView()
        setPresenter()
        setRightBarButton()
        setUI()
    }
    
    
    //MARK:- Functions
    private func setUI() {
        backButtonTitle("")
        title = REVIEWS
        self.navigationController?.navigationBar.isTranslucent = false
        listenNotificationCenter()
    }
    
    private func setTableView() {
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        tableView.register(UINib(nibName: "ReviewCell", bundle: nil), forCellReuseIdentifier: "ReviewCell")
    }
    
    private func setPresenter() {
        presenter = ReviewsVCPresenter(view: self)
        presenter.viewDidLoad(shopID: shopID)
    }
    
    private func setRightBarButton() {
        let searchBtn = UIButton(type: UIButton.ButtonType.system)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        searchBtn.setImage(UIImage(named: "add2")!, for: .normal)
        searchBtn.addTarget(self, action: #selector(addReviewClicked(_:)), for: .touchUpInside)
        let searchBarBtItem = UIBarButtonItem(customView: searchBtn)
        self.navigationItem.rightBarButtonItem = searchBarBtItem
    }
    
    
    @objc func addReviewClicked(_ sender: UIButton) {
        presenter.addNewReviewBtnClicked()
    }

    
    private func listenNotificationCenter() {
        _NC.addObserver(forName: .addReview, object: nil, queue: nil) { _ in
            self.presenter.viewDidLoad(shopID: self.shopID)
        }
        
        _NC.addObserver(forName: .chatNewMessageTapped, object: nil, queue: nil) { _ in
            let vc = ChatPageVC()
            vc.id = newMessage.id
            vc.reciverType = Int(newMessage.sender_type)!
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    
    

    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as? ReviewCell else {
            return UITableViewCell()
        }
        presenter.cellConfigure(cell: cell, index: indexPath.row)
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    
    
}
