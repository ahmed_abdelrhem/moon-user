//
//  ReviewsVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 2/4/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension ReviewsVC: ReviewsVCView {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        tableView.restore()
        tableView.reloadData()
     }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func onEmpty(_ img: UIImage, _ msg: String) {
        tableView.setEmptyView(title: msg, messageImage: img)
    }
    
    func navigateToAddReview() {
        let vc = AddReviewVC()
        vc.shopID = self.shopID
        navigationController?.pushViewController(vc, animated: true)
    }

}
