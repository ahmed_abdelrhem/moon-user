//
//  ReviewsVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 2/4/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol ReviewsVCView: LoaderDelegate {
    func navigateToAddReview()
}

protocol ReviewCellView {
    func configure(name: String?, date: String?, comment: String?, rate: String?)
}

class ReviewsVCPresenter {
    
    private weak var view: ReviewsVCView?
    private let interactor = ReviewsVCAPI()
    private var reviews = [UserReview]()
    
    
    init(view: ReviewsVCView) {
        self.view = view
    }
    
    var count: Int {
        return reviews.count
    }
    
    func viewDidLoad(shopID: Int) {
        
        view?.showProgress()
        
        interactor.getShopReviews(shopID: shopID, didDataReady: { [weak self](response) in
            guard self != nil else { return}
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.reviews = response.data ?? []
                self?.view?.onSuccess("")
                if self?.reviews.count == 0 {
                    self?.view?.onEmpty(.no_data_image, NO_DATA_FOUND)
                }
            } else if response.status == StatusCode.ExpiredJWT.rawValue {
                Messages.instance.loginMessage()
            } else {
                self?.view?.onEmpty(.no_connection_image, CONNECTION_ERROR)
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onEmpty(.no_connection_image, CONNECTION_ERROR)
        }
    }
    
    
    func cellConfigure(cell: ReviewCellView, index: Int) {
        let review = reviews[index]
        cell.configure(name: review.name, date: review.date, comment: review.comment, rate: review.rate)
    }
    
    func addNewReviewBtnClicked() {
        view?.navigateToAddReview()
    }
    
    
}
