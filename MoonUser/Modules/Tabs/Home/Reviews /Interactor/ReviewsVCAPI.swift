//
//  ReviewsVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 2/4/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class ReviewsVCAPI {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias model = (ReviewsShopModel)->()
    typealias statusModel = (StatusModel)->()
    
    
    func getShopReviews(shopID: Int, didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param = [
            "to_type": DEF.type,
            "shop_id": shopID
            ] as [String : Any]
        
        let url = URLs.userReviewShop.url.concatURL(param)
        
        apiManager.contectToApiWith(url: url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(ReviewsShopModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
    
    func addReviewShop(shopID: Int, comment: String, rate: Double, didDataReady: @escaping statusModel, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param = [
            "shop_id": shopID,
            "to_type": DEF.type,
            "comment": comment,
            "rate": rate,
            "account_type": 13
        ] as [String : Any]
                
        apiManager.contectToApiWith(url: URLs.addReviewShop.url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(StatusModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
    
   
}
