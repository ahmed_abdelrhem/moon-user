//
//  BookAppointmentVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 3/1/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import SDWebImage

extension BookAppointmentVC: BookAppointmentVCView {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        datesCollection.reloadData()
        timesCollection.reloadData()
        if timesCollection.numberOfItems(inSection: 0) == 0 {
            timesCollection.restore()
            timesCollection.setEmptyView(title: NO_APPOINTMENTS, messageImage: .no_data_image)
        }
        containerView.isHidden = false
    }
    
    func updateUI(_ details: DoctorDetails?) {
        DispatchQueue.main.async {
            let imgURL = URL(string: details?.image ?? "")
            self.doctorImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.doctorImg.sd_setImage(with: imgURL, placeholderImage: .avatar_image)
            self.nameLbl.text = details?.name
            self.experinceLbl.text = "\(details?.doctor_specialties ?? "") {\(details?.yearsExperience ?? 0) \(YEARS)}"
            self.descLbl.text = details?.description
            self.rateView.rating = Double(details?.rate ?? "0")!
            self.feesLbl.text = "\(details?.price ?? "0") \(DEF.country.currency)"
            self.cashBtn.isHidden = details?.payment_type == 2
            self.onlineBtn.isHidden = details?.payment_type == 1
        }
    }

    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func onEmpty(_ img: UIImage, _ msg: String) {
        timesCollection.restore()
        timesCollection.setEmptyView(title: msg, messageImage: img)
    }
    
    func warringMSG(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .warning, layout: .statusLine, style: .top)
    }
    
    func setCashPayment() {
        cashBtn.isSelected = true
        onlineBtn.isSelected = false
    }
    
    func setOnlinePayment() {
        cashBtn.isSelected = false
        onlineBtn.isSelected = true
    }

    func popViewController(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .success, layout: .cardView, style: .center)
        self.navigationController?.popViewController(animated: true)
    }
    
}
