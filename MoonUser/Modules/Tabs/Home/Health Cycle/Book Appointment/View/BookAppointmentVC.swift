//
//  BookAppointmentVC.swift
//  MoonUser
//
//  Created by Grand iMac on 3/1/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class BookAppointmentVC: UIViewController {

    
    //MARK:- Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var doctorImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var experinceLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var feesLbl: UILabel!
    @IBOutlet weak var cashBtn: UIButton!
    @IBOutlet weak var onlineBtn: UIButton!
    @IBOutlet weak var datesCollection: UICollectionView! {
        didSet {
            datesCollection.tag = 0
            datesCollection.delegate = self
            datesCollection.dataSource = self
            datesCollection.register(UINib(nibName: "DateCell", bundle: nil), forCellWithReuseIdentifier: "DateCell")
        }
    }
    @IBOutlet weak var timesCollection: UICollectionView! {
        didSet {
            timesCollection.tag = 1
            timesCollection.delegate = self
            timesCollection.dataSource = self
            timesCollection.register(UINib(nibName: "TimeCell", bundle: nil), forCellWithReuseIdentifier: "TimeCell")
            self.observer = timesCollection.observe(\.contentSize) { (collectioView, _) in
                self.timesCollectionHeight.constant = self.timesCollection.contentSize.height
                if self.timesCollectionHeight.constant == 0 {
                    self.timesCollectionHeight.constant = 220
                }
            }
        }
    }
    @IBOutlet weak var timesCollectionHeight: NSLayoutConstraint!

    
    
    //MARK:- Variables
    internal var presenter: BookAppointmentVCPresenter!
    var observer: NSKeyValueObservation!
    var id: Int!
    
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setPresenter()
        setUI()
    }
    
    
    //MARK:- Functions
    private func setPresenter() {
        presenter = BookAppointmentVCPresenter(view: self)
        presenter.viewDidLoad(id: self.id)
    }


    private func setUI() {
        title = BOOK_APPIONTMENT
        cashBtn.isSelected = true
        containerView.isHidden = true
    }
    
    
    //MARK:- Actions
    @IBAction func cashBtnClicked(_ sender: UIButton) {
        presenter.cashBtnClicked()
    }

    @IBAction func onlineBtnClicked(_ sender: UIButton) {
        presenter.onlineBtnClicked()
    }
    
    @IBAction func confirmBtnClicked(_ sender: UIButton) {
        presenter.confirmBtnClicked(id: id)
    }

  

}
