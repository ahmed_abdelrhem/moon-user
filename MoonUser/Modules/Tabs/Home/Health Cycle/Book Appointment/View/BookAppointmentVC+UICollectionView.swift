//
//  BookAppointmentVC+UICollectionView.swift
//  MoonUser
//
//  Created by Grand iMac on 3/1/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension BookAppointmentVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let tag = collectionView.tag
        return presenter.collectionCount(tag)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 0 {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DateCell", for: indexPath) as? DateCell else {
                return UICollectionViewCell()
            }
            presenter.dayCellConfigure(cell: cell, at: indexPath.row)
            cell.isSelected = indexPath.row == presenter.selectedIndex
            return cell
        } else {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TimeCell", for: indexPath) as? TimeCell else {
                return UICollectionViewCell()
            }
            presenter.timeCellConfigure(cell: cell, at: indexPath.row)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 0 {
            return CGSize(width: 45, height: 80)
        }
        let w = collectionView.width / 3
        return CGSize(width: w, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.didSelectItem(id: self.id, collectionView.tag, at: indexPath.row)
    }
    
    
    
    
}
