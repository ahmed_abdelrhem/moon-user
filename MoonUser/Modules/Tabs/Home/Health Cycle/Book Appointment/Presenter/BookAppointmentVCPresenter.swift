//
//  BookAppointmentVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 3/1/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import MOLH

protocol BookAppointmentVCView: LoaderDelegate {
    func updateUI(_ details: DoctorDetails?)
    func setCashPayment()
    func setOnlinePayment()
    func popViewController(_ msg: String)
}

struct AppointmentTime {
    var time : String = "00:'00"
    var period : String = "0"
    
    init(time : String, period : String) {
        self.time = time
        self.period = period
    }
}



class BookAppointmentVCPresenter {
    
    private weak var view: BookAppointmentVCView?
    private let interactor = BookAppointmentVCAPI()
    private var details: DoctorDetails?
    private var days : [String] =  [] // 30days
    private var times : [AppointmentTime] =  [] // 30days
    private var reservedTimes = [String]()
    private (set) var selectedIndex = 0
    private var selectedDate = ""
    private var selectedTime: AppointmentTime?
    private var paymentMethod = 1
    
    
    init(view: BookAppointmentVCView) {
        self.view = view
        self.selectedDate = self.addday(0)
    }
    
    func collectionCount(_ tag: Int) -> Int {
        if tag == 0 {
            return days.count
        }
        return times.count
    }
        
    func viewDidLoad(id: Int) {
        
        createDaysArray()
        
        view?.showProgress()
        
        let dateFormatter = convertDateFormater(selectedDate)
        
        interactor.getDoctorDetails(id: id, date: dateFormatter, didDataReady: { [weak self] response in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.details = response.data
                self?.reservedTimes = response.data?.reserved_time ?? []
                let scedules = response.data?.schedules ?? []
                for s in scedules {
                    let from = s.from ?? "00:00"
                    let to = s.to ?? "00:00"
                    let period = s.period ?? "00"
                    self?.createTimesArray(start: from, end: to, period: period)
                }
                self?.view?.updateUI(self?.details)
                self?.view?.onSuccess("")
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { [weak self] error in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
    
    func dayCellConfigure(cell: DateCellDelegate, at index: Int) {
        let day = getDayAsArray(index)
        cell.configure(day[1], day[0], day[2])
    }
    
    func timeCellConfigure(cell: TimeCellView, at index: Int) {
        let time = times[index].time
        cell.cellConfigue(time)
    }
    
    func didSelectItem(id: Int, _ tag: Int, at index: Int) {
        if tag == 0 {
            self.times = []
            self.reservedTimes = []
            self.selectedTime = nil
            self.selectedIndex = index
            self.selectedDate = days[index]
            self.viewDidLoad(id: id)
        } else {
            self.selectedTime = times[index]
        }
    }
    
    func cashBtnClicked() {
        paymentMethod = 1
        view?.setCashPayment()
    }

    func onlineBtnClicked() {
        paymentMethod = 2
        view?.setOnlinePayment()
    }

    func confirmBtnClicked(id: Int) {
        guard selectedTime != nil else {
            view?.warringMSG(SELECT_TIME)
            return
        }
        
        view?.showProgress()
        
        interactor.confirmAppointmnet(id: id, date: convertDateFormater(selectedDate), time: selectedTime!.time, dayID: details?.day_id ?? 0, payment: paymentMethod, period: selectedTime!.period, didDataReady: {  [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.view?.popViewController(response.message ?? "")
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { [weak self] error in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
    
    
    //Mark:- Private functions
    private func createDaysArray() { // create 30 days
        for i in 0..<100{
            days.append(addday(i))
        }
    }
        
    private func convertDateFormater(_ date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E d MMM yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return  dateFormatter.string(from: date!)
    }

    private func addday(_ value: Int) -> String{
        let today = Date()
        let next_day = Calendar.current.date(byAdding: .day, value: value, to: today)
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: MOLHLanguage.currentLocaleIdentifier())
        formatter.dateFormat = "E d MMM yyyy"
        let result = formatter.string(from: next_day!)
        return result
    }
    
    private func getDayAsArray(_ index: Int) -> [String] {
        let arr7 = days[index].components(separatedBy: " ")
        return arr7
    }
    
    private func createTimesArray(start s: String, end e: String, period p: String) {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        
        let df = changesDateFormatter(formatter.date(from: s)!)
        
        if !reservedTimes.contains(df) {
            let time = AppointmentTime(time: df, period: p)
            self.times.append(time)
        }
        
        let sd = formatter.date(from: s)!
        let ed = formatter.date(from: e)!
        let period = Double(p)!
        let date = sd.addingTimeInterval(period * 60.0)
        let newDate = changesDateFormatter(date)
        
        if date < ed {
            createTimesArray(start: newDate, end: e, period: p)
        }
    }
    
    private func changesDateFormatter(_ date: Date) -> String {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: MOLHLanguage.currentLocaleIdentifier())
        formatter.dateFormat = "HH:mm"
        let outStr = formatter.string(from: date)
        return outStr //04:50
    }

    
}
