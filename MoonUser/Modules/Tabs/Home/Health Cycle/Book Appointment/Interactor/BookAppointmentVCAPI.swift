//
//  BookAppointmentVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 3/1/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class BookAppointmentVCAPI {

    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias model = (DoctorResponseModel)->()
    typealias statusModel = (StatusModel)->()


    func getDoctorDetails(id: Int, date: String, didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param = ["id": id,
                     "date": date ] as [String : Any]
                
        let url = URLs.doctorSchedules.url.concatURL(param)
        
        apiManager.contectToApiWith(url: url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(DoctorResponseModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
    func confirmAppointmnet(id: Int, date: String, time: String, dayID: Int, payment: Int, period: String, didDataReady: @escaping statusModel, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param = [
            "type": DEF.type,
            "doctor_id": id,
            "date": date,
            "service": time,
            "day_id": dayID,
            "payment_method": payment,
            "period": period
        ] as [String : Any]
                                
        apiManager.contectToApiWith(url: URLs.reservationOrder.url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(StatusModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }


    
}
