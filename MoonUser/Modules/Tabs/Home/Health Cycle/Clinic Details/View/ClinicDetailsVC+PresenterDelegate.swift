//
//  ClinicDetailsVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 2/27/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import AVKit
import SDWebImage

extension ClinicDetailsVC: ClinicDetailsVCView {
    
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        tableView.reloadData()
        collectionView.reloadData()
        containerView.isHidden = false
     }
    
    func updateUI(_ details: ClinicDetails?) {
        DispatchQueue.main.async {
            let imgURL = URL(string: details?.image ?? "")
            self.clinicImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.clinicImg.sd_setImage(with: imgURL, placeholderImage: .placeHolder_image)
            self.nameLbl.text = details?.name
            self.descLbl.text = details?.description
            self.reviewCountLbl.text = "(\(details?.comment_count ?? 0))"
            self.addressLbl.text = details?.address
            self.phonesLbl.text = details?.phones
            self.rateView.rating = Double(details?.rate ?? "0")!
        }
    }

    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func onEmpty(_ img: UIImage, _ msg: String) {
        tableView.reloadData()
        tableView.setEmptyView(title: msg, messageImage: img)
    }
    
    
    
    func presentReviewsVC(_ clinicID: Int) {
        let vc = ReviewsVC()
        vc.shopID = clinicID
        navigationController?.pushViewController(vc, animated: true)
    }

    func playVideo(_ url: String?) {
        let videoURL = URL(string: url ?? "")!
        let player = AVPlayer(url: videoURL)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player

        present(playerViewController, animated: true, completion: {
            playerViewController.player!.play()
        })
    }

    func showDoctors() {
        doctorsView.backgroundColor = UIColor.darkBlueColor
        gallaryView.backgroundColor = .mainColor
        addsView.backgroundColor = .mainColor
        
        tableView.restore()
        tableView.isHidden = false
        collectionView.isHidden = true
        if tableView.numberOfRows(inSection: 0) == 0 {
            tableView.setEmptyView(title: NO_DATA_FOUND, messageImage: .no_data_image)
        }
        UIView.animate(withDuration: 0.2) {
            self.filterView.isHidden = false
            self.filterViewHieght.constant = 35
            self.filterView.layoutIfNeeded()
        }
    }
    
    func showGallary() {
        doctorsView.backgroundColor = .mainColor
        gallaryView.backgroundColor = UIColor.darkBlueColor
        addsView.backgroundColor = .mainColor

        collectionView.restore()
        collectionView.reloadData()
        tableView.isHidden = true
        collectionView.isHidden = false
        if collectionView.numberOfItems(inSection: 0) == 0 {
            collectionView.setEmptyView(title: NO_DATA_FOUND, messageImage: .no_data_image)
        }
        UIView.animate(withDuration: 0.2) {
            self.filterView.isHidden = true
            self.filterViewHieght.constant = 0
            self.filterView.layoutIfNeeded()
        }
    }
    
    func showAdds() {
        doctorsView.backgroundColor = .mainColor
        gallaryView.backgroundColor = .mainColor
        addsView.backgroundColor = UIColor.darkBlueColor

        collectionView.restore()
        collectionView.reloadData()
        tableView.isHidden = true
        collectionView.isHidden = false
        if collectionView.numberOfItems(inSection: 0) == 0 {
            collectionView.setEmptyView(title: NO_DATA_FOUND, messageImage: .no_data_image)
        }
        UIView.animate(withDuration: 0.2) {
            self.filterView.isHidden = true
            self.filterViewHieght.constant = 0
            self.filterView.layoutIfNeeded()
        }
    }
    
    func showLocation(lat: Double, lng: Double) {
        let vc = ShowLocationVC()
        vc.lat = lat
        vc.lng = lng
        present(vc, animated: true, completion: nil)
    }

    func showCallPhones(_ array: [[String]]) {
        
        var number: String?
        
        let alert = UIAlertController(style: .actionSheet, title: "", message: "")

        let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: 3)

        alert.addPickerView(values: array, initialSelection: pickerViewSelectedValue) { vc, picker, index, values in
            DispatchQueue.main.async { [weak self] in
                guard self != nil else { return }
                number = array[0][index.row]
            }
        }
        
        alert.addAction(image: nil, title: CALL, color: .mainColor, style: .default, isEnabled: true) { [weak self] (action) in
            guard self != nil else { return }
            let phone = number ?? array[0][0]
            if phone != "" {
                let url:NSURL = NSURL(string: "tel://\(phone)")!
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            } else {
                Messages.instance.showConfigMessage(title: NO_PHONE, body: "", state: .warning, layout: .statusLine, style: .top)
            }

        }
        
        alert.addAction(image: nil, title: CANCEL, color: .red, style: .cancel, isEnabled: true) { _ in
            alert.dismiss(animated: true, completion: nil)
        }
        
        present(alert, animated: true, completion: nil)
    }
    
    func presentSpecialistVC(_ array: [Specialist]) {
        let vc = SpecialistsVC()
        vc.delegate = self
        vc.specialists = array
        present(vc, animated: true, completion: nil)
    }
    
    func navigateToBookAppointment(_ id: Int) {
        let vc = BookAppointmentVC()
        vc.id = id
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToChatPageVC(reciverType: Int, id: Int) {
        let vc = ChatPageVC()
        vc.reciverType = reciverType
        vc.id = id
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }



}
