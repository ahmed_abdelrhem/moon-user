//
//  ClinicDetailsVC.swift
//  MoonUser
//
//  Created by Grand iMac on 2/27/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class ClinicDetailsVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var clinicImg: UIImageView!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var reviewCountLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var phonesLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var locationStack: UIStackView!
    @IBOutlet weak var phonesStack: UIStackView!
    @IBOutlet weak var doctorsView: UIView!
    @IBOutlet weak var gallaryView: UIView!
    @IBOutlet weak var addsView: UIView!
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var filterViewHieght: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            tableView.separatorStyle = .none
            tableView.register(UINib(nibName: "DoctorCell", bundle: nil), forCellReuseIdentifier: "DoctorCell")
        }
    }
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet {
            collectionView.tag = 1
            collectionView.delegate = self
            collectionView.dataSource = self
            collectionView.register(UINib(nibName: "DiscoverCell", bundle: nil), forCellWithReuseIdentifier: "DiscoverCell")
        }
    }

    
    //MARK:- Variables
    internal var presenter: ClinicDetailsVCPresenter!
    var id: Int!
    
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
        addGestures()
        setPresenter()
    }


    //MARK:- Functions
    private func updateUI() {
        backButtonTitle("")
        title = CLINIC_PROFILE
        clinicImg.fullScreenWhenTapped()
        containerView.isHidden = true
        rateView.didFinishTouchingCosmos = { _ in
            self.presenter.reviewViewClicked()
        }
    }
    
    private func addGestures() {
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.doctorsViewClicked))
        doctorsView.addGestureRecognizer(tap1)
        
        let tap2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.gallaryViewClicked))
        gallaryView.addGestureRecognizer(tap2)

        let tap3: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.addsViewClicked))
        addsView.addGestureRecognizer(tap3)
        
        let tap4: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.locationStackClicked))
        locationStack.addGestureRecognizer(tap4)

        
        let tap5: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.phonesStackClicked))
        phonesStack.addGestureRecognizer(tap5)

    }
    
    
    private func setPresenter() {
        presenter = ClinicDetailsVCPresenter(view: self)
        presenter.viewDidLoad(id: self.id, specialistID: 0)
    }

    
    
    //MARK:- Actions
    @objc func doctorsViewClicked() {
        presenter.doctorsViewClicked()
    }

    @objc func gallaryViewClicked() {
        presenter.gallaryViewClicked()
    }

    @objc func addsViewClicked() {
        presenter.addsViewClicked()
    }
    
    @objc func locationStackClicked() {
        presenter.locationStackClicked()
    }
    
    @objc func phonesStackClicked() {
        presenter.phonesStackClicked()
    }

    @IBAction func chatBtnClicked(_ sender: UIButton) {
        presenter.chatBtnActions()
    }
    
    @IBAction func filterBtnClicked(_ sender: UIButton) {
        presenter.filterBtnClicked()
    }
}


extension ClinicDetailsVC: FilterVCDelegate {
    
    func retriveID(_ id: Int) {
        presenter.viewDidLoad(id: self.id, specialistID: id)
    }
    
    
}
