/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation

struct ClinicData : Codable {
	let details : ClinicDetails?
	let specialist : [Specialist]?
	let gallery : [DiscoverData]?
	let adds : [DiscoverData]?
	let doctors_list : [DoctorData]?
    let services : [CategoryService]?

	enum CodingKeys: String, CodingKey {

		case details = "details"
		case specialist = "specialist"
		case gallery = "gallery"
		case adds = "Adds"
		case doctors_list = "doctors_list"
        case services = "services"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		details = try values.decodeIfPresent(ClinicDetails.self, forKey: .details)
		specialist = try values.decodeIfPresent([Specialist].self, forKey: .specialist)
		gallery = try values.decodeIfPresent([DiscoverData].self, forKey: .gallery)
		adds = try values.decodeIfPresent([DiscoverData].self, forKey: .adds)
		doctors_list = try values.decodeIfPresent([DoctorData].self, forKey: .doctors_list)
        services = try values.decodeIfPresent([CategoryService].self, forKey: .services)
	}

}

