/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation

struct ClinicDetails : Codable {
	let id : Int?
	let image : String?
	let name : String?
	let description : String?
	let lat : String?
	let lng : String?
	let address : String?
	let phones : String?
	let comment_count : Int?
	let rate : String?
    let payment_type: Int?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case image = "image"
		case name = "name"
		case description = "description"
		case lat = "lat"
		case lng = "lng"
		case address = "address"
		case phones = "phones"
		case comment_count = "comment_count"
		case rate = "rate"
        case payment_type = "payment_type"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		image = try values.decodeIfPresent(String.self, forKey: .image)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		description = try values.decodeIfPresent(String.self, forKey: .description)
		lat = try values.decodeIfPresent(String.self, forKey: .lat)
		lng = try values.decodeIfPresent(String.self, forKey: .lng)
		address = try values.decodeIfPresent(String.self, forKey: .address)
		phones = try values.decodeIfPresent(String.self, forKey: .phones)
		comment_count = try values.decodeIfPresent(Int.self, forKey: .comment_count)
		rate = try values.decodeIfPresent(String.self, forKey: .rate)
        payment_type = try values.decodeIfPresent(Int.self, forKey: .payment_type)
    }

}
