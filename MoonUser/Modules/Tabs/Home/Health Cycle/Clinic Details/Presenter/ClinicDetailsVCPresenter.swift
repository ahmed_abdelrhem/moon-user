//
//  ClinicDetailsVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 2/27/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol ClinicDetailsVCView: LoaderDelegate {
    func updateUI(_ details: ClinicDetails?)
    func presentReviewsVC(_ clinicID: Int)
    func playVideo(_ url: String?)
    func showLocation(lat: Double, lng: Double)
    func showCallPhones(_ array: [[String]])
    func presentSpecialistVC(_ array: [Specialist])
    func navigateToBookAppointment(_ id: Int)
    func showDoctors()
    func showGallary()
    func showAdds()
    func navigateToChatPageVC(reciverType: Int, id: Int)
}


class ClinicDetailsVCPresenter {
    
    private weak var view: ClinicDetailsVCView?
    private let interactor = ClinicDetailsAPI()
    private var details: ClinicDetails?
    private var doctors = [DoctorData]()
    private var gallery = [DiscoverData]()
    private var adds = [DiscoverData]()
    private var specialists = [Specialist]()
    private var selectedIndex: Int = 0      //NOTE: Doctors = 1, Gallary = 2, Adds = 3

    
    
    init(view: ClinicDetailsVCView) {
        self.view = view
    }
            
    
    func viewDidLoad(id: Int, specialistID: Int) {
        
        view?.showProgress()
        
        interactor.getClinicDetails(id: id, specialistID: specialistID, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.details = response.data?.details
                self?.doctors = response.data?.doctors_list ?? []
                self?.gallery = response.data?.gallery ?? []
                self?.adds = response.data?.adds ?? []
                self?.specialists = response.data?.specialist ?? []
                self?.view?.updateUI(self?.details)
                self?.view?.onSuccess("")
                self?.view?.showDoctors()
            } else {
                self?.view?.onFailure(CONNECTION_ERROR)
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
    
    func reviewViewClicked() {
        view?.presentReviewsVC(self.details?.id ?? 0)
    }
    
    
    //MARK:- Actions
    func doctorsViewClicked() {
        selectedIndex = 1
        view?.showDoctors()
    }

    func gallaryViewClicked() {
        selectedIndex = 2
        view?.showGallary()
    }

    func addsViewClicked() {
        selectedIndex = 3
        view?.showAdds()
    }
    
    func locationStackClicked() {
        let lat = Double(details?.lat ?? "0")!
        let lng = Double(details?.lng ?? "0")!
        view?.showLocation(lat: lat, lng: lng)
    }
    
    func phonesStackClicked() {
        if let array = details?.phones?.components(separatedBy: " , ") {
            view?.showCallPhones([array])
        }
    }
    
    func chatBtnActions() {
        if DEF.isLogin {
            let id = details?.id ?? 0
            let type = DEF.type
            view?.navigateToChatPageVC(reciverType: type, id: id)
        } else {
            showLoginMessage()
        }
    }

    
    func filterBtnClicked() {
        view?.presentSpecialistVC(self.specialists)
    }
    
    func bookBtnActions(at index: Int) {
        if DEF.isLogin {
            let id = doctors[index].id ?? 0
            view?.navigateToBookAppointment(id)
        } else {
            showLoginMessage()
        }
    }

    
    //MARK:- TableView Functions
    var doctorsCount: Int {
        return doctors.count
    }

    func cliniConfigure(cell: ClinicCellView, row: Int)  {
        let doctor = doctors[row]
        let specialist = "\(doctor.doctor_specialties ?? "") {\(doctor.yearsExperience ?? 0) \(YEARS)} "
        cell.configure(image: doctor.image, name: doctor.name, address: specialist, desc: doctor.description, rate: doctor.rate, isFollow: false)
    }
    

    //MARK:- CollectionView Functions
    var collectionViewCount: Int {
        if selectedIndex == 2 {
            return gallery.count
        }
        return adds.count
    }

    func cellConfigure(cell: DiscoverCellView, index: Int) {
        let dis = selectedIndex == 2 ? gallery[index] : adds[index]
        if dis.type == 1 {
            cell.configure(img: dis.image)
            cell.isVideo(false)
        } else {
            cell.configure(img: dis.capture)
            cell.isVideo(true)
        }
    }

    func didSelectItemAt(index: Int) {
        let dis = selectedIndex == 2 ? gallery[index] : adds[index]
        if dis.type == 2 {
            view?.playVideo(dis.image)
        }
    }

    
}
