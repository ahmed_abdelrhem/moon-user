//
//  ClinicDetailsAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 2/27/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class ClinicDetailsAPI {

    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias model = (ClinicResponseModel)->()
    typealias statusModel = (StatusModel)->()


    func getClinicDetails(id: Int, specialistID: Int, didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        var param = [
            "type": DEF.type,
            "id": id
            ] as [String : Any]
        
        if specialistID != 0 {
            param["filter"] = 1
            param["specialist_id"] = specialistID
        }
        
        let url = URLs.clinicDetails.url.concatURL(param)
        
        apiManager.contectToApiWith(url: url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(ClinicResponseModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
    
    func confirmBeautyOrder(id: Int, date: String, payment: Int, time: Int, services: [Int], didDataReady: @escaping statusModel, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
           
           var param = [
               "type": 10,
               "doctor_id": id,
               "date": date,
               "payment_method": payment,
               "period": time
            ] as [String : Any]
        
            let stringArray = services.map { String($0) }
            param["service"] = stringArray.joined(separator:",")
           
           apiManager.contectToApiWith(url: URLs.reservationOrder.url,
                                       methodType: .post,
                                       params: param,
                                       success: { (json) in
                
               if let data = try? JSONSerialization.data(withJSONObject: json) {
                   do {
                       let result = try self.decoder.decode(StatusModel.self, from: data)
                       didDataReady(result)
                   }catch{
                       print("error\(error)")
                   }
                   
               }
           }) { (error, msg) in
               print(error, msg!)
               errorCompletion(error)
           }
       }
    

}
