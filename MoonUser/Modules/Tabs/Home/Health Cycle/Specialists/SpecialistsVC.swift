//
//  SpecialistsVC.swift
//  MoonUser
//
//  Created by Grand iMac on 2/27/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class SpecialistsVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView(frame: .zero)
            tableView.delegate = self
            tableView.dataSource = self
            tableView.separatorStyle = .none
            tableView.allowsMultipleSelection = false
            tableView.register(UINib(nibName: "SelectionCell", bundle: nil), forCellReuseIdentifier: "SelectionCell")
        }
    }
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!

    
    //MARK:- Variables
    var delegate: FilterVCDelegate?
    var specialists = [Specialist]()
    var selectedID: Int = 0
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedID = specialists[0].id ?? 0
        backBtn.RotatIfNeeded()
    }

    
    //MARK:- Actions
    @IBAction func backBtnClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitBtnClicked(_ sender: UIButton) {
        delegate?.retriveID(selectedID)
        dismiss(animated: true, completion: nil)
    }


}


extension SpecialistsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return specialists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let selectionCell = tableView.dequeueReusableCell(withIdentifier: "SelectionCell") as? SelectionCell else {
            return UITableViewCell()
        }
        selectionCell.configure(title: specialists[indexPath.row].name, img: UIImage())
        selectionCell.hideImage()
        return selectionCell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerFrame = tableView.frame
        
        let label = UILabel()
        label.frame =  CGRect(x: 20, y: 0, width: headerFrame.size.width-40, height: 40)
        label.font = UIFont.myBoldSystemFont(ofSize: 17)
        label.text = CLINIC_SPEC
        label.textColor = .black
        
        let headerView:UIView = UIView(frame: CGRect(x: 0, y: 0,  width: headerFrame.size.width, height: headerFrame.size.height+20))
        headerView.backgroundColor = .white
        headerView.addSubview(label)

        return headerView
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedID = specialists[indexPath.row].id ?? 0
    }
        
    
}
