//
//  InstituationIntroVC.swift
//  MoonUser
//
//  Created by Grand iMac on 2/18/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit



enum IntroType {
    case Institution
    case Advertising
    
    var title: String {
        switch self {
        case .Institution:          return "Institution"
        case .Advertising:          return "Advertising"
        }
    }
    
    var image: UIImage? {
        switch self {
        case .Institution:          return .institution_image
        case .Advertising:          return .advertising_image
        }
    }
    
    
}


class InstituationIntroVC: UIViewController {
    
    
    //MARK:- Outlets
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var introImg: UIImageView!
    
    
    //MARK:- Variables
    var type: IntroType?

    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        backButtonTitle("")
        updateUI()
    }
    

    //MARK:- Functions
    private func updateUI() {
        titleLbl.text = type?.title
        introImg.image = type?.image
    }
    
    
    //MARK:- Actions
    @IBAction func enjoyBtnClicked(_ sender: UIButton) {
        if type == .Institution {
            let vc = SingleInstituationVC()
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = AdvertisingPageVC()
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
 

}
