//
//  SearchShopsVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 1/30/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol SearchShopsVCView: LoaderDelegate {
    func navigateToShopPage(_ shopID: Int)
    func navigateToInstitutionPage(_ institutionID: Int)
    func navigateToAdDetailsVC(_ id: Int, title: String?)
    func navigateToFamousDetailsVC(_ id: Int)
    func navigateToClinicDetailsVC(_ id: Int)
    func navigateToSalonProfileVC(_ id: Int)
}


class SearchShopsVCPresenter {
    
    private weak var view: SearchShopsVCView?
    private let interactor = SearchShopsAPI()
    private var shops = [Shop]()
    private (set) var title = ""

    
    init(view: SearchShopsVCView) {
        self.view = view
    }
    
    var count: Int {
        return shops.count
    }
    
    func cellConfigure(type: SearchType, cell: ShopCellView, index: Int) {
        let s = shops[index]
        cell.configure(image: s.image, name: s.name, tags: s.tags, rate: s.rate, avg: s.avg)
        if type == .Instituation {
            cell.setIsInstitution(address: s.address)
        } else if type == .Commercial {
            cell.setIsCommercial(seller: s.seller?.name, price: s.price)
        } else if type == .Famous {
            cell.setIsFamous()
        } else if type == .Clinic {
            cell.setIsClinic(address: s.address)
        }
    }
        
    func searchBarButtonClicked(type: SearchType, serviceID: Int, lat: String, lng: String, text: String?) {
        if text != "" {
            view?.showProgress()
            
            interactor.getShops(type: type, name: text!, lat: lat, lng: lng, service_id: serviceID, didDataReady: { [weak self](response) in
                guard self != nil else { return }
                self?.view?.hideProgress()
                if response.status == StatusCode.Success.rawValue {
                    self?.shops = response.data ?? []
                    self?.title = response.count ?? ""
                    if self?.shops.count == 0 {
                        self?.view?.onEmpty(.no_data_image, NO_DATA_FOUND)
                    } else {
                        self?.view?.onSuccess("")
                    }
                } else {
                    self?.view?.onFailure(response.message ?? "")
                }
            }) { [weak self](error) in
                guard self != nil else { return }
                self?.view?.hideProgress()
                self?.view?.onEmpty(.no_connection_image, CONNECTION_ERROR)
            }
        }
    }
    
    func didselectRowAt(type: SearchType, index: Int) {
        let shop = shops[index]
        let id = shop.id ?? 0

        if type == .Shop {
            if shop.work_days != 0 {
                view?.navigateToShopPage(id)
            } else {
                view?.warringMSG(SHOP_CLOSED)
            }
        } else if type == .Instituation {
            view?.navigateToInstitutionPage(id)
        } else if type == .Commercial {
            let title = shop.name ?? ""
            view?.navigateToAdDetailsVC(id, title: title)
        } else if type == .Famous {
            view?.navigateToFamousDetailsVC(id)
        } else if type == .Clinic {
            view?.navigateToClinicDetailsVC(id)
        } else if type == .Salon {
            view?.navigateToSalonProfileVC(id)
        }
    }

}
