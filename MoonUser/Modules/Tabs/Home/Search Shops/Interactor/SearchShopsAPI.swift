//
//  SearchShopsAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 1/30/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class SearchShopsAPI {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias model = (SearchShopsModel)->()
    
    func getShops(type: SearchType, name: String, lat: String, lng: String, service_id: Int, didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param = [
            "name": name,
            "type": DEF.type,
            "flag": DEF.flag,
            "country_id": DEF.country.id,
            "lat": "30.126095365428267",
            "lng": "31.375079266726974",
            "service_id": service_id
        ] as [String : Any]
        
        var url = ""
        if type == .Famous {
            url = URLs.famousSearch.url.concatURL(param)
        } else {
            url = URLs.searchShop.url.concatURL(param)
        }
        
        apiManager.contectToApiWith(url: url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(SearchShopsModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
}
