//
//  SearchShopsVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 1/30/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation


extension SearchShopsVC: SearchShopsVCView {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        tableView.restore()
        tableView.reloadData()
    }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func warringMSG(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .warning, layout: .messageView, style: .top)
    }
    
    func onEmpty(_ img: UIImage, _ msg: String) {
        tableView.reloadData()
        tableView.setEmptyView(title: msg, messageImage: img)
    }
    
    func navigateToShopPage(_ shopID: Int) {
        let vc = ShopPageVC()
        vc.shopID = shopID
        navigationController?.pushViewController(vc, animated: true)
    }

    func navigateToInstitutionPage(_ institutionID: Int) {
        let vc = InstituationPageVC()
        vc.id = institutionID
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToAdDetailsVC(_ id: Int, title: String?) {
        let vc = AdDetailsVC()
        vc.id = id
        vc.title = title
        navigationController?.pushViewController(vc, animated: true)
    }

    func navigateToFamousDetailsVC(_ id: Int) {
        let vc = FamousDetailsVC()
        vc.id = id
        vc.type = .Famous
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToClinicDetailsVC(_ id: Int) {
        let vc = ClinicDetailsVC()
        vc.id = id
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToSalonProfileVC(_ id: Int) {
        let vc = SalonProfileVC()
        vc.id = id
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
