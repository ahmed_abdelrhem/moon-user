//
//  SearchShopsVC.swift
//  MoonUser
//
//  Created by Grand iMac on 1/30/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

enum SearchType: Int {
    case Shop
    case Instituation
    case Commercial
    case Famous
    case Clinic
    case Salon
}

class SearchShopsVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.allowsMultipleSelection = false
            tableView.tableFooterView = UIView(frame: .zero)
            tableView.dataSource = self
            tableView.delegate = self
            tableView.separatorStyle = .none
            tableView.register(UINib(nibName: "SearchShop", bundle: nil), forCellReuseIdentifier: "SearchShop")
        }
    }
    @IBOutlet weak var searchBar: UISearchBar!

    
    
    //MARK:- Variables
    internal var presenter: SearchShopsVCPresenter!
    var serviceID: Int!
    var type: SearchType = .Shop
    
    
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        backButtonTitle("")
        setPresenter()
        title = SEARCH
        searchBar.delegate = self
    }
    
    
    //MARK:- Functions
    private func setPresenter() {
        presenter = SearchShopsVCPresenter(view: self)
    }
    


   
}


//MARK:- UISearchBarDelegate
extension SearchShopsVC: UISearchBarDelegate {
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.showsScopeBar = true
        searchBar.sizeToFit()
        searchBar.setShowsCancelButton(true, animated: true)
        return true
    }

    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.showsScopeBar = false
        searchBar.sizeToFit()
        searchBar.setShowsCancelButton(false, animated: true)
        return true
    }


    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        presenter.searchBarButtonClicked(type: self.type, serviceID: self.serviceID, lat: DEF.currentLat, lng: DEF.currentLng, text: searchBar.text)
        searchBar.showsCancelButton = false
        self.view.endEditing(true)
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        self.view.endEditing(true)
    }
}

