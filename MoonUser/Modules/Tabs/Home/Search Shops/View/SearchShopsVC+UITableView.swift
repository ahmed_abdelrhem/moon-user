//
//  SearchShopsVC+UITableView.swift
//  MoonUser
//
//  Created by Grand iMac on 1/30/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation


extension SearchShopsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SearchShop") as? SearchShop else {
            return UITableViewCell()
        }
        presenter.cellConfigure(type: self.type, cell: cell, index: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didselectRowAt(type: self.type, index: indexPath.row)
    }
        
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 48
    }
        
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel()
        label.backgroundColor = .white
        label.font = UIFont.myBoldSystemFont(ofSize: 17)
        label.text = presenter.title
        label.textColor = .black
        return label
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    

}
