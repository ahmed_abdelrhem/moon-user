//
//  ShopPageVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 2/2/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class ShopPageVCAPI {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias model = (ShopDetailsModel)->()
    typealias productsModel = (ProductsListModel)->()
    
    
    func getShopDetails(id: Int, didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param = [
            "type": DEF.type,
            "flag": DEF.flag,
            "id": id
            ] as [String : Any]
        
        let url = URLs.shopDetails.url.concatURL(param)
        
        apiManager.contectToApiWith(url: url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(ShopDetailsModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
    
    func getProductFromCategory(categoryID: Int, shopID: Int, didDataReady: @escaping productsModel, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param = [
            "type": DEF.type,
            "flag": DEF.flag,
            "category_id": categoryID,
            "shop_id": shopID
            ] as [String : Any]
        
        let url = URLs.getProductFromCategory.url.concatURL(param)
        
        apiManager.contectToApiWith(url: url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(ProductsListModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
    func followShop(shopID: Int, type: Int) {
        
        let param = [
            "to": shopID,
            "to_type": type,
            "account_type": 13
        ]
        
        apiManager.contectToApiWith(url: URLs.createFollow.url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    _ = try self.decoder.decode(StatusModel.self, from: data)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
        }
    }
    
}
