//
//  ShopPageVC+UICollectionView.swift
//  MoonUser
//
//  Created by Grand iMac on 2/2/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension ShopPageVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.getCount(collectionView.tag)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 0 {
            guard let sectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SectionCell", for: indexPath) as? SectionCell else { return UICollectionViewCell() }
            sectionCell.isSelected = indexPath.row == presenter.selectedSectionindex
            presenter.sectionCellConfigure(cell: sectionCell, index: indexPath.row)
            return sectionCell
        } else {
            if presenter.selectedSectionindex == 0 || presenter.selectedSectionindex == 1 {
                guard let discoverCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DiscoverCell", for: indexPath) as? DiscoverCell else { return UICollectionViewCell() }
                presenter.discoverCellConfigure(cell: discoverCell, index: indexPath.row)
                return discoverCell
            } else {
                if type == .Shop {
                    guard let productCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as? ProductCell else { return UICollectionViewCell() }
                    presenter.productCellConfigure(cell: productCell, index: indexPath.row)
                    return productCell
                }
                guard let companyProductCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CompanyProductCell", for: indexPath) as? CompanyProductCell else { return UICollectionViewCell() }
                presenter.productCellConfigure(cell: companyProductCell, index: indexPath.row)
                return companyProductCell

            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = collectionView.frame.width
        return presenter.getCellSize(collectionView.tag, type: self.type, width: w, index: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.didSelectItemAt(type: self.type, tag: collectionView.tag, index: indexPath.row)
    }
    
    
    
}
