//
//  ShopPageVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 2/2/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import SDWebImage
import AVKit

extension ShopPageVC: ShopPageVCView {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        productsCollectionView.restore()
        sectionsCollectionView.reloadData()
        productsCollectionView.reloadData()
        if productsCollectionView.numberOfItems(inSection: 0) == 0 {
            productsCollectionView.setEmptyView(title: NO_DATA_FOUND, messageImage: .no_data_image)
        }
        self.navigationController?.navigationBar.tintColor = .white
        containerView.isHidden = false
     }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func onEmpty(_ img: UIImage, _ msg: String) {
        productsCollectionView.reloadData()
        productsCollectionView.setEmptyView(title: msg, messageImage: img)
    }
    
    func updateUI(_ details: ShopDetails?) {
        DispatchQueue.main.async {
            let imgURL = URL(string: details?.image ?? "")
            self.productImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.productImg.sd_setImage(with: imgURL, placeholderImage: .placeHolder_image)
            self.shopNameLbl.text = details?.name
            self.tagsLbl.text = details?.tags
            self.reviewsCountLbl.text = "(\(details?.comment_count ?? 0))"
            self.followerCountLbl.text = "(\(details?.followers_count ?? 0))"
            self.rateView.rating = Double(details?.rate ?? "0")!
            self.avgLbl.text =  "\(AVG) \(details?.avg ?? "0") \(MIN)"
            self.minLbl.text = "\(MIN): \(details?.mini_charge ?? "0")"
            self.deliveryLbl.text = "\(DELIVERY) : \(details?.delivery ?? "0")"
            self.famousBtn.isHidden = !(details?.is_famous ?? false)
            self.followBtn.isSelected = details?.isFollow ?? false
            self.chatBtn.isHidden = self.type == .Company
        }
    }
    
    func changeFollowBtnSelected() {
        followBtn.isSelected = !followBtn.isSelected
    }
    
    func changeFollowCount(count: Int) {
        DispatchQueue.main.async {
            self.followerCountLbl.text = "(\(count))"
        }
    }
    
    func playVideo(_ url: String?) {
        let videoURL = URL(string: url ?? "")!
        let player = AVPlayer(url: videoURL)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player

        present(playerViewController, animated: true, completion: {
            playerViewController.player!.play()
        })
    }
    
    func presentReviewsVC(_ shopID: Int) {
        let vc = ReviewsVC()
        vc.shopID = shopID
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToProductDetailsVC(_ id: Int) {
        let vc = ProductDetailsVC()
        vc.productID = id
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToAdDetailsVC(_ id: Int, title: String?) {
        let vc = AdDetailsVC()
        vc.id = id
        vc.title = title
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToFamousDetailsVC(_ id: Int) {
        let vc = FamousDetailsVC()
        vc.id = id
        vc.type = .Famous
        navigationController?.pushViewController(vc, animated: true)
    }

    func navigateToChatPageVC(reciverType: Int, id: Int) {
        let vc = ChatPageVC()
        vc.reciverType = reciverType
        vc.id = id
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func showFilterView(_ isHidden: Bool) {
        filterView.isHidden = isHidden
    }

    func presentFilterAdsVC(_ id: Int) {
        let vc = FilterAdsVC()
        let nc = UINavigationController.init(rootViewController: vc)
        self.halfModalTransitioningDelegate = HalfModalTransitioningDelegate(viewController: self, presentingViewController: nc)
        vc.delegate = self
        vc.id = id
        vc.type = .Shop
        nc.modalPresentationStyle = .custom
        nc.transitioningDelegate = self.halfModalTransitioningDelegate
        present(nc, animated: true, completion: nil)
    }

    
}
