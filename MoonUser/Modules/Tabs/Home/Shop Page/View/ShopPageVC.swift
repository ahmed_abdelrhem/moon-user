//
//  ShopPageVC.swift
//  MoonUser
//
//  Created by Grand iMac on 2/2/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

enum ShopType {
    case Shop
    case Company
}

class ShopPageVC: UIViewController {

    
    //MARK:- Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var sectionsCollectionView: UICollectionView! {
        didSet {
            sectionsCollectionView.tag = 0
            sectionsCollectionView.delegate = self
            sectionsCollectionView.dataSource = self
            sectionsCollectionView.register(UINib(nibName: "SectionCell", bundle: nil), forCellWithReuseIdentifier: "SectionCell")
        }
    }
    @IBOutlet weak var productsCollectionView: UICollectionView!{
        didSet {
            productsCollectionView.tag = 1
            productsCollectionView.delegate = self
            productsCollectionView.dataSource = self
            productsCollectionView.register(UINib(nibName: "DiscoverCell", bundle: nil), forCellWithReuseIdentifier: "DiscoverCell")
            productsCollectionView.register(UINib(nibName: "ProductCell", bundle: nil), forCellWithReuseIdentifier: "ProductCell")
            productsCollectionView.register(UINib(nibName: "CompanyProductCell", bundle: nil), forCellWithReuseIdentifier: "CompanyProductCell")
        }
    }
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var scrollTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var famousBtn: UIButton!
    @IBOutlet weak var chatBtn: UIButton!
    @IBOutlet weak var followBtn: UIButton!
    @IBOutlet weak var shopNameLbl: UILabel!
    @IBOutlet weak var tagsLbl: UILabel!
    @IBOutlet weak var reviewsCountLbl: UILabel!
    @IBOutlet weak var followerCountLbl: UILabel!
    @IBOutlet weak var avgLbl: UILabel!
    @IBOutlet weak var minLbl: UILabel!
    @IBOutlet weak var deliveryLbl: UILabel!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var extraDataStack: UIStackView!
    
    
    
    //MARK:- Variables
    internal var presenter: ShopPageVCPresenter!
    internal var halfModalTransitioningDelegate: HalfModalTransitioningDelegate?
    var shopID: Int!
    var type: ShopType = .Shop
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setPresenter()
        setUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.tintColor = .mainColor
    }



    //MARK:- Functions
    private func setPresenter() {
        presenter = ShopPageVCPresenter(view: self)
        presenter.viewDidLoad(id: shopID)
    }

    private func setUI() {
        backButtonTitle("")
        setNavigationBarIsTransparent()
        containerView.isHidden = true
        filterView.isHidden = true
        scrollTopConstraint.constant = -90
        rateView.updateOnTouch = false
        productImg.setAlpha(0.5)
        productImg.fullScreenWhenTapped()
        rateView.didFinishTouchingCosmos = { _ in
            self.presenter.reviewViewClicked()
        }
        chatBtn.isHidden = (DEF.type == 1 && DEF.flag == 1)
        extraDataStack.isHidden = !(DEF.type == 1 && DEF.flag == 1)
    }
    
    
    


    
    //MARK:- Actions
    @IBAction func followBtnClicked(_ sender: UIButton) {
        presenter.followBtnClicked()
    }
    
    @IBAction func famousBtnClicked(_ sender: UIButton) {
        //lsa
//        presenter.famousBtnClicked()
    }
    
    @IBAction func chatBtnClicked(_ sender: UIButton) {
        presenter.chatBtnClicked()
    }
    
    @IBAction func filterBtnClicked(_ sender: UIButton) {
        presenter.filterBtnClicked()
    }

    
    
    

}



extension ShopPageVC: FilterDelegate {
    
    func getFliterID(_ id: Int) {
        presenter.filterShopAds(with: id)
    }
    
    
}
