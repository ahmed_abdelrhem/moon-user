//
//  ShopPageVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 2/2/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol ShopPageVCView: LoaderDelegate {
    func updateUI(_ details: ShopDetails?)
    func changeFollowCount(count: Int)
    func changeFollowBtnSelected()
    func showFilterView(_ isHidden: Bool)
    func playVideo(_ url: String?)
    func presentReviewsVC(_ shopID: Int)
    func presentFilterAdsVC(_ id: Int)
    func navigateToProductDetailsVC(_ id: Int)
    func navigateToAdDetailsVC(_ id: Int, title: String?)
    func navigateToFamousDetailsVC(_ id: Int)
    func navigateToChatPageVC(reciverType: Int, id: Int)
}

protocol SectionCellView {
    func setName(name: String?)
}

protocol ProductCellView {
    func configure(image: String?, name: String?, price: String?, desc: String?)
}

class ShopPageVCPresenter {
    
    
    private weak var view: ShopPageVCView?
    private let interactor = ShopPageVCAPI()
    private var details: ShopDetails?
    private var categories = [CategoryService]()
    private var gallery = [DiscoverData]()
    private var adds = [DiscoverData]()
    private var filterAdds = [DiscoverData]()
    private var products = [SingleProduct]()
    private (set) var selectedSectionindex = 0
    private var isFilter = false

    
    init(view: ShopPageVCView) {
        self.view = view
    }
    
    
    func viewDidLoad(id: Int) {
        
        self.createFirstCategories()
        
        view?.showProgress()
        
        interactor.getShopDetails(id: id, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            
            if response.status == StatusCode.Success.rawValue {
                self?.details = response.data?.shop_details
                self?.categories += response.data?.categories ?? []
                self?.gallery = response.data?.gallery ?? []
                self?.adds = response.data?.adds ?? []
                self?.view?.onSuccess("")
                self?.view?.updateUI(self?.details)
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
    
    private func createFirstCategories() {
        let gallary = CategoryService(name: GALLARY)
        let adds = CategoryService(name: ADDS)
        self.categories.append(gallary)
        self.categories.append(adds)
    }
    
    func getCount(_ tag: Int) -> Int {
        if tag == 0 {
            return categories.count
        } else {
            if selectedSectionindex == 0 {
                return gallery.count
            } else if selectedSectionindex == 1 {
                return isFilter ? filterAdds.count : adds.count
            } else {
                return products.count
            }
        }
    }
   
    func getCellSize(_ tag: Int, type: ShopType, width: CGFloat, index: Int) -> CGSize {
        if tag == 0 {
            let c = categories[index].name ?? ""
            let w = c.size(withAttributes: [.font: UIFont.myRegularSystemFont(ofSize: 17)]).width + 50
            return CGSize(width: w, height: 60)
        } else {
            if selectedSectionindex == 0 || selectedSectionindex == 1 || type == .Company {
                return CGSize(width: width/2, height: 150)
            } else {
                return CGSize(width: width, height: 120)
            }
        }
    }
    
    func sectionCellConfigure(cell: SectionCellView, index: Int) {
        cell.setName(name: categories[index].name)
    }
    
    func discoverCellConfigure(cell: DiscoverCellView, index: Int) {
        let item = selectedSectionindex == 0 ? gallery[index] : (isFilter ? filterAdds[index] : adds[index])
        if item.type == 1 {
            cell.configure(img: item.image)
            cell.isVideo(false)
        } else {
            cell.configure(img: item.capture)
            cell.isVideo(true)
        }
    }
    
    func productCellConfigure(cell: ProductCellView, index: Int) {
        let p = products[index]
        cell.configure(image: p.image, name: p.name, price: p.price, desc: p.description)
    }
    
    func followBtnClicked() {
        if DEF.isLogin {
            view?.changeFollowBtnSelected()
            if details?.isFollow ?? false {
                details?.isFollow = false
                let c = details?.followers_count ?? 0
                details?.followers_count = c - 1
                view?.changeFollowCount(count: (details?.followers_count!)!)
            } else {
                details?.isFollow = true
                let c = details?.followers_count ?? 0
                details?.followers_count = c + 1
                view?.changeFollowCount(count: (details?.followers_count!)!)
            }
            DispatchQueue.global(qos: .background).async {
                self.followShop(shopID: self.details?.id ?? 0)
            }
        } else {
            showLoginMessage()
        }
    }
    
    func didSelectItemAt(type: ShopType, tag: Int, index: Int) {
        self.view?.showFilterView(index != 1)
        if tag == 0 {
            selectedSectionindex = index
            if selectedSectionindex == 0 || selectedSectionindex == 1 {
                view?.onSuccess("")
            } else {
                let id = categories[index].id ?? 0
                self.getProductFromCategory(categoryID: id)
            }
        } else {
            if selectedSectionindex == 0 || selectedSectionindex == 1 {
                let dis = selectedSectionindex == 0 ? gallery[index] : (isFilter ? filterAdds[index] : adds[index])
                if dis.type == 2 {
                    view?.playVideo(dis.image)
                }
            } else {
                let id = self.products[index].id ?? 0
                if type == .Shop {
                    view?.navigateToProductDetailsVC(id)
                } else {
                    let title = self.products[index].name
                    view?.navigateToAdDetailsVC(id, title: title)
                }
            }
        }
    }
    
    func reviewViewClicked() {
        view?.presentReviewsVC(self.details?.id ?? 0)
    }
    
    func famousBtnClicked() {
        let id = details?.id ?? 0
        view?.navigateToFamousDetailsVC(id)
    }
    
    func chatBtnClicked() {
        let id = details?.id ?? 0
        let type = DEF.type
        view?.navigateToChatPageVC(reciverType: type, id: id)
    }

    func filterBtnClicked() {
        view?.presentFilterAdsVC(self.details?.id ?? 0)
    }
    
    func filterShopAds(with id: Int)  {
        isFilter = true
        filterAdds = adds.filter({ (item) -> Bool in
            return item.famous_id == id
        })
        view?.onSuccess("")
    }


    //MARK:- Private Functions
    private func followShop(shopID: Int) {
        DispatchQueue.global(qos: .background).async {
            self.interactor.followShop(shopID: shopID, type: DEF.type)
        }
    }
    
    private func getProductFromCategory(categoryID: Int) {
        
        view?.showProgress()
        
        interactor.getProductFromCategory(categoryID: categoryID, shopID: self.details?.id ?? 0, didDataReady: { [weak self] (response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.products = response.data ?? []
                self?.view?.onSuccess("")
            } else {
                self?.view?.onEmpty(.no_connection_image, CONNECTION_ERROR)
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onEmpty(.no_connection_image, CONNECTION_ERROR)
        }
    }
    
    
    
    
    
}
    
