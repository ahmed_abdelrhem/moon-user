//
//  FamousDetailsVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 2/25/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol FamousDetailsVCView: LoaderDelegate {
    func navigateToShopPage(_ shopID: Int)
    func updateHeader()
    func presentFilterAdsVC(_ id: Int)
    func navigateToDiscoverVC(_ albumID: Int, famousID: Int?)
    func navigateToChatPageVC(reciverType: Int, id: Int)
}

protocol FamousHeaderView: class {
    func setPhoto(with Url: String?)
    func setName(_ name: String?)
    func hideChat(_ hidden: Bool)
    func setFollower(_ followers: String?)
    func setIsFollow(_ isFollow: Bool)
    func setIsShop(_ isShop: Bool)
    func hideFilter(_ hidden: Bool)
}

protocol AlbumsCellView {
    func setPhoto(with Url: String?)
    func setName(_ nameLbl:String?)
    func setdesc(_ desc:String?)
    func setIsAlbum()
    func setIsAds()
    func isVideo(_ isVideo: Bool)
}



class FamousDetailsVCPresenter {
    
    private weak var view: FamousDetailsVCView?
    private let interactor = FamousDetailsVCAPI()
    private let shopInteractor = ShopPageVCAPI()
    private var details: FamousResponseModel?
    private var albums = [Album]()
    private var filterAlbums = [Album]()
    private var isFilter = false
    
    
    init(view: FamousDetailsVCView) {
        self.view = view
    }
    
    var count: Int {
        return isFilter ? filterAlbums.count : albums.count
    }
    
    func viewDidLoad(id: Int, tab: Int) {
        
        isFilter = false
        
        view?.showProgress()
        
        interactor.getFamousData(id: id, tab: tab, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.details = response
                self?.albums = response.data ?? []
                self?.view?.onSuccess("")
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onEmpty(.no_connection_image, CONNECTION_ERROR)
        }
    }
        
    func configureHeader(type: DiscoversType, header: FamousHeaderView, isAlbum: Bool)  {
        header.setName(details?.account_details?.nick_name)
        header.setPhoto(with: details?.account_details?.user_image)
        header.setFollower("\(details?.followers ?? 0)")
        header.setIsShop(details?.has_shop ?? false)
        header.setIsFollow(details?.is_follow == 1)
        header.hideFilter(isAlbum)
        header.hideChat(type == .Famous)
    }
    
    func configureCell(cell: AlbumsCellView, at row: Int, isAlbum: Bool)  {
        let album = isFilter ? filterAlbums[row] : albums[row]
        cell.setPhoto(with: album.image)
        cell.setName(album.name)
        let c = "\(album.count ?? 0) \(LIBRARY) - \(album.video_count ?? 0) \(VIDEO)"
        cell.setdesc(c)
        if isAlbum {
            cell.isVideo(false)
            cell.setIsAlbum()
        } else {
            cell.setIsAds()
            cell.setPhoto(with: album.type == 1 ? album.image : album.capture)
            cell.isVideo(album.type == 2)
        }
    }

    func didSelectItem(at row: Int, isAlbum: Bool) {
        if isAlbum {
            let famousID = details?.account_details?.id ?? 0
            let albumID = albums[row].id ?? 0
            view?.navigateToDiscoverVC(albumID, famousID: famousID)
        } else {
            let album = isFilter ? filterAlbums[row] : albums[row]
            DEF.type = album.shop_type ?? 0
            DEF.flag = album.flag ?? 0
            let id = album.shop_id ?? 0
            view?.navigateToShopPage(id)
        }
    }
    
    func shopViewClicked() {
        DEF.type = 1 //Constant because any famous or photographer has a type = 1 and a flag = 1
        DEF.flag = 1
        let id = details?.account_details?.id ?? 0
        view?.navigateToShopPage(id)

    }
    
    func followBtnClicked() {
        let id = details?.account_details?.id ?? 0
        view?.updateHeader()
        DispatchQueue.global(qos: .background).async {
            self.shopInteractor.followShop(shopID: id, type: DEF.type)
        }
    }
    
    func filterBtnPressed() {
        let id = details?.account_details?.id ?? 0
        view?.presentFilterAdsVC(id)
    }
    
    func filterShopAds(with id: Int)  {
        isFilter = true
        filterAlbums = albums.filter({ (item) -> Bool in
            return item.shop_id! == id
        })
        view?.onSuccess("")
    }
    
    func chatBtnActions() {
        if DEF.isLogin {
            let id = details?.account_details?.id ?? 0
            let type = DEF.type
            view?.navigateToChatPageVC(reciverType: type, id: id)
        } else {
            showLoginMessage()
        }
    }


    
    
}
