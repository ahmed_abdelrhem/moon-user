//
//  FamousDetailsVC+UICollectionView.swift
//  MoonUser
//
//  Created by Grand iMac on 2/25/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension FamousDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AlbumsCell", for: indexPath) as? AlbumsCell else {
            return UICollectionViewCell()
        }
        presenter.configureCell(cell: cell, at: indexPath.row, isAlbum: self.isAlbum)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if (kind == UICollectionView.elementKindSectionHeader) {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FamousHeader", for: indexPath) as! FamousHeader
            // Customize headerView here
            headerView.delegate = self
            presenter.configureHeader(type: self.type, header: headerView, isAlbum: self.isAlbum)
            return headerView
        }
        return  UICollectionReusableView(frame: .zero)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let h:CGFloat =  isAlbum ? 180 : 230.0
        return CGSize(width: collectionView.bounds.width, height: h)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: CGFloat(collectionView.width / 3.0), height: CGFloat(collectionView.height / 3.0 - 10))
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.didSelectItem(at: indexPath.row, isAlbum: self.isAlbum)
    }


    
}

//MARK:- Famous Header Delegate
extension FamousDetailsVC: FamousHeaderDelegate {
    
    func chatBtnPressed() {
        presenter.chatBtnActions()
    }
    
    func followBtnPressed() {
        presenter.followBtnClicked()
    }
    
    func shopViewPressed() {
        presenter.shopViewClicked()
    }
    
    func segmentControlChange(_ index: Int) {
        self.isAlbum = index == 0
        presenter.viewDidLoad(id: id, tab: index+1)
    }
    
    func filterBtnPressed() {
        presenter.filterBtnPressed()
    }
    
    
    
    
}
