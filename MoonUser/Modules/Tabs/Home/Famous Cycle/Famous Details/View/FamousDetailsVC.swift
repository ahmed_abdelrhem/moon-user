//
//  FamousDetailsVC.swift
//  MoonUser
//
//  Created by Grand iMac on 2/25/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class FamousDetailsVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.delegate = self
            collectionView.dataSource = self
            collectionView.register(UINib(nibName: "FamousHeader", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "FamousHeader")
            collectionView.register(UINib(nibName: "AlbumsCell", bundle: Bundle.main), forCellWithReuseIdentifier: "AlbumsCell")
            collectionView.allowsMultipleSelection = false
        }
    }

    
    //MARK:- Variables
    internal var presenter: FamousDetailsVCPresenter!
    internal var halfModalTransitioningDelegate: HalfModalTransitioningDelegate?
    var type: DiscoversType!
    var id: Int!
    var isAlbum = true

    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        backButtonTitle("")
        setPresenter()
        collectionView.isHidden = true
    }
    
    //MARK:- Functions
    private func setPresenter() {
        presenter = FamousDetailsVCPresenter(view: self)
        presenter.viewDidLoad(id: id, tab: 1)
    }
    
    

}


extension FamousDetailsVC: FilterDelegate {
    
    func getFliterID(_ id: Int) {
        presenter.filterShopAds(with: id)
    }
    
    
}
