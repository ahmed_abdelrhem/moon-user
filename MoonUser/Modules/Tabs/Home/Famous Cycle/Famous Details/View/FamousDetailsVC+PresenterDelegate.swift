//
//  FamousDetailsVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 2/25/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
extension FamousDetailsVC: FamousDetailsVCView {
    
    func showProgress() {
        startAnimating()
    }
    
    func hideProgress() {
        stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        collectionView.restore()
        collectionView.reloadData()
        if collectionView.numberOfItems(inSection: 0) == 0 {
            collectionView.setEmptyView(title: NO_DATA_FOUND, messageImage: .no_data_image)
        }
        collectionView.isHidden = false
    }
    
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func navigateToShopPage(_ shopID: Int) {
        let vc = ShopPageVC()
        vc.shopID = shopID
        navigationController?.pushViewController(vc, animated: true)
    }

    func updateHeader() {
        if let header = collectionView.supplementaryView(forElementKind: UICollectionView.elementKindSectionHeader, at: IndexPath(item: 0, section: 0)) as? FamousHeader {
            var c = Int(header.followersLbl.text ?? "0")!
            if header.followBtn.isSelected {
                c += 1
            } else {
                c -= 1
            }
            header.followersLbl.text = "\(c)"
        }
    }
    
    func presentFilterAdsVC(_ id: Int) {
        let vc = FilterAdsVC()
        let nc = UINavigationController.init(rootViewController: vc)
        halfModalTransitioningDelegate = HalfModalTransitioningDelegate(viewController: self, presentingViewController: nc)
        vc.delegate = self
        vc.id = id
        nc.modalPresentationStyle = .custom
        nc.transitioningDelegate = self.halfModalTransitioningDelegate
        present(nc, animated: true, completion: nil)
    }
    
    func navigateToDiscoverVC(_ albumID: Int, famousID: Int?) {
        let storyboard = UIStoryboard(name: Storyboard.Tabs.name, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: Identifiers.Discovers.id) as! DiscoversVC
        vc.discover = .Album
        vc.serviceID = albumID
        vc.famousID = famousID
        navigationController?.pushViewController(vc, animated: true)
    }

    func navigateToChatPageVC(reciverType: Int, id: Int) {
        let vc = ChatPageVC()
        vc.reciverType = reciverType
        vc.id = id
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }

    
}
