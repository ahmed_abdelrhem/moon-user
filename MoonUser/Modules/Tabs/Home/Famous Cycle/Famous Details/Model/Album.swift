/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation

struct Album : Codable {
	let id : Int?
	let image : String?
	let name : String?
	let type : Int?
	let count : Int?
    let video_count : Int?
    let capture : String?
    let shop_id : Int?
    let shop_type : Int?
    let flag : Int?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case image = "image"
		case name = "name"
		case type = "type"
		case count = "count"
        case video_count = "video_count"
        case capture = "capture"
        case shop_id = "shop_id"
        case shop_type = "shop_type"
        case flag = "flag"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		image = try values.decodeIfPresent(String.self, forKey: .image)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		type = try values.decodeIfPresent(Int.self, forKey: .type)
		count = try values.decodeIfPresent(Int.self, forKey: .count)
        video_count = try values.decodeIfPresent(Int.self, forKey: .video_count)
        capture = try values.decodeIfPresent(String.self, forKey: .capture)
        shop_id = try values.decodeIfPresent(Int.self, forKey: .shop_id)
        shop_type = try values.decodeIfPresent(Int.self, forKey: .shop_type)
        flag = try values.decodeIfPresent(Int.self, forKey: .flag)
	}

}
