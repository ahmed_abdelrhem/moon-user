//
//  FilterAdsVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 2/26/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol FilterAdsVCView: LoaderDelegate {
    func resetCollectionView()
    func closeViewController()
    func retriveFilterData(_ id: Int)
}

protocol FilterCellView {
    func setShopName(_ name: String?)
}


class FilterAdsVCPresenter {
    
    private weak var view: FilterAdsVCView?
    private let interactor = FilterAdsVCAPI()
    private var details: FamousResponseModel?
    private var isSearching: Bool = false
    private var selectedID: Int = 0
    private var shops = [FilterData]()
    private var filterShops = [FilterData]()

    
    init(view: FilterAdsVCView) {
        self.view = view
    }
    
    var count: Int {
        return isSearching ? filterShops.count : shops.count
    }
    
    func viewDidLoad(type: FiltetType, id: Int) {
        
        view?.showProgress()
        
        interactor.getFiltersData(type: type, id: id, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.shops = response.data ?? []
                self?.view?.onSuccess("")
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onEmpty(.no_connection_image, CONNECTION_ERROR)
        }
    }
            
    func configureCell(cell: FilterCellView, at row: Int)  {
        let name = isSearching ? filterShops[row].name : shops[row].name
        cell.setShopName(name)
    }

    func sizeForItemAt(at row: Int) -> CGSize {
        let name = isSearching ? filterShops[row].name : shops[row].name
        let size: CGSize = "  \(name ?? "")  ".size(withAttributes: [.font: UIFont.myRegularSystemFont(ofSize: 15)])
        let width = size.width + 4
        return CGSize(width: width, height: 40)
    }

    func didSelectItem(at row: Int) {
        let id = isSearching ? filterShops[row].id : shops[row].id
        self.selectedID = id ?? 0
    }
    
    func searchBarTextDidChange (_ text: String) {
        if text == "" {
            isSearching = false
        } else {
            isSearching = true
            filterShops = shops.filter {
                return $0.name?.range(of: text, options: .caseInsensitive) != nil
            }
        }
        view?.onSuccess("")
    }

    func resetBtnClicked() {
        view?.resetCollectionView()
    }

    
    
    func closeBtnClicked() {
        view?.closeViewController()
    }
    
    
    func filterNowBtnPressed() {
        view?.retriveFilterData(self.selectedID)
    }

    
    

    

    
    
}
