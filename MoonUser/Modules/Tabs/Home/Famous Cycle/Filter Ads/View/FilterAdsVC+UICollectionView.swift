//
//  FilterAdsVC+UICollectionView.swift
//  MoonUser
//
//  Created by Grand iMac on 2/26/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation


extension FilterAdsVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCell", for: indexPath) as? FilterCell else {
            return UICollectionViewCell()
        }
        presenter.configureCell(cell: cell, at: indexPath.row)
        return cell
    }
 
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return presenter.sizeForItemAt(at: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        filterBtn.isUserInteractionEnabled = true
        filterBtn.isHighlighted = false
        presenter.didSelectItem(at: indexPath.row)
    }
      
}
