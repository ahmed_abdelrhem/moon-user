//
//  FilterAdsVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 2/26/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension FilterAdsVC: FilterAdsVCView {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        collectionView.restore()
        collectionView.reloadData()
        if collectionView.numberOfItems(inSection: 0) == 0 {
            collectionView.setEmptyView(title: NO_DATA_FOUND, messageImage: .no_data_image)
        }
        collectionView.isHidden = false
    }
    
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }

    
    func resetCollectionView() {
        filterBtn.isUserInteractionEnabled = false
        filterBtn.isHighlighted = true
        collectionView.reloadData()
    }



    func closeViewController() {
        if let delegate = navigationController?.transitioningDelegate as? HalfModalTransitioningDelegate {
            delegate.interactiveDismiss = false
        }
        dismiss(animated: true, completion: nil)
    }


    func retriveFilterData(_ id: Int) {
        if let delegate = navigationController?.transitioningDelegate as? HalfModalTransitioningDelegate {
            delegate.interactiveDismiss = false
        }
        delegate?.getFliterID(id)
        dismiss(animated: true, completion: nil)
    }

    
}
