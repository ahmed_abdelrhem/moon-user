//
//  FilterAdsVC.swift
//  MoonUser
//
//  Created by Grand iMac on 2/26/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

enum FiltetType {
    case Shop
    case Famous
}


protocol FilterDelegate:class {
    func getFliterID(_ id: Int)
}


class FilterAdsVC: UIViewController, HalfModalPresentable {
    
    //MARK:- Outlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.register(UINib(nibName: "FilterCell", bundle: Bundle.main), forCellWithReuseIdentifier: "FilterCell")
            collectionView.dataSource = self
            collectionView.delegate = self
            collectionView.allowsMultipleSelection = false
        }
    }

    
    //MARK:- Variables
    internal var presenter: FilterAdsVCPresenter!
    weak var delegate : FilterDelegate?
    var id: Int!
    var type: FiltetType = .Famous
    
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
        addNavButtons()
        setPresenter()
    }
    
    
    //MARK:- Functions
    private func setPresenter() {
        presenter = FilterAdsVCPresenter(view: self)
        presenter.viewDidLoad(type: type, id: self.id)
    }
    private func updateUI() {
        self.navigationController?.navigationBar.layer.cornerRadius = 20
        self.navigationController?.navigationBar.clipsToBounds = true
        self.navigationController?.navigationBar.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        searchBar.delegate = self
        filterBtn.isUserInteractionEnabled = false
        filterBtn.isHighlighted = true
        title = FILTER
    }
    
    private func addNavButtons() {
        let resetBtn = UIButton(type: UIButton.ButtonType.system)
        resetBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        resetBtn.setTitle("Reset", for: .normal)
        resetBtn.addTarget(self, action: #selector(resetBtnClicked), for: .touchUpInside)
        let resetBarBtnItem = UIBarButtonItem(customView: resetBtn)
        self.navigationItem.rightBarButtonItem = resetBarBtnItem
        
        let closeBtn = UIButton(type: UIButton.ButtonType.system)
        closeBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        closeBtn.setImage(UIImage(named: "close5")!, for: .normal)
        closeBtn.addTarget(self, action: #selector(closeBtnClicked), for: .touchUpInside)
        let closeBarBtnItem = UIBarButtonItem(customView: closeBtn)
        self.navigationItem.leftBarButtonItem = closeBarBtnItem
    }
    
        
    
    //MARK:- Actions
    @objc func resetBtnClicked() {
        presenter.resetBtnClicked()
    }
    
    @objc func closeBtnClicked() {
        presenter.closeBtnClicked()
    }
    
    @IBAction func filterNowBtnPressed(_ sender: UIButton) {
        presenter.filterNowBtnPressed()
    }
    

    

    


}

