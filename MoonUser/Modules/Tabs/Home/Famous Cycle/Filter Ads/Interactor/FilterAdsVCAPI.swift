//
//  FilterAdsVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 2/26/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class FilterAdsVCAPI {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias model = (FilterResponseModel)->()
    
    func getFiltersData(type: FiltetType, id: Int, didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param: [String: Int]?
        let url: String?
        if type == .Famous {
            param = [
                "id": id,
                "type": 1,
                "account_type": 13
            ]
        
            url = URLs.filterShopAds.url.concatURL(param!)
        } else {
            param = [
                "shop_id": id,
            ]
            
            url = URLs.filter_shop_ads.url.concatURL(param!)
        }
        
        apiManager.contectToApiWith(url: url!,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(FilterResponseModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
}
