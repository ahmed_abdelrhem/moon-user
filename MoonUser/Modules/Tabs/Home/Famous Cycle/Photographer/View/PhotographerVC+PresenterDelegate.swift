//
//  PhotographerVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 2/23/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension PhotographerVC: PhotographerVCView {
    
    func showProgress() {
        startAnimating()
    }
    
    func hideProgress() {
        stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        collectionView.reloadData()
    }
    
    func setSlideShow(_ images: [String]) {
        SharedHandller.instance.setSlider(self, slideShow: sliderView, images: images)
    }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func onEmpty(_ img: UIImage, _ msg: String) {
        collectionView.setEmptyView(title: msg, messageImage: img)
    }
    
    func createPickerView() {
        
        filterBtn.resignFirstResponder()
        
        let alert = UIAlertController(style: .actionSheet, title: "", message: "")

        let pickerViewValues: [[String]] = [gender.map { $0.0 }]
        let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: 5)

        alert.addPickerView(values: pickerViewValues, initialSelection: pickerViewSelectedValue) { vc, picker, index, values in
            DispatchQueue.main.async { [weak self] in
                self?.filter = self!.gender[index.row]
            }
        }
        
        alert.addAction(image: nil, title: DONE, color: .mainColor, style: .default, isEnabled: true) { [weak self] (action) in
            self?.filterBtn.setTitle(self!.filter.0, for: .normal)
            self?.presenter.viewDidLoad(gender: self!.filter.1, flag: self!.filterFlag.1)
        }
        
        alert.addAction(image: nil, title: CANCEL, color: .red, style: .cancel, isEnabled: true) { _ in
            alert.dismiss(animated: true, completion: nil)
        }
        
        present(alert, animated: true, completion: nil)
    }
    
    func createFlagPickerView() {
        
        filterFlagBtn.resignFirstResponder()
        
        let alert = UIAlertController(style: .actionSheet, title: "", message: "")

        let pickerViewValues: [[String]] = [flag.map { $0.0 }]
        let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: 5)

        alert.addPickerView(values: pickerViewValues, initialSelection: pickerViewSelectedValue) { vc, picker, index, values in
            DispatchQueue.main.async { [weak self] in
                self?.filterFlag = self!.flag[index.row]
            }
        }
        
        alert.addAction(image: nil, title: DONE, color: .mainColor, style: .default, isEnabled: true) { [weak self] (action) in
            self?.filterFlagBtn.setTitle(self!.filterFlag.0, for: .normal)
            self?.presenter.viewDidLoad(gender: self!.filter.1, flag: self!.filterFlag.1)
        }
        
        alert.addAction(image: nil, title: CANCEL, color: .red, style: .cancel, isEnabled: true) { _ in
            alert.dismiss(animated: true, completion: nil)
        }
        
        present(alert, animated: true, completion: nil)
    }

    func navigateToDiscoverVC() {
        let storyboard = UIStoryboard(name: Storyboard.Tabs.name, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: Identifiers.Discovers.id) as! DiscoversVC
        vc.discover = self.type
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToSearchShopsVC() {
        let vc = SearchShopsVC()
        vc.serviceID = self.type.rawValue
        vc.type = .Famous
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToFamousDetailsVC(_ id: Int) {
        let vc = FamousDetailsVC()
        vc.id = id
        vc.type = self.type
        navigationController?.pushViewController(vc, animated: true)
    }


    
}

