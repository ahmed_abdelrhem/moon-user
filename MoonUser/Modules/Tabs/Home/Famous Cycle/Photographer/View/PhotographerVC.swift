//
//  PhotographerVC.swift
//  MoonUser
//
//  Created by Grand iMac on 2/23/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import ImageSlideshow

class PhotographerVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var sliderView: ImageSlideshow!
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.register(UINib(nibName: "FamousPeopleCell", bundle: Bundle.main), forCellWithReuseIdentifier: "FamousPeopleCell")
            collectionView.dataSource = self
            collectionView.delegate = self
            collectionView.allowsMultipleSelection = false

        }
    }
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var filterFlagBtn: UIButton!

    
    
    //MARK:- Variables
    internal var presenter: PhotographerVCPresenter!
    internal var gender = [(" All ",3),(" Men ",1),(" Woman ",2)]
    internal var flag = [(" All ",3),(" Designers ",1),(" Photographers ",2)]
    internal var filter = (" All ",3)
    internal var filterFlag = (" All ",3)
    var type: DiscoversType!
        
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setPresenter()
        backButtonTitle("")
        addSearchButton()
    }
    
    
    //MARK:- Functions
    private func setPresenter() {
        presenter = PhotographerVCPresenter(view: self)
        presenter.viewDidLoad(gender: filter.1, flag: filterFlag.1)
    }
    
    private func addSearchButton() {
        let searchBtn = UIButton(type: UIButton.ButtonType.system)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        searchBtn.setImage(UIImage(named: "search")!, for: .normal)
        searchBtn.addTarget(self, action: #selector(searchBtnClicked), for: .touchUpInside)
        let searchBarBtnItem = UIBarButtonItem(customView: searchBtn)
        self.navigationItem.rightBarButtonItem = searchBarBtnItem
    }
    
    @objc func searchBtnClicked(_ sender: UIButton) {
        presenter.searchBtnClicked()
    }


    
    
    
    //MARK:- Actions
    @IBAction func filterAllBtnClicked(_ sender: UIButton) {
        presenter.filterBtnClicked()
    }
    
    @IBAction func filterFlagBtnClicked(_ sender: UIButton) {
        presenter.filterFlagClicked()
    }
    
    @IBAction func discoverBtnClicked(_ sender: UIButton) {
        presenter.discoverBtnClicked()
    }


}

