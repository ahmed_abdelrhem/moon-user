//
//  PhotographerVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 2/23/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol PhotographerVCView: LoaderDelegate {
    func setSlideShow(_ images: [String])
    func createPickerView()
    func createFlagPickerView()
    func navigateToDiscoverVC()
    func navigateToSearchShopsVC()
    func navigateToFamousDetailsVC(_ id: Int)
}

class PhotographerVCPresenter {
    
    private weak var view: PhotographerVCView?
    private let interactor = PhotographerVCAPI()
    private var photographers = [Shop]()
    private var slider = [SingleStory]()
    
    
    init(view: PhotographerVCView) {
        self.view = view
    }
    
    var count: Int {
        return photographers.count
    }
    
    func viewDidLoad(gender: Int, flag: Int) {
        
        view?.showProgress()
        
        interactor.getFamousData(gender: gender, flag: flag, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.slider = response.data?.sliders ?? []
                self?.photographers = response.data?.all_famous ?? []
                var images = [String]()
                for s in self!.slider {
                    images.append(s.image ?? "")
                }
                self?.view?.onSuccess("")
                self?.view?.setSlideShow(images)
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onEmpty(.no_connection_image, CONNECTION_ERROR)
        }
    }
    
    
    func configurePhotographers(cell: FamousPeopleCellView, at row: Int)  {
        let item = photographers[row]
        cell.setPhoto(with: item.image ?? "")
        cell.setName(item.name ?? "")
    }

    func filterBtnClicked() {
        view?.createPickerView()
    }
    
    func filterFlagClicked() {
        view?.createFlagPickerView()
    }
    
    func discoverBtnClicked() {
        view?.navigateToDiscoverVC()
    }
    
    func searchBtnClicked() {
        view?.navigateToSearchShopsVC()
    }
    
    func didselectItem(at row: Int) {
        let id = photographers[row].id ?? 0
        view?.navigateToFamousDetailsVC(id)
    }

    
    
    
}
