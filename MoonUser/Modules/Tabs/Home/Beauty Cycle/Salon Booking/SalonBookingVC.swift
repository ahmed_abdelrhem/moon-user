//
//  SalonBookingVC.swift
//  MoonUser
//
//  Created by Grand iMac on 3/2/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import MOLH

protocol SalonBookingDelegate: class {
    func retriveData(date: String, time: Int, payment: Int)
}

class SalonBookingVC: UIViewController {


    //MARK:- Outlets
    @IBOutlet weak var morningBtn: UIButton!
    @IBOutlet weak var eveningBtn: UIButton!
    @IBOutlet weak var cashBtn: UIButton!
    @IBOutlet weak var onlineBtn: UIButton!
    @IBOutlet weak var datesCollection: UICollectionView! {
        didSet {
            datesCollection.tag = 0
            datesCollection.delegate = self
            datesCollection.dataSource = self
            datesCollection.register(UINib(nibName: "DateCell", bundle: nil), forCellWithReuseIdentifier: "DateCell")
        }
    }

    
    //MARK:- Variables
    weak var delegate: SalonBookingDelegate?
    var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
    private var days : [String] =  [] // 30days
    private var selectedIndex = 0
    private var time = 1
    var payment = 3
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.createDaysArray()
            self.morningBtn.isSelected = true
            
            if self.payment == 1 {
                self.cashBtn.isHidden = false
                self.onlineBtn.isHidden = true
                self.cashBtn.isSelected = true
            } else if self.payment == 2 {
                self.onlineBtn.isHidden = false
                self.cashBtn.isHidden = true
                self.onlineBtn.isSelected = true
            } else { //if 3
                self.cashBtn.isSelected = true
                self.payment = 1
            }
        }
    }

    
    //MARK:- Actions
    @objc func dismissViewController() {
        dismiss(animated: true, completion: nil)
    }

    
    @IBAction func handlePanGesture(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view?.window)
        
        if sender.state == .began {
            initialTouchPoint = touchPoint
        } else if sender.state == .changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        } else if sender.state == .ended || sender.state == .cancelled {
            if touchPoint.y - initialTouchPoint.y > 100 {
                self.dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
        
    }
        
    
    
    @IBAction func morningBtnClicked(_ sender: UIButton) {
        time = 1
        morningBtn.isSelected = true
        eveningBtn.isSelected = false
    }

    @IBAction func eveningBtnClicked(_ sender: UIButton) {
        time = 2
        morningBtn.isSelected = false
        eveningBtn.isSelected = true
    }

    @IBAction func cashBtnClicked(_ sender: UIButton) {
        payment = 1
        cashBtn.isSelected = true
        onlineBtn.isSelected = false
    }

    @IBAction func onlineBtnClicked(_ sender: UIButton) {
        payment = 2
        cashBtn.isSelected = false
        onlineBtn.isSelected = true
    }
    
    @IBAction func confirmBtnClicked(_ sender: UIButton) {
        let data = convertDateFormater(days[selectedIndex])
        delegate?.retriveData(date: data, time: time, payment: payment)
        dismiss(animated: true, completion: nil)
    }



 

}


//MARK:_ Helper Functions
extension SalonBookingVC {
    private func createDaysArray() { // create 30 days
        for i in 0..<100{
            days.append(addday(i))
        }
    }

    private func convertDateFormater(_ date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E d MMM yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return  dateFormatter.string(from: date!)
    }

    private func addday(_ value: Int) -> String{
        let today = Date()
        let next_day = Calendar.current.date(byAdding: .day, value: value, to: today)
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: MOLHLanguage.currentLocaleIdentifier())
        formatter.dateFormat = "E d MMM yyyy"
        let result = formatter.string(from: next_day!)
        return result
    }

    private func getDayAsArray(_ index: Int) -> [String] {
        let arr7 = days[index].components(separatedBy: " ")
        return arr7
    }

}


//MARK:- UICollectionView Delegate, Datasource
extension SalonBookingVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 100
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DateCell", for: indexPath) as? DateCell else {
            return UICollectionViewCell()
        }
        let day = getDayAsArray(indexPath.row)
        cell.configure(day[1], day[0], day[2])
        cell.isSelected = indexPath.row == selectedIndex
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 45, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        collectionView.reloadData()
    }
    
    
    
    
}
