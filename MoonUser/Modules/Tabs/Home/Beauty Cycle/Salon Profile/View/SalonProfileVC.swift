//
//  SalonProfileVC.swift
//  MoonUser
//
//  Created by Grand iMac on 3/2/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class SalonProfileVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var salonImg: UIImageView!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var reviewCountLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var phonesLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var locationStack: UIStackView!
    @IBOutlet weak var phonesStack: UIStackView!
    @IBOutlet weak var servicesView: UIView!
    @IBOutlet weak var gallaryView: UIView!
    @IBOutlet weak var addsView: UIView!
    @IBOutlet weak var totalView: UIView!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            tableView.separatorStyle = .none
            tableView.allowsMultipleSelection = true
            tableView.register(UINib(nibName: "AdditionalCell", bundle: nil), forCellReuseIdentifier: "AdditionalCell")
        }
    }
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet {
            collectionView.tag = 1
            collectionView.delegate = self
            collectionView.dataSource = self
            collectionView.register(UINib(nibName: "DiscoverCell", bundle: nil), forCellWithReuseIdentifier: "DiscoverCell")
        }
    }

    
    
    //MARK:- Variables
    internal var presenter: SalonProfileVCPresenter!
    var id: Int!
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
        setPresenter()
    }
    
    
    //MARK:- Functions
    private func setPresenter() {
        presenter = SalonProfileVCPresenter(view: self)
        presenter.viewDidLoad(id: id)
    }
    
    private func updateUI() {
        nextBtn.isUserInteractionEnabled = false
        nextBtn.isHighlighted = true
        addGestures()
        backButtonTitle("")
        title = SALON_PROFILE
        salonImg.fullScreenWhenTapped()
        containerView.isHidden = true
        rateView.didFinishTouchingCosmos = { _ in
            self.presenter.reviewViewClicked()
        }
    }
    
    private func addGestures() {
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.servicesViewClicked))
        servicesView.addGestureRecognizer(tap1)
        
        let tap2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.gallaryViewClicked))
        gallaryView.addGestureRecognizer(tap2)

        let tap3: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.addsViewClicked))
        addsView.addGestureRecognizer(tap3)
        
        let tap4: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.locationStackClicked))
        locationStack.addGestureRecognizer(tap4)

        
        let tap5: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.phonesStackClicked))
        phonesStack.addGestureRecognizer(tap5)

    }
    
    //MARK:- Actions
    @objc func servicesViewClicked() {
        presenter.servicesViewClicked()
    }

    @objc func gallaryViewClicked() {
        presenter.gallaryViewClicked()
    }

    @objc func addsViewClicked() {
        presenter.addsViewClicked()
    }
    
    @objc func locationStackClicked() {
        presenter.locationStackClicked()
    }
    
    @objc func phonesStackClicked() {
        presenter.phonesStackClicked()
    }

    @IBAction func chatBtnClicked(_ sender: UIButton) {
        presenter.chatBtnActions()
    }
    
    @IBAction func confirmBtnClicked(_ sender: UIButton) {
        presenter.confirmBtnActions()
    }


}

//MARK:- SalonBookingDelegate
extension SalonProfileVC: SalonBookingDelegate {
    func retriveData(date: String, time: Int, payment: Int) {
        presenter.bookingDataRetrived(date: date, time: time, payment: payment)
    }
    
    
}
