//
//  SalonProfileVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 3/2/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import AVKit
import SDWebImage


extension SalonProfileVC: SalonProfileVCView {
    
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        tableView.reloadData()
        collectionView.reloadData()
        containerView.isHidden = false
     }
    
    func updateUI(_ details: ClinicDetails?) {
        DispatchQueue.main.async {
            let imgURL = URL(string: details?.image ?? "")
            self.salonImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.salonImg.sd_setImage(with: imgURL, placeholderImage: .placeHolder_image)
            self.nameLbl.text = details?.name
            self.descLbl.text = details?.description
            self.reviewCountLbl.text = "(\(details?.comment_count ?? 0))"
            self.addressLbl.text = details?.address
            self.phonesLbl.text = details?.phones
            self.rateView.rating = Double(details?.rate ?? "0")!
            self.totalLbl.text = "\(0) \(DEF.country.currency)"
        }
    }

    func updateTotal(_ total: Int) {
        self.totalLbl.text = "\(total) \(DEF.country.currency)"
    }
    
    func setButtonIsHighlighted() {
        nextBtn.isUserInteractionEnabled = false
        nextBtn.isHighlighted = true
    }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func onEmpty(_ img: UIImage, _ msg: String) {
        tableView.reloadData()
        tableView.setEmptyView(title: msg, messageImage: img)
    }
    
    
    
    func presentReviewsVC(_ clinicID: Int) {
        let vc = ReviewsVC()
        vc.shopID = clinicID
        navigationController?.pushViewController(vc, animated: true)
    }

    func playVideo(_ url: String?) {
        let videoURL = URL(string: url ?? "")!
        let player = AVPlayer(url: videoURL)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player

        present(playerViewController, animated: true, completion: {
            playerViewController.player!.play()
        })
    }

    func showServices() {
        servicesView.backgroundColor = UIColor.darkBlueColor
        gallaryView.backgroundColor = .mainColor
        addsView.backgroundColor = .mainColor
        
        tableView.restore()
        tableView.isHidden = false
        collectionView.isHidden = true
        if tableView.numberOfRows(inSection: 0) == 0 {
            tableView.setEmptyView(title: NO_DATA_FOUND, messageImage: .no_data_image)
        }
        self.totalView.isHidden = false
    }
    
    func showGallary() {
        servicesView.backgroundColor = .mainColor
        gallaryView.backgroundColor = UIColor.darkBlueColor
        addsView.backgroundColor = .mainColor

        collectionView.restore()
        collectionView.reloadData()
        tableView.isHidden = true
        collectionView.isHidden = false
        if collectionView.numberOfItems(inSection: 0) == 0 {
            collectionView.setEmptyView(title: NO_DATA_FOUND, messageImage: .no_data_image)
        }
        self.totalView.isHidden = true
    }
    
    func showAdds() {
        servicesView.backgroundColor = .mainColor
        gallaryView.backgroundColor = .mainColor
        addsView.backgroundColor = UIColor.darkBlueColor

        collectionView.restore()
        collectionView.reloadData()
        tableView.isHidden = true
        collectionView.isHidden = false
        if collectionView.numberOfItems(inSection: 0) == 0 {
            collectionView.setEmptyView(title: NO_DATA_FOUND, messageImage: .no_data_image)
        }
        self.totalView.isHidden = true
    }
    
    func showLocation(lat: Double, lng: Double) {
        let vc = ShowLocationVC()
        vc.lat = lat
        vc.lng = lng
        present(vc, animated: true, completion: nil)
    }

    func showCallPhones(_ array: [[String]]) {
        
        var number: String?
        
        let alert = UIAlertController(style: .actionSheet, title: "", message: "")

        let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: 3)

        alert.addPickerView(values: array, initialSelection: pickerViewSelectedValue) { vc, picker, index, values in
            DispatchQueue.main.async { [weak self] in
                guard self != nil else { return }
                number = array[0][index.row]
            }
        }
        
        alert.addAction(image: nil, title: CALL, color: .mainColor, style: .default, isEnabled: true) { [weak self] (action) in
            guard self != nil else { return }
            let phone = number ?? array[0][0]
            if phone != "" {
                let url:NSURL = NSURL(string: "tel://\(phone)")!
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            } else {
                Messages.instance.showConfigMessage(title: NO_PHONE, body: "", state: .warning, layout: .statusLine, style: .top)
            }

        }
        
        alert.addAction(image: nil, title: CANCEL, color: .red, style: .cancel, isEnabled: true) { _ in
            alert.dismiss(animated: true, completion: nil)
        }
        
        present(alert, animated: true, completion: nil)
    }
    
    
    func presentSalonBookingVC(_ payment: Int) {
        let vc = SalonBookingVC()
        vc.delegate = self
        vc.payment = payment
        present(vc, animated: true, completion: nil)
    }

    func popViewController(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .success, layout: .cardView, style: .center)
        self.navigationController?.popViewController(animated: true)
    }

    func navigateToChatPageVC(reciverType: Int, id: Int) {
        let vc = ChatPageVC()
        vc.reciverType = reciverType
        vc.id = id
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }

    
}
