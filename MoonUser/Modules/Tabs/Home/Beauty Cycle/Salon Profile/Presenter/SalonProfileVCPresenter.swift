//
//  SalonProfileVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 3/2/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol SalonProfileVCView: LoaderDelegate {
    func updateUI(_ details: ClinicDetails?)
    func presentReviewsVC(_ clinicID: Int)
    func playVideo(_ url: String?)
    func showLocation(lat: Double, lng: Double)
    func showCallPhones(_ array: [[String]])
    func showServices()
    func showGallary()
    func showAdds()
    func updateTotal(_ total: Int)
    func setButtonIsHighlighted()
    func presentSalonBookingVC(_ payment: Int)
    func popViewController(_ msg: String)
    func navigateToChatPageVC(reciverType: Int, id: Int)
}


class SalonProfileVCPresenter {
    
    private weak var view: SalonProfileVCView?
    private let interactor = ClinicDetailsAPI()
    private var details: ClinicDetails?
    private var services = [CategoryService]()
    private var gallery = [DiscoverData]()
    private var adds = [DiscoverData]()
    private var selectedIndex: Int = 0      //NOTE: Doctors = 1, Gallary = 2, Adds = 3
    private var totalPrice: Int = 0
    private var selectedServiceIDs = [Int]()
    
    
    init(view: SalonProfileVCView) {
        self.view = view
    }
            
    
    func viewDidLoad(id: Int) {
        
        view?.showProgress()
        
        interactor.getClinicDetails(id: id, specialistID: 0, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.details = response.data?.details
                self?.services = response.data?.services ?? []
                self?.gallery = response.data?.gallery ?? []
                self?.adds = response.data?.adds ?? []
                self?.view?.updateUI(self?.details)
                self?.view?.onSuccess("")
                self?.view?.showServices()
            } else {
                self?.view?.onFailure(CONNECTION_ERROR)
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
    
    func reviewViewClicked() {
        view?.presentReviewsVC(self.details?.id ?? 0)
    }
    
    
    //MARK:- Actions
    func servicesViewClicked() {
        selectedIndex = 1
        view?.showServices()
    }

    func gallaryViewClicked() {
        selectedIndex = 2
        view?.showGallary()
    }

    func addsViewClicked() {
        selectedIndex = 3
        view?.showAdds()
    }
    
    func locationStackClicked() {
        let lat = Double(details?.lat ?? "0")!
        let lng = Double(details?.lng ?? "0")!
        view?.showLocation(lat: lat, lng: lng)
    }
    
    func phonesStackClicked() {
        if let array = details?.phones?.components(separatedBy: " , ") {
            view?.showCallPhones([array])
        }
    }
    
    func chatBtnActions() {
        if DEF.isLogin {
            let id = details?.id ?? 0
            let type = DEF.type
            view?.navigateToChatPageVC(reciverType: type, id: id)
        } else {
            showLoginMessage()
        }
    }

    
    
    func confirmBtnActions() {
        if DEF.isLogin {
            view?.presentSalonBookingVC(details?.payment_type ?? 3)
        } else {
            showLoginMessage()
        }
    }
    
    
    func bookingDataRetrived(date: String, time: Int, payment: Int) {
        view?.showProgress()
        
        interactor.confirmBeautyOrder(id: details?.id ?? 0, date: date, payment: payment, time: time, services: selectedServiceIDs, didDataReady: {  [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.view?.popViewController(response.message ?? "")
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { [weak self] error in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }

    
    //MARK:- TableView Functions
    var doctorsCount: Int {
        return services.count
    }
    
    func additionConfigure(cell: AdditionalCellView, at index: Int) {
        let item = services[index]
        cell.cellConfigure(name: item.name, price: "\(item.price ?? 0)")
    }
    
    func didSelectRow(at index: Int) {
        let id = services[index].id ?? 0
        let price = services[index].price ?? 0
        totalPrice += price
        view?.updateTotal(totalPrice)
        selectedServiceIDs.append(id)
    }
    
    func didDeSelectRow(at index: Int) {
        let id = services[index].id ?? 0
        let price = services[index].price ?? 0
        totalPrice -= price
        view?.updateTotal(totalPrice)
        
        if let index = selectedServiceIDs.firstIndex(where: { $0 == id}) {
            selectedServiceIDs.remove(at: index)
        }

        if totalPrice == 0 {
            view?.setButtonIsHighlighted()
        }
    }

    

    //MARK:- CollectionView Functions
    var collectionViewCount: Int {
        if selectedIndex == 2 {
            return gallery.count
        }
        return adds.count
    }

    func cellConfigure(cell: DiscoverCellView, index: Int) {
        let dis = selectedIndex == 2 ? gallery[index] : adds[index]
        if dis.type == 1 {
            cell.configure(img: dis.image)
            cell.isVideo(false)
        } else {
            cell.configure(img: dis.capture)
            cell.isVideo(true)
        }
    }

    func didSelectItemAt(index: Int) {
        let dis = selectedIndex == 2 ? gallery[index] : adds[index]
        if dis.type == 2 {
            view?.playVideo(dis.image)
        }
    }

    
}
