//
//  SingleCommercialVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 2/18/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import MapKit

protocol SingleCommercialVCView: LoaderDelegate {
    func navigateToMapVC()
    func showTableView()
    func showMap(instituations: [Shop])
    func presentFilterVC(_ serviceID: Int)
    func presentCitiesVC(_ serviceID: Int)
    func navigateToDiscoverVC(_ serviceID: Int, lat: String, lng: String)
    func navigateToSearchShopsVC(_ serviceID: Int)
    func navigateToShopPage(_ id: Int)
}


class SingleCommercialVCPresenter {
    
    private weak var view: SingleCommercialVCView?
    private let interactor = SingleCommercialVCAPI()
    private var services = [CategoryService]()
    private var commercials = [Shop]()
    private var selectedServiceID = 0
    private var lat: String = "0"
    private var lng: String = "0"
    private var tab: Int = 1        //Note: 1: Newest,  2: Offers, 3: Map
    private var cityID: Int = 0
    private var categoryID: Int = 0
    private var subCategoryID: Int = 0

    
    init(view: SingleCommercialVCView) {
        self.view = view
    }
    
    var serviceCount: Int {
        return services.count
    }
    
    var shopCount: Int {
        return commercials.count
    }
    
    
    func viewDidLoad(lat: String, lng: String) {
        self.lat = lat
        self.lng = lng
        self.getData()
    }
    
    
    private func getData() {
        
        view?.showProgress()
        
        let  url = URLs.companiesList.url
        
        interactor.getCommercialData(url: url, lat: self.lat, lng: self.lng, tab: self.tab, service_id: self.selectedServiceID, cityID: self.cityID, categoryID: self.categoryID, subCategoryID: self.subCategoryID, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.services = response.data?.services ?? []
                self?.commercials = response.data?.companies ?? []
                if self?.selectedServiceID == 0 {
                    self?.selectedServiceID = self?.services.first?.id ?? 0
                }
                if self?.tab == 3 {
                    self?.view?.showMap(instituations: self?.commercials ?? [])
                } else {
                    self?.view?.showTableView()
                }
                if self?.commercials.count == 0 {
                    self?.view?.onEmpty(.no_data_image, NO_DATA_FOUND)
                }
                self?.view?.onSuccess("")
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) {  [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
    
    
    func serviceConfigure(cell: ServiceCellView, index: Int) {
        let service = services[index]
        cell.configure(image: service.image, name: service.name)
        cell.setIsSelected(self.selectedServiceID == services[index].id)
    }
    
    
    func commercialConfigure(cell: InstituationCellView, index: Int) {
        let inst = commercials[index]
        cell.configure(image: inst.image, name: inst.name, address: "", rate: inst.rate)
    }
    
    
    func locationButtonClicked() {
        view?.navigateToMapVC()
    }
    
    
    func didSelectedServiceAt(index: Int) {
        let service = services[index]
        //Clear Data
        self.tab = 1
        self.cityID = 0
        self.categoryID = 0
        self.subCategoryID = 0
        self.selectedServiceID = service.id ?? 0
        //Call API
        self.getData()
    }

    
    func segmentValueChanged(_ index: Int) {
        self.tab = index + 1
        self.getData()
    }
    
    func userLocationChanged(lat: Double, lng: Double) {
        self.lat = "\(lat)"
        self.lng = "\(lng)"
        self.getData()
    }
    
    
    func filterBtnClicked() {
        view?.presentFilterVC(self.selectedServiceID)
    }
    
    func filterDataRetrived(catID: Int, subCat: Int) {
        self.categoryID = catID
        self.subCategoryID = subCat
        self.getData()
    }
    
    func citysBtnClicked() {
        view?.presentCitiesVC(self.selectedServiceID)
    }
    
    func cityTagRetriveID(_ id: Int) {
        self.cityID = id
        self.getData()
    }
    
    
    func discoverBtnClicked() {
        view?.navigateToDiscoverVC(self.selectedServiceID, lat: self.lat, lng: self.lng)
    }
    
    func searchBtnClicked() {
        view?.navigateToSearchShopsVC(self.selectedServiceID)
    }
    
    func didSelectRowAt(index: Int) {
        let id = commercials[index].id ?? 0
        view?.navigateToShopPage(id)
    }
    
    func markerDidTap(_ id: String?) {
        var lat: Double = 0.0
        var lng: Double = 0.0
        var address: String = ""
        let shopID = Int(id ?? "0")!
        if let s = commercials.first(where: {$0.id == shopID}) {
            lat = Double(s.lat ?? "0.0")!
            lng = Double(s.lng ?? "0.0")!
            address = s.address ?? ""
            
            //TODO:- Goolge map Navigaion
            let destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lng))))
            destination.name = address
            MKMapItem.openMaps(with: [destination], launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
        }
    }

    
    
}
