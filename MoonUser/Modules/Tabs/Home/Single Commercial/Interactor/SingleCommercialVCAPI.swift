//
//  SingleCommercialVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 2/18/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class SingleCommercialVCAPI {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias model = (CommercialModel)->()
    
    func getCommercialData(url: String, lat: String, lng: String, tab: Int, service_id: Int, cityID: Int, categoryID: Int, subCategoryID: Int, didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        var param = [
            "type": DEF.type,
            "country_id": DEF.country.id,
            "lat": lat,
            "lng": lng,
            "tab": tab
        ] as [String : Any]
        
        if service_id != 0 {
            param["service_id"] = service_id
        }
        
        if cityID != 0 {
            param["city_id"] = cityID
        }
        
        if categoryID != 0 {
            param["filter"] = 1
            param["category_id"] = categoryID
            param["sub_category_id"] = subCategoryID
        }
        
                
        let url = url.concatURL(param)
        
        apiManager.contectToApiWith(url: url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(CommercialModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
}
