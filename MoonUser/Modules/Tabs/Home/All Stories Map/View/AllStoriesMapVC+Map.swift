//
//  AllStoriesMapVC+Map.swift
//  MoonUser
//
//  Created by Grand iMac on 1/26/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import GoogleMaps


extension AllStoriesMapVC: CLLocationManagerDelegate, GMSMapViewDelegate {
    
    internal func locationAuth(){
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        if(!updateLocationBefor && manager.location?.coordinate != nil) {
            updateLocationBefor = true
            self.MapSetup(lat:"\((locValue.latitude))", long: "\((locValue.longitude))")
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        mapView.selectedMarker = marker
        presenter.markerDidTap(marker.accessibilityValue)
        return true
    }

    
    private func MapSetup(lat: String , long: String) {
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(lat)!, longitude: CLLocationDegrees(long)!, zoom: 15.0)
        
        let f: CGRect = view.frame
        let mapFrame = CGRect(x: f.origin.x, y: f.origin.y, width: f.size.width, height: f.size.height)
        mapView = GMSMapView.map(withFrame: mapFrame, camera: camera)
        mapView.settings.myLocationButton = true
        mapView.settings.compassButton = true
        mapView.isMyLocationEnabled = true
        mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.view.addSubview(mapView)
        self.startAnimating()
        mapView.delegate = self
        
        //Call API
        presenter.viewDidLoad()
    }
    
    internal func drawMarker(_ user: UserStories){
        let lat = Double(user.lat ?? "0.0")
        let lng = Double(user.lng ?? "0.0")
        let loc: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: lat ?? 0.0, longitude: lng ?? 0.0)
        
        let marker = GMSMarker()
        marker.position = loc
        let imageView = UIImageView(image: .avatar_image)
        imageView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        imageView.layer.cornerRadius = 20
        imageView.clipsToBounds = true
        imageView.sd_setImage(with: URL(string: user.image ?? ""), placeholderImage: .avatar_image)
        marker.iconView = imageView
        marker.title = user.name
        marker.accessibilityValue = "\(user.id ?? 0)"
        marker.map = mapView
    }

  
    

}

