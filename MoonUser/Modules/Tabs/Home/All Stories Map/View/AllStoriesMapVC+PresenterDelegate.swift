//
//  AllStoriesMapVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 1/26/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import GoogleMaps

extension AllStoriesMapVC: AllStoriesMapVCView {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ users: [UserStories]) {
        for s in users {
            drawMarker(s)
        }
    }
    
    func onFaluire(message: String) {
        Messages.instance.showConfigMessage(title: "", body: message, state: .error, layout: .messageView, style: .top)
    }
    

    
}

