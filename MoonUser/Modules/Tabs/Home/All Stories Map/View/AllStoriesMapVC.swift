//
//  AllStoriesMapVC.swift
//  MoonUser
//
//  Created by Grand iMac on 1/26/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import GoogleMaps

class AllStoriesMapVC: UIViewController {
    
    //MARK: Variables
    internal var presenter: AllStoriesMapVCPresenter!
    var mapView: GMSMapView!
    let locationManager = CLLocationManager()
    var updateLocationBefor = false
    var fromSideMenu = false
    
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        locationAuth()
        setPresenter()
        setNavigationBarIsTransparent()
        if fromSideMenu {
            SharedHandller.instance.sideMenus(self)
        }
    }
    
    //MARK:- Functions
    private func setPresenter() {
        presenter = AllStoriesMapVCPresenter(view: self)
    }
    
    
    
    
}

