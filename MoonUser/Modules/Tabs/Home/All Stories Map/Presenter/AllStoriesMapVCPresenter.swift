//
//  AllStoriesMapVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 1/26/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import MapKit

protocol AllStoriesMapVCView: class {
    func showProgress()
    func hideProgress()
    func onSuccess(_ users: [UserStories])
    func onFaluire(message: String)
}

class AllStoriesMapVCPresenter {
    
    private var view: AllStoriesMapVCView?
    private let interactor = AllStoriesMapVCAPI()
    private var stories = [UserStories]()
    
    
    init(view: AllStoriesMapVCView) {
        self.view = view
    }
    
    
    func viewDidLoad() {
        
        view?.showProgress()
        
        interactor.getAllStoriesMap(didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.stories = response.stories ?? []
                self?.view?.onSuccess(self!.stories)
            } else {
                self?.view?.onFaluire(message: response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return  }
            self?.view?.hideProgress()
            self?.view?.onFaluire(message: CONNECTION_ERROR)
        }
    }
    
    func markerDidTap(_ id: String?) {
        var lat: Double = 0.0
        var lng: Double = 0.0
        var address: String = ""
        let shopID = Int(id ?? "0")!
        if let s = stories.first(where: {$0.id == shopID}) {
            lat = Double(s.lat ?? "0.0")!
            lng = Double(s.lng ?? "0.0")!
            address = s.name ?? ""
            
            //TODO:- Goolge map Navigaion
            let destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lng))))
            destination.name = address
            MKMapItem.openMaps(with: [destination], launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
        }
    }

    
    
}

