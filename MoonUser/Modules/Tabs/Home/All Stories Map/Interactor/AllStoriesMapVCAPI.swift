//
//  AllStoriesMapVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 1/26/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class AllStoriesMapVCAPI: NSObject {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    
    typealias allStoriesModel = (AllStoriesModel)->()
    
    func getAllStoriesMap(didDataReady: @escaping allStoriesModel, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
                
        apiManager.contectToApiWith(url: URLs.allStories(DEF.country.id).url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(AllStoriesModel.self, from: data)
                    didDataReady(result)
                } catch {
                    print("error : ", error)
                }
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }

    
}
