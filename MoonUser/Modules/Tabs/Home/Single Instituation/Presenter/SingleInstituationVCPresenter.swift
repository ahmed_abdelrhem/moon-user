//
//  SingleInstituationVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 2/17/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import MapKit


protocol SingleInstituationVCView: LoaderDelegate {
    func navigateToMapVC()
    func showTableView()
    func showMap(instituations: [Shop])
    func presentFilterVC()
    func presentTagsVC(_ serviceID: Int)
    func navigateToDiscoverVC(_ serviceID: Int, lat: String, lng: String)
    func navigateToSearchShopsVC(_ serviceID: Int)
    func navigateToInstituationPage(_ id: Int)
    func navigateToClinicDetailsVC(_ id: Int)
    func navigateToSalonProfileVC(_ id: Int)
}


protocol InstituationCellView {
    func configure(image: String?, name: String?, address: String?, rate: String?)
}


class SingleInstituationVCPresenter {
    
    private weak var view: SingleInstituationVCView?
    private let interactor = SingleInstituationVCAPI()
    private let shopInteractor = ShopPageVCAPI()
    private var type: DiscoversType?
    private var services = [CategoryService]()
    private var instituations = [Shop]()
    private var selectedServiceID = 0
    private var lat: String = "0"
    private var lng: String = "0"
    private var tab: Int = 1        //Note: 1: Newest,  2: Offers, 3: Map
    private var filter: Int = 0     //Note: 1: Rating, 2: Newest firest, 3: A to Z, 4: Min order amount
    private var tags: [Int] = []

    
    init(view: SingleInstituationVCView, type: DiscoversType) {
        self.view = view
        self.type = type
    }
    
    var serviceCount: Int {
        return services.count
    }
    
    var shopCount: Int {
        return instituations.count
    }
    
    
    func viewDidLoad(lat: String, lng: String) {
        self.lat = lat
        self.lng = lng
        self.getData()
    }
    
    
    private func getData() {
        
        view?.showProgress()

        interactor.getInstituationsData(type: self.type, lat: self.lat, lng: self.lng, tab: self.tab, service_id: self.selectedServiceID, filter: self.filter, tags: self.tags, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.services = response.data?.services ?? []
                if let shops = response.data?.shops {
                    self?.instituations = shops
                } else {
                    self?.instituations = response.data?.clinics ?? []
                }
                if self?.selectedServiceID == 0 {
                    self?.selectedServiceID = self?.services.first?.id ?? 0
                }
                if self?.tab == 3 {
                    self?.view?.showMap(instituations: self?.instituations ?? [])
                } else {
                    self?.view?.showTableView()
                }
                if self?.instituations.count == 0 {
                    self?.view?.onEmpty(.no_data_image, NO_DATA_FOUND)
                }
                self?.view?.onSuccess("")
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) {  [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
    
    
    func serviceConfigure(cell: ServiceCellView, index: Int) {
        let service = services[index]
        cell.configure(image: service.image, name: service.name)
        cell.setIsSelected(self.selectedServiceID == services[index].id)
    }
    
    
    func instituationConfigure(cell: InstituationCellView, index: Int) {
        let inst = instituations[index]
        cell.configure(image: inst.image, name: inst.name, address: "", rate: inst.rate)
    }
    
    func clinicConfigure(cell: ClinicCellView, index: Int) {
        let shop = instituations[index]
        cell.configure(image: shop.image, name: shop.name, address: shop.address, desc: shop.description, rate: shop.rate, isFollow: shop.isFollow)
    }
    
    func followClinicBtnClicked(at index: Int) {
        if DEF.isLogin {
            let id = instituations[index].id ?? 0
            instituations[index].isFollow = !(instituations[index].isFollow ?? false)
            DispatchQueue.global(qos: .background).async {
                self.shopInteractor.followShop(shopID: id, type: DEF.type)
            }
        } else {
            showLoginMessage()
        }
    }

    func detailsBtnClicked(at index: Int) {
        let id = instituations[index].id ?? 0
        if type == .Clinic {
            view?.navigateToClinicDetailsVC(id)
        } else {
            view?.navigateToSalonProfileVC(id)
        }
    }
    
    func locationButtonClicked() {
        view?.navigateToMapVC()
    }
    
    
    func didSelectedServiceAt(index: Int) {
        let service = services[index]
        //Clear Data
        self.tab = 1
        self.filter = 0
        self.tags = []
        self.selectedServiceID = service.id ?? 0
        //Call API
        self.getData()
    }

    
    func segmentValueChanged(_ index: Int) {
        self.tab = index + 1
        self.getData()
    }
    
    func userLocationChanged(lat: Double, lng: Double) {
        self.lat = "\(lat)"
        self.lng = "\(lng)"
        self.getData()
    }
    
    
    func filterBtnClicked() {
        view?.presentFilterVC()
    }
    
    func filterRetriveID(_ id: Int) {
        self.filter = id
        self.getData()
    }
    
    func tagsBtnClicked() {
        view?.presentTagsVC(self.selectedServiceID)
    }
    
    func tagsRetriveIDs(_ ids: [Int]) {
        self.tags = ids
        self.getData()
    }
    
    func discoverBtnClicked() {
        view?.navigateToDiscoverVC(self.selectedServiceID, lat: self.lat, lng: self.lng)
    }
    
    func searchBtnClicked() {
        view?.navigateToSearchShopsVC(self.selectedServiceID)
    }
    
    func didSelectRowAt(index: Int) {
        let id = instituations[index].id ?? 0
        if !(self.type == .Clinic || self.type == .Salon) {
            view?.navigateToInstituationPage(id)
        }
    }
    
    func markerDidTap(_ id: String?) {
        var lat: Double = 0.0
        var lng: Double = 0.0
        var address: String = ""
        let shopID = Int(id ?? "0")!
        if let s = instituations.first(where: {$0.id == shopID}) {
            lat = Double(s.lat ?? "0.0")!
            lng = Double(s.lng ?? "0.0")!
            address = s.address ?? ""
            
            //TODO:- Goolge map Navigaion
            let destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lng))))
            destination.name = address
            MKMapItem.openMaps(with: [destination], launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
        }
    }


    
    
}
