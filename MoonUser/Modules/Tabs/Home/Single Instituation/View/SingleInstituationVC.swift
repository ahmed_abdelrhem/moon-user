//
//  SingleInstituationVC.swift
//  MoonUser
//
//  Created by Grand iMac on 2/17/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import GoogleMaps


class SingleInstituationVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var segmentBackgroundView: UIView!
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.tag = 0
            collectionView.delegate = self
            collectionView.dataSource = self
            collectionView.register(UINib(nibName: "ServiceCell", bundle: nil), forCellWithReuseIdentifier: "ServiceCell")
        }
    }
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.tag = 0
            tableView.delegate = self
            tableView.dataSource = self
            tableView.separatorStyle = .none
            tableView.register(UINib(nibName: "InstituationCell", bundle: nil), forCellReuseIdentifier: "InstituationCell")
            tableView.register(UINib(nibName: "ClinicCell", bundle: nil), forCellReuseIdentifier: "ClinicCell")
        }
    }
    @IBOutlet weak var myMapView: UIView!
    


    
    //MARK:- Variables
    internal var presenter: SingleInstituationVCPresenter!
    var numberOfSegments : CGFloat = 3
    var mapView: GMSMapView!
    let locationManager = CLLocationManager()
    var updateLocationBefor = false
    var type: DiscoversType = .All

    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        setPresenter()
        locationAuth()
    }
    
    
    //MARK:- Functions
    private func setUI() {
        backButtonTitle("")
        SharedHandller.instance.setupSegment(view: self.segmentBackgroundView, segmentControl: segmentControl, numberOfSegments: numberOfSegments)
        containerView.isHidden = true
        setRightBarButton()
        if type == .Clinic {
            title = HEALTH
        } else if type == .Salon {
            title = BEAUTY
        } else {
            setTitleView(title: "\(DEF.currentAdd)  ")
        }
    }
    
    //TODO:- Set UINavigation Button
    internal func setTitleView(title: String) {
        let button:UIButton  = UIButton(type: .system) as UIButton
        (button as UIView).frame = CGRect(x:0, y:0, width: 50, height:44)
        button.setTitle(title, for: .normal)
        button.setImage(UIImage(named: "arrow-down")!, for: .normal)
        button.titleLabel?.font = UIFont.myMeduimSystemFont(ofSize: 15)
        button.titleLabel?.lineBreakMode = .byTruncatingTail
        button.titleLabel?.numberOfLines = 2
        button.titleLabel?.textAlignment = .center
        button.semanticContentAttribute = .forceRightToLeft
        button.addTarget(self, action: #selector(self.locationButtonClicked(_:)), for: .touchUpInside)
        self.navigationItem.titleView = button
        self.navigationItem.titleView?.tintColor = .black
    }
    
    //TODO:- Set UINavigation Search Button
    private func setRightBarButton() {
        let searchBtn = UIButton(type: UIButton.ButtonType.system)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        searchBtn.setImage(UIImage(named: "search")!, for: .normal)
        searchBtn.addTarget(self, action: #selector(searchClicked(_:)), for: .touchUpInside)
        let searchBarBtItem = UIBarButtonItem(customView: searchBtn)
        self.navigationItem.rightBarButtonItem = searchBarBtItem
    }
    
    @objc func locationButtonClicked(_ sender: UIButton) {
        presenter.locationButtonClicked()
    }
    
    @objc func searchClicked(_ sender: UIButton) {
        presenter.searchBtnClicked()
    }

    
    private func setPresenter() {
        presenter = SingleInstituationVCPresenter(view: self, type: self.type)
        presenter.viewDidLoad(lat: DEF.currentLat, lng: DEF.currentLng)
    }
 
    //MARK:- Actions
    @IBAction func segmentValueChanged(_ sender: UISegmentedControl) {
        presenter.segmentValueChanged(sender.selectedSegmentIndex)
    }
    
    @IBAction func filterBtnClicked(_ sender: UIButton) {
        presenter.filterBtnClicked()
    }

    @IBAction func tagsBtnClicked(_ sender: UIButton) {
        presenter.tagsBtnClicked()
    }

    @IBAction func discoverBtnClicked(_ sender: UIButton) {
        presenter.discoverBtnClicked()
    }

    
}


//MARK:- Protcol LocationDelegate
extension SingleInstituationVC: LocationDelegate {
    
    func RetriveLocation(lat: Double, lng: Double, add: String) {
        self.setTitleView(title: "\(add)  ")
        presenter.userLocationChanged(lat: lat, lng: lng)
    }
}

//MARK:- Protcol FilterVCDelegate
extension SingleInstituationVC: FilterVCDelegate {
    func retriveID(_ id: Int) {
        presenter.filterRetriveID(id)
    }
    
    
}


//MARK:- Protcol TagsVCDelegate
extension SingleInstituationVC: TagsVCDelegate {
    
    func retriveIDs(_ ids: [Int]) {
        presenter.tagsRetriveIDs(ids)
    }
    
}

