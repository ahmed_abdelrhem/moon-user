//
//  SingleInstituationVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 2/17/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation


extension SingleInstituationVC: SingleInstituationVCView {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        collectionView.reloadData()
        tableView.reloadData()
        containerView.isHidden = false
     }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func onEmpty(_ img: UIImage, _ msg: String) {
        tableView.setEmptyView(title: msg, messageImage: img)
    }
    
    func navigateToMapVC() {
        let vc = MapVC()
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func showTableView() {
        tableView.restore()
        tableView.isHidden = false
        myMapView.isHidden = true
    }
    
    func showMap(instituations: [Shop]) {
        for inst in instituations {
            drawMarker(inst)
        }
        myMapView.isHidden = false
        tableView.isHidden = true
    }
    
    func presentFilterVC() {
        let vc = FilterVC()
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func presentTagsVC(_ serviceID: Int) {
        let vc = TagsVC()
        vc.serviceID = serviceID
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
        
    func navigateToDiscoverVC(_ serviceID: Int, lat: String, lng: String) {
        let storyboard = UIStoryboard(name: Storyboard.Tabs.name, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: Identifiers.Discovers.id) as! DiscoversVC
        vc.discover = .Service
        vc.serviceID = serviceID
        vc.lat = lat
        vc.lng = lng
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToSearchShopsVC(_ serviceID: Int) {
        let vc = SearchShopsVC()
        vc.serviceID = serviceID
        if self.type == .Clinic {
            vc.type = .Clinic
        } else if  self.type == .Salon {
            vc.type = .Salon
        } else {
            vc.type = .Instituation
        }
        navigationController?.pushViewController(vc, animated: true)
    }

    func navigateToInstituationPage(_ id: Int) {
        let vc = InstituationPageVC()
        vc.id = id
        navigationController?.pushViewController(vc, animated: true)
    }

    func navigateToClinicDetailsVC(_ id: Int) {
        let vc = ClinicDetailsVC()
        vc.id = id
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToSalonProfileVC(_ id: Int) {
        let vc = SalonProfileVC()
        vc.id = id
        navigationController?.pushViewController(vc, animated: true)
    }


    
}
