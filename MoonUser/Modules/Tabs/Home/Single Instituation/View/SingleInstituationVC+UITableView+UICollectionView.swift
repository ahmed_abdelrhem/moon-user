//
//  SingleInstituationVC+UITableView+UICollectionView.swift
//  MoonUser
//
//  Created by Grand iMac on 2/17/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension SingleInstituationVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.shopCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if type == .Clinic || type == .Salon {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ClinicCell") as? ClinicCell else {
                return UITableViewCell()
            }
            presenter.clinicConfigure(cell: cell, index: indexPath.row)
            cell.followBtnActions = {
                self.presenter.followClinicBtnClicked(at: indexPath.row)
            }
            cell.detailsBtnActions = {
                self.presenter.detailsBtnClicked(at: indexPath.row)
            }
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "InstituationCell") as? InstituationCell else {
                return UITableViewCell()
            }
            presenter.instituationConfigure(cell: cell, index: indexPath.row)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRowAt(index: indexPath.row)
    }
    
}


extension SingleInstituationVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.serviceCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ServiceCell", for: indexPath) as? ServiceCell else {
            return UICollectionViewCell()
        }
        presenter.serviceConfigure(cell: cell, index: indexPath.row)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 120)
    }
    

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        segmentControl.selectedSegmentIndex = 0
        presenter.didSelectedServiceAt(index: indexPath.row)
    }

    
}
