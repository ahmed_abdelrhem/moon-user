//
//  SingleInstituationVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 2/17/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class SingleInstituationVCAPI {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias model = (CategoryModel)->()
    
    func getInstituationsData(type: DiscoversType?, lat: String, lng: String, tab: Int, service_id: Int, filter: Int, tags: [Int], didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        var param = [
            "type": DEF.type,
            "country_id": DEF.country.id,
            "lat": "30.1341689",
            "lng": "31.3655877",
            "tab": tab
        ] as [String : Any]
        
        if service_id != 0 {
            param["service_id"] = service_id
        }
        if filter != 0 {
            param["filter"] = filter
        }
        for x in 0..<tags.count {
            param["tags[\(x)]"] = tags[x]
        }
        
        var url = ""
        if type == .Clinic || type == .Salon {
            url = URLs.clinics.url.concatURL(param)
        } else {
            url = URLs.institutionsList.url.concatURL(param)
        }
        
        apiManager.contectToApiWith(url: url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(CategoryModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
}
