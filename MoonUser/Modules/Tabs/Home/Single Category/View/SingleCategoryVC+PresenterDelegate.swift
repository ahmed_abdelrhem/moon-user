//
//  SingleCategoryVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 1/27/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation


extension SingleCategoryVC: SingleCategoryVCView {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        collectionView.reloadData()
        tableView.reloadData()
        containerView.isHidden = false
     }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func onEmpty(_ img: UIImage, _ msg: String) {
        tableView.setEmptyView(title: msg, messageImage: img)
    }
    
    func navigateToMapVC() {
        let vc = MapVC()
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func showTableView() {
        tableView.restore()
        tableView.isHidden = false
        myMapView.isHidden = true
    }
    
    func showMap(shops: [Shop]) {
        for s in shops {
            drawMarker(s)
        }
        myMapView.isHidden = false
        tableView.isHidden = true
    }
    
    func presentFilterVC() {
        let vc = FilterVC()
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func presentTagsVC(_ serviceID: Int) {
        let vc = TagsVC()
        vc.serviceID = serviceID
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
        
    func navigateToDiscoverVC(_ serviceID: Int, lat: String, lng: String) {
        let storyboard = UIStoryboard(name: Storyboard.Tabs.name, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: Identifiers.Discovers.id) as! DiscoversVC
        vc.discover = .Service
        vc.serviceID = serviceID
        vc.lat = lat
        vc.lng = lng
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToSearchShopsVC(_ serviceID: Int) {
        let vc = SearchShopsVC()
        vc.serviceID = serviceID
        navigationController?.pushViewController(vc, animated: true)
    }

    func navigateToShopPage(_ shopID: Int) {
        let vc = ShopPageVC()
        vc.shopID = shopID
        navigationController?.pushViewController(vc, animated: true)
    }


    
}
