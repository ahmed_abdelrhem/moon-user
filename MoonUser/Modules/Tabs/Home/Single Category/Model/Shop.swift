/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation

struct Shop : Codable {
	let id : Int?
	let name : String?
	let image : String?
	let description : String?
    let phone : String?
	let avg : String?
	let work_days : Int?
	let offer : Int?
    let type : Int?
	let voucher : Int?
	let rate : String?
	let tags : String?
    let lat : String?
    let lng : String?
    let address : String?
    let price : String?
    let seller : Seller?
    var isFollow : Bool?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case image = "image"
		case description = "description"
        case phone = "phone"
		case avg = "avg"
		case work_days = "work_days"
		case offer = "offer"
        case type = "type"
		case voucher = "voucher"
		case rate = "rate"
		case tags = "tags"
        case lat = "lat"
        case lng = "lng"
        case address = "address"
        case price = "price"
        case seller = "seller"
        case isFollow = "isFollow"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		image = try values.decodeIfPresent(String.self, forKey: .image)
		description = try values.decodeIfPresent(String.self, forKey: .description)
        phone = try values.decodeIfPresent(String.self, forKey: .phone)
		avg = try values.decodeIfPresent(String.self, forKey: .avg)
		work_days = try values.decodeIfPresent(Int.self, forKey: .work_days)
		offer = try values.decodeIfPresent(Int.self, forKey: .offer)
        type = try values.decodeIfPresent(Int.self, forKey: .type)
		voucher = try values.decodeIfPresent(Int.self, forKey: .voucher)
		rate = try values.decodeIfPresent(String.self, forKey: .rate)
		tags = try values.decodeIfPresent(String.self, forKey: .tags)
        lat = try values.decodeIfPresent(String.self, forKey: .lat)
        lng = try values.decodeIfPresent(String.self, forKey: .lng)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        price = try values.decodeIfPresent(String.self, forKey: .price)
        seller = try values.decodeIfPresent(Seller.self, forKey: .seller)
        isFollow = try values.decodeIfPresent(Bool.self, forKey: .isFollow)
	}

}
