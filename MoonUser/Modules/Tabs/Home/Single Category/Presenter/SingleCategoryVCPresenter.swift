//
//  SingleCategoryVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 1/27/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import MapKit

protocol SingleCategoryVCView: LoaderDelegate {
    func navigateToMapVC()
    func showTableView()
    func showMap(shops: [Shop])
    func presentFilterVC()
    func presentTagsVC(_ serviceID: Int)
    func navigateToDiscoverVC(_ serviceID: Int, lat: String, lng: String)
    func navigateToSearchShopsVC(_ serviceID: Int)
    func navigateToShopPage(_ shopID: Int)
}

protocol ServiceCellView {
    func configure(image: String?, name: String?)
    func setIsSelected(_ isSelected: Bool)
    
}

protocol ShopCellView {
    func configure(image: String?, name: String?, tags: String?, rate: String?, avg: String?)
    func setIsInstitution(address: String?)
    func setIsCommercial(seller: String?, price: String?)
    func setIsFamous()
    func setIsClinic(address: String?)
    func setIsOffer()
    func setIsVoucher()
    func setIsClosed()
}


class SingleCategoryVCPresenter {
    
    private weak var view: SingleCategoryVCView?
    private let interactor = SingleCategoryVCAPI()
    private var services = [CategoryService]()
    private var shops = [Shop]()
    private var selectedServiceID = 0
    private var lat: String = "0"
    private var lng: String = "0"
    private var tab: Int = 1        //Note: 1: Newest,  2: Offers, 3: Voucher, 4: Map
    private var filter: Int = 0     //Note: 1: Rating, 2: Newest firest, 3: A to Z, 4: Min order amount
    private var tags: [Int] = []

    
    init(view: SingleCategoryVCView) {
        self.view = view
    }
    
    var serviceCount: Int {
        return services.count
    }
    
    var shopCount: Int {
        return shops.count
    }
    
    
    func viewDidLoad(lat: String, lng: String) {
        self.lat = lat
        self.lng = lng
        self.getData()
    }
    
    
    private func getData() {

        view?.showProgress()

        interactor.getCategoryData(lat: self.lat, lng: self.lng, tab: self.tab, service_id: self.selectedServiceID, filter: self.filter, tags: self.tags, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.services = response.data?.services ?? []
                self?.shops = response.data?.shops ?? []
                if self?.selectedServiceID == 0 {
                    self?.selectedServiceID = self?.services.first?.id ?? 0
                }
                if self?.tab == 4 {
                    self?.view?.showMap(shops: self?.shops ?? [])
                } else {
                    self?.view?.showTableView()
                }
                if self?.shops.count == 0 {
                    self?.view?.onEmpty(.no_data_image, NO_DATA_FOUND)
                }
                self?.view?.onSuccess("")
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) {  [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
    
    
    func serviceConfigure(cell: ServiceCellView, index: Int) {
        let service = services[index]
        cell.configure(image: service.image, name: service.name)
        cell.setIsSelected(self.selectedServiceID == services[index].id)
    }
    
    
    func shopConfigure(cell: ShopCellView, index: Int) {
        let shop = shops[index]
        cell.configure(image: shop.image, name: shop.name, tags: shop.tags, rate: shop.rate, avg: shop.avg)
        if shop.offer == 1 {
            cell.setIsOffer()
        }
        if shop.voucher == 1 {
            cell.setIsVoucher()
        }
        if shop.work_days == 0 {
            cell.setIsClosed()
        }
    }
    
    
    func locationButtonClicked() {
        view?.navigateToMapVC()
    }
    
    
    func didSelectedServiceAt(index: Int) {
        let service = services[index]
        //Clear Data
        self.tab = 1
        self.filter = 0
        self.tags = []
        self.selectedServiceID = service.id ?? 0
        //Call API
        self.getData()
    }

    
    func segmentValueChanged(_ index: Int) {
        self.tab = index + 1
        self.getData()
    }
    
    func userLocationChanged(lat: Double, lng: Double) {
        self.lat = "\(lat)"
        self.lng = "\(lng)"
        self.getData()
    }
    
    
    func filterBtnClicked() {
        view?.presentFilterVC()
    }
    
    func filterRetriveID(_ id: Int) {
        self.filter = id
        self.getData()
    }
    
    func tagsBtnClicked() {
        view?.presentTagsVC(self.selectedServiceID)
    }
    
    func tagsRetriveIDs(_ ids: [Int]) {
        self.tags = ids
        self.getData()
    }
    
    func discoverBtnClicked() {
        view?.navigateToDiscoverVC(self.selectedServiceID, lat: self.lat, lng: self.lng)
    }
    
    func searchBtnClicked() {
        view?.navigateToSearchShopsVC(self.selectedServiceID)
    }
    
    func didSelectRowAt(index: Int) {
        let shop = shops[index]
        if shop.work_days != 0 {
            let id = shop.id ?? 0
            view?.navigateToShopPage(id)
        }
    }
    
    func markerDidTap(_ id: String?) {
        var lat: Double = 0.0
        var lng: Double = 0.0
        var address: String = ""
        let shopID = Int(id ?? "0")!
        if let s = shops.first(where: {$0.id == shopID}) {
            lat = Double(s.lat ?? "0.0")!
            lng = Double(s.lng ?? "0.0")!
            address = s.address ?? ""
            
            //TODO:- Goolge map Navigaion
            let destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lng))))
            destination.name = address
            MKMapItem.openMaps(with: [destination], launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
        }        
    }
    

    
    
}
