//
//  ProductDetailsVC.swift
//  MoonUser
//
//  Created by Grand iMac on 2/4/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import ImageSlideshow

class ProductDetailsVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var sliderView: ImageSlideshow!
    @IBOutlet weak var colorsView: UIView!
    @IBOutlet weak var sizesView: UIView!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView(frame: .zero)
            tableView.delegate = self
            tableView.dataSource = self
            tableView.separatorStyle = .none
            tableView.allowsMultipleSelection = true
            tableView.register(UINib(nibName: "SizeCell", bundle: nil), forCellReuseIdentifier: "SizeCell")
            tableView.register(UINib(nibName: "AdditionalCell", bundle: nil), forCellReuseIdentifier: "AdditionalCell")
            self.observer = tableView.observe(\.contentSize) { (collectioView, _) in
                self.tableViewHeight.constant = self.tableView.contentSize.height
            }

        }
    }
    @IBOutlet weak var colorsCollectionView: UICollectionView! {
        didSet {
            colorsCollectionView.tag = 0
            colorsCollectionView.delegate = self
            colorsCollectionView.dataSource = self
            colorsCollectionView.register(UINib(nibName: "ColorCell", bundle: nil), forCellWithReuseIdentifier: "ColorCell")
            self.colorsObserver = colorsCollectionView.observe(\.contentSize) { (collectioView, _) in
                self.colorsWidth.constant = self.colorsCollectionView.contentSize.width
            }
        }
    }
    @IBOutlet weak var sizesCollectionView: UICollectionView! {
        didSet {
            sizesCollectionView.tag = 1
            sizesCollectionView.delegate = self
            sizesCollectionView.dataSource = self
            sizesCollectionView.register(UINib(nibName: "SizesCell", bundle: nil), forCellWithReuseIdentifier: "SizesCell")
            self.sizesOobserver = sizesCollectionView.observe(\.contentSize) { (collectioView, _) in
                self.sizesWidth.constant = self.sizesCollectionView.contentSize.width
            }

        }
    }
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productDescLbl: UILabel!
    @IBOutlet weak var productQuantitiLbl: UILabel!
    @IBOutlet weak var productPriceLbl: UILabel!
    @IBOutlet weak var descriptionTxt: UITextView!
    @IBOutlet weak var favBtn: UIButton?
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var colorsWidth: NSLayoutConstraint!
    @IBOutlet weak var sizesWidth: NSLayoutConstraint!
    
    
    //MARK:- Variables
    private var observer: NSKeyValueObservation!
    private var colorsObserver: NSKeyValueObservation!
    private var sizesOobserver: NSKeyValueObservation!

    internal var presenter: ProductDetailsVCPresenter!
    var productID: Int!

    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setPresenter()
        setUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.tintColor = .white
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.tintColor = .mainColor
    }
    
    //MARK:- Functions
    private func setUI() {
        setNavigationBarIsTransparent()
        setBarButtons()
        backButtonTitle("")
        containerView.isHidden = true
    }
    
    private func setPresenter() {
        presenter = ProductDetailsVCPresenter(view: self)
        presenter.viewDidload(productID: productID)
    }
    
   //Set UINavigation Search Button
   private func setBarButtons() {
       let shareBtn = UIButton(type: .system)
       shareBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
       shareBtn.setImage(.shareImage, for: .normal)
       shareBtn.addTarget(self, action: #selector(shareClicked(_:)), for: .touchUpInside)
       let shareBarBtItem = UIBarButtonItem(customView: shareBtn)
       self.navigationItem.rightBarButtonItem = shareBarBtItem
   }
   
    
    //MARK:- Actions
    @objc func shareClicked(_ sender: UIButton) {
        presenter.shareClicked(self)
    }
    
    @IBAction func favBtnClicked(_ sender: UIButton) {
        presenter.favBtnClicked()
    }

    @IBAction func increaseQuantityBtnClicked(_ sender: UIButton) {
        let q = productQuantitiLbl.text!
        presenter.increaseQuantityBtnClicked(q: q)
    }
    
    @IBAction func decreaseQuantityBtnClicked(_ sender: UIButton) {
        let q = productQuantitiLbl.text!
        presenter.decreaseQuantityBtnClicked(q: q)
    }

    @IBAction func addToCartQuantityBtnClicked(_ sender: UIButton) {
        let q = productQuantitiLbl.text ?? "1"
        presenter.addToCartQuantityBtnClicked(qty: q, description: descriptionTxt.text)
    }

    

 

}

