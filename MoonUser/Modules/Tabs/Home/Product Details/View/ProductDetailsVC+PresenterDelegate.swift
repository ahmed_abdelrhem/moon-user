//
//  ProductDetailsVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 2/6/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension ProductDetailsVC: ProductDetailsVCView {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func updateUI(_ product: ProductData?) {
        DispatchQueue.main.async {
            SharedHandller.instance.setSlider(self, slideShow: self.sliderView, images: product?.images ?? [])
            self.productNameLbl.text = product?.product_name
            self.productDescLbl.text = product?.desc_name
            self.productPriceLbl.text = "\(product?.price ?? "0") \(DEF.country.currency)"
            self.productQuantitiLbl.text = "\(product?.min_qty ?? 1)"
            self.favBtn?.isSelected = product?.isFavourite ?? false
            self.containerView.isHidden = false
        }
    }

    
    func onSuccess(_ msg: String) {
        tableView.reloadData()
        colorsCollectionView.reloadData()
        sizesCollectionView.reloadData()
     }
    
    func reloadSizes() {
        sizesCollectionView.reloadData()
    }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func warringMSG(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .warning, layout: .statusLine, style: .top)
    }
    
    func DoneMSG(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .success, layout: .messageView, style: .center)
    }
    
    
    func updateQuantity(quantity: Int) {
        productQuantitiLbl.text = "\(quantity)"
    }
    
    func setIsClothes() {
        tableView.isHidden = true
        colorsView.isHidden = false
        sizesView.isHidden = false
    }
    
    func setIsResturant() {
        tableView.isHidden = false
        colorsView.isHidden = true
        sizesView.isHidden = true
    }
    
    func changeFavouriteBtnStatus() {
        favBtn!.isSelected = !favBtn!.isSelected
    }

    
}
