//
//  ProductDetailsVC+UITableView.swift
//  MoonUser
//
//  Created by Grand iMac on 2/5/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension ProductDetailsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let sizeCell = tableView.dequeueReusableCell(withIdentifier: "SizeCell") as? SizeCell else {
            return UITableViewCell()
        }
        guard let additionalCell = tableView.dequeueReusableCell(withIdentifier: "AdditionalCell") as? AdditionalCell else {
            return UITableViewCell()
        }
        if indexPath.section == 0 && presenter.hasSizes {
            presenter.sizeConfigure(cell: sizeCell, indexPath: indexPath)
            return sizeCell
        } else {
            presenter.additionConfigure(cell: additionalCell, indexPath: indexPath)
            return additionalCell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && indexPath.row == 0 {
            tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerFrame = tableView.frame
        
        let label = UILabel()
        label.frame =  CGRect(x: 20, y: 5, width: headerFrame.size.width-40, height: 20)
        label.font = UIFont.myBoldSystemFont(ofSize: 17)
        label.text = presenter.titleForHeader(section)
        label.textColor = .black
        
        let headerView:UIView = UIView(frame: CGRect(x: 0, y: 0,  width: headerFrame.size.width, height: headerFrame.size.height))
        headerView.backgroundColor = .groupTableViewBackground
        headerView.addSubview(label)

        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && presenter.hasSizes {
            if let indexPathsInSection = tableView.indexPathsForSelectedRows?.filter ({ $0.section == indexPath.section && $0.row != indexPath.row }) {
                for selectedPath in indexPathsInSection {
                    tableView.deselectRow(at: selectedPath, animated: true)
                }
            }
        }
        presenter.didSelectRowAt(section: indexPath.section, index: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        presenter.didDeSelectRowAt(section: indexPath.section, index: indexPath.row)
    }
    
    
}



extension ProductDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 0 {
            return presenter.colorsCount
        }
        return presenter.sizesCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 0 {
            guard let colorCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColorCell", for: indexPath) as? ColorCell else {
                return UICollectionViewCell()
            }
            if indexPath.row == 0 {
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
                colorCell.isSelected = true
            }
            presenter.colorConfigure(cell: colorCell, index: indexPath.row)
            return colorCell
        } else {
            guard let sizesCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SizesCell", for: indexPath) as? SizesCell else {
                return UICollectionViewCell()
            }
            if indexPath.row == 0 {
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
                sizesCell.isSelected = true
            }
            presenter.sizesConfigure(cell: sizesCell, index: indexPath.row)
            return sizesCell
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: 40, height: 75)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.didSelectItemAt(tag: collectionView.tag, index: indexPath.row)
    }

    
    
}
