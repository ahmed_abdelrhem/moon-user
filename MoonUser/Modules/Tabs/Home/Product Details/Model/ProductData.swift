/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation

struct ProductData : Codable {
	let product_id : Int?
    var cart_shop_id : Int = 0
    let flag : Int?
    let shop_id : Int?
	let product_name : String?
	let desc_name : String?
	let price : String?
	let product_image : String?
	let min_qty : Int?
    let isFavourite : Bool?
	let casee : Int?
	let sizes : [Adds]?
	let additions : [Additions]?
	let colors : [Colors]?
    let images : [String]?

	enum CodingKeys: String, CodingKey {

		case product_id = "product_id"
        case cart_shop_id = "cart_shop_id"
        case flag = "flag"
        case shop_id = "shop_id"
		case product_name = "product_name"
		case desc_name = "desc_name"
		case price = "price"
		case product_image = "product_image"
		case min_qty = "min_qty"
        case isFavourite = "isFavourite"
		case casee = "case"
		case sizes = "sizes"
		case additions = "additions"
		case colors = "colors"
        case images = "images"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		product_id = try values.decodeIfPresent(Int.self, forKey: .product_id)
        cart_shop_id = try (values.decodeIfPresent(Int.self, forKey: .cart_shop_id) ?? 0)
        flag = try values.decodeIfPresent(Int.self, forKey: .flag)
        shop_id = try values.decodeIfPresent(Int.self, forKey: .shop_id)
		product_name = try values.decodeIfPresent(String.self, forKey: .product_name)
		desc_name = try values.decodeIfPresent(String.self, forKey: .desc_name)
		price = try values.decodeIfPresent(String.self, forKey: .price)
		product_image = try values.decodeIfPresent(String.self, forKey: .product_image)
		min_qty = try values.decodeIfPresent(Int.self, forKey: .min_qty)
        isFavourite = try values.decodeIfPresent(Bool.self, forKey: .isFavourite)
		casee = try values.decodeIfPresent(Int.self, forKey: .casee)
		sizes = try values.decodeIfPresent([Adds].self, forKey: .sizes)
		additions = try values.decodeIfPresent([Additions].self, forKey: .additions)
		colors = try values.decodeIfPresent([Colors].self, forKey: .colors)
        images = try values.decodeIfPresent([String].self, forKey: .images)
	}

}
