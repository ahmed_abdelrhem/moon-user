//
//  ProductDetailsVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 2/6/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol ProductDetailsVCView: LoaderDelegate {
    func setIsClothes()
    func setIsResturant()
    func updateUI(_ product: ProductData?)
    func updateQuantity(quantity: Int)
    func reloadSizes()
    func DoneMSG(_ msg: String)
    func changeFavouriteBtnStatus()
}

protocol SizeCellView {
    func cellConfigure(name: String?, price: String?)
}

protocol AdditionalCellView {
    func cellConfigure(name: String?, price: String?)
}

protocol ColorCellView {
    func cellConfigure(color: String?)
}

protocol SizesCellView {
    func cellConfigure(size: String?)
}




class ProductDetailsVCPresenter {
    
    private weak var view: ProductDetailsVCView?
    private let interactor = ProductDetailsVCAPI()
    private let favouriteInteractor = FavouritesAPI()
    private var productData: ProductData?
    private var sizes = [Adds]()
    private var additions = [Additions]()
    private var colors = [Colors]()
    private (set) var hasSizes: Bool = false
    private var selectColorID: Int = 0
    private var selectedSizeID: Int = 0
    private var selectedAdditions = [Int]()
    
    init(view: ProductDetailsVCView) {
        self.view = view
    }
    
    
    //MARK:- Functions
    func viewDidload(productID: Int) {
        
        view?.showProgress()
        
        interactor.getproductDetails(productID: productID, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.productData = response.data
                self?.sizes = response.data?.sizes ?? []
                self?.additions = response.data?.additions ?? []
                self?.colors = response.data?.colors ?? []
                self?.hasSizes = self?.sizes.count != 0
                self?.view?.onSuccess("")
                self?.view?.updateUI(self?.productData)
                if response.data?.casee == 1 {
                    self?.view?.setIsClothes()
                } else {
                    self?.view?.setIsResturant()
                }
                
                if let sizeID = self?.sizes.first?.id {
                    self?.selectedSizeID = sizeID
                }

                if let colorID = self?.colors.first?.id {
                    self?.selectColorID = colorID
                    if let sizeID = self?.colors.first?.sizes?.first?.id {
                        self?.selectedSizeID = sizeID
                    }
                }
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
    
    func shareClicked(_ vc: UIViewController) {
        share(vc, data: productData?.product_name ?? "") { (_) in }
    }
    
    func favBtnClicked() {
        guard DEF.isLogin else {
            showLoginMessage()
            return
        }
        
        view?.changeFavouriteBtnStatus()
        DispatchQueue.global(qos: .background).async {
            self.favouriteInteractor.addOrRemoveFavourite(productID: self.productData?.product_id ?? 0, type: DEF.type)
        }
    }
    
    func increaseQuantityBtnClicked(q : String) {
        var qty = Int(q)!
        qty += 1
        view?.updateQuantity(quantity: qty)
    }
    
    func decreaseQuantityBtnClicked(q : String) {
        var qty = Int(q)!
        if qty != productData?.min_qty {
            qty -= 1
            view?.updateQuantity(quantity: qty)
        }
    }

    func addToCartQuantityBtnClicked(qty: String, description: String) {

        guard DEF.isLogin else {
            view?.warringMSG(PLEASE_LOGIN)
            return
        }
        
        if productData?.casee == 1 {
            guard selectColorID != 0 else {
                view?.warringMSG(SELECT_COLOR)
                return
            }
            
            guard selectedSizeID != 0 else {
                view?.warringMSG(SELECT_SIZE)
                return
            }
        }
        
        if productData?.casee == 2 && hasSizes {
            guard selectedSizeID != 0 else {
                view?.warringMSG(SELECT_SIZE)
                return
            }
        }
        
        guard (productData?.cart_shop_id == 0) || (productData?.shop_id == productData?.cart_shop_id && productData?.flag == DEF.flag)  else {
            Messages.instance.demoCentered(title: ATTENTION, body: Cart_Message, buttonTitle: CONTINUE) { (success) in
                if success {
                    self.DeleteOldCart { (success) in
                        if success {
                            self.addToCart(qty: qty, desc: description)
                        } else {
                            self.view?.onFailure(CONNECTION_ERROR)
                        }
                    }
                }
            }
            return
        }
        self.addToCart(qty: qty, desc: description)
    }

    
    //MARK:- TableView Functions
    var numberOfSections: Int {
        if hasSizes {
            return additions.count + 1
        } else {
            return additions.count
        }
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        if hasSizes {
            if section == 0 {
                return sizes.count
            } else {
                return additions[section-1].adds?.count ?? 0
            }
        } else {
            return additions[section].adds?.count ?? 0
        }
    }
    
    func titleForHeader(_ section: Int) -> String {
        if hasSizes {
            if section == 0 {
                return PLEASE_CHOOSE
            } else {
                return additions[section-1].name ?? ""
            }
        } else {
            return additions[section].name ?? ""
        }
    }
    
    func sizeConfigure(cell: SizeCellView, indexPath: IndexPath) {
        let item = sizes[indexPath.row]
        cell.cellConfigure(name: item.name, price: item.price)
    }
    
    func additionConfigure(cell: AdditionalCellView, indexPath: IndexPath) {
        if hasSizes {
            let item = additions[indexPath.section-1].adds?[indexPath.row]
            cell.cellConfigure(name: item?.name, price: item?.price)
        } else {
            let item = additions[indexPath.section].adds?[indexPath.row]
            cell.cellConfigure(name: item?.name, price: item?.price)
        }
    }
    
    func didSelectRowAt(section: Int, index: Int) {
        if section == 0 {
            let id = sizes[index].id ?? 0
            selectedSizeID = id
        } else {
            let id = additions[section-1].adds?[index].id ?? 0
            selectedAdditions.append(id)
        }
    }
    
    func didDeSelectRowAt(section: Int, index: Int) {
        if section != 0 {
            let id = additions[section-1].adds?[index].id ?? 0
            if let index = selectedAdditions.firstIndex(where: { $0 == id}) {
                selectedAdditions.remove(at: index)
            }
        }
    }

    
    //MARK:- CollectionView Functions
    var colorsCount: Int {
        return colors.count
    }
    
    var sizesCount: Int {
        if colors.count != 0 {
            if let i = colors.firstIndex(where: {$0.id == selectColorID }) {
                let count = colors[i].sizes?.count ?? 0
                return count
            }
            return 0
        }
        return 0
    }
  
    func colorConfigure(cell: ColorCellView, index: Int) {
        let color = colors[index].name
        cell.cellConfigure(color: color)
    }

    
    func sizesConfigure(cell: SizesCellView, index: Int) {
        if let i = colors.firstIndex(where: {$0.id == selectColorID }) {
            let size = colors[i].sizes?[index].name
            cell.cellConfigure(size: size)
        }
    }
    
    
    func didSelectItemAt(tag: Int, index: Int) {
        if tag == 0 {
            selectColorID = colors[index].id ?? 0
            selectedSizeID = colors.first?.id ?? 0
            view?.reloadSizes()
        } else {
            if let i = colors.firstIndex(where: {$0.id == selectColorID }) {
                selectedSizeID = colors[i].sizes?[index].id ?? 0
            }
        }
    }
    
    
    
    //MARK:- Private Functions
    private func addToCart(qty: String, desc: String) {
        
        view?.showProgress()
        
        interactor.addToCart(productID: productData?.product_id ?? 0, qty: qty, cartItemID: 0, sizeID: selectedSizeID, colorID: selectColorID, description: desc, additions: selectedAdditions, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.view?.DoneMSG(response.message ?? "")
            } else {
                self?.view?.warringMSG(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
    
    private func DeleteOldCart(completion: @escaping CompletionHandler) {
        view?.showProgress()
        
        interactor.DeletaAllCart(didDataReady: {  [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                completion(true)
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            completion(false)
        }
    }
}
