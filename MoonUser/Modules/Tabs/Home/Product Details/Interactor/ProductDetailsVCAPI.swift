//
//  ProductDetailsVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 2/6/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class ProductDetailsVCAPI {

    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias model = (ProductModelResponse)->()
    typealias statusModel = (StatusModel)->()


    func getproductDetails(productID: Int, didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param = [
            "product_id": productID,
            "flag": DEF.flag,
            "type": DEF.type
        ] as [String : Any]
        
        let url = URLs.getDetailsProduct.url.concatURL(param)
        
        apiManager.contectToApiWith(url: url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(ProductModelResponse.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
    func addToCart(productID: Int, qty: String, cartItemID: Int?, sizeID: Int?, colorID: Int?, description: String, additions: [Int], didDataReady: @escaping statusModel, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
           
        var param = [
           "type": DEF.type,
           "flag": DEF.flag,
           "product_id": productID,
           "qty": qty,
           "description": description,
        ] as [String : Any]

        
        if colorID != 0 {
            param["color_id"] = colorID
            param["size_id"] = sizeID
        } else if sizeID != 0 {
            param["size_id"] = sizeID
            if additions.count != 0 {
                let stringArray = additions.map { String($0) }
                param["additions"] = stringArray.joined(separator:",")
            }
        }
        
                  
        apiManager.contectToApiWith(url: URLs.addOrUpdateCart.url,
                                   methodType: .post,
                                   params: param,
                                   success: { (json) in
            
           if let data = try? JSONSerialization.data(withJSONObject: json) {
               do {
                   let result = try self.decoder.decode(StatusModel.self, from: data)
                   didDataReady(result)
               }catch{
                   print("error\(error)")
               }
               
           }
        }) { (error, msg) in
           print(error, msg!)
           errorCompletion(error)
        }
    }
    
    
    func DeletaAllCart(didDataReady: @escaping statusModel, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
           
        let param = [
           "status": 1
        ]
                  
        apiManager.contectToApiWith(url: URLs.deleteCart.url,
                                   methodType: .post,
                                   params: param,
                                   success: { (json) in
            
           if let data = try? JSONSerialization.data(withJSONObject: json) {
               do {
                   let result = try self.decoder.decode(StatusModel.self, from: data)
                   didDataReady(result)
               }catch{
                   print("error\(error)")
               }
               
           }
        }) { (error, msg) in
           print(error, msg!)
           errorCompletion(error)
        }
    }

    

}
