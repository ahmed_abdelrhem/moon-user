//
//  AddAdvertisingVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 2/20/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol AddAdvertisingVCView: LoaderDelegate {
    func updateUI(_ details: SingleProduct?)
    func changeImages(images: [UIImage])
    func presentMap()
    func createPickerView()
    func popUpViewController(_ msg: String)
    func presentEditorViewController(_ img: UIImage)
}

class AddAdvertisingVCPresenter {
    
    private weak var view: AddAdvertisingVCView?
    private let interactor = AddAdvertisingAPI()
    private let detailInteractor = AdDetailsVCAPI()
    private var details: SingleProduct?
    private var images = [UIImage]()
    private var cityID = 0
    private var address = ""
    private var lat: Double?
    private var lng: Double?
    private var tag = -1
    
    
    init(view: AddAdvertisingVCView) {
        self.view = view
    }
    
    func viewDidlLoad(_ id: Int) {
         
        view?.showProgress()
         
        detailInteractor.getProductDetails(id: id, didDataReady: {  [weak self](response) in
             guard self != nil else { return }
             self?.view?.hideProgress()
             if response.status == StatusCode.Success.rawValue {
                 self?.details = response.data
                 self?.address = self?.details?.address ?? ""
                 self?.lat = Double(self?.details?.lat ?? "0.0")!
                 self?.lng = Double(self?.details?.lng ?? "0.0")!
                 self?.view?.updateUI(response.data)
             } else {
                 self?.view?.onFailure(response.message ?? "")
             }
         }) { [weak self](error) in
             guard self != nil else { return }
             self?.view?.hideProgress()
             self?.view?.onFailure(CONNECTION_ERROR)
         }
    }
     
    
    func setCityID(_ newValue: Int) {
        self.cityID = newValue
    }
        
    func imageClicked(_ vc: UIViewController, sender: UIImageView){
        AttachmentHandler.shared.showAttachmentImage(vc, sender)
        
        self.tag = sender.tag

        AttachmentHandler.shared.singleImagePickedBlock = { (image, _) in
            self.view?.presentEditorViewController(image)
        }
    }
    
    func doneEditImage(_ image: UIImage) {
        DispatchQueue.main.async {
            if self.tag < self.images.count {
                self.images[self.tag] = image
            } else {
                self.images.append(image)
            }
            self.view?.changeImages(images: self.images)
        }
    }

    func cityTextFiledEditingBegin() {
        view?.createPickerView()
    }
    
    func locationBtnClicked() {
        view?.presentMap()
    }
    
    func RetriveLocation(lat: Double, lng: Double, address: String) {
        self.address = address
        self.lat = lat
        self.lng = lng
    }
    
    func submitBtnClicked(id: Int?, serviceID: Int, catID: Int, subCatID: Int, title: String?, price: String?, phone: String?, email: String?, desc: String) {
        
        guard title != "", price != "", phone != "", email != "", desc != "", self.address != ""  else {
            view?.warringMSG(FILL_ALL_DATA)
            return
        }
        
        if serviceID != 0 && cityID != 0 && images.count == 0 { //Check when Add New Advertise
            view?.warringMSG(FILL_ALL_DATA)
            return
        }
        
        view?.showProgress()
        
        if serviceID != 0 {
            interactor.addAdvertise(name: title!, price: price!, email: email!, phone: phone!, desc: desc, lat: self.lat!, lng: self.lng!, address: address, serviceID: serviceID, catID: catID, subCatID: subCatID, cityID: self.cityID, images: self.images, didDataReady: { [weak self](response) in
                guard self != nil else { return }
                self?.view?.hideProgress()
                if response.status == StatusCode.Success.rawValue {
                    self?.view?.onSuccess(response.message ?? "")
                } else {
                    self?.view?.onFailure(response.message ?? "")
                }
            }) { [weak self](error) in
                guard self != nil else { return }
                self?.view?.hideProgress()
                self?.view?.onFailure(CONNECTION_ERROR)
            }
        } else {
            interactor.editAdvertise(id: id!, name: title!, price: price!, email: email!, phone: phone!, desc: desc, lat: self.lat!, lng: self.lng!, address: address, images: self.images, didDataReady: { [weak self](response) in
                guard self != nil else { return }
                self?.view?.hideProgress()
                if response.status == StatusCode.Success.rawValue {
                    DispatchQueue.main.async {
                        _NC.post(name: .updateAd, object: nil)
                    }
                    self?.view?.popUpViewController(response.message ?? "")
                } else {
                    self?.view?.onFailure(response.message ?? "")
            }
            }) { [weak self](error) in
                guard self != nil else { return }
                self?.view?.hideProgress()
                self?.view?.onFailure(CONNECTION_ERROR)
            }
        }
    }
    
//    
//    private func editorCompletionBlock(result: IMGLYEditorResult, image: UIImage?) {
//        DispatchQueue.main.async {
//            if self.tag < self.images.count {
//                self.images[self.tag] = image!
//            } else {
//                self.images.append(image!)
//            }
//            self.view?.changeImages(images: self.images)
//        }
//    }


    
}

