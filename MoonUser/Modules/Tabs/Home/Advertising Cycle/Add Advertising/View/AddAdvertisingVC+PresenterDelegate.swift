//
//  AddAdvertisingVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 2/20/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import QCropper

extension AddAdvertisingVC: AddAdvertisingVCView {
    
    func showProgress() {
        startAnimating()
    }
    
    func hideProgress() {
        stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: AdvertisingPageVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        Messages.instance.showConfigMessage(title: "", body: msg, state: .success, layout: .cardView, style: .center)
    }
        
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func warringMSG(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .warning, layout: .statusLine, style: .top)
    }

    func updateUI(_ details: SingleProduct?) {
        DispatchQueue.main.async {
            self.titleTxt.text = details?.name
            self.priceTxt.text = details?.price
            self.phoneTxt.text = details?.phone
            self.emailTxt.text = details?.email
            self.locationBtn.setTitle(details?.address, for: .normal)
            self.descriptionTxt.text = details?.description ?? ""
            self.textViewHeight.constant = self.descriptionTxt.contentSize.height
            self.cityTxt.isHidden = true
        }
    }
    
    func changeImages(images: [UIImage]) {
        if images.count > 0 {
            image1View.image = images[0]
        }
        if images.count > 1 {
            image2View.image = images[1]
        }
        if images.count > 2 {
            image3View.image = images[2]
        }
        if images.count > 3 {
            image4View.image = images[3]
        }
        if images.count > 4 {
            image5View.image = images[4]
        }
        if images.count > 5 {
            image6View.image = images[5]
        }
    }

    
    func popUpViewController(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .success, layout: .cardView, style: .center)
        self.navigationController?.popViewController(animated: true)
    }
        
    func presentMap() {
        let vc = MapVC()
        vc.modalPresentationStyle = .overFullScreen
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func createPickerView() {
        cityTxt.isEnabled = true
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: DONE, style: .done, target: self, action: #selector(self.doneCityPressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: CANCEL, style: .plain, target: self, action: #selector(self.cancelCityClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        self.dataPickerView.reloadAllComponents()
        
        cityTxt.inputAccessoryView = toolBar
        cityTxt.inputView = dataPickerView
    }

    @objc func doneCityPressed() {
        DispatchQueue.main.async {
            self.cityTxt.endEditing(true)
            let index = self.dataPickerView.selectedRow(inComponent: 0)
            self.cityTxt.text = DEF.country.cities[index].city_name ?? ""
            let id = DEF.country.cities[index].id ?? 0
            self.presenter.setCityID(id)
        }
    }
    
    @objc func cancelCityClick() {
        cityTxt.resignFirstResponder()
    }
    
    

}


//MARK:- QCroper Functions
extension AddAdvertisingVC: CropperViewControllerDelegate {
    
    func presentEditorViewController(_ img: UIImage) {
        let cropper = CropperViewController(originalImage: img)
        cropper.delegate = self
        self.present(cropper, animated: true, completion: nil)
    }
    
    func cropperDidConfirm(_ cropper: CropperViewController, state: CropperState?) {
        cropper.dismiss(animated: true, completion: nil)
        
        if let state = state,
            let image = cropper.originalImage.cropped(withCropperState: state) {
            presenter.doneEditImage(image)
        }
    }
    

    

}
