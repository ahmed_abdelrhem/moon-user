//
//  AddAdvertisingVC.swift
//  MoonUser
//
//  Created by Grand iMac on 2/20/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class AddAdvertisingVC: UIViewController {

    
    //MARK:- Outlets
    @IBOutlet weak var image1View: UIImageView!
    @IBOutlet weak var image2View: UIImageView!
    @IBOutlet weak var image3View: UIImageView!
    @IBOutlet weak var image4View: UIImageView!
    @IBOutlet weak var image5View: UIImageView!
    @IBOutlet weak var image6View: UIImageView!
    @IBOutlet weak var titleTxt: UITextField!
    @IBOutlet weak var priceTxt: UITextField!
    @IBOutlet weak var phoneTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var cityTxt: UITextField!
    @IBOutlet weak var locationBtn: UIButton!
    @IBOutlet weak var descriptionTxt: UITextView!
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    
    
    //MARK:- Variables
    internal var presenter: AddAdvertisingVCPresenter!
    var id: Int?
    var serviceID: Int?
    var catID: Int?
    var subCatID: Int?
    
    let dataPickerView : UIPickerView = {
        let pick = UIPickerView()
        pick.backgroundColor = UIColor.clear
        pick.tag = 0
        return pick
    }()

    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        backButtonTitle("")
        setPresenter()
        setDelegates()
        addGestures()
        title = id != nil ? EDIT_ADVERTISING : ADD_ADVERTISING
    }
    
    
    //MARK:- Functions
    private func addGestures() {
        let image1Gesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.image1Clicked))
        image1View.tag = 0
        image1View.isUserInteractionEnabled = true
        image1View.addGestureRecognizer(image1Gesture)
        
        let image2Gesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.image2Clicked))
        image2View.tag = 1
        image2View.isUserInteractionEnabled = true
        image2View.addGestureRecognizer(image2Gesture)

        let image3Gesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.image3Clicked))
        image3View.tag = 2
        image3View.isUserInteractionEnabled = true
        image3View.addGestureRecognizer(image3Gesture)

        let image4Gesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.image4Clicked))
        image4View.tag = 3
        image4View.isUserInteractionEnabled = true
        image4View.addGestureRecognizer(image4Gesture)

        let image5Gesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.image5Clicked))
        image5View.tag = 4
        image5View.isUserInteractionEnabled = true
        image5View.addGestureRecognizer(image5Gesture)

        let image6Gesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.image6Clicked))
        image6View.tag = 5
        image6View.isUserInteractionEnabled = true
        image6View.addGestureRecognizer(image6Gesture)
    }
    
    private func setPresenter() {
        presenter = AddAdvertisingVCPresenter(view: self)
        if id != nil {
            self.cityTxt.isHidden = true
            presenter.viewDidlLoad(id!)
        }
    }
    
    
    //MARK:- Actions
    @objc func image1Clicked() {
        presenter.imageClicked(self, sender: image1View)
    }
    
    @objc func image2Clicked() {
        presenter.imageClicked(self, sender: image2View)
    }
    
    @objc func image3Clicked() {
        presenter.imageClicked(self, sender: image3View)
    }
    
    @objc func image4Clicked() {
        presenter.imageClicked(self, sender: image4View)
    }
    
    @objc func image5Clicked() {
        presenter.imageClicked(self, sender: image5View)
    }

    @objc func image6Clicked() {
        presenter.imageClicked(self, sender: image6View)
    }

    
    @IBAction func locationBtnClicked(_ sender: UIButton) {
        presenter.locationBtnClicked()
    }
    
    @IBAction func submitBtnClicked(_ sender: UIButton) {
        presenter.submitBtnClicked(id: self.id, serviceID: self.serviceID ?? 0, catID: self.catID ?? 0, subCatID: self.subCatID ?? 0, title: titleTxt.text, price: priceTxt.text, phone: phoneTxt.text, email: emailTxt.text, desc: descriptionTxt.text)
    }
    
}


//MARK:- UITextViewDelegate - UITextFieldDelegate
extension AddAdvertisingVC: UITextViewDelegate, UITextFieldDelegate {
    
    func setDelegates() {
        dataPickerView.delegate = self
        dataPickerView.dataSource = self
        
        titleTxt.delegate = self
        priceTxt.delegate = self
        phoneTxt.delegate = self
        emailTxt.delegate = self
        
        cityTxt.tag = 4
        cityTxt.delegate = self
        descriptionTxt.delegate = self
    }
    

    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.layer.borderColor = UIColor.clear.cgColor
        if textField.tag == 4 { //City UITextFiled
            presenter.cityTextFiledEditingBegin()
        }
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
      textViewHeight.constant = self.descriptionTxt.contentSize.height
    }

}



extension AddAdvertisingVC: LocationDelegate {

    func RetriveLocation(lat: Double, lng: Double, add: String) {
        presenter.RetriveLocation(lat: lat, lng: lng, address: add)
        locationBtn.setTitle(add , for: .normal)
    }
}
