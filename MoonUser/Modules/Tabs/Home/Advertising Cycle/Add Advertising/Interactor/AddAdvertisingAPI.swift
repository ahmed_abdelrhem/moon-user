//
//  AddAdvertisingAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 2/20/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import Alamofire
import MOLH

class AddAdvertisingAPI {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    
    typealias statusModel = (StatusModel)->()

    func addAdvertise(name: String, price: String, email: String, phone: String, desc: String, lat: Double, lng: Double, address: String, serviceID: Int,  catID: Int, subCatID: Int, cityID: Int, images: [UIImage], didDataReady: @escaping statusModel, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
           
        let param = [
            "type": "11",
            "name": name,
            "price": price,
            "email": email,
            "phone": phone,
            "description": desc,
            "lat": "\(lat)",
            "lng": "\(lng)",
            "address": address,
            "service_id": "\(serviceID)",
            "category_id": "\(catID)",
            "subCategory_id": "\(subCatID)",
            "country_id":"\( DEF.country.id)",
            "city_id": "\(cityID)"
        ]
           
       let HEADER = ["lang": MOLHLanguage.currentAppleLanguage(),
                     "Jwt": DEF.userData.jwt_token]

        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for x in 0..<images.count {
                multipartFormData.append(images[x].jpegData(compressionQuality: 0.5)!, withName: "product_images[\(x)]", fileName: "product_images\(Date.timeIntervalSinceReferenceDate).jpg", mimeType: "image/jpg")
            }

           for (key, value) in param {
               multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
           }
            
       }, to: URLs.addAdvertise.url, method: .post, headers: HEADER) { (result) in
           switch result {
           case .success(let upload, _, _):
               upload.uploadProgress(closure: { progress in
                   print("progress: \(progress.fractionCompleted)")
                   if progress.fractionCompleted == 1.0 {
                       print("upload complete")
                   }
               })
               upload.responseJSON { response in
                   print(response)
                   switch response.result {

                   case .success(_):
                       guard let data = response.data else { return }
                       do {
                           let json = try JSONDecoder().decode(StatusModel.self, from: data)
                           didDataReady(json)
                       } catch {
                           print("error : ", error)
                       }
                   case .failure(let error):
                       print("1:::\(error)")
                       errorCompletion(error as? ErrorCode)
                   }
               }

           case .failure(let error):
               print("2:::\(error)")
               errorCompletion(error as? ErrorCode)
           }
       }
    }
    
    
    
    func editAdvertise(id: Int, name: String, price: String, email: String, phone: String, desc: String, lat: Double, lng: Double, address: String, images: [UIImage], didDataReady: @escaping statusModel, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
           
        let param = [
            "id": "\(id)",
            "name": name,
            "price": price,
            "email": email,
            "phone": phone,
            "description": desc,
            "lat": "\(lat)",
            "lng": "\(lng)",
            "address": address,
            "type": "\(11)"
        ]
           
       let HEADER = ["lang": MOLHLanguage.currentAppleLanguage(),
                     "Jwt": DEF.userData.jwt_token]

        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for x in 0..<images.count {
                multipartFormData.append(images[x].jpegData(compressionQuality: 0.5)!, withName: "product_images[\(x)]", fileName: "product_images\(Date.timeIntervalSinceReferenceDate).jpg", mimeType: "image/jpg")
            }

           for (key, value) in param {
               multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
           }
            
       }, to: URLs.editAdvertise.url, method: .post, headers: HEADER) { (result) in
           switch result {
           case .success(let upload, _, _):
               upload.uploadProgress(closure: { progress in
                   print("progress: \(progress.fractionCompleted)")
                   if progress.fractionCompleted == 1.0 {
                       print("upload complete")
                   }
               })
               upload.responseJSON { response in
                   print(response)
                   switch response.result {

                   case .success(_):
                       guard let data = response.data else { return }
                       do {
                           let json = try JSONDecoder().decode(StatusModel.self, from: data)
                           didDataReady(json)
                       } catch {
                           print("error : ", error)
                       }
                   case .failure(let error):
                       print("1:::\(error)")
                       errorCompletion(error as? ErrorCode)
                   }
               }

           case .failure(let error):
               print("2:::\(error)")
               errorCompletion(error as? ErrorCode)
           }
       }
    }


    
    
}
