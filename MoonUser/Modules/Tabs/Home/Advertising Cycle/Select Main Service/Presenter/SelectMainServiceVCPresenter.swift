//
//  SelectMainServiceVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 2/20/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol SelectMainServiceVCView: class {
    func presentFilterVC(_ serviceID: Int)
    func navigateToAddAdvertisingVC(serviceID: Int, catID: Int,  subCatID: Int)
}


class SelectMainServiceVCPresenter {
    
    private weak var view: SelectMainServiceVCView?
    private var services = [CategoryService]()
    private var serviceID = 0
    private var categoryID: Int = 0
    private var subCategoryID: Int = 0

    
    init(view: SelectMainServiceVCView) {
        self.view = view
    }
    
    var serviceCount: Int {
        return services.count
    }
        
    func viewDidLoad(_ services: [CategoryService]) {
        self.services = services
    }
    
    
    
    
    func serviceConfigure(cell: ServiceCellView, index: Int) {
        let service = services[index]
        cell.configure(image: service.image, name: service.name)
        cell.setIsSelected(false)
    }
    
    
    func didSelectedServiceAt(index: Int) {
        let service = services[index]
        self.serviceID = service.id ?? 0
        view?.presentFilterVC(self.serviceID)
    }

    
    func filterDataRetrived(catID: Int, subCat: Int) {
        self.categoryID = catID
        self.subCategoryID = subCat
        view?.navigateToAddAdvertisingVC(serviceID: serviceID, catID: catID, subCatID: subCat)
    }
    
    

    
    
}
