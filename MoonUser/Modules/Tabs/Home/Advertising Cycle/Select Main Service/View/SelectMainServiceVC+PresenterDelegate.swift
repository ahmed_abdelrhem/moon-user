//
//  SelectMainServiceVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 2/20/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation


extension SelectMainServiceVC: SelectMainServiceVCView {
    
    func presentFilterVC(_ serviceID: Int) {
        let vc = CategoryFilterVC()
        vc.serviceID = serviceID
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }

    func navigateToAddAdvertisingVC(serviceID: Int, catID: Int,  subCatID: Int) {
        let vc = AddAdvertisingVC()
        vc.serviceID = serviceID
        vc.catID = catID
        vc.subCatID = subCatID
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
