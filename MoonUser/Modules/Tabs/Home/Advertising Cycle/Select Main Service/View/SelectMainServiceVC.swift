//
//  SelectMainServiceVC.swift
//  MoonUser
//
//  Created by Grand iMac on 2/20/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class SelectMainServiceVC: UIViewController {

      //MARK:- Outlets
    @IBOutlet weak var sectionsCollectionView: UICollectionView! {
        didSet {
            sectionsCollectionView.delegate = self
            sectionsCollectionView.dataSource = self
            sectionsCollectionView.register(UINib(nibName: "ServiceCell", bundle: nil), forCellWithReuseIdentifier: "ServiceCell")
        }
    }
    
    
    //MARK:- Variables
    internal var presenter: SelectMainServiceVCPresenter!
    var services = [CategoryService]()
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        backButtonTitle("")
        setPresenter()
        title = ADD_ADVERTISING
    }
    


    //MARK:- Functions
    private func setPresenter() {
        presenter = SelectMainServiceVCPresenter(view: self)
        presenter.viewDidLoad(services)
    }
    


}



//MARK:- Protcol CategoryFilterDelegate
extension SelectMainServiceVC: CategoryFilterDelegate {
    
    func retriveDate(catID: Int, subCat: Int) {
        presenter.filterDataRetrived(catID: catID, subCat: subCat)
    }
}


