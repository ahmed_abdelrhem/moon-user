//
//  AdvertisingPageVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 2/20/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol AdvertisingPageVCView: LoaderDelegate {
    func navigateToMapVC()
    func presentFilterVC(_ serviceID: Int)
    func presentCitiesVC(_ serviceID: Int)
    func navigateToSearchShopsVC(_ serviceID: Int)
    func navigateToAdDetailsVC(_ id: Int, title: String?)
    func navigateToSelectMainServiceVC(_ services: [CategoryService])
}


class AdvertisingPageVCPresenter {
    
    private weak var view: AdvertisingPageVCView?
    private let interactor = SingleCommercialVCAPI()
    private var services = [CategoryService]()
    private var advertisements = [Shop]()
    private var selectedServiceID = 0
    private var lat: String = "0"
    private var lng: String = "0"
    private var cityID: Int = 0
    private var categoryID: Int = 0
    private var subCategoryID: Int = 0

    
    init(view: AdvertisingPageVCView) {
        self.view = view
    }
    
    var serviceCount: Int {
        return services.count
    }
    
    var advertisementsCount: Int {
        return advertisements.count
    }
    
    
    func viewDidLoad(lat: String, lng: String) {
        self.lat = lat
        self.lng = lng
        self.getData()
    }
    
    
    private func getData() {
        
        view?.showProgress()
        
        let  url = URLs.advertisementsList.url
        
        interactor.getCommercialData(url: url, lat: self.lat, lng: self.lng, tab: 0, service_id: self.selectedServiceID, cityID: self.cityID, categoryID: self.categoryID, subCategoryID: self.subCategoryID, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.services = response.data?.services ?? []
                self?.advertisements = response.data?.advertisements ?? []
                if self?.selectedServiceID == 0 {
                    self?.selectedServiceID = self?.services.first?.id ?? 0
                }
                self?.view?.onSuccess("")
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) {  [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
    
    
    func serviceConfigure(cell: ServiceCellView, index: Int) {
        let service = services[index]
        cell.configure(image: service.image, name: service.name)
        cell.setIsSelected(self.selectedServiceID == services[index].id)
    }
    
    
    func advertisementConfigure(cell: ProductCellView, index: Int) {
        let p = advertisements[index]
        cell.configure(image: p.image, name: p.name, price: p.price, desc: p.description)
    }
    
    
    func locationButtonClicked() {
        view?.navigateToMapVC()
    }
    
    
    func didSelectedServiceAt(index: Int) {
        let service = services[index]
        //Clear Data
        self.cityID = 0
        self.categoryID = 0
        self.subCategoryID = 0
        self.selectedServiceID = service.id ?? 0
        //Call API
        self.getData()
    }
    
    func didSelectAdvertisingAt(index: Int) {
        let id = advertisements[index].id ?? 0
        let title = advertisements[index].name
        view?.navigateToAdDetailsVC(id, title: title)
    }

        
    func userLocationChanged(lat: Double, lng: Double) {
        self.lat = "\(lat)"
        self.lng = "\(lng)"
        self.getData()
    }
    
    
    func filterBtnClicked() {
        view?.presentFilterVC(self.selectedServiceID)
    }
    
    func filterDataRetrived(catID: Int, subCat: Int) {
        self.categoryID = catID
        self.subCategoryID = subCat
        self.getData()
    }
    
    func citysBtnClicked() {
        view?.presentCitiesVC(self.selectedServiceID)
    }
    
    func cityTagRetriveID(_ id: Int) {
        self.cityID = id
        self.getData()
    }
    
        
    func searchBtnClicked() {
        view?.navigateToSearchShopsVC(self.selectedServiceID)
    }
    
    
    func addNewAd() {
        if DEF.isLogin {
            view?.navigateToSelectMainServiceVC(self.services)
        } else {
            showLoginMessage()
        }
    }
    

    
    
}
