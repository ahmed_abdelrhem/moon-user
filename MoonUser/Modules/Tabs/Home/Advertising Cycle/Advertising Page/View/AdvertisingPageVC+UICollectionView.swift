//
//  AdvertisingPageVC+UICollectionView.swift
//  MoonUser
//
//  Created by Grand iMac on 2/20/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension AdvertisingPageVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 0 {
            return presenter.serviceCount
        }
        return presenter.advertisementsCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 0 {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ServiceCell", for: indexPath) as? ServiceCell else {
                return UICollectionViewCell()
            }
            presenter.serviceConfigure(cell: cell, index: indexPath.row)
            return cell
        } else {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CompanyProductCell", for: indexPath) as? CompanyProductCell else { return UICollectionViewCell() }
            presenter.advertisementConfigure(cell: cell, index: indexPath.row)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 0 {
            return CGSize(width: 100, height: 120)

        }
        let width = collectionView.frame.width
        return CGSize(width: width/2, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 0 {
            presenter.didSelectedServiceAt(index: indexPath.row)
        } else {
            presenter.didSelectAdvertisingAt(index: indexPath.row)
        }
    }
    
    
    
}
