//
//  AdvertisingPageVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 2/20/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension AdvertisingPageVC: AdvertisingPageVCView {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        productsCollectionView.restore()
        sectionsCollectionView.reloadData()
        productsCollectionView.reloadData()
        if productsCollectionView.numberOfItems(inSection: 0) == 0 {
            productsCollectionView.setEmptyView(title: NO_DATA_FOUND, messageImage: .no_data_image)
        }
     }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
        
    func navigateToMapVC() {
        let vc = MapVC()
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    
    func presentFilterVC(_ serviceID: Int) {
        let vc = CategoryFilterVC()
        vc.serviceID = serviceID
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }

    func presentCitiesVC(_ serviceID: Int) {
        let vc = TagsVC()
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
            
    func navigateToSearchShopsVC(_ serviceID: Int) {
        let vc = SearchShopsVC()
        vc.serviceID = serviceID
        vc.type = .Commercial
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToAdDetailsVC(_ id: Int, title: String?) {
        let vc = AdDetailsVC()
        vc.id = id
        vc.title = title
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToSelectMainServiceVC(_ services: [CategoryService]) {
        let vc = SelectMainServiceVC()
        vc.services = services
        navigationController?.pushViewController(vc, animated: true)

    }

    
    
    
}
