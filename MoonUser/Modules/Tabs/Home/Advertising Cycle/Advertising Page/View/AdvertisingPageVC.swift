//
//  AdvertisingPageVC.swift
//  MoonUser
//
//  Created by Grand iMac on 2/20/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class AdvertisingPageVC: UIViewController {

     //MARK:- Outlets
    @IBOutlet weak var sectionsCollectionView: UICollectionView! {
        didSet {
            sectionsCollectionView.tag = 0
            sectionsCollectionView.delegate = self
            sectionsCollectionView.dataSource = self
            sectionsCollectionView.register(UINib(nibName: "ServiceCell", bundle: nil), forCellWithReuseIdentifier: "ServiceCell")
        }
    }
    @IBOutlet weak var productsCollectionView: UICollectionView!{
        didSet {
            productsCollectionView.tag = 1
            productsCollectionView.delegate = self
            productsCollectionView.dataSource = self
            productsCollectionView.register(UINib(nibName: "CompanyProductCell", bundle: nil), forCellWithReuseIdentifier: "CompanyProductCell")
        }
    }
    
    
    
    //MARK:- Variables
    internal var presenter: AdvertisingPageVCPresenter!

    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setPresenter()
        setUI()
    }
    


    //MARK:- Functions
    private func setPresenter() {
        presenter = AdvertisingPageVCPresenter(view: self)
        presenter.viewDidLoad(lat: DEF.currentLat, lng: DEF.currentLng)
    }
    
    private func setUI() {
        backButtonTitle("")
        setTitleView(title: "\(DEF.currentAdd)  ")
        setRightBarButton()
    }
    

    //TODO:- Set UINavigation Button
    internal func setTitleView(title: String) {
        let button:UIButton  = UIButton(type: .system) as UIButton
        (button as UIView).frame = CGRect(x:0, y:0, width: 50, height:44)
        button.setTitle(title, for: .normal)
        button.setImage(UIImage(named: "arrow-down")!, for: .normal)
        button.titleLabel?.font = UIFont.myMeduimSystemFont(ofSize: 15)
        button.titleLabel?.lineBreakMode = .byTruncatingTail
        button.titleLabel?.numberOfLines = 2
        button.titleLabel?.textAlignment = .center
        button.semanticContentAttribute = .forceRightToLeft
        button.addTarget(self, action: #selector(self.locationButtonClicked(_:)), for: .touchUpInside)
        self.navigationItem.titleView = button
        self.navigationItem.titleView?.tintColor = .black
    }
    
    //TODO:- Set UINavigation Search Button
    private func setRightBarButton() {
        let searchBtn = UIButton(type: UIButton.ButtonType.system)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        searchBtn.setImage(UIImage(named: "search")!, for: .normal)
        searchBtn.addTarget(self, action: #selector(searchClicked(_:)), for: .touchUpInside)
        let searchBarBtItem = UIBarButtonItem(customView: searchBtn)
        self.navigationItem.rightBarButtonItem = searchBarBtItem
    }
    
    @objc func locationButtonClicked(_ sender: UIButton) {
        presenter.locationButtonClicked()
    }
    
    @objc func searchClicked(_ sender: UIButton) {
        presenter.searchBtnClicked()
    }
    
    
    //MARK:- Actions
    @IBAction func filterBtnClicked(_ sender: UIButton) {
        presenter.filterBtnClicked()
    }

    @IBAction func cityBtnClicked(_ sender: UIButton) {
        presenter.citysBtnClicked()
    }

    @IBAction func addAdvertisingBtnClicked(_ sender: UIButton) {
        presenter.addNewAd()
    }
}



//MARK:- Protcol LocationDelegate
extension AdvertisingPageVC: LocationDelegate {
    
    func RetriveLocation(lat: Double, lng: Double, add: String) {
        self.setTitleView(title: "\(add)  ")
        presenter.userLocationChanged(lat: lat, lng: lng)
    }
}


//MARK:- Protcol TagsVCDelegate
extension AdvertisingPageVC: TagsVCDelegate {
    
    func retriveIDs(_ ids: [Int]) {
        presenter.cityTagRetriveID(ids[0])
    }
    
}


//MARK:- Protcol CategoryFilterDelegate
extension AdvertisingPageVC: CategoryFilterDelegate {
    
    func retriveDate(catID: Int, subCat: Int) {
        presenter.filterDataRetrived(catID: catID, subCat: subCat)
    }
}
