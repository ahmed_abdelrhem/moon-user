//
//  AdDetailsVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 2/19/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension AdDetailsVC: AdDetailsVCView {
    
    func showProgress() {
        startAnimating()
    }
    
    func hideProgress() {
        stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        self.view.backgroundColor = .mainColor
        containerView.isHidden = false
        contactStack.isHidden = false
    }
    
    func updateUI(_ details: SingleProduct?) {
        DispatchQueue.main.async {
            self.sellerNameLbl.text = details?.name
            self.priceLbl.text = "\(details?.price ?? "0") \(DEF.country.currency)"
            self.descriptionLbl.text = details?.description
            self.favBtn?.isSelected = details?.isFavourite ?? false
        }
    }

    
    func setSlideShow(_ images: [String]) {
        SharedHandller.instance.setSlider(self, slideShow: sliderView, images: images)
    }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func showLocation(lat: Double, lng: Double) {
        let vc = ShowLocationVC()
        vc.lat = lat
        vc.lng = lng
        present(vc, animated: true, completion: nil)
    }
    
    func navigateToChatPageVC(id: Int) {
        let vc = ChatPageVC()
        vc.reciverType = 13
        vc.id = id
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }

    func changeFavouriteBtnStatus() {
        favBtn!.isSelected = !favBtn!.isSelected
    }

    
}
