//
//  AdDetailsVC.swift
//  MoonUser
//
//  Created by Grand iMac on 2/19/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import ImageSlideshow

class AdDetailsVC: UIViewController {

    
    //MARK:- Outltes
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var sliderView: ImageSlideshow!
    @IBOutlet weak var sellerNameLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var favBtn: UIButton!
    @IBOutlet weak var contactStack: UIStackView!
    @IBOutlet weak var chatStack: UIStackView!
    @IBOutlet weak var emailStack: UIStackView!
    @IBOutlet weak var callStack: UIStackView!
    
    
    //MARK:- Variables
    internal var presenter: AdDetailsVCPresenter!
    var id: Int!

    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addGestures()
        setupUI()
        setPresenter()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isTranslucent = false
    }

        
    
    //MARK:- Fcuntions
    private func setupUI() {
        self.view.backgroundColor = .white
        backButtonTitle("")
        containerView.isHidden = true
        contactStack.isHidden = true
    }
    
    private func addGestures() {
         let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.chatClicked))
         chatStack.addGestureRecognizer(tap1)

         let tap2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.emailClicked))
         emailStack.addGestureRecognizer(tap2)

         let tap3: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.callClicked))
         callStack.addGestureRecognizer(tap3)
     }
    
    private func setPresenter() {
        presenter = AdDetailsVCPresenter(view: self)
        presenter.viewDidlLoad(self.id)
    }
    
    
    
    //MARK:- Actions
    @objc func chatClicked() {
        presenter.chatClicked()
    }
    
    @objc func emailClicked() {
        presenter.emailClicked()
    }
    
    @objc func callClicked() {
        presenter.callClicked()
    }
    
    @IBAction func favBtnClicked(_ sender: UIButton) {
        presenter.favBtnClicked()
    }

    @IBAction func locationBtnClicked(_ sender: UIButton) {
        presenter.locationBtnClicked()
    }

    

}
