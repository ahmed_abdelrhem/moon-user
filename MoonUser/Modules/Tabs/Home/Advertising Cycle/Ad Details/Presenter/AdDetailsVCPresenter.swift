//
//  AdDetailsVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 2/19/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol AdDetailsVCView: LoaderDelegate {
    func setSlideShow(_ images: [String])
    func updateUI(_ details: SingleProduct?)
    func showLocation(lat: Double, lng: Double)
    func navigateToChatPageVC(id: Int)
    func changeFavouriteBtnStatus()
}

class AdDetailsVCPresenter {
    
    private weak var view: AdDetailsVCView?
    private let interactor = AdDetailsVCAPI()
    private let favouriteInteractor = FavouritesAPI()
    private var details: SingleProduct?

    
    
    init(view: AdDetailsVCView) {
        self.view = view
    }
    
    func viewDidlLoad(_ id: Int) {
        
        view?.showProgress()
        
        interactor.getProductDetails(id: id, didDataReady: {  [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.details = response.data
                let slider = response.data?.product_images ?? []
                var images = [String]()
                for img in slider {
                    images.append(img.image ?? "")
                }
                self?.view?.setSlideShow(images)
                self?.view?.updateUI(response.data)
                self?.view?.onSuccess("")
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
    
    func favBtnClicked() {
        guard DEF.isLogin else {
            showLoginMessage()
            return
        }
        
        view?.changeFavouriteBtnStatus()
        DispatchQueue.global(qos: .background).async {
            self.favouriteInteractor.addOrRemoveFavourite(productID: self.details?.id ?? 0, type: DEF.type)
        }
    }

    
    func chatClicked() {
        let id = details?.id ?? 0
        view?.navigateToChatPageVC(id: id)
    }
    
    func emailClicked() {
        let userURL = details?.email ?? ""
        let defaultURL = "https://mail.google.com"
        UIApplication.tryURL(urls: [userURL, defaultURL])
    }
    
    func callClicked() {
        if let phone = details?.phone, phone != "" {
            let url:NSURL = NSURL(string: "tel://\(phone)")!
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        } else {
            Messages.instance.showConfigMessage(title: "", body: NO_PHONE, state: .warning, layout: .statusLine, style: .top)
        }
    }

    func locationBtnClicked() {
        let lat = Double(details?.lat ?? "0")!
        let lng = Double(details?.lng ?? "0")!
        view?.showLocation(lat: lat, lng: lng)
    }

    

    
}
