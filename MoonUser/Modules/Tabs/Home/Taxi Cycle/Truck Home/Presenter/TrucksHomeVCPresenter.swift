//
//  TrucksHomeVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 3/9/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol TrucksHomeVCView: LoaderDelegate {
    func navigateToPickupMapVC(_ id: Int)
}

class TrucksHomeVCPresenter {
    
    private weak var view: TrucksHomeVCView?
    private let interactor = TrucksHomeVCAPI()
    private var trucks = [SingleTruck]()
    private var drivers = [Driver]()
    private (set) var selectedTruckIndex = 0
    
    
    init(view: TrucksHomeVCView) {
        self.view = view
    }
    
    func viewDidLoad() {
        
        view?.showProgress()
        
        interactor.getAllTrucks(didDataReady:{ [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            
            if response.status == StatusCode.Success.rawValue {
                self?.trucks = response.data ?? []
                if self?.trucks.count != 0 {
                    self?.drivers = self?.trucks[0].drivers ?? []
                }
                self?.view?.onSuccess("")
            } else if response.status == StatusCode.ExpiredJWT.rawValue {
                Messages.instance.loginMessage()
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) {  [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onEmpty(.no_connection_image, CONNECTION_ERROR)
        }
    }
    
    
    
    
    //MARK:- UICollectionView Functions
    var collectionCout: Int {
        return trucks.count
    }
    
    func truckConfigure(cell: TruckCellView, at index: Int) {
        let truck = trucks[index]
        cell.configure(image: truck.image, name: truck.name)
    }
    
    func didSelectItem(at index: Int) {
        selectedTruckIndex = index
        drivers = trucks[index].drivers ?? []
        view?.onSuccess("")
    }
    
    
    //MARK:- UITableView Functions
    var tableCount: Int {
        return drivers.count
    }
    
    func driverConfigure(cell: DriverCellView, at index: Int) {
        let driver = drivers[index]
        cell.configure(image: driver.image, name: driver.name, price: driver.price)
    }
    
    func applyBtnClicked(at index: Int) {
        let id = drivers[index].id ?? 0
        view?.navigateToPickupMapVC(id)
    }
    
    
}
