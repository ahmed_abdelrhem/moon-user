//
//  TrucksHomeVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 3/9/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension TrucksHomeVC: TrucksHomeVCView {
    
    func showProgress() {
        startAnimating()
    }
    
    func hideProgress() {
        stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        tableView.restore()
        collectionView.reloadData()
        tableView.reloadData()
        if tableView.numberOfRows(inSection: 0) == 0 {
            tableView.setEmptyView(title: NO_DRIVERS, messageImage: .no_data_image)
        }
    }

    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func onEmpty(_ img: UIImage, _ msg: String) {
        tableView.restore()
        tableView.setEmptyView(title: msg, messageImage: img)
    }
    
    func navigateToPickupMapVC(_ id: Int) {
        let vc = TaxiHomeVC()
        vc.type = .Truck
        vc.driverID = id
        navigationController?.pushViewController(vc, animated: true)
    }


}
