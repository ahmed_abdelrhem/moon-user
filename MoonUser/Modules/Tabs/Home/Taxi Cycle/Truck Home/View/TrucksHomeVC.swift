//
//  TrucksHomeVC.swift
//  MoonUser
//
//  Created by Grand iMac on 3/9/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class TrucksHomeVC: UIViewController {

    
    //MARK:- Outlets
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.tag = 0
            collectionView.delegate = self
            collectionView.dataSource = self
            collectionView.register(UINib(nibName: "TruckCell", bundle: nil), forCellWithReuseIdentifier: "TruckCell")
        }
    }
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView(frame: .zero)
            tableView.delegate = self
            tableView.dataSource = self
            tableView.separatorStyle = .none
            tableView.register(UINib(nibName: "DriverCell", bundle: nil), forCellReuseIdentifier: "DriverCell")
        }
    }

    
    //MARK:- Variables
    internal var presenter: TrucksHomeVCPresenter!

    
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        backButtonTitle("")
        title = MOON_TRNASPORTATIONS
        setPresenter()
    }
    
    
    //MARK:- Functions
    private func setPresenter() {
        presenter = TrucksHomeVCPresenter(view: self)
        presenter.viewDidLoad()
    }


}
