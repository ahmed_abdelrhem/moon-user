//
//  TrucksHomeVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 3/9/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class TrucksHomeVCAPI: NSObject {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    
    typealias model = (AllTrucksModel)->()
    
    
    func getAllTrucks(didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param = [
            "country_id": DEF.country.id
        ]
        
        let url = URLs.allTrucks.url.concatURL(param)
        
        apiManager.contectToApiWith(url: url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                if let result = try? self.decoder.decode(AllTrucksModel.self, from: data) {
                    didDataReady(result)
                } else {
                    print("get Trips error")
                }
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
    
    
}
