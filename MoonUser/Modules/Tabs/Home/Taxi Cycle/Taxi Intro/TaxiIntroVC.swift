//
//  TaxiIntroVC.swift
//  MoonUser
//
//  Created by Grand iMac on 3/4/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

enum TaxiIntroVCType {
    case Taxi
    case Truck
    
    var id: Int {
        switch self {
        case .Taxi:
            return 7
        case .Truck:
            return 12
        }
    }

    var navigationTitle: String {
        switch self {
        case .Taxi:
            return TAXI_HISTORY
        case .Truck:
            return TRUCK_HISTORY
        }
    }

    
    var title: String {
        switch self {
        case .Taxi:
            return TAXI
        case .Truck:
            return TRUCK
        }
    }
    
    var image: UIImage {
        switch self {
        case .Taxi:
            return .taxi_image
        case .Truck:
            return .truck_image
        }
    }
    
    var mapTitle: String {
        switch self {
        case .Taxi:
            return  MOON_TAXI
        case .Truck:
            return MOON_TRNASPORTATIONS
        }
    }
}

class TaxiIntroVC: UIViewController {

    @IBOutlet weak var introImg: UIImageView!
    
    var type: TaxiIntroVCType!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = type.title
        backButtonTitle("")
        introImg.image = type.image
    }

    
    @IBAction func enjoyWithMoonBtnClicked(_ sender: UIButton) {
        if type == .Taxi {
            let vc = TaxiHomeVC()
            vc.type = .Taxi
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = TrucksHomeVC()
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    

}
