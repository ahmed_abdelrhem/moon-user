//
//  ScheduleTimeVC.swift
//  MoonUser
//
//  Created by Grand iMac on 3/4/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

protocol ScheduleTimeVCDelegate: class {
    func retriveDate(_ date: String, backendDate: String, backendTime: String)
}

class ScheduleTimeVC: BaseViewController {

    //MARK:- Outlets
    @IBOutlet weak var datePicker: UIDatePicker!
    
    
    //MARK:- Variables
    var delegate: ScheduleTimeVCDelegate?
    var scheduleDate: String = ""
    var backendDate: String = ""
    var backendTime: String = ""
    var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)

    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addGestures()
        setDate()
    }


    
    //MARK:- Fcuntions
    fileprivate func addGestures() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissViewController))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissViewController() {
        dismiss(animated: true, completion: nil)
    }
    
    fileprivate func setDate() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E, MMM d, h:mm a"
        var dateAsString = dateFormatter.string(from: datePicker.date)
        dateAsString = dateFormatter.string(from: datePicker.date)
        scheduleDate = dateAsString
        
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd"
        var date = dateFormat.string(from: datePicker.date)
        date = dateFormat.string(from: datePicker.date)
        backendDate = date

        let timeFormat = DateFormatter()
        timeFormat.dateFormat = "HH:mm"
        var time = timeFormat.string(from: datePicker.date)
        time = timeFormat.string(from: datePicker.date)
        backendTime = time
        
        datePicker.minimumDate = Date()
    }
    
    
    
    //MARK:- Actions
    //MARK:- Actions
    @IBAction func handlePanGesture(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view?.window)
        
        if sender.state == .began {
            initialTouchPoint = touchPoint
        } else if sender.state == .changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        } else if sender.state == .ended || sender.state == .cancelled {
            if touchPoint.y - initialTouchPoint.y > 100 {
                self.dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
        
    }
    
    @IBAction func datePickerValueChanged(_ sender: UIDatePicker) {
        setDate()
    }
    
    
    @IBAction func confirmBtnClicked(_ sender: UIButton) {
        delegate?.retriveDate(scheduleDate, backendDate: backendDate, backendTime: backendTime)
        dismiss(animated: true, completion: nil)
    }
    


}
