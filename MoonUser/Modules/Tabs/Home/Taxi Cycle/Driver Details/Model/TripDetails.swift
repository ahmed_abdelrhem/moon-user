/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct TripDetails : Codable {
	let driver_id : Int?
	let id : Int?
	let status : Int?
	let start_lat : String?
	let start_lng : String?
	let start_address : String?
	let end_lat : String?
	let end_lng : String?
	let end_address : String?
	let price : Int?
	let date : String?
	let time : String?
	let phone : String?
	let name : String?
	let image : String?
	let rate : String?
	let car_brand : String?
	let car_number : Int?
	let car_color : String?

	enum CodingKeys: String, CodingKey {

		case driver_id = "driver_id"
		case id = "id"
		case status = "status"
		case start_lat = "start_lat"
		case start_lng = "start_lng"
		case start_address = "start_address"
		case end_lat = "end_lat"
		case end_lng = "end_lng"
		case end_address = "end_address"
		case price = "price"
		case date = "date"
		case time = "time"
		case phone = "phone"
		case name = "name"
		case image = "image"
		case rate = "rate"
		case car_brand = "car_brand"
		case car_number = "car_number"
		case car_color = "car_color"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		driver_id = try values.decodeIfPresent(Int.self, forKey: .driver_id)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		status = try values.decodeIfPresent(Int.self, forKey: .status)
		start_lat = try values.decodeIfPresent(String.self, forKey: .start_lat)
		start_lng = try values.decodeIfPresent(String.self, forKey: .start_lng)
		start_address = try values.decodeIfPresent(String.self, forKey: .start_address)
		end_lat = try values.decodeIfPresent(String.self, forKey: .end_lat)
		end_lng = try values.decodeIfPresent(String.self, forKey: .end_lng)
		end_address = try values.decodeIfPresent(String.self, forKey: .end_address)
		price = try values.decodeIfPresent(Int.self, forKey: .price)
		date = try values.decodeIfPresent(String.self, forKey: .date)
		time = try values.decodeIfPresent(String.self, forKey: .time)
		phone = try values.decodeIfPresent(String.self, forKey: .phone)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		image = try values.decodeIfPresent(String.self, forKey: .image)
		rate = try values.decodeIfPresent(String.self, forKey: .rate)
		car_brand = try values.decodeIfPresent(String.self, forKey: .car_brand)
		car_number = try values.decodeIfPresent(Int.self, forKey: .car_number)
		car_color = try values.decodeIfPresent(String.self, forKey: .car_color)
	}

}
