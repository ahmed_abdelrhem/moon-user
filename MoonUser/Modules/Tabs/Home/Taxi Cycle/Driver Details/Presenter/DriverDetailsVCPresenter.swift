//
//  DriverDetailsVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 3/5/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

import GoogleMaps

protocol DriverDetailsVCView: LoaderDelegate {
    func setupUI(_ data: TripDetails?)
    func showContainerView()
    func setArrivedStatus()
    func presentCancelVC()
    func backToHome()
}

class DriverDetailsVCPresenter {
    
    private weak var view: DriverDetailsVCView?
    private let taxiInteractor = TaxiHomeVCAPI()
    private let interactor = DriverDetailsVCAPI()
    private var trip: TripDetails?
    
    
    init(view: DriverDetailsVCView) {
        self.view = view
    }
    
    
    func getTripDate() {
        
        view?.showProgress()
        
        interactor.getTripDetails(tripID: DEF.tripID, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()

            if response.status == StatusCode.Success.rawValue {
                self?.trip = response.data

                DispatchQueue.main.async {
                    self?.view?.setupUI(self?.trip)
                }
                if let status = response.data?.status {
                    switch status {
                    case 1:
                        self?.view?.showContainerView()
                        break
                        
                    case 3,4:
                        self?.view?.setArrivedStatus()
                        break
                                                
                    default:
                        break
                    }
                }
            } else if response.status == StatusCode.ExpiredJWT.rawValue {
                Messages.instance.loginMessage()
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
    
    func drawPath(completion: @escaping (DirectionModelData?, Error?)->Void) {
        
        let start = CLLocationCoordinate2D(latitude: Double(trip?.start_lat ?? "0")!, longitude: Double(trip?.start_lng ?? "0")!)
        let end = CLLocationCoordinate2D(latitude: Double(trip?.end_lat ?? "0")!, longitude: Double(trip?.end_lng ?? "0")!)
        
        //Draw Path
        taxiInteractor.drawPathWithPoints(startLocation: start, endLocation: end, waypoints: []) { (directionData, error) in
            completion(directionData, error)
        }
    }
    
    func callCaptaionBtnClicked() {
        if let url = URL(string: "tel://\(trip?.phone ?? "0")"),
            UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
        } else {
            Messages.instance.showMessage(title: "", body: NO_PHONE, state: .info, layout: .messageView)
        }
    }
    
    func CancelTripBtnClicked() {
        view?.presentCancelVC()
    }
    
    func didRecivedCancelReason(_ reason: String) {
        view?.showProgress()
        
        interactor.cancelTrip(tripId: trip?.id ?? 0, comment: reason, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            
            if response.status == StatusCode.Success.rawValue {
                self?.view?.onSuccess(response.message ?? "")
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { (error) in
            self.view?.hideProgress()
            self.view?.onFailure(CONNECTION_ERROR)
        }
    }
        
    func backToHome() {
        view?.backToHome()
    }
    
    
    
}
