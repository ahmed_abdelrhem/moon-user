//
//  DriverDetailsVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 3/5/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class DriverDetailsVCAPI: NSObject {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    
    typealias statusModel = (StatusModel)->()
    typealias model = (TripResponseModel)->()
    
    func getTripDetails(tripID: Int, didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param = [
            "type": 13,
            "id":tripID
        ]
        
        let url = URLs.tripDetails.url.concatURL(param)
        
        apiManager.contectToApiWith(url: url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                if let result = try? self.decoder.decode(TripResponseModel.self, from: data) {
                    didDataReady(result)
                } else {
                    print("Trip details error")
                }
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
    func cancelTrip(tripId: Int, comment: String, didDataReady: @escaping statusModel, andErrorCompletion errorCompletion: @escaping errorCompletionType) {

        var param = [
            "trip_id": tripId,
            "status": 5,
            "type": 13,
            "trip_type": DEF.type,
        ] as [String : Any]
        
        if comment != "" {
            param["comment"] = comment
        }

        apiManager.contectToApiWith(url: URLs.cancelTrip.url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in

            if let data = try? JSONSerialization.data(withJSONObject: json) {
                if let result = try? self.decoder.decode(StatusModel.self, from: data) {
                    didDataReady(result)
                } else {
                    print("cancel trip error")
                }
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
    
}
