//
//  DriverDetailsVC.swift
//  MoonUser
//
//  Created by Grand iMac on 3/5/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Firebase


class DriverDetailsVC: BaseViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var driverImg: UIImageView!
    @IBOutlet weak var driverNameLbl: UILabel!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var actionsStack: UIStackView!
    @IBOutlet weak var priceStack: UIStackView!
    @IBOutlet weak var brandCarLbl: UILabel!
    @IBOutlet weak var carNumberLbl: UILabel!
    @IBOutlet weak var carColorView: UIView!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var actionViewHeight: NSLayoutConstraint!
    
    
    //MARK:- Variables
    var presenter: DriverDetailsVCPresenter!
    let carMarker = GMSMarker()
    var polyline = GMSPolyline()
    
    //Firebase
//    var ref = DatabaseReference.init()

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPresenter()
//        readFirebaseData()
        listenNotificationCenter()
        containerView.isHidden = true

    }
    
    //MARK:- Functions
    fileprivate func setPresenter() {
        presenter =  DriverDetailsVCPresenter(view: self)
        presenter.getTripDate()
    }
    
    
    private func listenNotificationCenter() {
        _NC.addObserver(forName: .driverArrivedLocation, object: nil, queue: nil) { _ in
            self.updateUI()
        }
                
        _NC.addObserver(forName: .driverCancelOrder, object: nil, queue: nil) { _ in
            self.onSuccess("")
        }
    }
    
    
    internal func updateUI() {
        UIView.animate(withDuration: 0.5, animations: {
            self.actionViewHeight.constant = 0
            self.view.layoutIfNeeded()
        }) { _ in
            self.containerView.isHidden = false
            self.actionsStack.isHidden = true
            self.priceStack.isHidden = true
            UIView.animate(withDuration: 0.5, animations: {
                self.actionViewHeight.constant = 160
                self.view.layoutIfNeeded()
            })
        }
    }


    
    //MARK:- Actions
    @IBAction func backBtnClicked(_ sender: UIButton) {
        homeScreen(GO_TO_YOUR_TRIPS_TO_CONTINUES)
    }
    
    @IBAction func callCaptaionBtnClicked(_ sender: UIButton) {
        presenter.callCaptaionBtnClicked()
    }
    
    @IBAction func cancelTripBtnClicked(_ sender: UIButton) {
        presenter.CancelTripBtnClicked()
    }
    
    
    
}


extension DriverDetailsVC: CancelTripVCDelegate {
    
    func retriveReason(_ reason: String) {
        presenter.didRecivedCancelReason(reason)
    }
    
    
    
    
}



//Firebase
//extension DriverDetailsVC {
//
//    func readFirebaseData() {
//        ref = Database.database().reference()
//
//        ref.child("Drivers").queryOrderedByKey().observe(.value) { (snapshot) in
//            if let snapshot = snapshot.children.allObjects as? [DataSnapshot] {
//                for snap in snapshot {
//                    if snap.key == "\(DEF.userID)" {
//                        if let postDict = snap.value as? [String:Any] {
//                            if let array = postDict["l"] as? NSArray {
//                                let lat = "\(array[0])"
//                                let lng = "\(array[1])"
//                                self.CarMarkerSetup(lat: lat, lng: lng)
//                            }
//                        }
//                    }
//                }
//            }
//        }
//
//
//    }
//}
