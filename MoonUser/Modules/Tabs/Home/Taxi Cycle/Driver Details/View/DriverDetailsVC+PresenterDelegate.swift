//
//  DriverDetailsVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 3/5/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import GoogleMaps
import SDWebImage

extension DriverDetailsVC: DriverDetailsVCView {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        dismiss(animated: true, completion: nil)
        Messages.instance.showConfigMessage(title: "", body: msg, state: .info, layout: .centeredView, style: .center)
    }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .info, layout: .messageView, style: .top)
    }
    
    func setupUI(_ data: TripDetails?) {
        let driverImgURL = URL(string: data?.image ?? "")
        driverImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        driverImg.sd_setImage(with: driverImgURL, placeholderImage: .avatar_image)
        driverNameLbl.text = data?.name
        rateView.rating = Double(data?.rate ?? "0")!
        brandCarLbl.text = data?.car_brand
        carNumberLbl.text = "\(data?.car_number ?? 0)"
        carColorView.backgroundColor = HexStringToUIColor.hexStringToUIColor(hex: data?.car_color ?? "000000")
        priceLbl.text = "\(data?.price ?? 0) \(DEF.country.currency)"
        
        let start = CLLocationCoordinate2D(latitude: Double(data?.start_lat ?? "0")!, longitude: Double(data?.start_lng ?? "0")!)
        let destination = CLLocationCoordinate2D(latitude: Double(data?.end_lat ?? "0")!, longitude: Double(data?.end_lng ?? "0")!)
        pickupMarker(start: start)
        destinationMarker(destination: destination)

    }
    
    func showContainerView() {
        UIView.animate(withDuration: 0.5, animations: {
            self.containerView.isHidden = false
        })
    }
    
    func setArrivedStatus() {
        self.updateUI()
    }

    func presentCancelVC() {
        let vc = CancelTripVC()
        vc.modalPresentationStyle = .overFullScreen
        vc.delegate = self
        present(vc, animated: false, completion: nil)
    }

    func backToHome() {
        dismiss(animated: true, completion: nil)
        Messages.instance.showConfigMessage(title: "", body: CONTINUES, state: .info, layout: .centeredView, style: .center)
    }

}
