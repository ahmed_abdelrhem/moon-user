//
//  DriverDetailsVC+Maps.swift
//  MoonUser
//
//  Created by Grand iMac on 3/5/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import GoogleMaps
import Alamofire

extension DriverDetailsVC {
    
    internal func pickupMarker(start location : CLLocationCoordinate2D?){
        let pickMarker = GMSMarker()
        pickMarker.position = location!
        pickMarker.map = mapView
        pickMarker.icon = .pinImage
        let camera = GMSCameraPosition.camera(withLatitude: location!.latitude, longitude: location!.longitude, zoom: 15)
        setUpMap(camera)
    }
    
    internal func destinationMarker(destination location : CLLocationCoordinate2D?) {
        let destMarker = GMSMarker()
        destMarker.position = location!
        destMarker.map = mapView
        destMarker.icon = .pinImage
        let camera = GMSCameraPosition.camera(withLatitude: location!.latitude, longitude: location!.longitude, zoom: 5)
        setUpMap(camera)
    }
    
    internal func CarMarkerSetup(lat: String, lng: String) {
        carMarker.position = CLLocationCoordinate2D(latitude: Double(lat)!, longitude: Double(lng)!)
        carMarker.icon = .carImage
        carMarker.map = mapView
    }
    
    
    fileprivate func setUpMap(_ camera: GMSCameraPosition) {
        
        self.startAnimating()

        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.camera = camera
        mapView.settings.rotateGestures = false
        mapView.accessibilityElementsHidden = false
        mapView.isMyLocationEnabled = true
        mapView.settings.indoorPicker = true
        
        //TODO:- To Change Map Style
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "MapStyle", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        
        
        presenter.drawPath() { (directionData, error) in
            if let error = error {
                print(error)
            } else {
                guard let directionData = directionData else { return }
                let routeOverviewPolyline = directionData.routes
                for route in routeOverviewPolyline! {
                    let poverviewPolylineoints = route.overview_polyline
                    let points = poverviewPolylineoints?.points
                    let path = GMSPath.init(fromEncodedPath: points!)
                    self.polyline = GMSPolyline.init(path: path)
                    self.polyline.strokeWidth = 3
                    self.polyline.strokeColor = UIColor.darkGray
                    self.polyline.map = self.mapView
                    
                    
                    // MARK:- MAPVIEW ZOOM OUT TO SHOW LINE
                    DispatchQueue.main.async {
                        if self.mapView != nil {
                            let bounds = GMSCoordinateBounds(path: path!)
                            self.mapView!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 100.0))
                        }
                    }
                }
            }
            self.stopAnimating()
        }
    }
    
   
    
}
