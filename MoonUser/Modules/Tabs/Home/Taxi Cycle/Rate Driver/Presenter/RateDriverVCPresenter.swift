//
//  RateDriverVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 3/8/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class RateDriverVCPresenter {
    
    private weak var view: LoaderDelegate?
    private let interactor = RateDriverVCAPI()
    private var rate: Double = 0
    
    init(view: LoaderDelegate) {
        self.view = view
    }
    
    
    func didFinishRating(_ rate: Double) {
        self.rate = rate
    }
    
    
    func sendFeedbackBtnClicke(comment: String) {
        
        view?.showProgress()
        
        interactor.rateDriver(rate: rate, comment: comment, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            
            if response.status == StatusCode.Success.rawValue {
                DEF.tripID = 0
                self?.view?.onSuccess(response.message ?? "")
            } else if response.status == StatusCode.ExpiredJWT.rawValue {
                Messages.instance.loginMessage()
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { (error) in
            self.view?.hideProgress()
            self.view?.onFailure(CONNECTION_ERROR)
        }
    }
    
    
}







