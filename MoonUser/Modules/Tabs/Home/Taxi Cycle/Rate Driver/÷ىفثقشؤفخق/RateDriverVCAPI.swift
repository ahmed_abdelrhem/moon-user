//
//  RateDriverVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 3/8/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class RateDriverVCAPI: NSObject {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    
    typealias statusModel = (StatusModel)->()
    
    
    func rateDriver(rate: Double, comment: String, didDataReady: @escaping statusModel, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param = [
            "trip_id": driverData.tripID ?? 0,
            "type": 13,
            "rate": rate,
            "comment": comment,
            "to_type":7
        ] as [String : Any]
                
        apiManager.contectToApiWith(url: URLs.rateTrip.url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                if let result = try? self.decoder.decode(StatusModel.self, from: data) {
                    didDataReady(result)
                } else {
                    print("Rate error")
                }
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
    
}
