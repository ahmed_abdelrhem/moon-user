//
//  DriverDataModel.swift
//  MoonUser
//
//  Created by Grand iMac on 3/8/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation


struct DriverData {
    var tripID: String?
    var price: String?
    var name: String?
    var image: String?
    
    init() {
        self.tripID = "0"
        self.price = "0"
        self.name = ""
        self.image = ""
    }

    init(tripID: String, price: String, name: String, image: String) {
        self.tripID = tripID
        self.price = price
        self.name = name
        self.image = image
    }

}
