//
//  RateDriverVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 3/8/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension RateDriverVC: LoaderDelegate {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        dismiss(animated: true, completion: nil)
        Messages.instance.showConfigMessage(title: "", body: msg, state: .info, layout: .centeredView, style: .center)
    }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .statusLine, style: .top)
    }
    
    
}
