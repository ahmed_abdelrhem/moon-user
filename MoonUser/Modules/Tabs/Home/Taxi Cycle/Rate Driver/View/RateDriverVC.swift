//
//  RateDriverVC.swift
//  MoonUser
//
//  Created by Grand iMac on 3/8/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import SDWebImage

class RateDriverVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var driverImage: UIImageView!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var commentTxt: UITextView!
    
    //MARK:- Variables
    var presenter: RateDriverVCPresenter!

    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setPresenter()
        DispatchQueue.main.async {
            self.setupUI()
        }
        rateView.didFinishTouchingCosmos = { rating in
            self.presenter.didFinishRating(rating)
        }
    }

    
    
    //MARK:- Functions
    private func setPresenter() {
        presenter = RateDriverVCPresenter(view: self)
    }

    private func setupUI() {
        let imgURL = URL(string: driverData.image ?? "")
        self.driverImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        driverImage.sd_setImage(with: imgURL, placeholderImage: .avatar_image)
        nameLbl.text = driverData.name ?? ""
        let price = driverData.price ?? "0"
        priceLbl.text = "\(price) \(DEF.country.currency)"
    }

    @IBAction func sendFeedbackBtnClicked(_ sender: UIButton) {
        presenter.sendFeedbackBtnClicke(comment: commentTxt.text)
    }
    
    

}
