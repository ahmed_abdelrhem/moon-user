//
//  CancelTripVC.swift
//  MoonUser
//
//  Created by Grand iMac on 3/5/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

protocol CancelTripVCDelegate: class {
    func retriveReason(_ reson: String)
}

class CancelTripVC: BaseViewController {

    //MARK:- Outlets
    @IBOutlet weak var reasonTxt: UITextView!
    
    
    //MARK:- Variables
    var delegate: CancelTripVCDelegate?

    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
    }

    
    
    //MARK:- Actions
    @IBAction func keepRideBtnClicked(_ sender: UIButton) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func cancelRideBtnClicked(_ sender: UIButton) {
        delegate?.retriveReason(reasonTxt.text)
        dismiss(animated: false, completion: nil)
    }
    


}
