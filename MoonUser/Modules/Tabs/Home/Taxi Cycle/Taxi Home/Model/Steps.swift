/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Steps : Codable {
	let travel_mode : String?
	let start_location : Start_location?
	let end_location : End_location?
	let polyline : Polyline?
	let duration : Duration?
	let html_instructions : String?
	let distance : Distance?

	enum CodingKeys: String, CodingKey {

		case travel_mode = "travel_mode"
		case start_location = "start_location"
		case end_location = "end_location"
		case polyline = "polyline"
		case duration = "duration"
		case html_instructions = "html_instructions"
		case distance = "distance"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		travel_mode = try values.decodeIfPresent(String.self, forKey: .travel_mode)
		start_location = try values.decodeIfPresent(Start_location.self, forKey: .start_location)
		end_location = try values.decodeIfPresent(End_location.self, forKey: .end_location)
		polyline = try values.decodeIfPresent(Polyline.self, forKey: .polyline)
		duration = try values.decodeIfPresent(Duration.self, forKey: .duration)
		html_instructions = try values.decodeIfPresent(String.self, forKey: .html_instructions)
		distance = try values.decodeIfPresent(Distance.self, forKey: .distance)
	}

}