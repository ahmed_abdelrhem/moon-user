//
//  TaxiHomeVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 3/4/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import GoogleMaps
import Alamofire

class TaxiHomeVCAPI: NSObject {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    
    typealias directionModelData = (DirectionModelData?, Error?)->()
    typealias statusModel = (StatusModel)->()
    
    
    //TODO:- Draw Path between two Locations with way points
    func drawPathWithPoints(startLocation: CLLocationCoordinate2D, endLocation: CLLocationCoordinate2D, waypoints: [String], completion: @escaping directionModelData) {
        
        let origin = "\(startLocation.latitude),\(startLocation.longitude)"
        let destination = "\(endLocation.latitude),\(endLocation.longitude)"
        
        let baseURLDirections = "https://maps.googleapis.com/maps/api/directions/json?"
        var directionsURLString = baseURLDirections + "origin=\(origin)&destination=\(destination)"
        
        if waypoints.count > 0 {
            directionsURLString += "&waypoints=optimize:true"
            for waypoint in waypoints {
                directionsURLString += "%7C" + waypoint
            }
        }
        directionsURLString += "&sensor=false&mode=driving&key=\(GOOGLE_API_KEY)"
                
        Alamofire.request(directionsURLString).responseJSON { response in
            
            switch response.result {
            case .success( _):
                guard let data = response.data else { return }
                do {
                    let points = try JSONDecoder().decode(DirectionModelData.self, from: data)
                    completion(points, nil)
                } catch let jsonError {
                    completion(nil, jsonError)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    
    func createTripNow(type: TimeType, distance: Double, duration: Int, total: Double, addresses: [String], locations: [CLLocationCoordinate2D], date: String, time: String, didDataReady: @escaping statusModel, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        var param = [
            "type": type.rawValue,
            "trip_type": DEF.type,
            "distance": distance,
            "trip_time": duration,
            "total_price": total
        ] as [String : Any]
        
        for x in 0..<locations.count {
            param["address[\(x)]"] = addresses[x]
            param["lat[\(x)]"] = locations[x].latitude
            param["lng[\(x)]"] = locations[x].longitude
        }
        
        if type == .Scheduled {
            param["date"] = date
            param["time"] = time
        }
        
        apiManager.contectToApiWith(url: URLs.createTrip.url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                if let result = try? self.decoder.decode(StatusModel.self, from: data) {
                    didDataReady(result)
                } else {
                    print("confirm Order error")
                }
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }

        

    }
    
}
