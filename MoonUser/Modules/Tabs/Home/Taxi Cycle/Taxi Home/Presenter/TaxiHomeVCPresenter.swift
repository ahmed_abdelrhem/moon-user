//
//  TaxiHomeVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 3/4/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import GoogleMaps


protocol TaxiHomeVCView: LoaderDelegate {
    func changeConfirmButtonTitle(to title: String)
    func removeScheduleTimeView()
    func presentGMSAutocompleteViewController()
    func setPickupLocation(_ address: String)
    func setDestination(_ address: String)
    func presentScheduleTimeViewController()
    func drawRoute(_ camera: GMSCameraPosition)
}

class TaxiHomeVCPresenter {
    
    private weak var view: TaxiHomeVCView?
    private let interactor = TaxiHomeVCAPI()
    
    public private(set) var editing: TextFiledEditingType = .pickup
    public private(set) var locations = [kCLLocationCoordinate2DInvalid, kCLLocationCoordinate2DInvalid]
    public private(set) var isOrder: Bool = false
    private var tripType: TimeType = .Urgent
    private var scheduleDate: String = ""
    private var scheduleTime: String = ""
    
    
    init(view: TaxiHomeVCView) {
        self.view = view
    }
    
    
    
        
    func removeScheduleTimeBtnClicked() {
        tripType = .Urgent
        view?.removeScheduleTimeView()
        scheduleDate = ""
        scheduleTime = ""
    }
    
    func addressTextFiledEditingBegin() {
        view?.presentGMSAutocompleteViewController()
    }
    
    
    func RetriveSelectedLocation(add: String, location: CLLocationCoordinate2D, editing: TextFiledEditingType) {
        isOrder = false
        view?.changeConfirmButtonTitle(to: CONFIRM_DESTINATION)
        if editing.index < locations.count {
            locations[editing.index] = location
            self.editing = editing
        }
        
        switch self.editing {
        case .pickup:
            view?.setPickupLocation(add)
            break
        case .destination:
            view?.setDestination(add)
            break
        }
    }
    
    func drawPath(completion: @escaping (DirectionModelData?, Error?)->Void) {
        
        var waypoints = [String]()
        if locations.count > 2 {
            for x in 1..<locations.count-1 {
                let loc = locations[x]
                if CLLocationCoordinate2DIsValid(loc) {
                    let locationAsString = "\(loc.latitude),\(loc.longitude)"
                    waypoints.append(locationAsString)
                }
            }
        }
        
        //get the last valid locations and set it as end location
        let size = locations.count
        var end: CLLocationCoordinate2D?
        if CLLocationCoordinate2DIsValid(locations[size-1]) {
            end = locations[size-1]
        } else if CLLocationCoordinate2DIsValid(locations[size-2]) {
            end = locations[size-2]
        } else {
            end = locations[1]
        }
        
        //Draw Path
        interactor.drawPathWithPoints(startLocation: locations[0], endLocation: end!, waypoints: waypoints) { (directionData, error) in
            completion(directionData, error)
        }
    }
    
        
    func scheduleBtnClicked() {
        view?.presentScheduleTimeViewController()
    }
    
    func setScheduleTime(date: String, time: String) {
        tripType = .Scheduled
        scheduleDate = date
        self.scheduleTime = time
    }
    
    func confirmBtnClicked(pickupTxt: UITextField, destTxt: UITextField, distance: Double, duration: Int, totalPrice: Double) {
        
        var addresses = [String]()

        guard let pickup = pickupTxt.text, !pickupTxt.isEmpty, let dest1 = destTxt.text, !destTxt.isEmpty else {
            UITextField.textEmptyValidation([pickupTxt, destTxt])
            return
        }
        addresses.append(pickup)
        addresses.append(dest1)
                
        var newLocations = [CLLocationCoordinate2D]()
        for loc in locations {
            if CLLocationCoordinate2DIsValid(loc) {
                newLocations.append(loc)
            }
        }
        
        if !isOrder {
            isOrder = true
            view?.changeConfirmButtonTitle(to: CONFIRM_NOW)
            let camera = GMSCameraPosition.camera(withTarget: locations[0], zoom: 15)
            view?.drawRoute(camera)
            
        } else {
            
            view?.showProgress()
                        
            interactor.createTripNow(type: self.tripType, distance: distance, duration: duration, total: totalPrice, addresses: addresses, locations: newLocations, date: scheduleDate, time: scheduleTime, didDataReady: { [weak self](response) in
                guard self != nil else { return }
                self?.view?.hideProgress()
                if response.status == StatusCode.Success.rawValue {
                    self?.view?.onSuccess(response.message ?? "")
                } else if response.status == StatusCode.ExpiredJWT.rawValue {
                    Messages.instance.loginMessage()
                } else {
                    self?.view?.onFailure(response.message ?? "")
                }
            }) { [weak self](error) in
                guard self != nil else { return }
                self?.view?.hideProgress()
                self?.view?.onFailure(CONNECTION_ERROR)
            }
            
        }
        
    }
    
}
