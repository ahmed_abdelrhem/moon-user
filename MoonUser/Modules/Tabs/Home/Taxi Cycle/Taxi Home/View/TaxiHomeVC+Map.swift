//
//  TaxiHomeVC+Map.swift
//  MoonUser
//
//  Created by Grand iMac on 3/4/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import GoogleMaps
import GooglePlaces


//MARK:- Maps Helper Funcs
extension TaxiHomeVC: CLLocationManagerDelegate, GMSMapViewDelegate {
    
    internal func locationAuth() {
        mapView = GMSMapView()
        pickMarker = GMSMarker()
        destMarker = GMSMarker()

        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined, .restricted, .denied:
            self.NoPremissionToAccessLocation()
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        @unknown default:
            fatalError()
        }
    }
    
    fileprivate func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if (status == .notDetermined) || (status == .restricted) || (status == .denied) {
            self.NoPremissionToAccessLocation()
        } else if (status == .authorizedAlways) || (status == .authorizedWhenInUse) {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        if(!updateLocationBefor && manager.location?.coordinate != nil) {
            updateLocationBefor = true
            presenter.RetriveSelectedLocation(add: "", location: locValue, editing: self.editingType)
            self.MapSetup(lat:"\((locValue.latitude))", long: "\((locValue.longitude))")
        }
    }
    
    
    internal func MapSetup(lat: String , long: String) {
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(lat)!, longitude: CLLocationDegrees(long)!, zoom: 14)
        
        let f: CGRect = view.frame
        let mapFrame = CGRect(x: f.origin.x, y: 0, width: f.width, height: f.size.height)
        mapView = GMSMapView.map(withFrame: mapFrame, camera: camera)
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        mapView.padding = UIEdgeInsets(top: 150, left: 0, bottom: 120, right: 0)
        self.myMapView.addSubview(mapView)
        mapView.delegate = self
        
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "MapStyle", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
    
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
        let latitude = mapView.camera.target.latitude
        let longitude = mapView.camera.target.longitude
        
        //To Set the Marker of selected location on the center of the map
        centerMapCoordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        if !self.presenter.isOrder {
            if presenter.editing == .pickup {
                self.pickupMarker(start: centerMapCoordinate)
            } else {
                self.destinationMarker(destination: centerMapCoordinate)
            }
        }
    }

    
    internal func pickupMarker(start location : CLLocationCoordinate2D?){
        pickMarker.position = location!
        pickMarker.map = mapView
        pickMarker.icon = .pinImage
    }
    
    internal func destinationMarker(destination location : CLLocationCoordinate2D?) {
        destMarker.position = location!
        destMarker.map = mapView
        destMarker.icon = .pinImage
    }
    

    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        returnPostionOfMapView(mapView: mapView)
    }
    
    fileprivate func returnPostionOfMapView(mapView:GMSMapView) {
        let geocoder = GMSGeocoder()
        let latitute = mapView.camera.target.latitude
        let longitude = mapView.camera.target.longitude
        let location = CLLocationCoordinate2D(latitude: latitute, longitude: longitude)

        let position = CLLocationCoordinate2DMake(latitute, longitude)
        geocoder.reverseGeocodeCoordinate(position) { response , error in
            if error != nil {
                print("Error: \(String(describing: error?.localizedDescription))")
            } else {
                let result = response?.results()?.first
                let address = result?.lines?.reduce("") { $0 == "" ? $1 : $0 + ", " + $1 }
                if !self.presenter.isOrder {
                    self.presenter.RetriveSelectedLocation(add: address ?? "", location: location, editing: self.presenter.editing)
                }
            }
        }
    }
    
    //Set up map To draw route
    func setUpMap(_ camera: GMSCameraPosition) {
        
        self.startAnimating()
        
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.camera = camera
        mapView.accessibilityElementsHidden = false
        mapView.isMyLocationEnabled = true
        mapView.settings.indoorPicker = true
        self.myMapView.addSubview(mapView)
        
        presenter.drawPath() { (directionData, error) in
            if let error = error {
                print(error)
            } else {
                guard let directionData = directionData else { return }
                let routeOverviewPolyline = directionData.routes
                for route in routeOverviewPolyline! {
                    let poverviewPolylineoints = route.overview_polyline
                    let points = poverviewPolylineoints?.points
                    let path = GMSPath.init(fromEncodedPath: points!)
                    self.polyline = GMSPolyline.init(path: path)
                    self.polyline.strokeWidth = 3
                    self.polyline.strokeColor = UIColor.darkGray
                    self.polyline.map = self.mapView

                    self.totalDistance = Double((routeOverviewPolyline?[0].legs?[0].distance?.value)!) /  1000
                    self.totalDuration = Int((routeOverviewPolyline?[0].legs?[0].duration?.value)!) 


                    // MARK:- MAPVIEW ZOOM OUT TO SHOW LINE
                    DispatchQueue.main.async {
                        if self.mapView != nil {
                            let bounds = GMSCoordinateBounds(path: path!)
                            self.mapView!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 80))
                            self.totalPrice = (DEF.userData.taxi_cost * self.totalDistance) + DEF.userData.start_counter
                            self.totalPrice = self.totalPrice.rounded(.up)
                            self.tripPriceLbl.text = "\(self.totalPrice) \(DEF.country.currency)"
                            //Show Views
                            UIView.animate(withDuration: self.animationDurations, animations: {
                                self.mapView.padding = UIEdgeInsets(top: 150, left: 0, bottom: 140, right: 0)
                                self.costView.isHidden = false
                            })
                        }
                    }
                }
            }
            self.stopAnimating()
        }
    }
    
    
}


//MARK:- Places Autocomplete
extension TaxiHomeVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        //set address coordinates on the map
        let address = "\(String(describing: place.formattedAddress!))"
        let latitute = place.coordinate.latitude
        let longitude = place.coordinate.longitude
        let location = CLLocationCoordinate2D(latitude: latitute, longitude: longitude)
        DispatchQueue.main.async {
            //Zoom in with Animation to selected location
            let camera = GMSCameraPosition(target: location, zoom: 16)
            self.mapView.animate(to: camera)
            self.presenter.RetriveSelectedLocation(add: address, location: location, editing: self.editingType)
            self.setupUI()
        }
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    
}
