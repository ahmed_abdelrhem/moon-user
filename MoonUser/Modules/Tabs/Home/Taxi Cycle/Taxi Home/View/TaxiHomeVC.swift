//
//  TaxiHomeVC.swift
//  MoonUser
//
//  Created by Grand iMac on 3/4/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import GoogleMaps


enum TextFiledEditingType {
    case pickup
    case destination
    
    var index: Int {
        switch self {
        case .pickup:
            return 0
        case .destination:
            return 1
        }
    }
}

enum TimeType: String {
    case Urgent = "urgent"
    case Scheduled = "scheduled"
}




class TaxiHomeVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var myMapView: UIView!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var scheduleTimeView: UIView!
    @IBOutlet weak var scheduleTimeTxt: UITextField!
    @IBOutlet weak var pickupLocationTxt: UITextField!
    @IBOutlet weak var destinationTxt: UITextField!
    @IBOutlet weak var costView: UIView!
    @IBOutlet weak var tripPriceLbl: UILabel!

    
    //MARK:- Variables
    internal var presenter: TaxiHomeVCPresenter!
    var updateLocationBefor = false
    var animationDurations = 0.3
    var type: TaxiIntroVCType!
    var editingType: TextFiledEditingType = .pickup
    var driverID: Int?
    
    
    //Google Map
    let locationManager = CLLocationManager()
    var mapView: GMSMapView!
    var centerMapCoordinate: CLLocationCoordinate2D!
    var pickMarker: GMSMarker!
    var destMarker: GMSMarker!
    var polyline = GMSPolyline()
    var animationPath = GMSMutablePath()
    let animationPolyline = GMSPolyline()
    var timerAnimation: Timer!
    
    var totalPrice = 0.0
    var totalDistance: Double = 0
    var totalDuration: Int = 0

    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setPresenter()
        locationAuth()
        setupUI()
    }
    
    

    //MARK:- Function
    private func setPresenter() {
        presenter = TaxiHomeVCPresenter(view: self)
    }
        
        
    func NoPremissionToAccessLocation() {
        self.MapSetup(lat:"0.0", long: "0.0")
        Messages.instance.actionsConfigMessage(title: "", body: TURN_LOCATION, buttonTitle: SEETING) { (pressed) in
            if pressed {
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)! as URL, options: [:], completionHandler: nil)
            }
        }
    }
    
    
    internal func setupUI() {
        backButtonTitle("")
        title = type.mapTitle
        setNavigationBarIsTransparent()
        scheduleTimeView.isHidden = true
        costView.isHidden = true
        pickupLocationTxt.delegate = self
        destinationTxt.delegate = self
        polyline.map = nil // delete route
    }
    
    
    //MARK:- Actions
    @IBAction func removeScheduleTimeBtnClicked(_ sender: UIButton) {
        presenter.removeScheduleTimeBtnClicked()
    }
    
    @IBAction func pickupLocationTextFiledEditingBegin(_ sender: UITextField) {
        self.editingType = .pickup
        presenter.addressTextFiledEditingBegin()
    }
    
    @IBAction func destinationTextFiledEditingBegin(_ sender: UITextField) {
        self.editingType = .destination
        presenter.addressTextFiledEditingBegin()
    }
        
    @IBAction func scheduleBtnClicked(_ sender: UIButton) {
        presenter.scheduleBtnClicked()
    }
    
    @IBAction func confirmBtnClicked(_ sender: UIButton) {
        presenter.confirmBtnClicked(pickupTxt: pickupLocationTxt, destTxt: destinationTxt, distance: totalDistance, duration: totalDuration, totalPrice: totalPrice)
    }
    
}




//MARK:- ScheduleTimeVC Delegate
extension TaxiHomeVC: ScheduleTimeVCDelegate {
    
    func retriveDate(_ date: String, backendDate: String, backendTime: String) {
        UIView.animate(withDuration: 0.3) {
            self.scheduleTimeView.isHidden = false
            self.scheduleTimeTxt.text = date
        }
        presenter.setScheduleTime(date: backendDate, time: backendTime)
    }
}


//MARK:- UITextFiled Delegate
extension TaxiHomeVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.layer.borderColor = UIColor.clear.cgColor
    }
}


