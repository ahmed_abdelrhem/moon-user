//
//  TaxiHomeVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 3/4/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import GoogleMaps
import GooglePlaces

extension TaxiHomeVC: TaxiHomeVCView {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        DispatchQueue.main.async {
            homeScreen(msg)
        }
    }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .info, layout: .messageView, style: .top)
    }
    
    func changeConfirmButtonTitle(to title: String) {
        confirmBtn.setTitle(title, for: .normal)
    }
    
    
    func removeScheduleTimeView() {
        UIView.animate(withDuration: animationDurations) {
            self.scheduleTimeTxt.text = ""
            self.scheduleTimeView.isHidden = true
        }
    }
    
    func presentGMSAutocompleteViewController() {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    func setPickupLocation(_ address: String) {
        pickupLocationTxt.text = address
        pickupMarker(start: presenter.locations[0])
        pickMarker.snippet = address
    }
    
    func setDestination(_ address: String) {
        destinationTxt.text = address
        destinationMarker(destination: presenter.locations[1])
        destMarker.snippet = address
    }
        
    
    func presentScheduleTimeViewController() {
        let vc = ScheduleTimeVC()
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: true, completion: nil)
    }
    
    func drawRoute(_ camera: GMSCameraPosition) {
        self.setUpMap(camera)
    }

    
}
