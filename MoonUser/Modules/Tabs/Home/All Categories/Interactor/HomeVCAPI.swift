//
//  HomeVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 1/26/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class HomeVCAPI {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias model = (HomeResponseModel)->()
    
    func getHomeData( didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        apiManager.contectToApiWith(url: URLs.homeServices(DEF.country.id).url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(HomeResponseModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
}
