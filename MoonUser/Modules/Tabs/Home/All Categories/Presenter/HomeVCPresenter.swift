//
//  HomeVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 1/26/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol HomeVCView: LoaderDelegate {
    func setSlideShow(_ images: [String])
    func navigateToAllStoriesVC()
    func navigateToSingleCategoryVC()
    func navigateToSingleCommercialVC()
    func navigateToIntroVC(_ type: IntroType)
    func navigateToPhotographersVC(type: DiscoversType, _ title: String)
    func navigateToSingleInstituationVC(_ type: DiscoversType)
    func navigateToTaxiIntro(_ type: TaxiIntroVCType)
    func presentStoriesViewController(_ stories: [UserStories], index: Int)
}

protocol StoryCellConfigure {
    func configure(image: String?, name: String?)
}

class HomeVCPresenter {
    
    private weak var view: HomeVCView?
    private let interactor = HomeVCAPI()
    private var categories = [HomeCategory]()
    private var sliders = [HomeSliders]()
    private var stories = [UserStories]()

    
    
    init(view: HomeVCView) {
        self.view = view
    }
    
    func viewDidlLoad() {
        
        view?.showProgress()
        
        interactor.getHomeData(didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.categories = response.data ?? []
                self?.sliders = response.sliders ?? []
                self?.stories = response.stories ?? []
                var images = [String]()
                for s in self!.sliders {
                    images.append(s.image ?? "")
                }
                self?.view?.onSuccess("")
                self?.view?.setSlideShow(images)
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
    
    func collectionCount(_ tag: Int) -> Int {
        if tag == 0 {
            return stories.count == 0 ? 0 : stories.count + 1
        } else {
            return categories.count
        }
    }
    
    func storyCellCongifure(cell: StoryCellConfigure,  index: Int) {
        if index != stories.count {
            cell.configure(image: stories[index].image, name: stories[index].name)
        }
    }
    
    func categoryCellCongifure(cell: StoryCellConfigure,  index: Int) {
        cell.configure(image: categories[index].image, name: categories[index].name)
    }
    
    
    func didSelectRowAt(tag: Int, index: Int) {
        if tag == 0 {
            if index == stories.count {
                self.view?.navigateToAllStoriesVC()
            } else {
                self.view?.presentStoriesViewController(self.stories, index: index)
            }
        } else {
            let type = categories[index].type ?? 0
            let flag = categories[index].flag ?? 0

            DEF.type = type
            DEF.flag = flag
            
            switch type {
            case 1, 2, 3:
                view?.navigateToSingleCategoryVC()
                
            case 4:
                view?.navigateToPhotographersVC(type: .Famous, FAMOUS)
                
            case 5:
                view?.navigateToPhotographersVC(type: .Photographer, PHOTOGRAPHERS)
                
            case 6:
                view?.navigateToSingleCommercialVC()

            case 7:
                view?.navigateToTaxiIntro(.Taxi)

            case 8:
                view?.navigateToIntroVC(.Institution)
                
            case 9:
                view?.navigateToSingleInstituationVC(.Clinic)

            case 10:
                view?.navigateToSingleInstituationVC(.Salon)

            case 11:
                view?.navigateToIntroVC(.Advertising)

            case 12:
                view?.navigateToTaxiIntro(.Truck)
                
            default:
                break
            }
        }
    }
    
}
