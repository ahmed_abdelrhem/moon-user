//
//  HomeVC.swift
//  MoonUser
//
//  Created by Grand iMac on 1/26/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import ImageSlideshow


class HomeVC: BaseViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var storyCollectionView: UICollectionView! {
        didSet {
            storyCollectionView.tag = 0
            storyCollectionView.delegate = self
            storyCollectionView.dataSource = self
            storyCollectionView.register(UINib(nibName: "StoryCell", bundle: nil), forCellWithReuseIdentifier: "StoryCell")
        }
    }
    @IBOutlet weak var categoryCollectionView: UICollectionView! {
        didSet {
            categoryCollectionView.tag = 1
            categoryCollectionView.delegate = self
            categoryCollectionView.dataSource = self
            categoryCollectionView.register(UINib(nibName: "CategoryCell", bundle: nil), forCellWithReuseIdentifier: "CategoryCell")
            self.observer = categoryCollectionView.observe(\.contentSize) { (collectioView, _) in
                self.categoryCollectionViewHeight.constant = self.categoryCollectionView.contentSize.height
            }
        }
    }
    @IBOutlet weak var sliderView: ImageSlideshow!
    @IBOutlet weak var categoryCollectionViewHeight: NSLayoutConstraint!
    
    
    //MARK:- Variables
    internal var presenter: HomeVCPresenter!
    var observer: NSKeyValueObservation!
    
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        SharedHandller.instance.sideMenus(self)
        setPresenter()        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setChatHistoryBadgeValue()
    }
    
    
    
    //MARK:- Functions
    private func setPresenter() {
        presenter = HomeVCPresenter(view: self)
        presenter.viewDidlLoad()
    }
    
   
        
    
}

