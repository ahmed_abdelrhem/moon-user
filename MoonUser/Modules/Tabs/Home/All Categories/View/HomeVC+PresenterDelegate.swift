//
//  HomeVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 1/26/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension HomeVC: HomeVCView {
    
    func showProgress() {
        startAnimating()
    }
    
    func hideProgress() {
        stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        storyCollectionView.reloadData()
        categoryCollectionView.reloadData()
    }
    
    func setSlideShow(_ images: [String]) {
        SharedHandller.instance.setSlider(self, slideShow: sliderView, images: images)
    }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func navigateToAllStoriesVC() {
        let vc = AllStoriesMapVC()
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
        
    func navigateToSingleCategoryVC() {
        let vc = SingleCategoryVC()
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToSingleCommercialVC() {
        let vc = SingleCommercialVC()
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToIntroVC(_ type: IntroType) {
        let vc = InstituationIntroVC()
        vc.type = type
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToPhotographersVC(type: DiscoversType, _ title: String) {
        let vc = PhotographerVC()
        vc.title = title
        vc.type = type
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToSingleInstituationVC(_ type: DiscoversType) {
        let vc = SingleInstituationVC()
        vc.type = type
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToTaxiIntro(_ type: TaxiIntroVCType) {
        let vc = TaxiIntroVC()
        vc.type = type
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func presentStoriesViewController(_ stories: [UserStories], index: Int) {
        DispatchQueue.main.async {
            let storyPreviewScene = IGStoryPreviewController.init(stories: stories, handPickedStoryIndex:  index)
            self.present(storyPreviewScene, animated: true, completion: nil)
        }
    }
    
}
