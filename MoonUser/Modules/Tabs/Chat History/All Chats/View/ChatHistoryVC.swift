//
//  ChatHistoryVC.swift
//  MoonUser
//
//  Created by Grand iMac on 3/9/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class ChatHistoryVC: BaseViewController {

    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView(frame: .zero)
            tableView.delegate = self
            tableView.dataSource = self
            tableView.separatorStyle = .singleLine
            tableView.register(UINib(nibName: "ChatCell", bundle: nil), forCellReuseIdentifier: "ChatCell")
        }
    }
    
    
    //MARK:- Variables
    internal var presenter: ChatHistoryVCPresenter!

    
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        SharedHandller.instance.sideMenus(self)
        setPresenter()
        listenChatNotificationCenter()
    }
    
    
    //MARK: Functions
    private func setPresenter() {
        presenter = ChatHistoryVCPresenter(view: self)
        presenter.viewDidLoad()
    }
    
    func listenChatNotificationCenter() {
        _NC.addObserver(forName: .chatNewMessage, object: nil, queue: nil) { _ in
            self.presenter.viewDidLoad()
        }
    }
    

   

}




/*

import UIKit

protocol SPKRecordViewDelegate: class {
    
    func SPKRecordViewDidSelectRecord(sender : SPKRecordView, button: UIView)
    func SPKRecordViewDidStopRecord(sender : SPKRecordView, button: UIView)
    func SPKRecordViewDidCancelRecord(sender : SPKRecordView, button: UIView)
    
}

class SPKRecordView: UIView {
    
    enum SPKRecordViewState {
        
        case Recording
        case None
        
    }
    
    var state : SPKRecordViewState = .None {
        
        didSet {
            
            UIView.animate(withDuration: 0.3) { () -> Void in
                
                self.slideToCancel.alpha = 1.0
                self.invalidateIntrinsicContentSize()
                self.setNeedsLayout()
                self.layoutIfNeeded()
                
            }
            
        }
    }
    
    let recordButton : UIButton = UIButton(type: .custom)
    let slideToCancel : UILabel = UILabel(frame: CGRect.zero)
    
    weak var delegate : SPKRecordViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
        
        setupRecordButton()
        setupLabel()
    }
    
    func setupRecordButton() {
        
        recordButton.translatesAutoresizingMaskIntoConstraints = false
        addSubview(recordButton)
        let hConsts = NSLayoutConstraint.constraints(withVisualFormat: "H:[recordButton]|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["recordButton":recordButton])
        self.addConstraints(hConsts)
        
        let vConsts = NSLayoutConstraint.constraints(withVisualFormat: "V:|[recordButton]|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["recordButton":recordButton])
        self.addConstraints(vConsts)
        
        recordButton.setImage(UIImage(named: "microphone")!, for: .normal)
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(userDidTapRecord(_:)))
        longPress.cancelsTouchesInView = false
        longPress.allowableMovement = 10
        longPress.minimumPressDuration = 0.2
        recordButton.addGestureRecognizer(longPress)
        
    }
    
    func setupLabel() {
        
        slideToCancel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(slideToCancel)
        backgroundColor = UIColor.clear
        
        let hConsts = NSLayoutConstraint.constraints(withVisualFormat: "H:|[slideToCancel][bt]", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["slideToCancel":slideToCancel,"bt":recordButton])
        self.addConstraints(hConsts)
        
        let vConsts = NSLayoutConstraint.constraints(withVisualFormat: "V:|[slideToCancel]|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["slideToCancel":slideToCancel])
        self.addConstraints(vConsts)
        
        slideToCancel.alpha = 0.0
        slideToCancel.font = UIFont(name: "Lato-Bold", size: 17)
        slideToCancel.textAlignment = .center
        slideToCancel.textColor = UIColor.black
    }
    
    
    override public var intrinsicContentSize: CGSize {
        if state == .none {
            return recordButton.intrinsicContentSize
        } else {
            
            return CGSize(width: recordButton.intrinsicContentSize.width * 3, height: recordButton.intrinsicContentSize.height)
        }
    }
    
    func userDidTapRecordThenSwipe(sender: UIButton) {
        slideToCancel.text = nil
        delegate?.SPKRecordViewDidCancelRecord(sender: self, button: sender)
    }
    
    func userDidStopRecording(sender: UIButton) {
        slideToCancel.text = nil
        delegate?.SPKRecordViewDidStopRecord(sender: self, button: sender)
    }
    
    func userDidBeginRecord(sender : UIButton) {
        slideToCancel.text = "Slide to cancel <"
        delegate?.SPKRecordViewDidSelectRecord(sender: self, button: sender)
        
    }
    
    @objc func userDidTapRecord(_ sender: UIGestureRecognizer) {
        
        print("Long Tap")
        
        let button = sender.view as! UIButton
        
        let location = sender.location(in: button)
        
        var startLocation = CGPoint.zero
        
        switch sender.state {
            
        case .began:
            startLocation = location
            userDidBeginRecord(sender: button)
        case .changed:
            
            let translate = CGPoint(x: location.x - startLocation.x, y: location.y - startLocation.y)
            
            if !button.bounds.contains(translate) {
                
                if state == .Recording {
                    userDidTapRecordThenSwipe(sender: button)
                }
            }
            
        case .ended:
            
            if state == .None { return }
            
            let translate = CGPoint(x: location.x - startLocation.x, y: location.y - startLocation.y)
            
            if !button.frame.contains(translate) {
                
                userDidStopRecording(sender: button)
                
            }
            
        case .failed, .possible ,.cancelled :
            
            if state == .Recording {
                userDidStopRecording(sender: button)
            }
            else {
                userDidTapRecordThenSwipe(sender: button)
            }
        @unknown default:
            fatalError()
        }
        
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ChatHistoryVC : SPKRecordViewDelegate {
    
    func SPKRecordViewDidCancelRecord(sender: SPKRecordView, button: UIView) {
        
        sender.state = .None
        
        print("Cancelled")
    }
    
    func SPKRecordViewDidSelectRecord(sender: SPKRecordView, button: UIView) {
        
        sender.state = .Recording
        
        print("Began " + NSUUID().uuidString)
        
    }
    
    func SPKRecordViewDidStopRecord(sender : SPKRecordView, button: UIView) {
        
        sender.state = .None
        
        print("Done")
    }
}



*/
