//
//  ChatHistoryVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 3/9/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation


extension ChatHistoryVC: ChatHistoryVCView {
    
    
    func showProgress() {
        startAnimating()
    }
    
    func hideProgress() {
        stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        tableView.reloadData()
    }
    
    func onEmpty(_ img: UIImage, _ msg: String) {
        tableView.setEmptyView(title: msg, messageImage: img)
    }
    
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func navigateToChatPageVC(reciverType: Int, id: Int) {
        let vc = ChatPageVC()
        vc.reciverType = reciverType
        vc.id = id
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func changeChatHistoryBadgeValue() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.setChatHistoryBadgeValue()
        }
    }
    
}
