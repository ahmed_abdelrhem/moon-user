//
//  ChatHistoryVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 3/9/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation


protocol ChatHistoryVCView: LoaderDelegate {
    func navigateToChatPageVC(reciverType: Int, id: Int)
    func changeChatHistoryBadgeValue()
}

class ChatHistoryVCPresenter {
    
    private weak var view: ChatHistoryVCView?
    private let interactor = ChatHistoryVCAPI()
    private var chats = [UserReview]()
    
    
    init(view: ChatHistoryVCView) {
        self.view = view
    }
    
    func viewDidLoad() {
                
        if DEF.isLogin  {
            view?.showProgress()

            interactor.getAllChats(didDataReady: { [weak self](response) in
                guard self != nil else { return }
                self?.view?.hideProgress()
                if response.status == StatusCode.Success.rawValue {
                self?.chats = response.data ?? []
                if self?.chats.count == 0 {
                    self?.view?.onEmpty(.no_data_image, NO_CHATS)
                    } else {
                        self?.view?.onSuccess("")
                    }
                } else if response.status == StatusCode.ExpiredJWT.rawValue {
                    Messages.instance.loginMessage()
                } else {
                    self?.view?.onFailure(response.message ?? "")
                }
            }) { [weak self](error) in
                guard self != nil else { return }
                self?.view?.hideProgress()
                self?.view?.onEmpty(.no_connection_image, CONNECTION_ERROR)
            }
        } else {
            self.view?.onEmpty(.no_data_image, PLEASE_LOGIN)
        }
    }
    
    var count: Int {
        return chats.count
    }
    
    func cellConfigure(cell: ChatCellView, at index: Int) {
        let chat = chats[index]
        cell.configure(image: chat.image, name: chat.name, unReadMessages: chat.chats_count)
    }
    
    func didSelectRow(at indexPath: IndexPath) {
        let id = chats[indexPath.row].id ?? 0
        let type = chats[indexPath.row].type ?? 0
        view?.navigateToChatPageVC(reciverType: type, id: id)
        
        //To change chats badge value
        DispatchQueue.global(qos: .background).async {
            if self.chats[indexPath.row].chats_count != 0 {
                self.chats[indexPath.row].chats_count = 0
                DEF.userData.chats_count -= (self.chats[indexPath.row].chats_count ?? 0)
                self.view?.changeChatHistoryBadgeValue()
            }
        }
    }
    
    
}
