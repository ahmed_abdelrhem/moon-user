//
//  ChatPageVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 3/9/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import AVFoundation

enum RecordStatus {
    case stop
    case play
    case reRecord
}


protocol ChatPageVCView: LoaderDelegate {
    func recordBtn(_ flag: RecordStatus)
    func loadRecordingUI()
    func audioPlayerDidFinishPlayingUI()
    func updateTimer(_ second:Int,_ min:Int)
}


class ChatPageVCPresenter: NSObject {
    
    internal weak var view: ChatPageVCView?
    private let interactor = ChatPageVCAPI()
    private var messages = [SingleMessage]()
    private var user: UserReview?
    private var delegate: UserReview?
    private var type: ChatType = .Chat
    private var id: Int = 0
    private var receiverType: Int = 0
    private var receiverID: Int?

    internal var timer = Timer()
    internal var isPlaying = false
    internal var audioData : Data?
    internal var recordingSession: AVAudioSession!
    internal var audioRecorder: AVAudioRecorder!
    internal var audioPlayer:AVAudioPlayer!
    var seconds = 0
    var minutes = 0

    
    
    init(view: ChatPageVCView, type: ChatType, id: Int, receiverType: Int, receiverID: Int?) {
        self.view = view
        self.type = type
        self.id = id
        self.receiverType = receiverType
        self.receiverID = receiverID
    }
    
    var count: Int {
        return messages.count
    }
    
    func viewDidLoad() {
        view?.showProgress()
        
        interactor.getChat(type: type, id: id, receiverType: receiverType, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.messages = response.chats ?? []
                self?.user = response.user
                self?.delegate = response.delegate
                self?.view?.onSuccess("")
            } else if response.status == StatusCode.ExpiredJWT.rawValue {
                Messages.instance.loginMessage()
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onEmpty(.no_connection_image, CONNECTION_ERROR)
        }
    }
        
    func cellForRowAtIndex(_ tb: UITableView, index: Int) -> UITableViewCell {
        let msg = messages[index]
            
        guard let senderCell = tb.dequeueReusableCell(withIdentifier: "SenderChatCell") as? SenderChatCell else {
            return UITableViewCell()
        }
        senderCell.configure(userImg: user?.image, name: user?.name, text: msg.message, image: msg.image, date: msg.date)
        if msg.message != "" {
            senderCell.setIsText()
        } else if msg.image != "" {
            senderCell.setIsImage()
        }

        guard let senderAudioCell = tb.dequeueReusableCell(withIdentifier: "SenderAudioCell") as? SenderAudioCell else {
            return UITableViewCell()
        }
        senderAudioCell.setAudioPlayer(msg.audio ?? "", userImg: user?.image, date: msg.date)

        
        guard let reciverCell = tb.dequeueReusableCell(withIdentifier: "ReciverChatCell") as? ReciverChatCell else {
            return UITableViewCell()
        }
        reciverCell.configure(userImg: delegate?.image, name: delegate?.name, text: msg.message, image: msg.image, date: msg.date)
        if msg.message != "" {
            reciverCell.setIsText()
        } else if msg.image != "" {
            reciverCell.setIsImage()
        }
        
        guard let receiverAudioCell = tb.dequeueReusableCell(withIdentifier: "ReceiverAudioCell") as? ReceiverAudioCell else {
            return UITableViewCell()
        }
        receiverAudioCell.setAudioPlayer(msg.audio ?? "", userImg: delegate?.image, date: msg.date)

        
        if msg.type == 13 {
            return msg.audio != "" ? senderAudioCell : senderCell
        } else {
            return msg.audio != "" ? receiverAudioCell : reciverCell
        }
    }
    
    
    func imageBtnPressed(_ vc: UIViewController, _ sender: AnyObject) {
        AttachmentHandler.shared.showAttachmentImage(vc, sender)
        
        AttachmentHandler.shared.singleImagePickedBlock = { (image, date) in
            self.sendMessage(img: image, msg: "")
        }
    }
    
    
    func sendBtnPressed(_ msg: String) {
        self.sendMessage(img: nil, msg: msg)
    }
    
    
    func didRecivedNewMessage() {
        if newMessage.id == delegate?.id {
            let newMSG = SingleMessage(id: 0, message: newMessage.body, type: Int(newMessage.sender_type)!, image: newMessage.image, audio: newMessage.audio)
            self.messages.append(newMSG)
            self.view?.onSuccess("")
        }
    }
    
    
    //MARK:- Private Functions
    private func sendMessage(img: UIImage?, msg: String) {
        
        view?.showProgress()

        interactor.sendNewMessage(type: self.type, reciverID: self.receiverID, id: id, receiverType: receiverType, msg: msg, image: img, audio: nil, didDataReady: { [weak self](response) in
              guard self != nil else { return }
              self?.view?.hideProgress()
              if response.status == StatusCode.Success.rawValue {
                  let newMSG = response.data!
                  self?.messages.append(newMSG)
                  self?.view?.onSuccess("")
              } else if response.status == StatusCode.ExpiredJWT.rawValue {
                Messages.instance.loginMessage()
              } else {
                  self?.view?.onFailure(response.message ?? "")
              }
          }) { [weak self](error) in
              guard self != nil else { return }
              self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
          }
    }
    
    func sendAudio() {

        view?.showProgress()
        
        interactor.sendNewMessage(type: self.type, reciverID: self.receiverID, id: id, receiverType: receiverType, msg: "", image: nil, audio: audioData, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                let newMSG = response.data!
                self?.messages.append(newMSG)
                self?.view?.onSuccess("")
            } else if response.status == StatusCode.ExpiredJWT.rawValue {
                Messages.instance.loginMessage()
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }

    
}
