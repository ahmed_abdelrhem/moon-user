//
//  ChatPageVCPresenter+Timer.swift
//  MoonUser
//
//  Created by Islam 3bRahiem on 4/1/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension ChatPageVCPresenter {
    
    internal func startTimer() {
        if(isPlaying) {
            return
        }
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(UpdateTimer), userInfo: nil, repeats: true)
        isPlaying = true
    }
    
    internal  func pauseTimer() {
        timer.invalidate()
        isPlaying = false
    }
    
    internal  func resetTimer() {
        timer.invalidate()
        isPlaying = false
        view?.updateTimer(00, 00)
    }
    
    @objc internal func UpdateTimer() {
        if minutes == 3 {
            seconds = 00
            minutes = 00
        }else{
            if seconds > 59{
                seconds = 00
                minutes = minutes + 1
            }else{
                seconds = seconds + 1
            }
            view?.updateTimer(seconds, minutes)
        }
    }
    
}
