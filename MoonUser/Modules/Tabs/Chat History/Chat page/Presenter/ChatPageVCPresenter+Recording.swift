//
//  ChatPageVCPresenter+Recording.swift
//  MoonUser
//
//  Created by Islam 3bRahiem on 4/1/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import AVFoundation


extension ChatPageVCPresenter: AVAudioRecorderDelegate,AVAudioPlayerDelegate {
    
    func  request_recording_permission() {
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        self.view?.loadRecordingUI()
                    } else {
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }
    }
    
    private  func startRecording() {
        let audioFilename = getFileURL()
        
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        do {
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record(forDuration: 180)
            startTimer()
            print(" #currentTime ==>",  audioRecorder.currentTime)
            view?.recordBtn(.stop)
            //            recordButton.setTitle("Tap to Stop", for: .normal)
        } catch {
            finishRecording(success: false)
        }
    }
    
    func trackRecordTime()  {
    }
    
    func finishRecording(success: Bool) {
        audioRecorder.stop()
        //        audioRecorder = nil
        pauseTimer()
        do{
            audioData = try  Data(contentsOf: getFileURL())
            print("audioData",audioData!)
        }catch{
            fatalError("catch audio block error\(error)")
        }
        if success {
            view?.recordBtn(.reRecord)
        } else {
            view?.recordBtn(.play)
        }
    }
    
    func deleteRecord()  {
        resetTimer()
        audioRecorder.deleteRecording()
        audioRecorder = nil
        audioPlayer = nil
        
    }
    
    private func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    private func getFileURL() -> URL {
        let path = getDocumentsDirectory().appendingPathComponent("recording.m4a")
        return path as URL
    }
    
    func preparePlayer() {
        var error: NSError?
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: getFileURL() as URL)
        } catch let error1 as NSError {
            error = error1
            audioPlayer = nil
        }
        
        if let err = error {
            print("AVAudioPlayer error: \(err.localizedDescription)")
        } else {
            audioPlayer.delegate = self
            audioPlayer.prepareToPlay()
            audioPlayer.volume = 10.0
        }
    }
    func playAudio()  {
        resetTimer()
        startTimer()
        preparePlayer()
        audioPlayer.play()
    }
    func stopAudio()  {
        pauseTimer()
        audioPlayer.stop()
    }
    
    
    func recordTapped() {
        if audioRecorder == nil {
            startRecording()
        } else {
            finishRecording(success: true)
        }
    }
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        print("Error while recording audio \(error!.localizedDescription)")
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        view?.audioPlayerDidFinishPlayingUI()
        resetTimer()
        view?.recordBtn(.play)
    }
    
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print("Error while playing audio \(error!.localizedDescription)")
    }
}
