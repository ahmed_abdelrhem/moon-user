//
//  FirebaseMessage.swift
//  MoonUser
//
//  Created by Grand iMac on 3/10/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

struct FirebaseMessage {
    let id : Int
    let body: String
    let image : String
    let audio : String
    let type_data: Int
    let sender_type: String
    let receiver_type: String
    let receiver_id: String
    
    init() {
        self.id = 0
        self.body = ""
        self.image = ""
        self.audio = ""
        self.type_data = 0
        self.sender_type = "0"
        self.receiver_type = "0"
        self.receiver_id = "0"
   }
    
    init(id: Int, body: String, image : String, audio : String, type_data: Int, sender_type: String, receiver_type: String, receiver_id: String) {
       self.id = id
        self.body = body
        self.image = image
        self.audio = audio
        self.type_data = type_data
        self.sender_type = sender_type
        self.receiver_type = receiver_type
        self.receiver_id = receiver_id
    }
    
}
