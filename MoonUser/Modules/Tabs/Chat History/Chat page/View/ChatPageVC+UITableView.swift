//
//  ChatPageVC+UITableView.swift
//  MoonUser
//
//  Created by Grand iMac on 3/9/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension ChatPageVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return presenter.cellForRowAtIndex(tableView, index: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
        
    
    
}
