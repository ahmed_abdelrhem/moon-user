//
//  ChatPageVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 3/9/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension ChatPageVC: ChatPageVCView {
    
    func showProgress() {
        startAnimating()
    }
    
    func hideProgress() {
        stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        messageTxt.text = ""
        chatView.isHidden = hideChat
        tableView.reloadData()
        
        let count = tableView.numberOfRows(inSection: 0)
        if count != 0 {
            DispatchQueue.main.async {
                let indexPath = IndexPath(row: count-1, section: 0)
                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
            }
        }
    }

    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func updateTimer(_ second:Int,_ min:Int) {
        timerLbl.text =  String(format: "%02d:%02d",min, second)
    }
    
    func audioPlayerDidFinishPlayingUI() {
        playBtn.isSelected = false
    }
    
    func loadRecordingUI() {
        playBtn.isEnabled = false
        deleteBtn.isEnabled = false
    }
    
    
    func recordBtn(_ flag: RecordStatus) {
        switch flag {
        case .play,.reRecord:
            playBtn.isSelected = true
            playBtn.isEnabled = true
            sendAudioBtn.isEnabled = true
            deleteBtn.isEnabled = true
            voiceBtn.tintColor = .mainColor
        case .stop:
            deleteBtn.isEnabled = false
            voiceBtn.tintColor = .red
            sendAudioBtn.isEnabled = false
            playBtn.isEnabled = false
            playBtn.isSelected = false
            
            
        }
    }


    
}
