//
//  ChatPageVC.swift
//  MoonUser
//
//  Created by Grand iMac on 3/9/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

enum ChatType {
    case Chat
    case Order
}


class ChatPageVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView(frame: .zero)
            tableView.delegate = self
            tableView.dataSource = self
            tableView.separatorStyle = .none
            tableView.register(UINib(nibName: "SenderChatCell", bundle: nil), forCellReuseIdentifier: "SenderChatCell")
            tableView.register(UINib(nibName: "ReciverChatCell", bundle: nil), forCellReuseIdentifier: "ReciverChatCell")
            tableView.register(UINib(nibName: "SenderAudioCell", bundle: nil), forCellReuseIdentifier: "SenderAudioCell")
            tableView.register(UINib(nibName: "ReceiverAudioCell", bundle: nil), forCellReuseIdentifier: "ReceiverAudioCell")
            tableView.rowHeight = UITableView.automaticDimension
            tableView.estimatedRowHeight = 60
        }
    }
    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var recordingView: UIView! {
        didSet{
            recordingView.isHidden = true
        }
    }
    @IBOutlet weak var messageTxt: UITextViewX! {
        didSet {
            messageTxt.delegate = self
        }
    }
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var timerLbl: UILabel!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var sendAudioBtn: UIButton! {
        didSet {
            let flipped_iamge = sendAudioBtn.image(for: .normal)?.flipIfNeeded()
            sendAudioBtn.setImage(flipped_iamge, for: .normal)
        }
    }
    @IBOutlet weak var voiceBtn: UIButton!
    @IBOutlet weak var sendBtn: UIButton!{
        didSet {
            let flipped_iamge = sendBtn.image(for: .normal)?.flipIfNeeded()
            sendBtn.setImage(flipped_iamge, for: .normal)
        }
    }

    
    //MARK:- Variables
    internal var presenter: ChatPageVCPresenter!
    var type: ChatType = .Chat
    var reciverType: Int!
    var id: Int!
    var receiverID: Int?
    var hideChat: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        setPresenter()
        listenNotificationCenter()
    }
    
    
    //MARK:- Functions
    private func setUI() {
        backButtonTitle("")
        title = CHAT
        chatView.isHidden = true
        hideKeyboardWhenTappedAround()
        messageTxt.forceSwitchingRegardlessOfTag = true
    }
    
    private func setPresenter() {
        presenter = ChatPageVCPresenter(view: self, type: type, id: id, receiverType: reciverType, receiverID: receiverID)
        presenter.viewDidLoad()
        presenter.request_recording_permission()
    }
    
    private func listenNotificationCenter() {
        _NC.addObserver(forName: .chatNewMessage, object: nil, queue: nil) { _ in
            self.presenter.didRecivedNewMessage()
        }
    }

    
    
    
    //MARK:- Actions
    @IBAction func playBtnPressed(_ sender: UIButton) {
        if sender.isSelected {
            presenter.playAudio()
        }else{
            presenter.stopAudio()
        }
        sender.isSelected =  !sender.isSelected
        
    }
    
    @IBAction func deleteBtnPressed(_ sender: UIButton) {
        recordingView.isHidden =  true
        presenter.deleteRecord()
    }

    @IBAction func recordBtnPressed(_ sender: UIButton) {
        presenter.minutes = 00
        presenter.seconds = 00
        recordingView.isHidden = false
        presenter.recordTapped()
    }
    
    @IBAction func sendAudioBtnPressed(_ sender: UIButton) {
        presenter.sendAudio()
        recordingView.isHidden = true
    }
    
    @IBAction func imageBtnPressed(_ sender: UIButton) {
        presenter.imageBtnPressed(self, sender)
    }
    
    @IBAction func sendBtnPressed(_ sender: UIButton) {
        guard let msg = messageTxt.text, messageTxt.text != "" else {
            return
        }
        presenter.sendBtnPressed(msg)
    }


}



//MARK:- UITextViewDelegate
extension ChatPageVC: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        textViewHeight.constant = self.messageTxt.contentSize.height
    }

}
