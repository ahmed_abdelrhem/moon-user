//
//  ChatPageVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 3/9/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import Alamofire
import MOLH

class ChatPageVCAPI {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    
    typealias model = (GetChatModel)->()
    typealias sendMessageModel = (SendMessageModel)->()
    

    
    func getChat(type: ChatType, id: Int, receiverType: Int, didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        var param = [
            "receiver_type": receiverType,
            "type":13
        ]

        var url = ""
        
        if type == .Chat {
            param["id"] = id
            url = URLs.chat.url.concatURL(param)
        } else {
            param["order_id"] = id
            url = URLs.orderChat.url.concatURL(param)
        }
        
        apiManager.contectToApiWith(url: url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(GetChatModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
    
    func sendNewMessage(type: ChatType, reciverID: Int?, id: Int, receiverType: Int, msg: String, image: UIImage?, audio: Data?, didDataReady: @escaping sendMessageModel, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let HEADER = ["lang": MOLHLanguage.currentAppleLanguage(),
                      "Jwt": DEF.userData.jwt_token]

        
        var param = [
            "receiver_type": "\(receiverType)",
            "type": "13"
        ]
        
        if msg != "" {
            param["message"] = msg
        }
        
        var url = ""
        if type == .Chat {
             param["receiver_id"] = "\(id)"
            url = URLs.sendMessage.url
        } else {
            param["receiver_id"] = "\(reciverID ?? 0)"
            param["order_id"] = "\(id)"
            url = URLs.sendOrderMessage.url
        }
        
        print("$ URL:", url)
        print("$Param:", param)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if image != nil {
                multipartFormData.append(image!.jpegData(compressionQuality: 0.5)!, withName: "image", fileName: "image\(Date.timeIntervalSinceReferenceDate).jpg", mimeType: "image/jpg")
            }
            
            if audio != nil {
                multipartFormData.append(audio!, withName: "audio", fileName: "audio\(Date.timeIntervalSinceReferenceDate).m4a", mimeType: "audio/m4a")
            }

            for (key, value) in param {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: url, method: .post, headers: HEADER) { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { progress in
                    print("progress: \(progress.fractionCompleted)")
                    if progress.fractionCompleted == 1.0 {
                        print("upload complete")
                    }
                })
                upload.responseJSON { response in
                    print(response)
                    switch response.result {

                    case .success(_):
                        guard let data = response.data else { return }
                        do {
                            let json = try JSONDecoder().decode(SendMessageModel.self, from: data)
                            didDataReady(json)
                        } catch {
                            print("error : ", error)
                        }
                    case .failure(let error):
                        print("1:::\(error)")
                        errorCompletion(error as? ErrorCode)
                    }
                }

            case .failure(let error):
                print("2:::\(error)")
                errorCompletion(error as? ErrorCode)
            }
        }

    }
    
    
    
    

    
}
