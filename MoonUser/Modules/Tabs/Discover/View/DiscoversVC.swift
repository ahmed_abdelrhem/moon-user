//
//  DiscoversVC.swift
//  MoonUser
//
//  Created by Grand iMac on 1/29/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

enum DiscoversType: Int {
    case All = 0
    case Service = 1
    case Famous = 4
    case Photographer = 5
    case Album = -1
    case Clinic = 9
    case Salon = 10
}

class DiscoversVC: BaseViewController {
    
    
    //MARK:- Outlets
    @IBOutlet weak var mainServicescollectionView: UICollectionView! {
        didSet {
            mainServicescollectionView.delegate = self
            mainServicescollectionView.dataSource = self
            mainServicescollectionView.register(UINib(nibName: "MainServicesCell", bundle: nil), forCellWithReuseIdentifier: "MainServicesCell")
        }
    }
    @IBOutlet weak var servicesCategoriescollectionView: UICollectionView! {
        didSet {
            servicesCategoriescollectionView.delegate = self
            servicesCategoriescollectionView.dataSource = self
            servicesCategoriescollectionView.register(UINib(nibName: "MainServicesCell", bundle: nil), forCellWithReuseIdentifier: "MainServicesCell")
        }
    }
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.delegate = self
            collectionView.dataSource = self
            collectionView.register(UINib(nibName: "DiscoverCell", bundle: nil), forCellWithReuseIdentifier: "DiscoverCell")
        }
    }
    @IBOutlet weak var searchBar: UISearchBar!
    

    //MARK:- Variables
    internal var presenter: DiscoversVCPresenter!
    var discover: DiscoversType = .All
    var famousID: Int?
    var serviceID: Int?
    var lat = DEF.currentLat
    var lng = DEF.currentLng
    
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setPresenter()
        setUI()
    }
    
    
    //MARK:- Functions
    private func setPresenter() {
        presenter = DiscoversVCPresenter(view: self)
        presenter.viewDidLoad(discover: discover, serviceID: serviceID, lat: lat, lng: lng, famuosID: self.famousID)
    }
    
    private func setUI() {
        if discover == .All {
            SharedHandller.instance.sideMenus(self)
        }
        searchBar.delegate = self
        searchBar.isHidden = true
        backButtonTitle("")
        if discover == .All || discover == .Service {
            addSearchButton()
        }
    }
    
    
    private func addSearchButton() {
        let searchBtn = UIButton(type: UIButton.ButtonType.system)
        searchBtn.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        searchBtn.setImage(UIImage(named: "search")!, for: .normal)
        searchBtn.addTarget(self, action: #selector(searchBtnClicked), for: .touchUpInside)
        let searchBarBtnItem = UIBarButtonItem(customView: searchBtn)
        self.navigationItem.rightBarButtonItem = searchBarBtnItem
    }
    
    @objc func searchBtnClicked() {
        UIView.animate(withDuration: 0.5) {
            self.searchBar.isHidden = !self.searchBar.isHidden
            self.view.layoutIfNeeded()
            self.searchBar.text = ""
            self.presenter.searchBarTextDidChange(text: "")
            self.view.endEditing(true)
        }
    }
    
    
    


   

}

//MARK:- Search Bar Delegate
extension DiscoversVC: UISearchBarDelegate {
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.showsScopeBar = true
        searchBar.sizeToFit()
        searchBar.setShowsCancelButton(true, animated: true)
        return true
    }

    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.showsScopeBar = false
        searchBar.sizeToFit()
        searchBar.setShowsCancelButton(false, animated: true)
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter.searchBarTextDidChange(text: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        self.view.endEditing(true)
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        self.view.endEditing(true)
    }
    
}

