//
//  DiscoversVC+UICollectionView.swift
//  MoonUser
//
//  Created by Grand iMac on 1/29/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension DiscoversVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == mainServicescollectionView {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainServicesCell", for: indexPath) as? MainServicesCell else {
                return UICollectionViewCell()
            }
//            presenter.cellConfigure(cell: cell, index: indexPath.row)
            return cell
        }else if collectionView == servicesCategoriescollectionView {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainServicesCell", for: indexPath) as? MainServicesCell else {
                return UICollectionViewCell()
            }
//            presenter.cellConfigure(cell: cell, index: indexPath.row)
            return cell
        }else {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DiscoverCell", for: indexPath) as? DiscoverCell else {
                return UICollectionViewCell()
            }
            presenter.cellConfigure(cell: cell, index: indexPath.row)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = collectionView.frame.width
        if discover == .Album {
            return CGSize(width: w / 2, height: w / 2)
        } else {
            return CGSize(width: w / 3, height: w / 3)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.didSelectItemAt(index: indexPath.row)
    }
    
    
}
