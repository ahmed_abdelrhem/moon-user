//
//  DiscoversVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 1/29/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import AVKit

extension DiscoversVC: DiscoversVCView {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        mainServicescollectionView.restore()
        mainServicescollectionView.reloadData()
        servicesCategoriescollectionView.restore()
        servicesCategoriescollectionView.reloadData()
        collectionView.restore()
        collectionView.reloadData()
     }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func onEmpty(_ img: UIImage, _ msg: String) {
        collectionView.setEmptyView(title: msg, messageImage: img)
    }

    func playVideo(_ url: String?) {
        let videoURL = URL(string: url ?? "")!
        let player = AVPlayer(url: videoURL)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player

        present(playerViewController, animated: true, completion: {
            playerViewController.player!.play()
        })
    }
    
    
    

}
