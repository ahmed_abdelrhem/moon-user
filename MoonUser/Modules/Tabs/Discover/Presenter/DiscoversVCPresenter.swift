//
//  DiscoversVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 1/29/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation


protocol DiscoversVCView: LoaderDelegate {
    func playVideo(_ url: String?)
}

protocol DiscoverCellView {
    func configure(img: String?)
    func isVideo(_ isVideo: Bool)
}

class DiscoversVCPresenter {
    
    private weak var view: DiscoversVCView?
    private let interactor = DiscoverVCAPI()
    private var discovers = [DiscoverData]()
    private var filteredData = [DiscoverData]()
    private var isSearching: Bool = false

    
    init(view: DiscoversVCView) {
        self.view = view
    }
    
    var count: Int {
        if isSearching {
            return filteredData.count
        }
        return discovers.count
    }
    
    func viewDidLoad(discover: DiscoversType, serviceID: Int?, lat: String, lng: String, famuosID: Int?) {
        
        view?.showProgress()
        
        interactor.getAllDiscovers(discvoer: discover, serviceID: serviceID, lat: lat, lng: lng, famuosID: famuosID, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.discovers = response.data ?? []
                if self?.discovers.count == 0 {
                    self?.view?.onEmpty(.no_data_image, NO_DATA_FOUND)
                } else {
                    self?.view?.onSuccess("")
                }
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) {  [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onEmpty(.no_connection_image, CONNECTION_ERROR)
        }
        
    }
    
    func cellConfigure(cell: DiscoverCellView, index: Int) {
        let dis = isSearching ? filteredData[index] : discovers[index]
        if dis.type == 1 {
            cell.configure(img: dis.image)
            cell.isVideo(false)
        } else {
            cell.configure(img: dis.capture)
            cell.isVideo(true)
        }
    }
    
    func didSelectItemAt(index: Int) {
        let dis = isSearching ? filteredData[index] : discovers[index]
        if dis.type == 2 {
            view?.playVideo(dis.image)
        }
    }
    
    func searchBarTextDidChange(text: String) {
        if text == "" {
            isSearching = false
            self.view?.onSuccess("")
        } else {
            isSearching = true
            filteredData = discovers.filter {
                return $0.shop_name?.range(of: text, options: .caseInsensitive) != nil
            }
            if filteredData.count == 0 {
                self.view?.onSuccess("")
                self.view?.onEmpty(.no_data_image, NO_DATA_FOUND)
            } else {
                self.view?.onSuccess("")
            }
        }
    }

}
