//
//  DiscoverVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 1/29/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class DiscoverVCAPI {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias model = (DiscoversModel)->()
    
    func getAllDiscovers(discvoer: DiscoversType, serviceID: Int?, lat: String, lng: String, famuosID: Int? , didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        var params = [:] as [String : Any]
        var url = ""
        
        if discvoer == .Album {
            params = [
                "album_id": serviceID ?? 0,
                "type": 1,
                "id": famuosID ?? 0
            ]
            
            url = URLs.albumsImages.url.concatURL(params)
        } else if discvoer == .Photographer || discvoer == .Famous {
            params = [
                "type": discvoer.rawValue,
                "country_id": DEF.country.id
                ]
            
            url = URLs.famousDiscover.url.concatURL(params)
        } else {
            params = [
                "country_id": DEF.country.id,
                "lat": lat,
                "lng": lng
                ] as [String : Any]
                        
            if discvoer == .All {
                params = [
                    "country_id": DEF.country.id,
                    ]
                url = URLs.allDiscovers.url.concatURL(params)
            } else {
                params["type"] = DEF.type
                params["flag"] = DEF.flag
                params["service_id"] = serviceID ?? 0
                url = URLs.serviceDiscover.url.concatURL(params)
            }
        }
        
        
        apiManager.contectToApiWith(url: url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(DiscoversModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
        
}
