//
//  CartVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 2/11/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation


protocol CartVCView: LoaderDelegate {
    func updateUI()
    func reloadItemAt(_ indexPath: IndexPath)
    func navigateToChcekout(_ shopShopping: Bool, payment: Int)
}

protocol CartCellView {
    func configure(name: String, price: String, desc: String, size: String, color: String, extra: String, qty: String, additionsPrice: String, specialRequest: String)
    func hideActions()
}


class CartVCPresenter {
    
    private weak var view: CartVCView?
    private let interactor = CartVCAPI()
    private var items = [CartItem]()
    private var isFirst = true      //NOTE: used for reload data when move from cart tabBar to another
    private (set) var subTotal: Int = 0
    private (set) var deleivery: Int = 0
    private (set) var total: Int = 0
    private (set) var payment: Int = 3
    
    
    init(view: CartVCView) {
        self.view = view
    }

    var count: Int {
        return items.count
    }
    
    func viewDidLoad() {
        
        if DEF.isLogin  {
            if isFirst {
                view?.showProgress()
                
                interactor.getCartData(didDataReady: { [weak self](response) in
                    guard self != nil else { return }
                    self?.view?.hideProgress()
                    if response.status == StatusCode.Success.rawValue {
                        self?.items = response.data?.items ?? []
                        self?.subTotal = Int(response.data?.sub_total ?? "0")!
                        self?.deleivery = Int(response.data?.delivery ?? "0")!
                        self?.total = Int(response.data?.total_price ?? "0")!
                        self?.payment = response.data?.payment_type ?? 3
                        
                        if self?.items.count == 0 {
                            self?.view?.onEmpty(.no_data_image, NO_DATA_FOUND)
                        } else {
                            self?.view?.updateUI()
                            self?.view?.onSuccess("")
                        }
                    } else if response.status == StatusCode.ExpiredJWT.rawValue {
                        Messages.instance.loginMessage()
                    } else {
                        self?.view?.onFailure(CONNECTION_ERROR)
                    }
                }) { [weak self](error) in
                    guard self != nil else { return }
                    self?.view?.hideProgress()
                    self?.view?.onEmpty(.no_connection_image, CONNECTION_ERROR)
                }
            } else {
                self.isFirst = true
            }
        } else {
            self.view?.onEmpty(.no_data_image, PLEASE_LOGIN)
        }
    }
    
    func cellConfigure(cell: CartCellView, index: Int) {
        let name = items[index].name ?? ""
        let price = items[index].price ?? "0"
        let desc = items[index].desc ?? ""
        let color = items[index].color ?? ""
        var size = ""
        if let s = items[index].size_name {
            size = s
        } else {
            size = items[index].size ?? ""
        }
        let extra = items[index].additions ?? ""
        let additionsPrice = items[index].price_addition ?? "0"
        let qty = "\(items[index].qty ?? 1)"
        let specialRequest = items[index].special_request ?? ""

        cell.configure(name: name, price: price, desc: desc, size: size, color: color, extra: extra, qty: qty, additionsPrice: additionsPrice, specialRequest: specialRequest)
    }
    
    //Cell Actions
    func deleteBtnCilciked(_ indexPath: IndexPath) {
        let id = items[indexPath.row].id ?? 0
        let p = Int(items[indexPath.row].price ?? "0")!
        let q = items[indexPath.row].qty ?? 1
        
        if let index = items.firstIndex(where: { $0.id == id}) {
            
            Messages.instance.actionsConfigMessage(title: "", body: SURE_TO_DELETE, buttonTitle: DELETE) { (success) in
                if success {
                    self.subTotal -= ( p * q)
                    self.total -= ( p * q)
                    self.items.remove(at: index)
                    self.view?.updateUI()
                    self.view?.onSuccess("")
                    //Call API
                    DispatchQueue.global(qos: .background).async {
                        self.interactor.DeleteItem(itemID: id)
                    }
                }
            }
        }
    }
    
    func increaseBtnClicked(_ indexPath: IndexPath) {
        let id = items[indexPath.row].id ?? 0
        let pID = items[indexPath.row].product_id ?? 0
        var q = items[indexPath.row].qty ?? 1
        let p = Int(items[indexPath.row].price ?? "0")!
        
        q += 1
        items[indexPath.row].qty = q
        subTotal += p
        total += p
        view?.updateUI()
        view?.reloadItemAt(indexPath)
        //Call API
        DispatchQueue.global(qos: .background).async {
            self.interactor.updateItem(itemID: id, productID: pID, qty: q)
        }
    }
    
    func decreaseBtnClicked(_ indexPath: IndexPath) {
        let id = items[indexPath.row].id ?? 0
        let pID = items[indexPath.row].product_id ?? 0
        var q = items[indexPath.row].qty ?? 1
        let p = Int(items[indexPath.row].price ?? "0")!
        
        if q != items[indexPath.row].min_qty {
            q -= 1
            items[indexPath.row].qty = q
            subTotal -= p
            total -= p
            view?.updateUI()
            view?.reloadItemAt(indexPath)
            //Call API
            DispatchQueue.global(qos: .background).async {
                self.interactor.updateItem(itemID: id, productID: pID, qty: q)
            }
        }
    }

    func chechOutBtnClicked() {
        if let flag = items.first?.flag {
            self.isFirst = false
            let type = items.first?.type ?? 0
            let showShipping = (type != 3 || flag != 2)
            view?.navigateToChcekout(showShipping, payment: self.payment)
        }
    }
    
    
    
    
}
