//
//  CartVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 2/12/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class CartVCAPI {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    
    typealias model = (CartResponseModel)->()
    typealias statusModel = (StatusModel)->()

    
    func getCartData( didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        apiManager.contectToApiWith(url: URLs.cart.url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(CartResponseModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
    
    func DeleteItem(itemID: Int) {
              
        let param = [
            "cart_item_id": itemID,
            "status": 2
        ]
                 
        apiManager.contectToApiWith(url: URLs.deleteCart.url,
                                  methodType: .delete,
                                  params: param,
                                  success: { (json) in
           
              if let data = try? JSONSerialization.data(withJSONObject: json) {
                  do {
                    _ = try self.decoder.decode(StatusModel.self, from: data)
                  } catch{
                      print("error\(error)")
                  }
              }
        }) { (error, msg) in
            print(error, msg!)
        }
    }
    
    
    func updateItem(itemID: Int, productID: Int, qty: Int) {
              
        let param = [
            "product_id": productID,
            "cart_item_id": itemID,
            "qty": qty
        ]
                 
        apiManager.contectToApiWith(url: URLs.addOrUpdateCart.url,
                                  methodType: .post,
                                  params: param,
                                  success: { (json) in
           
              if let data = try? JSONSerialization.data(withJSONObject: json) {
                  do {
                    _ = try self.decoder.decode(StatusModel.self, from: data)
                  }catch{
                      print("error\(error)")
                  }
                  
              }
        }) { (error, msg) in
            print(error, msg!)
        }
    }
    
        
}
