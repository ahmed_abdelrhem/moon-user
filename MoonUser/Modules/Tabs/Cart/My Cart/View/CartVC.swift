//
//  CartVC.swift
//  MoonUser
//
//  Created by Grand iMac on 2/10/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class CartVC: BaseViewController {

    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.allowsMultipleSelection = false
            tableView.tableFooterView = UIView(frame: .zero)
            tableView.dataSource = self
            tableView.delegate = self
            tableView.separatorStyle = .none
            tableView.register(UINib(nibName: "CartCell", bundle: nil), forCellReuseIdentifier: "CartCell")
            tableView.estimatedRowHeight = 100
            tableView.rowHeight = UITableView.automaticDimension
        }
    }
    @IBOutlet weak var totalView: UIView!
    @IBOutlet weak var subTotalLbl: UILabel!
    @IBOutlet weak var deliverylLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var subTotalAndDeliveryStack: UIStackView!
    
    
    
    //MARK:- Variables
    internal var presenter: CartVCPresenter!
    
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        SharedHandller.instance.sideMenus(self)
        setPresenter()
        totalView.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        presenter.viewDidLoad()
    }
    
    
    //MARK:- Functions
    private func setPresenter() {
        presenter = CartVCPresenter(view: self)
    }
    
    
    //MARK:- Actions
    @IBAction func checkoutBtnClicked(_ sender: UIButton) {
        presenter.chechOutBtnClicked()
    }

}
