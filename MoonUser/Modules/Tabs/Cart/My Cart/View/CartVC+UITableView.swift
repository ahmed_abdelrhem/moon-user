//
//  CartVC+UITableView.swift
//  MoonUser
//
//  Created by Grand iMac on 2/10/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension CartVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell") as? CartCell else {
            return UITableViewCell()
        }
        presenter.cellConfigure(cell: cell, index: indexPath.row)
        cell.deleteBtn = {
            self.presenter.deleteBtnCilciked(indexPath)
        }
        cell.increaseBtn = {
            self.presenter.increaseBtnClicked(indexPath)
        }
        cell.decreaseBtn = {
            self.presenter.decreaseBtnClicked(indexPath)
        }
        return cell
    }
                
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    

}
