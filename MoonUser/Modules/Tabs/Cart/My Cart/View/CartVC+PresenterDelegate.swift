//
//  CartVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 2/11/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension CartVC: CartVCView {
    
    
    func showProgress() {
        startAnimating()
    }
    
    func hideProgress() {
        stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        totalView.isHidden = false
        tableView.reloadData()
        
        if tableView.numberOfRows(inSection: 0) == 0 {
            totalView.isHidden = true
            tableView.setEmptyView(title: NO_DATA_FOUND, messageImage: .no_data_image)
        }
    }
    
    func onEmpty(_ img: UIImage, _ msg: String) {
        totalView.isHidden = true
        tableView.setEmptyView(title: msg, messageImage: img)
    }
    
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func updateUI() {
        subTotalLbl.text = "\(presenter.subTotal) \(DEF.country.currency)"
        deliverylLbl.text = "\(presenter.deleivery) \(DEF.country.currency)"
        totalLbl.text = "\(presenter.total) \(DEF.country.currency)"
        subTotalAndDeliveryStack.isHidden = presenter.deleivery == 0
    }
    
    func reloadItemAt(_ indexPath: IndexPath) {
        tableView.reloadRows(at: [indexPath], with: .none)
    }

    func navigateToChcekout(_ shopShopping: Bool, payment: Int) {
        let vc = CheckoutVC()
        vc.shopShopping = shopShopping
        vc.payment = payment
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }

}
