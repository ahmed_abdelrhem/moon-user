/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation

struct CartItem : Codable {
	let id : Int?
	let shop_id : Int?
	let flag : Int?
    let type : Int?
	let name : String?
	let description : String?
	let additions : String?
	let size_name : String?
	let color : String?
	let size : String?
	let price : String?
	let total_price : String?
	let desc : String?
	var qty : Int?
    var min_qty : Int?
	let special_request : String?
	let product_id : Int?
    let price_addition : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case shop_id = "shop_id"
		case flag = "flag"
        case type = "type"
		case name = "name"
		case description = "description"
		case additions = "additions"
		case size_name = "size_name"
		case color = "color"
		case size = "size"
		case price = "price"
		case total_price = "total_price"
		case desc = "desc"
		case qty = "qty"
        case min_qty = "min_qty"
		case special_request = "special_request"
		case product_id = "product_id"
        case price_addition = "price_addition"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		shop_id = try values.decodeIfPresent(Int.self, forKey: .shop_id)
		flag = try values.decodeIfPresent(Int.self, forKey: .flag)
        type = try values.decodeIfPresent(Int.self, forKey: .type)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		description = try values.decodeIfPresent(String.self, forKey: .description)
		additions = try values.decodeIfPresent(String.self, forKey: .additions)
		size_name = try values.decodeIfPresent(String.self, forKey: .size_name)
		color = try values.decodeIfPresent(String.self, forKey: .color)
		size = try values.decodeIfPresent(String.self, forKey: .size)
		price = try values.decodeIfPresent(String.self, forKey: .price)
		total_price = try values.decodeIfPresent(String.self, forKey: .total_price)
		desc = try values.decodeIfPresent(String.self, forKey: .desc)
		qty = try values.decodeIfPresent(Int.self, forKey: .qty)
        min_qty = try values.decodeIfPresent(Int.self, forKey: .min_qty)
		special_request = try values.decodeIfPresent(String.self, forKey: .special_request)
		product_id = try values.decodeIfPresent(Int.self, forKey: .product_id)
        price_addition = try values.decodeIfPresent(String.self, forKey: .price_addition)
	}

}
