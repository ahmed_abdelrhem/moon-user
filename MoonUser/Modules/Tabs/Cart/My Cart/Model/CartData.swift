/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct CartData : Codable {
	let items : [CartItem]?
	let sub_total : String?
	let delivery : String?
	let total_price : String?
	let has_delegate : Bool?
    let payment_type: Int?

	enum CodingKeys: String, CodingKey {

		case items = "items"
		case sub_total = "sub_total"
		case delivery = "delivery"
		case total_price = "total_price"
		case has_delegate = "has_delegate"
        case payment_type = "payment_type"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		items = try values.decodeIfPresent([CartItem].self, forKey: .items)
		sub_total = try values.decodeIfPresent(String.self, forKey: .sub_total)
		delivery = try values.decodeIfPresent(String.self, forKey: .delivery)
		total_price = try values.decodeIfPresent(String.self, forKey: .total_price)
		has_delegate = try values.decodeIfPresent(Bool.self, forKey: .has_delegate)
        payment_type = try values.decodeIfPresent(Int.self, forKey: .payment_type)
	}

}
