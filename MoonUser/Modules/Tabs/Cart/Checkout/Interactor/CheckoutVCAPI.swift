//
//  CheckoutVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 2/12/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class CheckoutVCAPI {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    
    typealias model = (ShippingCompaniesModel)->()
    typealias statusModel = (StatusModel)->()

    
    func getShippingCompanies(didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        apiManager.contectToApiWith(url: URLs.getShippingCompany.url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(ShippingCompaniesModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
    
    func confirmOrder(name: String, phone: String, lat: Double, lng: Double, address: String, payment: Int, delivery: Int, companyID: Int, didDataReady: @escaping statusModel, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        var param = [
            "name": name,
            "phone": phone,
            "lat": lat,
            "lng": lng,
            "address": address,
            "payment_method": payment,
            "delivery_method": delivery,
            ] as [String : Any]
        
        if delivery == 3 {
            param["company_id"] = companyID
        }
        
        apiManager.contectToApiWith(url: URLs.confirmOrder.url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(StatusModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    

        
}
