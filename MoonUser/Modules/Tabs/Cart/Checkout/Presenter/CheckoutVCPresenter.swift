//
//  CheckoutVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 2/11/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol CheckoutVCView: LoaderDelegate {
    func presentMap()
    func reloadTableView()
}

protocol SelectionCellView {
    func configure(title: String?, img: UIImage?)
    func hideImage()
}

//enum PaymentMethods: CaseIterable {
//    case Cash
//    case Credit
//
//    var id: Int {
//        switch self {
//        case .Cash:
//            return 1
//        case .Credit:
//            return 2
//        }
//    }
//
//    var title: String {
//        switch self {
//        case .Cash:
//            return CASH_DELIVERY
//        case .Credit:
//            return CREDIT
//        }
//    }
//
//    var image: UIImage? {
//        switch self {
//        case .Cash:
//            return .money_image
//        case .Credit:
//            return .credit_image
//        }
//    }
//
//}

struct paymentModel {
    private (set) var id: Int?
    private (set) var title : String?
    private (set) var image: UIImage?
    
    init(id: Int?, title: String?, image: UIImage?) {
        self.id = id
        self.title = title
        self.image = image
    }
}


class CheckoutVCPresenter {
    
    private weak var view: CheckoutVCView?
    private let interactor = CheckoutVCAPI()
    private var payments = [paymentModel]()
    private var compaines = [ShippingCompany]()
    
    private var paymentMethod: Int = 1
    private var deleiveryMethod: Int = 2
    private var companyID: Int = 0
    private var lat: Double?
    private var lng: Double?
    
    
    init(view: CheckoutVCView, payment: Int) {
        self.view = view
        self.paymentMethod = payment
        createShippingCompainesAndPayments()

    }
    
    private func createShippingCompainesAndPayments() {
        let s = ShippingCompany(id: 1, name: From_Shop, phone: "", price: "0")
        let h = ShippingCompany(id: 2, name: HOME_DELIVERY, phone: "", price: "0")
        compaines.append(h)
        compaines.append(s)
        
        let cash = paymentModel(id: 1, title: CASH_DELIVERY, image: .money_image)
        let credit = paymentModel(id: 2, title: CREDIT, image: .credit_image)
        
        if self.paymentMethod == 1 {
            self.payments.append(cash)
            self.paymentMethod = 1
        } else if self.paymentMethod == 2 {
            self.payments.append(credit)
            self.paymentMethod = 2
        } else {
            self.payments.append(cash)
            self.payments.append(credit)
            self.paymentMethod = 1
        }
    }
    
    func viewDidLoad(_ shopShopping: Bool) {
        if shopShopping {
            view?.showProgress()
            
            interactor.getShippingCompanies(didDataReady: { [weak self](response) in
                guard self != nil else { return }
                self?.view?.hideProgress()
                if response.status == StatusCode.Success.rawValue {
                    self?.compaines += response.data ?? []
                    self?.view?.reloadTableView()
                } else if response.status == StatusCode.ExpiredJWT.rawValue {
                    Messages.instance.loginMessage()
                } else {
                    self?.view?.onFailure(response.message ?? "")
                }
            }) { [weak self](error) in
                guard self != nil else { return }
                self?.view?.hideProgress()
                self?.view?.onFailure(CONNECTION_ERROR)
            }
        }
    }
        
    func addressTextFiledEditingBegin() {
        view?.presentMap()
    }
    
    func setLocation(lat: Double, lng: Double) {
        self.lat = lat
        self.lng = lng
    }
    
    
    //MARK:- UITableView Functions
    func numberOfSections(_ shopShopping: Bool) -> Int {
        if shopShopping {
            return 2
        }
        return 1
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        if section == 0 {
            return payments.count
        } else {
            return compaines.count
        }
    }
    
    func titleForHeader(_ section: Int) -> String {
        if section == 0 {
            return PAYMENT
        } else {
            return SHIPPING
        }
    }
    
    func cellConfigure(cell: SelectionCellView, section: Int, index: Int) {
        if section == 0 {
            let item = payments[index]
            cell.configure(title: item.title, img: item.image)
        } else {
            let item = compaines[index]
            cell.configure(title: item.name, img: UIImage())
            cell.hideImage()
        }        
    }
    
    func didSelectRowAt(section: Int, index: Int) {
        if section == 0 {
            paymentMethod = payments[index].id ?? 1
        } else {
            if index == 0 || index == 1 {
                deleiveryMethod = compaines[index].id ?? 0
            } else {
                deleiveryMethod = 3
                companyID = compaines[index].id ?? 0
            }
        }
    }
    
    func didDeSelectRowAt(section: Int) {
        if section == 0 {
            paymentMethod = 0
        } else {
            deleiveryMethod = 0
        }
    }
    
    
    
    //Order Now
    func orderNowBtnClicked(name: String?, address: String?, phone: String?) {
        guard name != "", address != "", phone != "" else {
            view?.warringMSG(FILL_ALL_DATA)
            return
        }
        
        guard paymentMethod != 0 else {
            view?.warringMSG(SELECT_PAYMENT)
            return
        }
        
        guard deleiveryMethod != 0 else {
            view?.warringMSG(SELECT_SHIPPING)
            return
        }
        
        view?.showProgress()
        
        interactor.confirmOrder(name: name!, phone: phone!, lat: lat!, lng: lng!, address: address!, payment: paymentMethod, delivery: deleiveryMethod, companyID: companyID, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.view?.onSuccess(response.message ?? "")
            } else if response.status == StatusCode.ExpiredJWT.rawValue {
                Messages.instance.loginMessage()
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
    

}
