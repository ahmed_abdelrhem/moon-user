//
//  CheckoutVC.swift
//  MoonUser
//
//  Created by Grand iMac on 2/11/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class CheckoutVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView(frame: .zero)
            tableView.delegate = self
            tableView.dataSource = self
            tableView.separatorStyle = .none
            tableView.allowsMultipleSelection = true
            tableView.register(UINib(nibName: "SelectionCell", bundle: nil), forCellReuseIdentifier: "SelectionCell")
            self.observer = tableView.observe(\.contentSize) { (tableView, _) in
                self.tableViewHeight.constant = self.tableView.contentSize.height
            }
        }
    }
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var phoneNumberLbl: UILabel!
    @IBOutlet weak var userNameTxt: UITextField! {
        didSet {
            userNameTxt.tag = 0
        }
    }
    @IBOutlet weak var addressTxt: UITextField!{
        didSet {
            addressTxt.tag = 1
        }
    }
    @IBOutlet weak var phoneNumberTxt: UITextField!{
        didSet {
            phoneNumberTxt.tag = 2
        }
    }
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!

    
    //MARK:- Variables
    internal var presenter: CheckoutVCPresenter!
    private var observer: NSKeyValueObservation!
    var shopShopping: Bool = true
    var payment: Int = 3
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        title = CHECKOUT
        backButtonTitle("")
        setDelegates()
        setPresenter()
    }
    
    
    //MARK:- Functions
    private func setDelegates() {
        userNameTxt.delegate = self
        addressTxt.delegate = self
        phoneNumberTxt.delegate = self
    }
    
    private func setPresenter() {
        presenter = CheckoutVCPresenter(view: self, payment: self.payment)
        presenter.viewDidLoad(shopShopping)
    }

    
    //MARK:- Actions
    @IBAction func orderNowBtnClicked(_ sender: UIButton) {
        presenter.orderNowBtnClicked(name: userNameTxt.text, address: addressTxt.text, phone: phoneNumberTxt.text)
    }
    
  

}


//MARK:- LocationDelegate
extension CheckoutVC: LocationDelegate {
    
    func RetriveLocation(lat: Double, lng: Double, add: String) {
        addressTxt.text = add
        presenter.setLocation(lat: lat, lng: lng)
    }
    
    func dismiss() {
        addressTxt.placeholder = ADDRESS
        addressLbl.isHidden = addressTxt.text == ""
    }
    
}



//MARK:- UITextFieldDelegate
extension CheckoutVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.placeholder = ""
        switch textField.tag {
        case 0:
            userNameLbl.isHidden = false
            break
        case 1:
            addressLbl.isHidden = false
            presenter.addressTextFiledEditingBegin()
            addressLbl.isHidden = false
            break
        case 2:
            phoneNumberLbl.isHidden = false
            break

        default:
            break
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            textField.placeholder = USER_NAME
            userNameLbl.isHidden = textField.text == ""
            break
        case 2:
            textField.placeholder = PHONE_NUMBER
            phoneNumberLbl.isHidden = textField.text == ""
            break

        default:
            break
        }
    }
    
}
