//
//  CheckoutVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 2/11/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension CheckoutVC: CheckoutVCView {
    
    func showProgress() {
        startAnimating()
    }
    
    func hideProgress() {
        stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        homeScreen(msg)
    }
    
    func reloadTableView() {
        tableView.reloadData()
    }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func warringMSG(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .warning, layout: .statusLine, style: .top)
    }
    
    func presentMap() {
        addressTxt.resignFirstResponder()
        let vc = MapVC()
        vc.modalPresentationStyle = .overFullScreen
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }



}
