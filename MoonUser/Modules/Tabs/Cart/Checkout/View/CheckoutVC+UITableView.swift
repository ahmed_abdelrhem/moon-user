//
//  CheckoutVC+UITableView.swift
//  MoonUser
//
//  Created by Grand iMac on 2/11/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension CheckoutVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.numberOfSections(self.shopShopping)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let selectionCell = tableView.dequeueReusableCell(withIdentifier: "SelectionCell") as? SelectionCell else {
            return UITableViewCell()
        }
        presenter.cellConfigure(cell: selectionCell, section: indexPath.section, index: indexPath.row)
        return selectionCell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerFrame = tableView.frame
        
        let label = UILabel()
        label.frame =  CGRect(x: 20, y: 0, width: headerFrame.size.width-40, height: 40)
        label.font = UIFont.myBoldSystemFont(ofSize: 17)
        label.text = presenter.titleForHeader(section)
        label.textColor = .black
        
        let headerView:UIView = UIView(frame: CGRect(x: 0, y: 0,  width: headerFrame.size.width, height: headerFrame.size.height+20))
        headerView.backgroundColor = .white
        headerView.addSubview(label)

        return headerView
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let indexPathsInSection = tableView.indexPathsForSelectedRows?.filter ({ $0.section == indexPath.section && $0.row != indexPath.row }) {
            for selectedPath in indexPathsInSection {
                tableView.deselectRow(at: selectedPath, animated: true)
            }
        }
        presenter.didSelectRowAt(section: indexPath.section, index: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        presenter.didDeSelectRowAt(section: indexPath.section)
    }
    
    
}
