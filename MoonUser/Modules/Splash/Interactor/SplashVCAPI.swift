//
//  SplashVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 1/23/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class SplashVCAPI {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    
    
    func updateFirebaseToken() {
        
        let param = [
            "type": 13,
            "firebase_token": DEF.DeviceToken,
            "lat": DEF.currentLat,
            "lng": DEF.currentLng,
            "address": DEF.currentAdd
        ] as [String : Any]
        
        apiManager.contectToApiWith(url: URLs.firebase.url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    _ = try self.decoder.decode(StatusModel.self, from: data)
                }catch{
                    print("error: \(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
        }
    }
    
}
