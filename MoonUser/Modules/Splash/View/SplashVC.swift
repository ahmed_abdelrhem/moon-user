//
//  SplashVC.swift
//  MoonUser
//
//  Created by Grand iMac on 1/19/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import GoogleMaps


class SplashVC: UIViewController {

    //MARK:- Variables
    private let locationManager = CLLocationManager()
    private let interactor = SplashVCAPI()

    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationAuth()
        self.rootViewController()
    }

    
    //MARK:- Functions
    private func rootViewController() {
        if DEF.language == "" {
            self.perform(#selector(self.StartLanguageScreen), with: nil, afterDelay: 2.0)
        } else if DEF.country.id == 0 {
            self.perform(#selector(self.StartCountryScreen), with: nil, afterDelay: 2.0)
        } else if DEF.currentLat == "" {
            self.perform(#selector(self.StartLocationScreen), with: nil, afterDelay: 2.0)
        } else if DEF.isFirstTime {
            self.perform(#selector(self.StartWelcomeScreen), with: nil, afterDelay: 2.0)
        } else {
            self.perform(#selector(self.StartHomeScreen), with: nil, afterDelay: 2.0)
        }
    }
    
    @objc func StartLanguageScreen() {
        let mainStorybord: UIStoryboard = UIStoryboard(name: Storyboard.Main.name, bundle: nil)
        let vc = mainStorybord.instantiateViewController(withIdentifier: Identifiers.Language.id) as! LanguageVC
        UIApplication.shared.keyWindow?.rootViewController = vc
    }

    @objc func StartCountryScreen() {
        let mainStorybord: UIStoryboard = UIStoryboard(name: Storyboard.Main.name, bundle: nil)
        let vc = mainStorybord.instantiateViewController(withIdentifier: Identifiers.Country.id) as! CountryVC
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
    
    @objc func StartLocationScreen() {
        let mainStorybord: UIStoryboard = UIStoryboard(name: Storyboard.Main.name, bundle: nil)
        let vc = mainStorybord.instantiateViewController(withIdentifier: Identifiers.TurnLocation.id) as! TurnOnLocationVC
        UIApplication.shared.keyWindow?.rootViewController = vc
    }


    @objc func StartWelcomeScreen() {
        let mainStorybord: UIStoryboard = UIStoryboard(name: Storyboard.Main.name, bundle: nil)
        let vc = mainStorybord.instantiateViewController(withIdentifier: Identifiers.Pages.id) as! PageViewController
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
    
    
    @objc func StartLoginScreen() {
        let storyboard = UIStoryboard(name: Storyboard.Auth.name, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: Identifiers.Login.id) as! UINavigationController
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
    
    @objc func StartHomeScreen() {
        let storyboard = UIStoryboard(name: Storyboard.Tabs.name, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: Identifiers.SWReveal.id)
        UIApplication.shared.keyWindow?.rootViewController = vc
    }



}



extension SplashVC: CLLocationManagerDelegate {
    
    private func locationAuth() {
        if CLLocationManager.authorizationStatus() == .authorizedAlways ||
            CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if (status == .authorizedAlways) || (status == .authorizedWhenInUse) {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        DEF.currentLat = "\(locValue.latitude)"
        DEF.currentLng = "\(locValue.longitude)"
        self.getAddressFromLocation(lat: Double(locValue.latitude), long: Double(locValue.longitude))
        locationManager.stopUpdatingLocation()
    }
    
    private func getAddressFromLocation(lat: Double, long: Double) {
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = long
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil) {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                
                if let pm = placemarks  {
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        var addressString : String = ""
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country! + ", "
                        }
                        print(addressString)
                        DEF.currentAdd = addressString
                        print("addressString", addressString)
                        self.interactor.updateFirebaseToken()
                    }
                }
        })
    }
    
}
