/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation

struct UserData : Codable {
    var id : Int = 0
	var name : String = ""
	var email : String = ""
	var phone : String = ""
	var image : String = ""
    var account_type : Int = 0
    var start_counter : Double = 0
    var taxi_cost : Double = 0
    var country_id : Int = 0
    var country_name : String = ""
	var city_id : Int = 0
	var package_id : Int = 0
	var lat : String = ""
	var lng : String = ""
	var address : String = ""
	var jwt_token : String = ""
    var chats_count : Int = 0
    
	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case email = "email"
		case phone = "phone"
		case image = "image"
        case start_counter = "start_counter"
        case taxi_cost = "taxi_cost"
        case country_id = "country_id"
        case country_name = "country_name"
        case city_id = "city_id"
		case account_type = "account_type"
		case package_id = "package_id"
        case lat = "lat"
        case lng = "lng"
		case address = "address"
		case jwt_token = "jwt_token"
        case chats_count = "chats_count"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id) ?? 0
		name = try values.decodeIfPresent(String.self, forKey: .name) ?? ""
		email = try values.decodeIfPresent(String.self, forKey: .email) ?? ""
		phone = try values.decodeIfPresent(String.self, forKey: .phone) ?? ""
		image = try values.decodeIfPresent(String.self, forKey: .image) ?? ""
        start_counter = try values.decodeIfPresent(Double.self, forKey: .start_counter) ?? 0
        taxi_cost = try values.decodeIfPresent(Double.self, forKey: .taxi_cost) ?? 0
        country_id = try values.decodeIfPresent(Int.self, forKey: .country_id) ?? 0
        country_name = try values.decodeIfPresent(String.self, forKey: .country_name) ?? ""
        city_id = try values.decodeIfPresent(Int.self, forKey: .city_id) ?? 0
		account_type = try values.decodeIfPresent(Int.self, forKey: .account_type) ?? 0
		package_id = try values.decodeIfPresent(Int.self, forKey: .package_id) ?? 0
        lat = try values.decodeIfPresent(String.self, forKey: .lat) ?? ""
        lng = try values.decodeIfPresent(String.self, forKey: .lng) ?? ""
		address = try values.decodeIfPresent(String.self, forKey: .address) ?? ""
		jwt_token = try values.decodeIfPresent(String.self, forKey: .jwt_token) ?? ""
        chats_count = try values.decodeIfPresent(Int.self, forKey: .chats_count) ?? 0
        
	}
    
    init() {
        self.id = 0
        self.name = ""
        self.email = ""
        self.phone = ""
        self.image = ""
        self.country_id = 0
        self.country_name = ""
        self.city_id = 0
        self.account_type = 0
        self.package_id = 0
        self.lat = ""
        self.lng = ""
        self.address = ""
        self.jwt_token = ""
        self.chats_count = 0
    }


}
