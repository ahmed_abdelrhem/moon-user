//
//  SignupVC+UIPickerView.swift
//  MoonUser
//
//  Created by Grand iMac on 1/23/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension SignupVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    //PickerView
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return presenter.pickerCount
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label = UILabel()
        if let v = view {
            label = v as! UILabel
        }
        label.font = UIFont.myRegularSystemFont(ofSize: 20)
        label.text =  presenter.pickerName(index: row)
        label.textAlignment = .center
        return label
    }
    
}
