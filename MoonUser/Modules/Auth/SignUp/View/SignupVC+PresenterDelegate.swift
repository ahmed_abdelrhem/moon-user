//
//  SignupVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 1/22/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension SignupVC: SignupVCView {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        homeScreen(msg)
    }
    
    func warringMSG(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .warning, layout: .messageView, style: .top)
    }
        
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
        
    func changeProfileImage(img: UIImage) {
        profileImg.image = img
    }
    
    func showTermasAndConditions(_ terms: String) {
        let alert = UIAlertController(style: .actionSheet)
        let text: [AttributedTextBlock] = [
            .normal(""),
            .header1(TERMS),
            .normal("\(terms)\n"),]
        alert.addTextViewer(text: .attributedText(text))
        alert.addAction(title: OK, style: .cancel)
        alert.show()
    }
    
    func presentMap() {
        addressTxt.resignFirstResponder()
        let vc = MapVC()
        vc.modalPresentationStyle = .overFullScreen
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }

    
    func createPickerView() {
        cityTxt.isEnabled = true
        regionTxt.isEnabled = true
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: DONE, style: .done, target: self, action: #selector(self.donePickerPressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: CANCEL, style: .plain, target: self, action: #selector(self.cancelPickerClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        self.dataPickerView.reloadAllComponents()
        
        cityTxt.inputAccessoryView = toolBar
        cityTxt.inputView = dataPickerView
        
        regionTxt.inputAccessoryView = toolBar
        regionTxt.inputView = dataPickerView

    }
    
    func setSelectedCity(_ city: String) {
        cityTxt.text = city
    }
    
    func setSelectedRegion(_ region: String) {
        regionTxt.text = region
    }

    //Actions
    @objc func donePickerPressed() {
        self.cityTxt.endEditing(true)
        self.regionTxt.endEditing(true)

        let index = dataPickerView.selectedRow(inComponent: 0)
        presenter.pickerSelected(index: index)
    }
    
    @objc func cancelPickerClick() {
        cityTxt.resignFirstResponder()
        regionTxt.resignFirstResponder()
    }


}
