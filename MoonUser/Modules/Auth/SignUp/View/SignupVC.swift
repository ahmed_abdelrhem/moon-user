//
//  SignupVC.swift
//  MoonUser
//
//  Created by Grand iMac on 1/22/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class SignupVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var userNameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var mobileNumberTxt: UITextField!
    @IBOutlet weak var cityTxt: UITextField!
    @IBOutlet weak var regionTxt: UITextField!
    @IBOutlet weak var addressTxt: UITextField!
    @IBOutlet weak var termsBtn: UIButton!
    
    
    //MARK:- Variables
    internal var presenter: SignupVCPresenter!
    var phone: String?
    var lng: Double?
    var lat: Double?
    var address: String?

    let dataPickerView : UIPickerView = {
        let pick = UIPickerView()
        pick.backgroundColor = UIColor.clear
        pick.tag = 1
        return pick
    }()



    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        setDelegates()
        setUI()
        setPresenter()
    }
    
    
    //MARK:- Functions
    private func setPresenter() {
        presenter = SignupVCPresenter(view: self)
        presenter.viewdidload()
    }
    
    private  func setUI() {
        title = COMPLETE_PROFILE
        mobileNumberTxt.text = phone
        mobileNumberTxt.isUserInteractionEnabled = false
        termsBtn.underline()
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.profileImgClicked))
        profileImg.isUserInteractionEnabled = true
        profileImg.addGestureRecognizer(tap1)
    }
    
    
    //MARK:- Actions
    @objc func profileImgClicked() {
        presenter.profileImageClicked(self, profileImg)
    }
    
    @IBAction func saveBtnClicked(_ sender: UIButton) {
        UITextField.textEmptyValidation([userNameTxt, emailTxt, passwordTxt, mobileNumberTxt, cityTxt, regionTxt, addressTxt])

        presenter.signup(userName: userNameTxt.text, email: emailTxt.text, phone: phone, password: passwordTxt.text, mobileNumber: mobileNumberTxt.text, lat: lat, lng: lng, address: addressTxt.text)
    }
    
    @IBAction func termsBtnClicked(_ sender: UIButton) {
        presenter.termsAndConditions()
    }
    

  

}


extension SignupVC: UITextFieldDelegate {
    
    private func setDelegates() {        
        dataPickerView.delegate = self
        dataPickerView.dataSource = self

        userNameTxt.delegate = self
        emailTxt.delegate = self
        passwordTxt.delegate = self
        mobileNumberTxt.delegate = self
        cityTxt.delegate = self
        regionTxt.delegate = self
        addressTxt.delegate = self
        
        cityTxt.tag = 1
        regionTxt.tag = 2
        addressTxt.tag = 3
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.layer.borderColor = UIColor.mainColor.cgColor
        presenter.textFieldDidBeginEditing(textField.tag)
    }
}


extension SignupVC: LocationDelegate {
    
    func RetriveLocation(lat: Double, lng: Double, add: String) {
        self.lat = lat
        self.lng = lng
        self.address = add
        addressTxt.text = add
    }

}
