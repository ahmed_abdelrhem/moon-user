//
//  SignupVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 1/22/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol SignupVCView: LoaderDelegate {
    func showTermasAndConditions(_ terms: String)
    func changeProfileImage(img: UIImage)
    func createPickerView()
    func presentMap()
    func setSelectedCity(_ city: String)
    func setSelectedRegion(_ region: String)
}


class SignupVCPresenter {
    
    private weak var view: SignupVCView?
    private let interactor = SignupVCAPI()
    private var cities = [Cities]()
    private var regions = [Regions]()
    private var terms = ""
    private var selectedCityID: Int = 0
    private var selectedRegionID: Int = 0
    private var editUITextFieldTag: Int?

    private var profileImage: UIImage?
    
    init(view: SignupVCView) {
        self.view = view
    }
    
    var pickerCount: Int {
        if editUITextFieldTag == 1 {
            return cities.count
        } else {
            return regions.count
        }
    }

    func pickerName(index: Int) -> String {
        if editUITextFieldTag == 1 {
            return cities.count != 0 ? cities[index].city_name ?? "" : ""
        } else {
            return regions.count != 0 ? regions[index].region_name ?? "" : ""
        }
    }

    
    func viewdidload()  {
        
        view?.showProgress()
        
        let countries = DEF.allCountries
        for c in countries {
            if c.id == DEF.country.id {
                self.cities = c.cities
            }
        }
        
        interactor.getSettings(type: 1, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.terms = response.data ?? ""
        }) { _ in }
    }

    
    func profileImageClicked(_ vc: UIViewController, _ sender: AnyObject) {
        AttachmentHandler.shared.showAttachmentImage(vc, sender)
        
        AttachmentHandler.shared.singleImagePickedBlock = { (image, _) in
            DispatchQueue.main.async {
                self.profileImage = image
                self.view?.changeProfileImage(img: image)
            }
        }
    }
    
    func textFieldDidBeginEditing(_ tag: Int) {
        editUITextFieldTag = tag
        if tag == 1 || tag == 2 {
            view?.createPickerView()
        } else if tag == 3 {
            view?.presentMap()
        }
    }
    
    func pickerSelected(index: Int) {
        if editUITextFieldTag == 1 {
            if cities.count != 0 {
                let city = cities[index].city_name ?? ""
                selectedCityID = cities[index].id ?? 0
                view?.setSelectedCity(city)
                for c in cities {
                    if c.id == selectedCityID {
                        self.regions = c.regions ?? []
                    }
                }
            }
        } else {
            if regions.count != 0 {
                let region = regions[index].region_name ?? ""
                selectedRegionID = regions[index].id ?? 0
                view?.setSelectedRegion(region)
            }
        }
    }
    
    
    func signup(userName: String?, email: String?, phone: String?, password: String?, mobileNumber: String?, lat: Double?, lng: Double?, address: String?) {
        
        guard userName != "", email != "", password != "", mobileNumber != "", selectedCityID != 0, selectedRegionID != 0, address != "" else {
            return
        }
        
        guard email!.isValidEmail() else {
            view?.warringMSG(ENTER_VALIDATE_EMAIL)
            return
        }
        
        guard profileImage != nil else {
            view?.warringMSG(SELECT_PICTURE)
            return
        }
        
        view?.showProgress()
        
        interactor.signUp(userName: userName!, pwd: password!, phone: phone!, email: email!, lat: lat!, lng: lng!, address: address!, cityID: selectedCityID, regionID: selectedRegionID, image: profileImage!, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                DEF.isLogin = true
                DEF.userData = response.data!
                self?.view?.onSuccess(response.message ?? "")
            } else {
                self?.view?.warringMSG(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
        
    }
    
    func termsAndConditions() {
        view?.showTermasAndConditions(self.terms)
    }

    
    
    
}


