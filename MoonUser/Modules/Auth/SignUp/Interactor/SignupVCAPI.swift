//
//  SignupVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 1/23/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import Alamofire
import MOLH

class SignupVCAPI {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    
    typealias dataModel = (DataResponseModel)->()
    typealias loginModel = (LoginModel)->()
    
    func getSettings(type: Int, didDataReady: @escaping dataModel, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param = [
            "type": type,
            "app": 1
        ] as [String : Any]
        
        apiManager.contectToApiWith(url: URLs.settings.url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(DataResponseModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
        }
    }
    
    func signUp(userName: String, pwd: String, phone: String, email: String, lat: Double, lng: Double, address: String, cityID: Int, regionID: Int, image: UIImage, didDataReady: @escaping loginModel, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
           
        let param = [
            "name": userName,
            "password": pwd,
            "phone": phone,
            "email": email,
            "lat": "\(lat)",
            "lng": "\(lng)",
            "address": address,
            "country_id": "\(DEF.country.id)",
            "city_id": "\(cityID)",
            "region_id": "\(regionID)",
            "type": "\(13)"
        ]
           
       let HEADER = ["lang": MOLHLanguage.currentAppleLanguage(),
                     "Jwt": DEF.userData.jwt_token]

        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(image.jpegData(compressionQuality: 0.5)!, withName: "image", fileName: "profileImage\(Date.timeIntervalSinceReferenceDate).jpg", mimeType: "image/jpg")

           for (key, value) in param {
               multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
           }
       }, to: URLs.register.url, method: .post, headers: HEADER) { (result) in
           switch result {
           case .success(let upload, _, _):
               upload.uploadProgress(closure: { progress in
                   print("progress: \(progress.fractionCompleted)")
                   if progress.fractionCompleted == 1.0 {
                       print("upload complete")
                   }
               })
               upload.responseJSON { response in
                   print(response)
                   switch response.result {

                   case .success(_):
                       guard let data = response.data else { return }
                       do {
                           let json = try JSONDecoder().decode(LoginModel.self, from: data)
                           didDataReady(json)
                       } catch {
                           print("error : ", error)
                       }
                   case .failure(let error):
                       print("1:::\(error)")
                       errorCompletion(error as? ErrorCode)
                   }
               }

           case .failure(let error):
               print("2:::\(error)")
               errorCompletion(error as? ErrorCode)
           }
       }
    }
    

}
