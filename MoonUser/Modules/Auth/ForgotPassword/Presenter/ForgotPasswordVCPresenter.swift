//
//  ForgotPasswordVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 1/21/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import FirebaseAuth
import MOLH

protocol ForgotPasswordVCView: class {
    func showProgress()
    func hideProgress()
    func onSuccess(phone: String, verificationID: String,_ msg: String)
    func onFailure(_ msg: String)
}


class ForgotPasswordVCPresenter {
    
    private weak var view: ForgotPasswordVCView?
    private let interactor =  ForgotPasswordVCAPI()
    
    init(view: ForgotPasswordVCView) {
        self.view = view
    }
    
    func sendEmail(email: String?) {
        
        guard email != "" else {
            return
        }
        
        view?.showProgress()
        
        interactor.checkEmail(email: email!, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.sendVerificationCode2UserPhone(response.data ?? "")
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
        
    }
    
    private func sendVerificationCode2UserPhone(_ phone: String)  {
                
        view?.showProgress()
        
        Auth.auth().languageCode = MOLHLanguage.currentAppleLanguage()

        PhoneAuthProvider.provider().verifyPhoneNumber(phone, uiDelegate: nil) {  [weak self](verificationID, error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if let error = error {
                print("err :\( error.localizedDescription)")
                self?.view?.onFailure(WRONG_NUMBER)
                return
            }
            // Sign in using the verificationID and the code sent to the user
            // ...
            self?.view?.onSuccess(phone: phone, verificationID: verificationID ?? "", CODE_SEND)
        }
    }

    
}
