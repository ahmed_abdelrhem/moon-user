//
//  ForgotPasswordVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 1/23/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class ForgotPasswordVCAPI {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    
    typealias dataModel = (DataResponseModel)->()
    
    func checkEmail(email: String, didDataReady: @escaping dataModel, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param = [
            "email": email,
            "type": 13,
            "check_phone": 1
            ] as [String : Any]
        
        apiManager.contectToApiWith(url: URLs.checkPhone.url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(DataResponseModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
}
