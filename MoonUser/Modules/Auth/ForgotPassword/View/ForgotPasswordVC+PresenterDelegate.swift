//
//  ForgotPasswordVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 1/21/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension ForgotPasswordVC: ForgotPasswordVCView {
    
    func showProgress() {
        self.startAnimating()
    }

    func hideProgress() {
        self.stopAnimating()
    }

    func onSuccess(phone: String, verificationID: String, _ msg: String) {
        let storyBoard = UIStoryboard(name: Storyboard.Auth.name, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: Identifiers.Code.id) as! CodeVC
        vc.type = .reset
        vc.phone = phone
        vc.verificationID = verificationID
        navigationController?.pushViewController(vc, animated: true)

        Messages.instance.showConfigMessage(title: "", body: msg, state: .success, layout: .messageView, style: .top)
    }

    func onFailure(_ message: String) {
        Messages.instance.showConfigMessage(title: "", body: message, state: .error, layout: .messageView, style: .top)
    }
    
    
}
