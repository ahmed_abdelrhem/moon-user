//
//  ForgotPasswordVC.swift
//  MoonUser
//
//  Created by Grand iMac on 1/21/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var emailTxt: UITextField!
    
    //MARK:- Variables
    internal var presenter: ForgotPasswordVCPresenter!
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = FOTRGOT_PASSWORD
        hideKeyboardWhenTappedAround()
        setDelegates()
        presenter = ForgotPasswordVCPresenter(view: self)
    }
    
    //MARK:- Actions
    @IBAction func sendBtnPressed(_ sender: UIButton) {
        UITextField.textEmptyValidation([emailTxt])
        presenter.sendEmail(email: emailTxt.text)
    }
    
    
}

extension ForgotPasswordVC: UITextFieldDelegate {
    
    private func setDelegates() {
        emailTxt.delegate = self
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.layer.borderColor = UIColor.mainColor.cgColor
    }
}
