//
//  LoginVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 1/23/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class LoginVCAPI {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    
    typealias loginModel = (LoginModel)->()
    
    func login(email: String, pwd: String, didDataReady: @escaping loginModel, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param = [
            "email": email,
            "password": pwd,
            "firebase_token": DEF.DeviceToken,
            "type": 13
            ] as [String : Any]
        
        apiManager.contectToApiWith(url: URLs.login.url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(LoginModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
}
