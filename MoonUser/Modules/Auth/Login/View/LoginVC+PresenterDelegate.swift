//
//  LoginVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 1/21/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension LoginVC: LoginVCView {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        homeScreen(msg)
    }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
        
    func enterValidEmail() {
        Messages.instance.showConfigMessage(title: "", body: ENTER_VALIDATE_EMAIL, state: .warning, layout: .messageView, style: .top)
        emailTxt.shake()
        emailTxt.layer.borderColor = UIColor.red.cgColor
    }
    
    func navigateToForgetPasword() {
        let storyBoard = UIStoryboard(name: Storyboard.Auth.name, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: Identifiers.ForgotPassword.id) as! ForgotPasswordVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToCheckPhone() {
        let storyBoard = UIStoryboard(name: Storyboard.Auth.name, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: Identifiers.CheckPhone.id) as! CheckPhoneVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
