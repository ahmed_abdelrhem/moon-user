//
//  LoginVC.swift
//  MoonUser
//
//  Created by Grand iMac on 1/21/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!

    
    //MARK:- Variables
    internal var presenter: LoginVCPresenter!

    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        SharedHandller.instance.sideMenus(self)
        hideKeyboardWhenTappedAround()
        setDelegates()
        presenter = LoginVCPresenter(view: self)
        title = LOGIN
    }
        
    
    //MARK:- Actions
    @IBAction func loginBtnClicked(_ sender: UIButton) {
        UITextField.textEmptyValidation([emailTxt, passwordTxt])
        presenter.login(email: emailTxt.text, password: passwordTxt.text)
    }
    
    @IBAction func forgetPasswordBtnClicked(_ sender: UIButton) {
        presenter.forgetPassword()
    }
    
    
    @IBAction func signupBtnClicked(_ sender: UIButton) {
        presenter.signup()
    }
    
    @IBAction func facebookBtnClicked(_ sender: UIButton) {
        presenter.facebookLogin()
    }
    


}

extension LoginVC: UITextFieldDelegate {
    
    private func setDelegates() {
        emailTxt.delegate = self
        emailTxt.textContentType = .emailAddress
        
        passwordTxt.delegate = self
        passwordTxt.textContentType = .password
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.layer.borderColor = UIColor.mainColor.cgColor
    }
}
