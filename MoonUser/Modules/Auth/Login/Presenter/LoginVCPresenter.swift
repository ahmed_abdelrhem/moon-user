//
//  LoginVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 1/21/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol LoginVCView: LoaderDelegate {
    func enterValidEmail()
    func navigateToForgetPasword()
    func navigateToCheckPhone()
}


class LoginVCPresenter {
    
    private weak var view: LoginVCView?
    private let interactor = LoginVCAPI()
    
    init(view: LoginVCView) {
        self.view = view
    }
    
    func login(email: String?, password: String?) {
        
        guard email != "", password != "" else {
            return
        }
        
        guard email!.isValidEmail() else {
            view?.enterValidEmail()
            return
        }
        
        view?.showProgress()
        
        interactor.login(email: email!, pwd: password!, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                //Save user Defaults
                DEF.isLogin = true
                DEF.userData = response.data!
                self?.view?.onSuccess(response.message ?? "")
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
        
    func forgetPassword() {
        view?.navigateToForgetPasword()
    }
    
    func signup() {
        view?.navigateToCheckPhone()
    }
        
    func facebookLogin() {
        //lsa
    }
}
