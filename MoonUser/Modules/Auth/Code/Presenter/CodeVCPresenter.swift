//
//  CodeVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 1/21/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import FirebaseAuth


protocol CodeVCView: class {
    func showProgress()
    func hideProgress()
    func presentSignupScreen()
    func onFailure(message: String)
    func presentNewPassswordScreen()
}

class CodeVCPresenter {
    
    private weak var view: CodeVCView?
    
    init(view: CodeVCView) {
        self.view = view
    }
    
    func confirmCode(type: CodeType, code: String?, verificationID: String) {
        
        guard code != "" else {
            return
        }
        
        view?.showProgress()
        
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID,
            verificationCode: code!)
        
        Auth.auth().signIn(with: credential) {[weak self] (authResult, error) in
            guard self != nil else { return }

            self?.view?.hideProgress()

            if let error = error {
                print("error:", error.localizedDescription)
                self?.view?.onFailure(message: WRONG_CODE)
                return
            }
            // User is signed in
            // ...
            print(" user log in successflly by firebase auth with phone")
            if type == .active {
                self?.view?.presentSignupScreen()
            } else {
                self?.view?.presentNewPassswordScreen()
            }
        }
    }
    
}
