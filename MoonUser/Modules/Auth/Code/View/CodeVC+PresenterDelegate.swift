//
//  CodeVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 1/21/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension CodeVC: CodeVCView {
    
    func showProgress() {
        startAnimating()
    }
    
    func hideProgress() {
        stopAnimating()
    }

    func onFailure(message: String) {
        codeTxt.shake()
        codeTxt.text = ""
        Messages.instance.showConfigMessage(title: "", body: message, state: .error, layout: .messageView, style: .top)
    }
    
    func presentSignupScreen() {
        let storyBoard = UIStoryboard(name: Storyboard.Auth.name, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: Identifiers.Signup.id) as! SignupVC
        vc.phone = self.phone
        let nc = UINavigationController(rootViewController: vc)
        UIApplication.shared.keyWindow?.rootViewController = nc
    }
    
    
    func presentNewPassswordScreen() {
        let storyBoard = UIStoryboard(name: Storyboard.Auth.name, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: Identifiers.NewPassword.id) as! NewPasswordVC
        vc.phone = self.phone
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    
    
    
    
}
