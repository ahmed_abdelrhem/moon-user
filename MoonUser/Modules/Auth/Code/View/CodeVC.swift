//
//  CodeVC.swift
//  MoonUser
//
//  Created by Grand iMac on 1/21/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

enum CodeType: String {
    case reset
    case active
}


class CodeVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var codeTxt: UITextField!
    
    
    //MARK:- Variables
    internal var presenter: CodeVCPresenter!
    var type: CodeType!
    var verificationID = ""
    var phone = ""

    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = CODE
        hideKeyboardWhenTappedAround()
        setTextFieldDelagates()
        presenter = CodeVCPresenter(view: self)
    }
    
    
    //MARK:- Actions
    @IBAction func sendBtnPressed(_ sender: UIButton) {
        UITextField.textEmptyValidation([codeTxt])
        presenter.confirmCode(type: type, code: codeTxt.text, verificationID: verificationID)
    }


}


extension CodeVC: UITextFieldDelegate {
    
    func setTextFieldDelagates() {
        codeTxt.delegate = self
    }
        
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.layer.borderColor = UIColor.mainColor.cgColor
    }
}
