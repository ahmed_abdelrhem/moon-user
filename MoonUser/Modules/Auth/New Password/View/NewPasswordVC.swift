//
//  NewPasswordVC.swift
//  MoonUser
//
//  Created by Grand iMac on 1/21/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class NewPasswordVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var newPasswordTxt: UITextField!
    @IBOutlet weak var confirmPasswordTxt: UITextField!
    @IBOutlet weak var viewBtn: UITextField!
    
    
    //MARK:- Variables
    internal var presenter: NewPasswordVCPresenter!
    var phone = ""
    
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = NEW_PASSWORD
        hideKeyboardWhenTappedAround()
        setDelegates()
        presenter = NewPasswordVCPresenter(view: self)
    }
    
    
    //MARK:- Actions
    @IBAction func resetPasswordBtnPressed(_ sender: UIButton) {
        UITextField.textEmptyValidation([newPasswordTxt, confirmPasswordTxt])
        presenter.resetPassword(phone: phone, pwd: newPasswordTxt.text, confirm: confirmPasswordTxt.text)
    }
    
    @IBAction func viewPasswordBtnClicked(_ sender: UITextField) {
        newPasswordTxt.isSecureTextEntry = !newPasswordTxt.isSecureTextEntry
        viewBtn.isSelected = !viewBtn.isSelected
    }
    
}

extension NewPasswordVC: UITextFieldDelegate {
    
    private func setDelegates() {
        newPasswordTxt.delegate = self
        confirmPasswordTxt.delegate = self
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.layer.borderColor = UIColor.mainColor.cgColor
    }
}
