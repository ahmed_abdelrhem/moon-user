//
//  NewPasswordVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 1/21/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension NewPasswordVC: LoaderDelegate {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        let storyboard = UIStoryboard(name: Storyboard.Auth.name, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: Identifiers.Login.id) as! UINavigationController
        UIApplication.shared.keyWindow?.rootViewController = vc
        Messages.instance.showConfigMessage(title: "", body: msg, state: .success, layout: .centeredView, style: .center)
    }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func warringMSG(_ msg: String) {
        confirmPasswordTxt.text = ""
        UITextField.textEmptyValidation([confirmPasswordTxt])
        Messages.instance.showConfigMessage(title: "", body: msg, state: .warning, layout: .messageView, style: .top)
    }
    
}
