//
//  NewPasswordVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 1/21/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation


class NewPasswordVCPresenter {
    
    private weak var view: LoaderDelegate?
    private let interactor = NewPasswordVCAPI()
    
    init(view: LoaderDelegate) {
        self.view = view
    }
    
    func resetPassword(phone: String, pwd: String?, confirm: String?) {
        
        guard pwd != "", confirm != "" else {
            return
        }
        
        if !UITextField.compare(testStr: pwd!, confirm: confirm!) {
            view?.warringMSG(NOT_MATCHED)
            return
        }
        
        view?.showProgress()
        
        interactor.changePassword(newPassword: pwd!, phone: phone, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.view?.onSuccess(response.message ?? "")
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
    
}
