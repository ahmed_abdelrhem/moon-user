//
//  NewPasswordVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 1/23/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class NewPasswordVCAPI {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    
    typealias statusModel = (StatusModel)->()
    
    func changePassword(newPassword: String, phone: String, didDataReady: @escaping statusModel, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param = [
            "new_password": newPassword,
            "phone": phone,
            "type": 13
            ] as [String : Any]
        
        apiManager.contectToApiWith(url: URLs.changeForgetPassword.url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(StatusModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
    
    func newPassword(oldPassword: String, newPassword: String, didDataReady: @escaping statusModel, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param = [
            "new_password": newPassword,
            "old_password": oldPassword,
            "type": 13
            ] as [String : Any]
        
        apiManager.contectToApiWith(url: URLs.changeForgetPassword.url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(StatusModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }

    
}
