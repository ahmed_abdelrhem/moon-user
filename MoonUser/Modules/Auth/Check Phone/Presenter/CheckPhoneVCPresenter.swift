//
//  CheckPhoneVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 1/22/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import FirebaseAuth


protocol CheckPhoneVCView: class {
    func showProgress()
    func hideProgress()
    func onSuccess(verificationID: String,_ msg: String)
    func onFailure(_ msg: String)
    func showCountryPicker()
}


class CheckPhoneVCPresenter {
    
    private weak var view: CheckPhoneVCView?
    private let interactor = CheckPhoneVCAPI()
    
    
    init(view: CheckPhoneVCView) {
        self.view = view
    }
    
    func countryCode() {
        view?.showCountryPicker()
    }
        
    
    func sendPhone(code: String, phone: String?) {
        
        guard phone != "" else {
            return
        }
        
        view?.showProgress()
        
        let myPhone = "\(code)\(phone!)"
        
        interactor.checkPhone(phone: myPhone, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.sendVerificationCode2UserPhone(myPhone)
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
    

    private func sendVerificationCode2UserPhone(_ phone: String)  {
                
        view?.showProgress()
        
        Auth.auth().languageCode = "en"

        PhoneAuthProvider.provider().verifyPhoneNumber(phone, uiDelegate: nil) {  [weak self](verificationID, error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if let error = error {
                print("err :\( error)")
                self?.view?.onFailure(WRONG_NUMBER)
                return
            }
            // Sign in using the verificationID and the code sent to the user
            // ...
            self?.view?.onSuccess(verificationID: verificationID ?? "", CODE_SEND)
        }
    }

    
    
}


