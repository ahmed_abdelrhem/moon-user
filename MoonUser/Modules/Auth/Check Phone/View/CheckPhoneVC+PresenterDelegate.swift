//
//  CheckPhoneVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 1/22/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension CheckPhoneVC: CheckPhoneVCView {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(verificationID: String, _ msg: String) {
        let storyBoard = UIStoryboard(name: Storyboard.Auth.name, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: Identifiers.Code.id) as! CodeVC
        vc.type = .active
        vc.phone = "\(self.code)\(phoneTxt.text!)"
        vc.verificationID = verificationID
        navigationController?.pushViewController(vc, animated: true)

        Messages.instance.showConfigMessage(title: "", body: msg, state: .success, layout: .messageView, style: .top)
    }
    
    func onFailure(_ message: String) {
        Messages.instance.showConfigMessage(title: "", body: message, state: .error, layout: .messageView, style: .top)
    }

    func showCountryPicker() {
        let alert = UIAlertController(style: .actionSheet, title: nil)
        alert.addLocalePicker(type: .phoneCode) { info in
            DispatchQueue.main.async {
                self.countryFlag.image = info!.flag!
                self.countryCode.text = info!.phoneCode
                self.code = info!.phoneCode
            }
        }
        alert.addAction(title: CANCEL, style: .cancel)
        alert.show()
    }
    
    
    
}
