//
//  CheckPhoneVC.swift
//  MoonUser
//
//  Created by Grand iMac on 1/22/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class CheckPhoneVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var countryFlag: UIImageView!
    @IBOutlet weak var countryCode: UILabel!
    @IBOutlet weak var phoneTxt: UITextField!
    
    //MARK:- Variables
    internal var presenter: CheckPhoneVCPresenter!
    var code = "+20"
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = CHECK_PHONE
        hideKeyboardWhenTappedAround()
        presenter = CheckPhoneVCPresenter(view: self)
    }
    
    
    //MARK:- Actions
    @IBAction func codeBtnPressed(_ sender: UIButton) {
        presenter.countryCode()
    }

    @IBAction func sendBtnPressed(_ sender: UIButton) {
        UITextField.textEmptyValidation([phoneTxt])
        presenter.sendPhone(code: code, phone: phoneTxt.text)
        
    }

   
}
