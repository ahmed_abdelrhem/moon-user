//
//  FollowingVC.swift
//  MoonUser
//
//  Created by Grand iMac on 3/4/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class FollowingVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var storiesStack: UIStackView!
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.tag = 0
            collectionView.delegate = self
            collectionView.dataSource = self
            collectionView.register(UINib(nibName: "StoryCell", bundle: nil), forCellWithReuseIdentifier: "StoryCell")
            self.observer = collectionView.observe(\.contentSize) { (collectionView, _) in
                self.collectionViewHeight.constant = self.collectionView.contentSize.height
            }
        }
    }
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView(frame: .zero)
            tableView.register(UINib(nibName: "FollowingCell", bundle: nil), forCellReuseIdentifier: "FollowingCell")
            tableView.dataSource = self
            tableView.delegate = self
        }
    }
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!


    //MARK:- Variables
    internal var presenter: FollowingVCPresenter!
    var observer: NSKeyValueObservation!

    
    
    //MARK:_ Storyboard
    override func viewDidLoad() {
        super.viewDidLoad()
        SharedHandller.instance.sideMenus(self)
        title = FOLLOWING
        setPresenter()
        listenNotificationCenter()
    }
    
    
    
    //MARK:- Private Functions
    private func setPresenter() {
        presenter = FollowingVCPresenter(view: self)
        presenter.viewDidLoad()
    }

    private func listenNotificationCenter() {
        _NC.addObserver(forName: .chatNewMessageTapped, object: nil, queue: nil) { _ in
            let vc = ChatPageVC()
            vc.id = newMessage.id
            vc.reciverType = Int(newMessage.sender_type)!
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }


  

}

