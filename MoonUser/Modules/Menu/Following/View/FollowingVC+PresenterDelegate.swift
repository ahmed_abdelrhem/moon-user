//
//  FollowingVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 3/4/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension FollowingVC: FollowingVCView {
    
    func showProgress() {
         startAnimating()
     }
     
     func hideProgress() {
         stopAnimating()
     }
     
     func onSuccess(_ msg: String) {
         collectionView.reloadData()
         tableView.reloadData()
        
        if collectionView.numberOfItems(inSection: 0) == 0 {
            self.storiesStack.isHidden = true
        }
     }
     
     func onEmpty(_ img: UIImage, _ msg: String) {
        self.storiesStack.isHidden = true
        tableView.setEmptyView(title: msg, messageImage: img)
     }
     
     func onFailure(_ msg: String) {
         Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
     }

    func presentStoriesViewController(_ stories: [UserStories], index: Int) {
        DispatchQueue.main.async {
            let storyPreviewScene = IGStoryPreviewController.init(stories: stories, handPickedStoryIndex:  index)
            self.present(storyPreviewScene, animated: true, completion: nil)
        }
    }
    
    
    
    func navigateToShopPage(_ shopID: Int) {
        let vc = ShopPageVC()
        vc.shopID = shopID
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToInstituationPage(_ id: Int) {
        let vc = InstituationPageVC()
        vc.id = id
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToClinicDetailsVC(_ id: Int) {
        let vc = ClinicDetailsVC()
        vc.id = id
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToSalonProfileVC(_ id: Int) {
        let vc = SalonProfileVC()
        vc.id = id
        navigationController?.pushViewController(vc, animated: true)
    }

    func navigateToFamousDetailsVC(_ id: Int, type: DiscoversType) {
        let vc = FamousDetailsVC()
        vc.id = id
        vc.type = type
        navigationController?.pushViewController(vc, animated: true)
    }

}
