//
//  FollowingVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 3/4/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol FollowingVCView: LoaderDelegate {
    func presentStoriesViewController(_ stories: [UserStories], index: Int)
    func navigateToShopPage(_ shopID: Int)
    func navigateToInstituationPage(_ id: Int)
    func navigateToClinicDetailsVC(_ id: Int)
    func navigateToSalonProfileVC(_ id: Int)
    func navigateToFamousDetailsVC(_ id: Int, type: DiscoversType)
}

protocol FollowingCellView {
    func configure(image: String?, name: String?, rate: String?, isFollow: Bool)
}


class FollowingVCPresenter {
    
    private var view: FollowingVCView?
    private let interactor = AllStoriesMapVCAPI()
    private let shopInteractor = ShopPageVCAPI()
    private var stories = [UserStories]()
    private var followers = [UserReview]()
    
    
    init(view: FollowingVCView) {
        self.view = view
    }
    
    
    func viewDidLoad() {
        if DEF.isLogin {
            view?.showProgress()
            
            interactor.getAllStoriesMap(didDataReady: { [weak self](response) in
                guard self != nil else { return }
                self?.view?.hideProgress()
                if response.status == StatusCode.Success.rawValue {
                    self?.stories = response.stories ?? []
                    if let f = response.followers {
                        self?.followers = f
                    } else {
                        self?.view?.onEmpty(.no_data_image, NO_DATA_FOUND)
                    }
                    self?.view?.onSuccess("")
                } else if response.status == StatusCode.ExpiredJWT.rawValue {
                    Messages.instance.loginMessage()
                } else {
                    self?.view?.onFailure(response.message ?? "")
                }
            }) { [weak self](error) in
                guard self != nil else { return  }
                self?.view?.hideProgress()
                self?.view?.onEmpty(.no_connection_image, CONNECTION_ERROR)
            }
        } else {
            self.view?.onEmpty(.no_data_image, PLEASE_LOGIN)
        }
    }
    
    //MARK:- CollectionView Functions
    var storiesCount: Int {
        return stories.count
    }
    
    func storyCellCongifure(cell: StoryCellConfigure,  index: Int) {
        let s = stories[index]
        cell.configure(image: s.stories?.last?.image, name: s.name)
    }
    
    func didSelectItemAt(at index: Int) {
        view?.presentStoriesViewController(stories, index: index)
    }

    
    
    //MARK:- TableView Functions
    var followersCount: Int {
        return followers.count
    }
    
    func followerCellConfigure(cell: FollowingCellView, at index: Int) {
        let follower = followers[index]
        cell.configure(image: follower.image, name: follower.name, rate: follower.rate, isFollow: follower.isFollow)
    }
    
    func followBtnActionClicked(at index: Int) {
        let id = followers[index].id ?? 0
        let type = followers[index].type ?? 0
        followers[index].isFollow = !followers[index].isFollow
        DispatchQueue.global(qos: .background).async {
            self.shopInteractor.followShop(shopID: id, type: type)
        }
    }
    
    func didSelectRowAt(_ index: Int) {
        let f = followers[index]
        let id = f.id ?? 0

        switch f.type ?? 0{
        case 1, 2, 3:
            DEF.type = f.type ?? 0
            DEF.flag = f.flag ?? 0
            view?.navigateToShopPage(id)
            break
            
        case 4:
            view?.navigateToFamousDetailsVC(id, type: .Famous)
            break

        case 5:
            view?.navigateToFamousDetailsVC(id, type: .Photographer)
            break

        case 6, 8:
            view?.navigateToInstituationPage(id)
            break

        case 9:
            view?.navigateToClinicDetailsVC(id)
            break

        case 10:
            view?.navigateToSalonProfileVC(id)
            break

        default:
            break
        }
    }
    
    
    
    
    
}

