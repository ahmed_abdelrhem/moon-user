//
//  FAQVC.swift
//  MoonUser
//
//  Created by apple on 11/15/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class FAQVC: UITableViewController {
        
    
    //MARK:- Variables
    internal var presenter: FAQVCPresenter!
    
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = FAQ
        SharedHandller.instance.sideMenus(self)
        setPresenter()
        setTableView()
        listenNotificationCenter()
    }
    
    
    
    
    // MARK:- Functions
    private func setTableView() {
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 70
        tableView.register(UINib(nibName: "ListCell", bundle: nil), forCellReuseIdentifier: "ListCell")
    }
    
    private func setPresenter() {
        presenter = FAQVCPresenter(view: self)
        presenter.viewDidLoad()
    }
    
    private func listenNotificationCenter() {
        _NC.addObserver(forName: .chatNewMessageTapped, object: nil, queue: nil) { _ in
            let vc = ChatPageVC()
            vc.id = newMessage.id
            vc.reciverType = Int(newMessage.sender_type)!
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell", for: indexPath) as? ListCell else {
            return UITableViewCell()
        }
        presenter.cellConfigure(cell: cell, index: indexPath.row)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    
}
