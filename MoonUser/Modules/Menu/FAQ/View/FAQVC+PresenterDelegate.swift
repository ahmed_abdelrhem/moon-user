//
//  FAQVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by apple on 11/15/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension FAQVC: LoaderDelegate {
    
    func showProgress() {
         startAnimating()
     }
     
     func hideProgress() {
         stopAnimating()
     }
     
     func onSuccess(_ msg: String) {
         tableView.reloadData()
     }
     
     func onEmpty(_ img: UIImage, _ msg: String) {
         tableView.setEmptyView(title: msg, messageImage: img)
     }
     
     func onFailure(_ msg: String) {
         Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
     }
    

}
