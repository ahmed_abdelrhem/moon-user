//
//  MyAdsVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 2/23/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension MyAdsVC: MyAdsVCView {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        tableView.reloadData()
        if tableView.numberOfRows(inSection: 0) == 0 {
            tableView.setEmptyView(title: NO_DATA_FOUND, messageImage: .no_data_image)
        }
    }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func onEmpty(_ img: UIImage, _ msg: String) {
        tableView.setEmptyView(title: msg, messageImage: img)
    }
            
    func navigateToAdDetailsVC(_ id: Int, title: String?) {
        let vc = AdDetailsVC() 
        vc.id = id
        vc.title = title
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToEditAd(_ id: Int, title: String?) {
        let vc = AddAdvertisingVC()
        vc.id = id
        navigationController?.pushViewController(vc, animated: true)
    }

        
    
    
}
