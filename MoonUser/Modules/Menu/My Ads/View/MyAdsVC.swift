//
//  MyAdsVC.swift
//  MoonUser
//
//  Created by Grand iMac on 2/23/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class MyAdsVC: UITableViewController {
        
    
    //MARK: -Variables
    internal var presenter: MyAdsVCPresenter!
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        SharedHandller.instance.sideMenus(self)
        title = MY_ADS
        setPresenter()
        setTableView()
        listenNotificationCenter()
    }
    
    // MARK:- Functions
    private func setPresenter() {
        presenter = MyAdsVCPresenter(view: self)
        presenter.viewDidLoad()
    }
    
    private func setTableView() {
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.register(UINib(nibName: "MyAdsCell", bundle: nil), forCellReuseIdentifier: "MyAdsCell")
        tableView.separatorStyle = .none
    }
    
    private func listenNotificationCenter() {
        _NC.addObserver(forName: .updateAd, object: nil, queue: nil) { _ in
            self.presenter.viewDidLoad()
        }
        
        _NC.addObserver(forName: .chatNewMessageTapped, object: nil, queue: nil) { _ in
            let vc = ChatPageVC()
            vc.id = newMessage.id
            vc.reciverType = Int(newMessage.sender_type)!
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }



    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MyAdsCell", for: indexPath) as? MyAdsCell else {
            return UITableViewCell()
        }
        presenter.cellConfigure(cell: cell, index: indexPath.row)
        cell.deleteBtn = {
            self.presenter.deleteBtnCilciked(at: indexPath.row)
        }
        cell.editBtn = {
            self.presenter.editBtnCilciked(at: indexPath.row)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRowAt(indexPath.row)
    }
    
}
