//
//  MyAdsVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 2/23/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol MyAdsVCView: LoaderDelegate {
    func navigateToAdDetailsVC(_ id: Int, title: String?)
    func navigateToEditAd(_ id: Int, title: String?)
}

protocol MyAdsCellView {
    func configure(image: String?, name: String?, price: String?)
}

class MyAdsVCPresenter {
    
    private weak var view: MyAdsVCView?
    private let interactor = MyAdsVCAPI()
    private var myAds = [Adds]()
    
    
    init(view: MyAdsVCView) {
        self.view = view
    }
    
    var count: Int {
        return myAds.count
    }
    
    func viewDidLoad() {
        if DEF.isLogin {
            
            view?.showProgress()
            
            interactor.getMyAds(didDataReady: { [weak self](response) in
                guard self != nil else { return }
                self?.view?.hideProgress()
                if response.status == StatusCode.Success.rawValue {
                    self?.myAds = response.data ?? []
                    self?.view?.onSuccess("")
                } else if response.status == StatusCode.ExpiredJWT.rawValue {
                    Messages.instance.loginMessage()
                } else {
                    self?.view?.onFailure(response.message ?? "")
                }
            }) { [weak self](error) in
                guard self != nil else { return }
                self?.view?.hideProgress()
                self?.view?.onEmpty(.no_connection_image, CONNECTION_ERROR)
            }
        } else {
            view?.onEmpty(.no_data_image, PLEASE_LOGIN)
        }
    }
    
    func cellConfigure(cell: MyAdsCellView, index: Int) {
        let ad = myAds[index]
        cell.configure(image: ad.image, name: ad.name, price: ad.price)
    }
    
    func didSelectRowAt(_ index: Int) {
        let id = myAds[index].id ?? 0
        let title = myAds[index].name
        view?.navigateToAdDetailsVC(id, title: title)
    }
    
    func deleteBtnCilciked(at index: Int) {
        let id = myAds[index].id ?? 0
        if let index = myAds.firstIndex(where: { $0.id == id}) {
            Messages.instance.actionsConfigMessage(title: "", body: SURE_TO_DELETE, buttonTitle: DELETE) { (success) in
                if success {
                    self.myAds.remove(at: index)
                    self.view?.onSuccess("")
                    //Call API
                    DispatchQueue.global(qos: .background).async {
                        self.interactor.DeleteAd(adID: id)
                    }
                }
            }
        }
        
    }
    
    func editBtnCilciked(at index: Int) {
        let id = myAds[index].id ?? 0
        let title = myAds[index].name
        view?.navigateToEditAd(id, title: title)
    }
    
    

    
    
}
