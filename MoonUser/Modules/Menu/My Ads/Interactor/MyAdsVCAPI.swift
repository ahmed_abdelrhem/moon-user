//
//  MyAdsVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 2/23/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class MyAdsVCAPI {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    
    typealias model = (MyAdvertisementsModel)->()

    
    func getMyAds( didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
                
        apiManager.contectToApiWith(url: URLs.MyAds.url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(MyAdvertisementsModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
    
    func DeleteAd(adID: Int) {
              
        let param = [
            "type": "11"
        ]
        
        let url = "\(URLs.deleteAdvertise.url)/\(adID)".concatURL(param)
                 
        apiManager.contectToApiWith(url: url,
                                  methodType: .delete,
                                  params: param,
                                  success: { (json) in
           
              if let data = try? JSONSerialization.data(withJSONObject: json) {
                  do {
                    _ = try self.decoder.decode(StatusModel.self, from: data)
                  } catch{
                      print("error\(error)")
                  }
              }
        }) { (error, msg) in
            print(error, msg!)
        }
    }

    
        
}
