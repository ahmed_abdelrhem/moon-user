//
//  MenuTableViewController+UITableView.swift
//  MoonUser
//
//  Created by Grand iMac on 1/26/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension MenuTableViewController: MenuVCView {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        //Do Nothing
    }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func pushFromSideMenu(storyBoard: String, vcID: String) {
        let vc = UIStoryboard.init(name: storyBoard, bundle: nil).instantiateViewController(withIdentifier: vcID)
        let rvc:SWRevealViewController = self.revealViewController() as SWRevealViewController
        rvc.pushFrontViewController(vc, animated: true)
    }
    
    func pushNibFile(_ vc: UIViewController) {
        let nc = UINavigationController.init(rootViewController: vc)
        let rvc:SWRevealViewController = self.revealViewController() as SWRevealViewController
        rvc.pushFrontViewController(nc, animated: true)
    }
        
    func shareApp() {
        let appLink = "https://itunes.apple.com/us/app/Moon-User/id1504053237?&mt=8"
        share(self, data: appLink) { _ in }
    }
 

}
