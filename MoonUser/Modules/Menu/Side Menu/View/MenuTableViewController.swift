//
//  MenuTableViewController.swift
//  MoonUser
//
//  Created by Grand iMac on 1/26/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import MOLH

class MenuTableViewController: UITableViewController {
        
    
    //MARK:- Variables
    internal var presenter: MenuTableViewControllerPresenter!
    
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        presenter = MenuTableViewControllerPresenter(view: self)
    }
    
    

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 25
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {   //Header Cell
            let headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! HeaderCell
            presenter.configure(cell: headerCell)
            return headerCell
        } else if indexPath.row == 23 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoginCell") as! LoginCell
            cell.configure()
            return cell
        }
        //Default Cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell\(indexPath.row)", for: indexPath)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didselect(index: indexPath.row)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 120
        } else if indexPath.row == 16 && !DEF.isLogin {
            return 0
        }
        return 40
    }

   
}
