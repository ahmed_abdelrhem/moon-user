//
//  MenuTableViewControllerPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 1/26/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol MenuVCView: LoaderDelegate {
    func pushFromSideMenu(storyBoard: String, vcID: String)
    func pushNibFile(_ vc: UIViewController)
    func shareApp()
}

protocol headerCellView {
    func setUserImage()
    func setUserName()
}


class MenuTableViewControllerPresenter {
    
    private weak var view: MenuVCView?
    
    init(view: MenuVCView) {
        self.view = view
    }
    
    func configure(cell: headerCellView) {
        cell.setUserImage()
        cell.setUserName()
    }
    
        
    func didselect(index: Int) {
        switch index {
            
        //Home
        case 1:
            view?.pushFromSideMenu(storyBoard: Storyboard.Tabs.name, vcID: Identifiers.SWReveal.id)
            break
            
        //Reservetions
        case 2:
            let vc = ReservetionsVC()
            view?.pushNibFile(vc)
            break
            
        //Orders
        case 3:
            let vc = OrdersVC()
            view?.pushNibFile(vc)
            break
           
        //Notifications
        case 4:
            let vc = NotificationsVC()
            view?.pushNibFile(vc)
            break

        //Ads
        case 5:
            let vc = MyAdsVC()
            view?.pushNibFile(vc)
            break
            
        //Taxi History
        case 6:
            let vc = RideHistoryVC()
            vc.type = .Taxi
            view?.pushNibFile(vc)
            break

        //Trucks History
        case 7:
            let vc = RideHistoryVC()
            vc.type = .Truck
            view?.pushNibFile(vc)
            break

        //Following
        case 8:
            let vc = FollowingVC()
            view?.pushNibFile(vc)
            break
            
        //Favourite
        case 9:
            let vc = FavouritesVC()
            view?.pushNibFile(vc)
            break

        //Moon Map
        case 10:
            let vc = AllStoriesMapVC()
            vc.fromSideMenu = true
            view?.pushNibFile(vc)
            break

        //Be a shop
        case 11:
            let userURL = "https://itunes.apple.com/us/app/Moon-Shop/id1503964771?&mt=8"
            let defaultURL = "https://itunes.apple.com"
            UIApplication.tryURL(urls: [userURL, defaultURL])
            view?.pushFromSideMenu(storyBoard: Storyboard.Tabs.name, vcID: Identifiers.SWReveal.id)
            break

        //Be a delegete
        case 12:
            let userURL = "https://itunes.apple.com/us/app/Moon-Delegate/id1504232054?&mt=8"
            let defaultURL = "https://itunes.apple.com"
            UIApplication.tryURL(urls: [userURL, defaultURL])
            view?.pushFromSideMenu(storyBoard: Storyboard.Tabs.name, vcID: Identifiers.SWReveal.id)
            break
            
        //Country
        case 13:
            let sb = UIStoryboard(name: Storyboard.Main.name, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: Identifiers.Country.id)
            view?.pushNibFile(vc)
            break
            
        //Language
        case 14:
            let sb = UIStoryboard(name: Storyboard.Main.name, bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: Identifiers.Language.id)
            view?.pushNibFile(vc)
            break
            
        //Contact Us
        case 15:
            let vc = ContactUsVC()
            view?.pushNibFile(vc)
            break
            
        //Technical Supoort
        case 16:
            let vc = TechnicalSupportVC()
            view?.pushNibFile(vc)
            break
            
        //About Us
        case 17:
            let vc = AboutUsVC()
            vc.setting = .AboutUs
            view?.pushNibFile(vc)
            break
            
        //Privacy Policy
        case 18:
            let vc = PrivacyPolicyVC()
            view?.pushNibFile(vc)
            break
            
        //Terms And Conditions
        case 19:
            let vc = TermsAndConditionsVC()
            view?.pushNibFile(vc)
            break
            
        //FAQ
        case 20:
            let vc = FAQVC()
            view?.pushNibFile(vc)
            break
            
        //Share App
        case 21:
            view?.shareApp()
            break

        //Rate App
        case 22:
            let userURL = "https://itunes.apple.com/us/app/Moon-User/id1504053237?&mt=8"
            let defaultURL = "https://itunes.apple.com"
            UIApplication.tryURL(urls: [userURL, defaultURL])
            view?.pushFromSideMenu(storyBoard: Storyboard.Tabs.name, vcID: Identifiers.SWReveal.id)
            break

        //Log out
        case 23:
            if DEF.isLogin {
                //Clear data
                DEF.userData.id = 0
                DEF.isLogin = false
                DEF.userData.name = ""
                DEF.userData.email = ""
                DEF.userData.phone = ""
                DEF.userData.image = ""
                DEF.userData.account_type = 0
                DEF.userData.package_id = 0
                DEF.userData.jwt_token = ""
                homeScreen("")
            } else {
                view?.pushFromSideMenu(storyBoard: Storyboard.Auth.name, vcID: Identifiers.Login.id)
            }
            break
                        
        default:
            break
        }
    }
    
}
