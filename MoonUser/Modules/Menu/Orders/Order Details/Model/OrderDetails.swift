
/*
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation

struct OrderDetails : Codable {
	let delegate : Shop?
	let shop : Shop?
	let order_id : Int?
	let sub_total : String?
	let delivery : String?
	let status_id : Int?
	let order_status : String?
	let total_price : String?
	let products : [CartItem]?

	enum CodingKeys: String, CodingKey {

		case delegate = "delegate"
		case shop = "shop"
		case order_id = "order_id"
		case sub_total = "sub_total"
		case delivery = "delivery"
		case status_id = "status_id"
		case order_status = "order_status"
		case total_price = "total_price"
		case products = "products"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		delegate = try values.decodeIfPresent(Shop.self, forKey: .delegate)
		shop = try values.decodeIfPresent(Shop.self, forKey: .shop)
		order_id = try values.decodeIfPresent(Int.self, forKey: .order_id)
		sub_total = try values.decodeIfPresent(String.self, forKey: .sub_total)
		delivery = try values.decodeIfPresent(String.self, forKey: .delivery)
		status_id = try values.decodeIfPresent(Int.self, forKey: .status_id)
		order_status = try values.decodeIfPresent(String.self, forKey: .order_status)
		total_price = try values.decodeIfPresent(String.self, forKey: .total_price)
		products = try values.decodeIfPresent([CartItem].self, forKey: .products)
	}

}
