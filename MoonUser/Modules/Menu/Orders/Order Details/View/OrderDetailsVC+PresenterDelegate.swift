//
//  OrderDetailsVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 2/13/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension OrderDetailsVC: OrderDetailsVCView {
    
    func showProgress() {
        startAnimating()
    }

    func hideProgress() {
        stopAnimating()
    }

    func onSuccess(_ msg: String) {
        tableView.reloadData()
    }

    func updateUI(_ details: OrderDetails?) {
        let imgURL = URL(string: details?.delegate?.image ?? "")
        delegateImg.sd_setImage(with: imgURL, placeholderImage: .avatar_image)
        delegateNameLbl.text = details?.delegate?.name
        shopNameLbl.text = details?.shop?.name
        self.subTotalLbl.text = "\(details?.sub_total ?? "0") \(DEF.country.currency)"
        self.deliveryLbl.text = "\(details?.delivery ?? "0") \(DEF.country.currency)"
        self.totalLbl.text = "\(details?.total_price ?? "0") \(DEF.country.currency)"
        containerView.isHidden = false
        totalView.isHidden = false
    }


    func onEmpty(_ img: UIImage, _ msg: String) {
        tableView.setEmptyView(title: msg, messageImage: img)
    }

    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func showLocation(lat: Double, lng: Double) {
        let vc = ShowLocationVC()
        vc.lat = lat
        vc.lng = lng
        present(vc, animated: true, completion: nil)
    }
    
    func hideDelegate() {
        delegateNameLbl.isHidden = true
        delegateActionsStack.isHidden = true
    }
    
    func hideDelegateLocation() {
        delegateLocationBtn.isHidden = true
    }
    
    func navigateToChatPageVC(reciverType: Int, reciverID: Int, id: Int, hideChat: Bool) {
        let vc = ChatPageVC()
        vc.type = .Order
        vc.reciverType = reciverType
        vc.id = id
        vc.receiverID = reciverID
        vc.hideChat = hideChat
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }

}
