//
//  OrderDetailsVC+UITableView.swift
//  MoonUser
//
//  Created by Grand iMac on 2/13/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension OrderDetailsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell") as? CartCell else {
            return UITableViewCell()
        }
        presenter.cellConfigure(cell: cell, index: indexPath.row)
        return cell
    }
                
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerFrame = tableView.frame
        
        let label = UILabel()
        label.frame =  CGRect(x: 20, y: 0, width: headerFrame.size.width-40, height: 40)
        label.font = UIFont.myMeduimSystemFont(ofSize: 17)
        label.text = ORDER_DETAILS
        label.textColor = .black
        
        let headerView:UIView = UIView(frame: CGRect(x: 0, y: 0,  width: headerFrame.size.width, height: headerFrame.size.height+20))
        headerView.backgroundColor = .white
        headerView.addSubview(label)

        return headerView
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }


}
