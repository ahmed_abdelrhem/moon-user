//
//  OrderDetailsVC.swift
//  MoonUser
//
//  Created by Grand iMac on 2/13/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class OrderDetailsVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var totalView: UIView!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView(frame: .zero)
            tableView.dataSource = self
            tableView.delegate = self
            tableView.separatorStyle = .none
            tableView.register(UINib(nibName: "CartCell", bundle: nil), forCellReuseIdentifier: "CartCell")
            tableView.estimatedRowHeight = 100
            tableView.rowHeight = UITableView.automaticDimension
            self.observer = tableView.observe(\.contentSize) { (tableView, _) in
                self.tableViewHeight.constant = self.tableView.contentSize.height
            }
        }
    }
    @IBOutlet weak var delegateImg: UIImageView!
    @IBOutlet weak var delegateNameLbl: UILabel!
    @IBOutlet weak var shopNameLbl: UILabel!
    @IBOutlet weak var delegateLocationBtn: UIButton!
    @IBOutlet weak var subTotalLbl: UILabel!
    @IBOutlet weak var deliveryLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var delegateActionsStack: UIStackView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!

    
    //MARK:- Variables
    private var observer: NSKeyValueObservation!
    internal var presenter: OrderDetailsVCPresenter!
    var orderID: Int!
    
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        setPresenter()
    }
    
    
    //MARK:- Functions
    private func setUI() {
        backButtonTitle("")
        title = ORDER_DETAILS
        containerView.isHidden = true
        totalView.isHidden = true
    }
    
    private func setPresenter() {
        presenter = OrderDetailsVCPresenter(view: self)
        presenter.viewDidLoad(orderID: orderID)
    }
    
    
    //MARK:- Actions
    @IBAction func chatWithShopBtnClicked(_ sender: UIButton) {
        presenter.chatWithShopBtnClicked()
    }

    @IBAction func delegateLocationBtnClicked(_ sender: UIButton) {
        presenter.delegateLocationBtnClicked()
    }

    @IBAction func chatWithDelegateBtnClicked(_ sender: UIButton) {
        presenter.chatWithDelegateBtnClicked()
    }


}
