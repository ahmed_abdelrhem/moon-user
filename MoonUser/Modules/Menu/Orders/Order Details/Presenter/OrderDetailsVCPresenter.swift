//
//  OrderDetailsVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 2/13/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol OrderDetailsVCView: LoaderDelegate {
    func updateUI(_ details: OrderDetails?)
    func showLocation(lat: Double, lng: Double)
    func hideDelegate()
    func hideDelegateLocation()
    func navigateToChatPageVC(reciverType: Int, reciverID: Int, id: Int, hideChat: Bool)
}


class OrderDetailsVCPresenter {
    
    private weak var view: OrderDetailsVCView?
    private let interactor = OrderDetailsVCAPI()
    private var details: OrderDetails?
    private var products = [CartItem]()
    private var lat: Double?
    private var lng: Double?
    
    init(view: OrderDetailsVCView) {
        self.view = view
    }
    
    var count: Int {
        return products.count
    }
    
    func viewDidLoad(orderID: Int) {
        
        view?.showProgress()
        
        interactor.getOrderDetails(orderID: orderID, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.details = response.data
                self?.products = response.data?.products ?? []
                self?.view?.onSuccess("")
                self?.view?.updateUI(response.data)
                if let delegate = response.data?.delegate {
                    if let lat = delegate.lat, let lng = delegate.lng {
                        self?.lat = Double(lat)!
                        self?.lng = Double(lng)!
                    } else {
                        self?.view?.hideDelegateLocation()
                    }
                } else {
                    self?.view?.hideDelegate()
                }
            } else if response.status == StatusCode.ExpiredJWT.rawValue {
                Messages.instance.loginMessage()
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onEmpty(.no_connection_image, CONNECTION_ERROR)
        }
    }
    
    func cellConfigure(cell: CartCellView, index: Int) {
        let p = products[index]
        let name = p.name ?? ""
        let price = p.price ?? "0"
        let desc = p.desc ?? ""
        let color = p.color ?? ""
        var size = ""
        if let s = p.size_name {
            size = s
        } else {
            size = p.size ?? ""
        }
        let extra = p.additions ?? ""
        let additionsPrice = p.price_addition ?? "0"
        let qty = "\(p.qty ?? 1)"
        let specialRequest = p.special_request ?? ""
        
        cell.configure(name: name, price: price, desc: desc, size: size, color: color, extra: extra, qty: qty, additionsPrice: additionsPrice, specialRequest: specialRequest)
        cell.hideActions()
    }

    func chatWithShopBtnClicked() {
        let id = details?.order_id ?? 0
        let reciverID = details?.shop?.id ?? 0
        let type = details?.shop?.type ?? 0
        let hide = details?.status_id == 4
        view?.navigateToChatPageVC(reciverType: type, reciverID: reciverID, id: id, hideChat: hide)
    }

    func delegateLocationBtnClicked() {
        view?.showLocation(lat: self.lat ?? 0, lng: self.lng ?? 0)
    }

    func chatWithDelegateBtnClicked() {
        let id = details?.order_id ?? 0
        let reciverID = details?.delegate?.id ?? 0
        let type = details?.delegate?.type ?? 0
        let hide = details?.status_id == 4
        view?.navigateToChatPageVC(reciverType: type, reciverID: reciverID, id: id, hideChat: hide)
    }

    
    
    
}
