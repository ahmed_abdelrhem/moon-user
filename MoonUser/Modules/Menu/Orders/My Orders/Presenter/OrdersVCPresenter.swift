//
//  OrdersVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 2/13/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol OrdersVCView: LoaderDelegate {
    func deleteRow(at indexPath: IndexPath)
    func navigateToOrderDetails(_ id: Int)
}

protocol OrderCellView {
    func configure(date: String?, id: String?, status: String?)
    func hideDeleteOrderBtn()
}

class OrdersVCPresenter {
    
    private weak var view: OrdersVCView?
    private let interactor = OrdersVCAPI()
    private var orders = [OrderData]()
    
    
    init(view: OrdersVCView) {
        self.view = view
    }
    
    var count: Int {
        return orders.count
    }
    
    func viewDidLoad() {
        if DEF.isLogin {
            view?.showProgress()
            
            interactor.getMyOrders(didDataReady: { [weak self](response) in
                guard self != nil else { return }
                self?.view?.hideProgress()
                if response.status == StatusCode.Success.rawValue {
                    self?.orders = response.data ?? []
                    if self?.orders.count != 0 {
                        self?.view?.onSuccess("")
                    } else {
                        self?.view?.onEmpty(.no_data_image, NO_DATA_FOUND)
                    }
                } else if response.status == StatusCode.ExpiredJWT.rawValue {
                    Messages.instance.loginMessage()
                } else {
                    self?.view?.onFailure(response.message ?? "")
                }
            }) { [weak self](error) in
                guard self != nil else { return }
                self?.view?.hideProgress()
                self?.view?.onEmpty(.no_connection_image, CONNECTION_ERROR)
            }
        } else {
            view?.onEmpty(.no_data_image, PLEASE_LOGIN)
        }
    }
    
    func cellConfigure(cell: OrderCellView, index: Int) {
        let order = orders[index]
        
        cell.configure(date: order.order_date, id: order.order_number, status: order.order_status)
        if order.status_id != 1 {
            cell.hideDeleteOrderBtn()
        }
    }
    
    func didSelectRowAt(_ index: Int) {
        let id = orders[index].id ?? 0
        view?.navigateToOrderDetails(id)
    }

    func deleteBtnClicked(at indexPath: IndexPath) {
        
        view?.showProgress()
        
        let id = orders[indexPath.row].id ?? 0

        if let index = orders.firstIndex(where: { $0.id == id }) {
            interactor.deleteOrder(id: id, didDataReady: { [weak self](response) in
                guard let self = self else { return }
                self.view?.hideProgress()
                if response.status == StatusCode.Success.rawValue {
                    self.orders.remove(at: index)
                    self.view?.deleteRow(at: indexPath)
                } else {
                    self.view?.onFailure(response.message ?? "")
                }
            }) { [weak self](error) in
                guard let self = self else { return }
                self.view?.hideProgress()
                self.view?.onFailure(CONNECTION_ERROR)
            }
        }
    }
    
    
}
