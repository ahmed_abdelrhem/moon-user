//
//  OrdersVC.swift
//  MoonUser
//
//  Created by Grand iMac on 2/13/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class OrdersVC: UITableViewController {
    
    //MARK:- Variables
    internal var presenter: OrdersVCPresenter!
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = MY_ORDERS
        SharedHandller.instance.sideMenus(self)
        setPresenter()
        setTableView()
        listenNotificationCenter()
    }
    
    
    // MARK:- Functions
    private func setTableView() {
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.register(UINib(nibName: "OrderCell", bundle: nil), forCellReuseIdentifier: "OrderCell")
    }
    
    private func setPresenter() {
        presenter = OrdersVCPresenter(view: self)
        presenter.viewDidLoad()
    }
    
    private func listenNotificationCenter() {
        _NC.addObserver(forName: .chatNewMessageTapped, object: nil, queue: nil) { _ in
            let vc = ChatPageVC()
            vc.id = newMessage.id
            vc.reciverType = Int(newMessage.sender_type)!
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath) as? OrderCell else {
            return UITableViewCell()
        }
        cell.deleteBtnAction = {
            self.presenter.deleteBtnClicked(at: indexPath)
        }
        presenter.cellConfigure(cell: cell, index: indexPath.row)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRowAt(indexPath.row)
    }
    
    
}
