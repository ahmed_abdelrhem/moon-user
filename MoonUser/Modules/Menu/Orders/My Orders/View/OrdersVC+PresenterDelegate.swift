//
//  OrdersVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 2/13/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation


extension OrdersVC: OrdersVCView {
    
    func showProgress() {
         startAnimating()
     }
     
     func hideProgress() {
         stopAnimating()
     }
     
     func onSuccess(_ msg: String) {
         tableView.reloadData()
     }
     
     func onEmpty(_ img: UIImage, _ msg: String) {
         tableView.setEmptyView(title: msg, messageImage: img)
     }
     
     func onFailure(_ msg: String) {
         Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
     }
    
    func navigateToOrderDetails(_ id: Int) {
        let vc = OrderDetailsVC()
        vc.orderID = id
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func deleteRow(at indexPath: IndexPath) {
        tableView.deleteRows(at: [indexPath], with: .automatic)
    }
}
