//
//  OrdersVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 2/13/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class OrdersVCAPI {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    
    typealias model = (OrdersModel)->()
    typealias statusModel = (StatusModel)->()

    
    func getMyOrders( didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param = [
            "type": 13
        ]
        
        let url = URLs.ordersList.url.concatURL(param)
        
        apiManager.contectToApiWith(url: url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(OrdersModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
    func deleteOrder(id: Int, didDataReady: @escaping statusModel, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let url = URLs.deleteOrder(id).url
        
        apiManager.contectToApiWith(url: url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
                                        
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(StatusModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }

    
        
}
