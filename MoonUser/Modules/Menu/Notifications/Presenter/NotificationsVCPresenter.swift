//
//  NotificationsVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 3/4/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation


class NotificationsVCPresenter {
    
    private weak var view: LoaderDelegate?
    private let interactor = NotificationsVCAPI()
    private var notifications = [SingleNotification]()
    
    
    init(view: LoaderDelegate) {
        self.view = view
    }
    
    var count: Int {
        return notifications.count
    }
    
    func viewDidLoad() {
        if DEF.isLogin {
            view?.showProgress()
            
            interactor.getAllNotifications(didDataReady: { [weak self](response) in
                guard self != nil else { return }
                self?.view?.hideProgress()
                if response.status == StatusCode.Success.rawValue {
                    self?.notifications = response.data ?? []
                    if self?.notifications.count != 0 {
                        self?.view?.onSuccess("")
                    } else {
                        self?.view?.onEmpty(.no_data_image, NO_DATA_FOUND)
                    }
                } else if response.status == StatusCode.ExpiredJWT.rawValue {
                    Messages.instance.loginMessage()
                } else {
                    self?.view?.onFailure(response.message ?? "")
                }
            }) { [weak self](error) in
                guard self != nil else { return }
                self?.view?.hideProgress()
                self?.view?.onEmpty(.no_connection_image, CONNECTION_ERROR)
            }
        } else {
            view?.onEmpty(.no_data_image, PLEASE_LOGIN)
        }
    }
    
    func cellConfigure(cell: NotificationCellView, index: Int) {
        let notify = notifications[index]
        cell.configure(title: notify.title, date: notify.created_at, notification: notify.message)
    }
    
    
}
