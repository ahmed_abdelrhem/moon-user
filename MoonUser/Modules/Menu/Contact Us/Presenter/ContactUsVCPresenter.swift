//
//  ContactUsVCPresenter.swift
//  MoonUser
//
//  Created by Islam 3bRahiem on 6/30/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class ContactUsVCPresenter {
    
    private weak var view: LoaderDelegate?
    private let interactor = ContactUsVCAPI()
    
    
    init(view: LoaderDelegate) {
        self.view = view
    }
    
    func sendBtnClicked(name: String?, email: String?, subject: String?, message: String?) {
        guard name != "", email != "", subject != "", message != "" else {
            view?.warringMSG(FILL_ALL_DATA)
            return
        }
    
        view?.showProgress()
        
        interactor.sendContactUs(name: name!, email: email!, title: subject!, message: message!, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.view?.onSuccess(response.message ?? "")
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
        
    }
    
}
