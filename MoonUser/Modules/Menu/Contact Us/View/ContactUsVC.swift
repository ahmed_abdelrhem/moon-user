//
//  ContactUsVC.swift
//  MoonUser
//
//  Created by Islam 3bRahiem on 6/30/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class ContactUsVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var subjectTxt: UITextField!
    @IBOutlet weak var messageTxt: UITextView!
    
    
    //MARK:- Variables
    private var presenter: ContactUsVCPresenter!
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        SharedHandller.instance.sideMenus(self)
        title = CONTACT_US
        setPresenter()
    }
    
    
    //MARK:- Functions
    private func setPresenter() {
        presenter = ContactUsVCPresenter(view: self)
    }
    
    //MARK:- Actions 
    @IBAction func sendBtnClicked(_ sender: UIButton) {
        presenter.sendBtnClicked(name: nameTxt.text, email: emailTxt.text, subject: subjectTxt.text, message: messageTxt.text)
    }
    
 

}
