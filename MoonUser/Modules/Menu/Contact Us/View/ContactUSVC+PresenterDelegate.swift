//
//  ContactUSVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Islam 3bRahiem on 6/30/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension ContactUsVC: LoaderDelegate {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        homeScreen(DONE_SEND_MESSAGE)
    }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func warringMSG(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .warning, layout: .messageView, style: .top)
    }
    
}
