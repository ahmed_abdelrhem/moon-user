//
//  ContactUsVCAPI.swift
//  MoonUser
//
//  Created by Islam 3bRahiem on 6/30/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class ContactUsVCAPI {

    private let apiManager = APIManager()
    private let decoder = JSONDecoder()

    typealias statusModel = (StatusModel)->()

    func sendContactUs(name: String, email: String, title: String, message: String, didDataReady: @escaping statusModel, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param = [
            "name": name,
            "email": email,
            "title": title,
            "message": message,
            "type": 13
            ] as [String : Any]
        
        apiManager.contectToApiWith(url: URLs.contactUs.url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in
                                        
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(StatusModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }


    func sendTechnicalSupport(message: String, didDataReady: @escaping statusModel, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param = [
            "message": message,
            "type": 13
            ] as [String : Any]
        
        apiManager.contectToApiWith(url: URLs.contactUs.url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in
                                        
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(StatusModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    

}
