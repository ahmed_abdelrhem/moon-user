//
//  RideHistoryVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 3/8/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension RideHistoryVC: RideHistoryVCView {    
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .info, layout: .messageView, style: .top)
    }
    
    func onSuccess(_ msg: String) {
        tableView.reloadData()
    }
    
    func onEmpty(_ img: UIImage, _ msg: String) {
        tableView.setEmptyView(title: msg, messageImage: img)
    }
    
    func navigateToRideDetailsVC(_ trip: SingleTrip?) {
        let vc = RideDetailsVC()
        vc.trip = trip
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
