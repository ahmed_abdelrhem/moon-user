//
//  RideHistoryVC.swift
//  MoonUser
//
//  Created by Grand iMac on 3/8/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit


class RideHistoryVC: UITableViewController {
        
  
    //MARK:- Variables
    internal var presenter: RideHistoryVCPresenter!
    var type: TaxiIntroVCType!
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = type.navigationTitle
        SharedHandller.instance.sideMenus(self)
        setPresenter()
        setTableView()
        listenNotificationCenter()
    }

    
    
    //MARK:- Functions
    fileprivate func setPresenter() {
        presenter = RideHistoryVCPresenter(view: self)
        presenter.viewDidLoad(reservedType: type.id)
    }
    
    fileprivate func setTableView() {
        clearsSelectionOnViewWillAppear = false
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.register(UINib(nibName: "RideHistoryCell", bundle: nil), forCellReuseIdentifier: "RideHistoryCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
    }
    
    private func listenNotificationCenter() {
        _NC.addObserver(forName: .chatNewMessageTapped, object: nil, queue: nil) { _ in
            let vc = ChatPageVC()
            vc.id = newMessage.id
            vc.reciverType = Int(newMessage.sender_type)!
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    //MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.count
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerFrame = tableView.frame
        
        let label = UILabel()
        label.frame =  CGRect(x: 20, y: 0, width: headerFrame.size.width-40, height: 40)
        label.font = UIFont.myRegularSystemFont(ofSize: 18)
        label.text = RECENT_RIDES
        label.textColor = .mainColor
        
        let headerView:UIView = UIView(frame: CGRect(x: 0, y: 0,  width: headerFrame.size.width, height: headerFrame.size.height+20))
        headerView.backgroundColor = .white
        headerView.addSubview(label)

        return headerView
    }
        
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "RideHistoryCell", for: indexPath) as? RideHistoryCell else {
            return UITableViewCell() }
        presenter.cellConfigure(cell: cell, index: indexPath.row)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.5) {
            cell.transform = CGAffineTransform.identity
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRow(at: indexPath.row)
    }

    
}
