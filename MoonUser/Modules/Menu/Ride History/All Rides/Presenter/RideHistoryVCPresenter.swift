//
//  RideHistoryVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 3/8/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol RideHistoryVCView: LoaderDelegate {
    func navigateToRideDetailsVC(_ trip: SingleTrip?)
}

class RideHistoryVCPresenter {
    
    private weak var view: RideHistoryVCView?
    private let interactor = RideHistoryVCAPI()
    private var tripList = [SingleTrip]()
    
    init(view: RideHistoryVCView) {
        self.view = view
    }
    
    var count: Int {
        return tripList.count
    }
    
    func viewDidLoad(reservedType: Int) {
        if DEF.isLogin {
            view?.showProgress()
            
            interactor.getTripHistory(reservedType: reservedType, didDataReady: { [weak self](response) in
                guard self != nil else { return }
                self?.view?.hideProgress()
                
                if response.status == StatusCode.Success.rawValue {
                    self?.tripList = response.data ?? []
                    self?.view?.onSuccess("")
                    if self?.tripList.count == 0 {
                        self?.view?.onEmpty(.no_data_image, NO_DATA_FOUND)
                    }
                } else if response.status == StatusCode.ExpiredJWT.rawValue {
                    Messages.instance.loginMessage()
                } else {
                    self?.view?.onFailure(response.message ?? "")
                }
            }) {  [weak self](error) in
                guard self != nil else { return }
                self?.view?.hideProgress()
                self?.view?.onEmpty(.no_connection_image, CONNECTION_ERROR)
            }
        } else {
            self.view?.onEmpty(.no_data_image, PLEASE_LOGIN)
        }
    }
    
    
    func cellConfigure(cell: RideHistoryCellView, index: Int) {
        cell.setRideHistory(tripList[index])
    }
    
    
    func didSelectRow(at index: Int) {
        let trip = tripList[index]
        view?.navigateToRideDetailsVC(trip)
    }
}
