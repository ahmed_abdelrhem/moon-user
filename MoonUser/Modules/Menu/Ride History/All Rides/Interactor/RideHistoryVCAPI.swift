//
//  RideHistoryVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 3/8/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class RideHistoryVCAPI: NSObject {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    
    typealias model = (RideHistoryModel)->()
    
    
    func getTripHistory(reservedType: Int, didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param = [
            "type": 13,
            "reserved_type": reservedType
        ]
        
        let url = URLs.rideHistory.url.concatURL(param)
        
        apiManager.contectToApiWith(url: url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                if let result = try? self.decoder.decode(RideHistoryModel.self, from: data) {
                    didDataReady(result)
                } else {
                    print("get Trips error")
                }
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
    
    
}
