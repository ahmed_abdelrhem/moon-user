//
//  RideDetailsVC.swift
//  MoonUser
//
//  Created by Grand iMac on 3/8/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class RideDetailsVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var carTypeLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var startLbl: UILabel!
    @IBOutlet weak var endLbl: UILabel!
    @IBOutlet weak var driverLbl: UILabel!
    @IBOutlet weak var rateView: CosmosView!
    
    
    //MARK:- Variables
    var trip: SingleTrip?
    
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        backButtonTitle("")
        title = TRIP_DETAILS
        setUI()
    }
    
    private func setUI() {
        DispatchQueue.main.async {
            self.dateLbl.text = self.trip?.trip_date
            let type = "\(self.trip?.car_brand ?? ""), \(self.trip?.car_version ?? "")"
            self.carTypeLbl.text = type
            self.priceLbl.text = "\(self.trip?.price ?? 0) \(DEF.country.currency)"
            self.statusLbl.text = (self.trip?.status == 2 || self.trip?.status == 5) ? CANCELLED : FINISHED
            self.startLbl.text = self.trip?.start_address
            self.endLbl.text = self.trip?.end_address
            let driver = "\(YOU_RATE) \(self.trip?.driver_name ?? "")"
            self.driverLbl.text = driver
            self.rateView.rating = Double(self.trip?.rate ?? "0")!
        }
    }


   

}
