//
//  ReservationDetailsVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 3/3/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class ReservationDetailsVCAPI {

    private let apiManager = APIManager()
    private let decoder = JSONDecoder()

    typealias model = (AppointmentResponseModel)->()


    func getReservationDetails(id: Int, type: Int, didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param = [
            "type": 13,
            "order_id": id,
            "reserved_type": type
        ]
        
        let url = URLs.AppointmentDetails.url.concatURL(param)
        
        apiManager.contectToApiWith(url: url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(AppointmentResponseModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }

    
    
}
