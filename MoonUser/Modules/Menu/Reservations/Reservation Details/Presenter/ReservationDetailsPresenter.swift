//
//  ReservationDetailsPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 3/3/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol ReservationDetailsVCView: LoaderDelegate {
    func updateUI(_ details: AppointmentData?)
    func setIsClinic()
    func setIsSalon()
    func showCallPhones(_ array: [[String]])
}

class ReservationDetailsVCPresenter {
    
    private weak var view: ReservationDetailsVCView?
    private let interactor = ReservationDetailsVCAPI()
    private var phone: String?
    
    
    init(view: ReservationDetailsVCView) {
        self.view = view
    }
    
    func viewDidLoad(id: Int, type: Int ) {
        
        if type == 9 {
            view?.setIsClinic()
        } else {
            view?.setIsSalon()
        }
        
        view?.showProgress()
        
        interactor.getReservationDetails(id: id, type: type, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.phone = response.data?.phone
                self?.view?.updateUI(response.data)
                self?.view?.onSuccess("")
            } else if response.status == StatusCode.ExpiredJWT.rawValue {
                Messages.instance.loginMessage()
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
        
    }
    
    func phonesStackClicked() {
        if let array = phone?.components(separatedBy: " , ") {
            view?.showCallPhones([array])
        }
    }

}
