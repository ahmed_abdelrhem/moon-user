/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation

struct AppointmentData : Codable {
	let order_id : Int?
	let order_status : Int?
	let payment : Int?
	let name : String?
	let doctor_name : String?
	let doctor_image : String?
	let yearsExperience : Int?
	let doctor_specialties : String?
	let description : String?
	let rate : String?
	let phone : String?
	let image : String?
	let day : String?
	let order_date : String?
	let fees : Int?
	let time : String?
	let services : [Tag]?

	enum CodingKeys: String, CodingKey {

		case order_id = "order_id"
		case order_status = "order_status"
		case payment = "payment"
		case name = "name"
		case doctor_name = "doctor_name"
		case doctor_image = "doctor_image"
		case yearsExperience = "yearsExperience"
		case doctor_specialties = "doctor_specialties"
		case description = "description"
		case rate = "rate"
		case phone = "phone"
		case image = "image"
		case day = "day"
		case order_date = "order_date"
		case fees = "fees"
		case time = "time"
		case services = "services"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		order_id = try values.decodeIfPresent(Int.self, forKey: .order_id)
		order_status = try values.decodeIfPresent(Int.self, forKey: .order_status)
		payment = try values.decodeIfPresent(Int.self, forKey: .payment)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		doctor_name = try values.decodeIfPresent(String.self, forKey: .doctor_name)
		doctor_image = try values.decodeIfPresent(String.self, forKey: .doctor_image)
		yearsExperience = try values.decodeIfPresent(Int.self, forKey: .yearsExperience)
		doctor_specialties = try values.decodeIfPresent(String.self, forKey: .doctor_specialties)
		description = try values.decodeIfPresent(String.self, forKey: .description)
		rate = try values.decodeIfPresent(String.self, forKey: .rate)
		phone = try values.decodeIfPresent(String.self, forKey: .phone)
		image = try values.decodeIfPresent(String.self, forKey: .image)
		day = try values.decodeIfPresent(String.self, forKey: .day)
		order_date = try values.decodeIfPresent(String.self, forKey: .order_date)
		fees = try values.decodeIfPresent(Int.self, forKey: .fees)
		time = try values.decodeIfPresent(String.self, forKey: .time)
		services = try values.decodeIfPresent([Tag].self, forKey: .services)
	}

}
