//
//  ReservationDetailsVC.swift
//  MoonUser
//
//  Created by Grand iMac on 3/2/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class ReservationDetailsVC: UIViewController {

    
    //MARK:- Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var clinicImg: UIImageView!
    @IBOutlet weak var clinicName: UILabel!
    @IBOutlet weak var headerSepratorView: UIView!
    @IBOutlet weak var doctoView: UIView!
    @IBOutlet weak var doctorImg: UIImageView!
    @IBOutlet weak var doctorName: UILabel!
    @IBOutlet weak var doctorSpecialistLbl: UILabel!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var feesTitleLbl: UILabel!
    @IBOutlet weak var feesLbl: UILabel!
    @IBOutlet weak var servicesLbl: UILabel!
    @IBOutlet weak var servicesStack: UIStackView!
    @IBOutlet weak var phoneStack: UIStackView!
    @IBOutlet weak var paymentTypeLbl: UILabel!
    @IBOutlet weak var orderStatusLbl: UILabel!
    @IBOutlet weak var reservationDayLbl: UILabel!
    @IBOutlet weak var reservationTimeLbl: UILabel!

    
    
    //MARK:- Variables
    internal var presenter: ReservationDetailsVCPresenter!
    var id: Int!
    var type: Int!

    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setPresenter()
        updateUI()
        addGestures()
    }
    
    
    //MARK:- Functions
    private func setPresenter() {
        presenter = ReservationDetailsVCPresenter(view: self)
        presenter.viewDidLoad(id: id, type: type)
    }
    
    private func updateUI() {
        containerView.isHidden = true
        title = APPOINTMENT_DETAILS
        backButtonTitle("")
    }
    
    private func addGestures() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.phonesStackClicked))
        phoneStack.addGestureRecognizer(tap)

    }

    @objc func phonesStackClicked() {
        presenter.phonesStackClicked()
    }



}
