//
//  ReservationDetailsVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 3/3/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import SDWebImage

extension ReservationDetailsVC: ReservationDetailsVCView {
    
    func showProgress() {
        startAnimating()
    }
    
    func hideProgress() {
        stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        containerView.isHidden = false
    }
        
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func updateUI(_ details: AppointmentData?) {
        DispatchQueue.main.async {
            let imgURL = URL(string: details?.image ?? "")
            self.clinicImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.clinicImg.sd_setImage(with: imgURL, placeholderImage: .placeHolder_image)
            self.clinicName.text = details?.name
            let doctorURL = URL(string: details?.doctor_image ?? "")
            self.doctorImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.doctorImg.sd_setImage(with: doctorURL, placeholderImage: .avatar_image)
            self.doctorName.text = details?.doctor_name
            self.doctorSpecialistLbl.text = "\(details?.doctor_specialties ?? "") {\(details?.yearsExperience ?? 0) \(YEARS)}"
            self.rateView.rating = Double(details?.rate ?? "0")!
            self.descLbl.text = details?.description
            self.phoneLbl.text = details?.phone
            self.feesLbl.text = "\(details?.fees ?? 0) \(DEF.country.currency)"
            self.paymentTypeLbl.text = details?.payment == 1 ? CASH : ONLINE
            self.reservationDayLbl.text = details?.day
            self.reservationTimeLbl.text = details?.time
            switch details?.order_status {
            case 0:
                self.orderStatusLbl.text = AWAITING_ACCEPT
            case 1:
                self.orderStatusLbl.text = CONFIRMED
            case 2:
                self.orderStatusLbl.text = FINISHED
            case 3:
                self.orderStatusLbl.text = CANCELLED
            default:
                self.orderStatusLbl.text = ""
            }
            
            let s = details?.services ?? []
            let services = s.map {$0.name ?? ""}.joined(separator: ", ")
            self.servicesLbl.text = services
        }
    }
    
    func setIsClinic() {
        feesTitleLbl.text = DOCTOR_FEES
        servicesStack.isHidden = true
    }
    
    func setIsSalon() {
        feesTitleLbl.text = TOTAL
        headerSepratorView.isHidden = true
        doctoView.isHidden = true
        doctorName.isHidden = true
        doctorSpecialistLbl.isHidden = true
    }

    func showCallPhones(_ array: [[String]]) {
        
        var number: String?
        
        let alert = UIAlertController(style: .actionSheet, title: "", message: "")

        let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: 3)

        alert.addPickerView(values: array, initialSelection: pickerViewSelectedValue) { vc, picker, index, values in
            DispatchQueue.main.async { [weak self] in
                guard self != nil else { return }
                number = array[0][index.row]
            }
        }
        
        alert.addAction(image: nil, title: CALL, color: .mainColor, style: .default, isEnabled: true) { [weak self] (action) in
            guard self != nil else { return }
            let phone = number ?? array[0][0]
            if phone != "" {
                let url:NSURL = NSURL(string: "tel://\(phone)")!
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            } else {
                Messages.instance.showConfigMessage(title: NO_PHONE, body: "", state: .warning, layout: .statusLine, style: .top)
            }

        }
        
        alert.addAction(image: nil, title: CANCEL, color: .red, style: .cancel, isEnabled: true) { _ in
            alert.dismiss(animated: true, completion: nil)
        }
        
        present(alert, animated: true, completion: nil)
    }


}
