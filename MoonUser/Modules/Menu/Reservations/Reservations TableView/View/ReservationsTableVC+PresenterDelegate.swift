//
//  ReservationsTableVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 3/2/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension ReservationsTableVC: LoaderDelegate {
    
    func showProgress() {
        startAnimating()
    }
    
    func hideProgress() {
        stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        tableView.restore()
        tableView.reloadData()
        
        if tableView.numberOfRows(inSection: 0) == 0 {
            tableView.setEmptyView(title: NO_DATA_FOUND, messageImage: .no_data_image)
        }
    }
    
    func onEmpty(_ img: UIImage, _ msg: String) {
        tableView.restore()
        tableView.setEmptyView(title: msg, messageImage: img)
    }
    
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }

}
