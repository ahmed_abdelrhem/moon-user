//
//  ReservationsTableVC.swift
//  MoonUser
//
//  Created by Grand iMac on 3/2/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

enum AppointmentType: CaseIterable {
    case Recent
    case History
    
    var id: Int {
        switch self {
        case .Recent:
            return 0
        case .History:
            return 1
        }
    }

}

class ReservationsTableVC: UITableViewController {
    
    
    //MARK:- Variables
    internal var presenter: ReservationsTableVCPresenter!
    var type: Int!
    var status: AppointmentType = .Recent
        
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: "ReservationCell", bundle: nil), forCellReuseIdentifier: "ReservationCell")
        setPresenter()
        listenNotificationCenter()
    }

    
    //MARK:- Functions
    private func setPresenter() {
        presenter = ReservationsTableVCPresenter(view: self, status: status)
        presenter.viewDidLoad(type: type)
    }
    
    private func listenNotificationCenter() {
        _NC.addObserver(forName: .deleteOrder, object: nil, queue: nil) { _ in
            self.presenter.viewDidLoad(type: self.type)
        }
        
        _NC.addObserver(forName: .chatNewMessageTapped, object: nil, queue: nil) { _ in
            let vc = ChatPageVC()
            vc.id = newMessage.id
            vc.reciverType = Int(newMessage.sender_type)!
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }

    }

    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard  let cell = tableView.dequeueReusableCell(withIdentifier: "ReservationCell", for: indexPath) as? ReservationCell else {
            return UITableViewCell()
        }
        presenter.cellConfigure(cell: cell, at: indexPath.row)
        cell.cancelBtnAction = {
            self.presenter.cancelReservationBtnClicked(at: indexPath.row)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRow(at: indexPath.row, type: self.type)
    }
    
}
