/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation

struct Reservation : Codable {
	let id : Int?
	let order_status : Int?
	let user_id : Int?
	let name : String?
	let image : String?
	let phone : String?
	let time : String?
	let cost : Int?
	let day : String?
	let order_date : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case order_status = "order_status"
		case user_id = "user_id"
		case name = "name"
		case image = "image"
		case phone = "phone"
		case time = "time"
		case cost = "cost"
		case day = "day"
		case order_date = "order_date"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		order_status = try values.decodeIfPresent(Int.self, forKey: .order_status)
		user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		image = try values.decodeIfPresent(String.self, forKey: .image)
		phone = try values.decodeIfPresent(String.self, forKey: .phone)
		time = try values.decodeIfPresent(String.self, forKey: .time)
		cost = try values.decodeIfPresent(Int.self, forKey: .cost)
		day = try values.decodeIfPresent(String.self, forKey: .day)
		order_date = try values.decodeIfPresent(String.self, forKey: .order_date)
	}

}
