//
//  ReservationsTableVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 3/2/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation


class ReservationsTableVCPresenter {
    
    private weak var view: LoaderDelegate?
    private let interactor = ReservationsTableVCAPI()
    private var reservations = [Reservation]()
    private var status: AppointmentType?
    
    
    init(view: LoaderDelegate, status: AppointmentType) {
        self.view = view
        self.status = status
    }
    
    var count: Int {
        return reservations.count
    }
    
    func viewDidLoad(type: Int) {
        if DEF.isLogin  {
            view?.showProgress()
            
            interactor.getAllReservations(status: self.status!.id, type: type, didDataReady: { [weak self](response) in
                guard self != nil else { return }
                self?.view?.hideProgress()
                if response.status == StatusCode.Success.rawValue {
                    self?.reservations = response.data ?? []
                    self?.view?.onSuccess("")
                } else if response.status == StatusCode.ExpiredJWT.rawValue {
                    Messages.instance.loginMessage()
                } else {
                    self?.view?.onFailure(response.message ?? "")
                }
            }) { [weak self](error) in
                guard self != nil else { return }
                self?.view?.hideProgress()
                self?.view?.onEmpty(.no_connection_image, CONNECTION_ERROR)
            }
         } else {
            self.view?.onEmpty(.no_data_image, PLEASE_LOGIN)
        }
    }
    
    func cellConfigure(cell: ReservationCellView, at index: Int) {
        let item = reservations[index]
        var action: String?
        switch item.order_status {
        case 0:
            action = CANCEL
            break
        case 1:
            cell.preventAction()
            action = CONFIRMED
            break
        case 2:
            cell.preventAction()
            action = FINISHED
            break
        case 3:
            cell.preventAction()
            action = CANCELLED
            break
        default:
            break
        }
        cell.configure(image: item.image, name: item.name, date: item.order_date, day: item.day, time: item.time, actiontitle: action)
    }
    
    func cancelReservationBtnClicked(at index: Int) {
        let id = reservations[index].id ?? 0
        if let index = reservations.firstIndex(where: { $0.id == id}) {
            
            Messages.instance.actionsConfigMessage(title: "", body: SURE_TO_CANCEL, buttonTitle: OK) { (success) in
                if success {
                    self.view?.showProgress()
                    
                    //Call API
                    DispatchQueue.global(qos: .background).async {
                        self.interactor.cancelReservation(id: id, didDataReady: { [weak self](response) in
                            guard self != nil else { return }
                            DispatchQueue.main.async {
                                self?.view?.hideProgress()
                                if response.status == StatusCode.Success.rawValue {
                                    _NC.post(name: .deleteOrder, object: nil)
                                    self?.reservations.remove(at: index)
                                    DispatchQueue.main.async {
                                        self?.view?.onSuccess(response.message ?? "")
                                    }
                                } else if response.status == StatusCode.ExpiredJWT.rawValue {
                                    Messages.instance.loginMessage()
                                } else {
                                    self?.view?.onFailure(response.message ?? "")
                                }
                            }
                        }) { [weak self](error) in
                            guard self != nil else { return }
                            DispatchQueue.main.async {
                                self?.view?.hideProgress()
                                self?.view?.onFailure(CONNECTION_ERROR)
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    func didSelectRow(at index: Int, type: Int) {
        let id = reservations[index].id ?? 0
        let info = ["id": id,
                    "type": type]
        _NC.post(name: .showAppointmentDetails, object: nil, userInfo: info)
    }

    
    
}
