//
//  ReservetionsVC.swift
//  MoonUser
//
//  Created by Grand iMac on 3/1/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class ReservetionsVC: BaseViewController {

    //MARK:- Outlets
    @IBOutlet weak var healthView: UIView!
    @IBOutlet weak var beautyView: UIView!
    @IBOutlet weak var healthImg: UIImageView!
    @IBOutlet weak var beautyImg: UIImageView!
    
    
    //MARK:- Variables
    override func viewDidLoad() {
        super.viewDidLoad()
        SharedHandller.instance.sideMenus(self)
        title = RESERVATIONS
        addGestures()
        healthImg.image = healthImg.image?.flipIfNeeded()
        beautyImg.image = beautyImg.image?.flipIfNeeded()
    }
    
    
    //MARK:- Functions
    private func addGestures() {
          let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.healthViewClicked))
          healthView.addGestureRecognizer(tap1)
          
          let tap2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.beautyViewClicked))
          beautyView.addGestureRecognizer(tap2)

      }
            
      
    //MARK:- Actions
    @objc func healthViewClicked() {
        let vc = AllReservationsVC()
        vc.type = .Health
        navigationController?.pushViewController(vc, animated: true)
    }

    @objc func beautyViewClicked() {
        let vc = AllReservationsVC()
        vc.type = .Beauty
        navigationController?.pushViewController(vc, animated: true)
    }

}
