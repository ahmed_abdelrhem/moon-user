//
//  AllReservationsVC.swift
//  MoonUser
//
//  Created by Grand iMac on 3/2/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

enum ReservationType: CaseIterable {
    case Health
    case Beauty
    
    var title: String {
        switch self {
        case .Health:
            return HEALTH_RESERVATIONS
        case .Beauty:
            return BEAUTY_RESERVATIONS
        }
    }
    
    var reservedType: Int {
        switch self {
        case .Health:
            return 9
        case .Beauty:
            return 10
        }
    }
}

class AllReservationsVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var segmentView: UIView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var containerView: UIView!

    
    //MARK:- Variables
    var type: ReservationType?
    
    lazy var recentViewController: ReservationsTableVC = {
        var vc = ReservationsTableVC()
        vc.type = type?.reservedType
        vc.status = .Recent
        self.addViewControllerAsChild(childeViewController: vc)
        return vc
    }()
    
    lazy var historyViewController: ReservationsTableVC = {
        var vc = ReservationsTableVC()
        vc.type = type?.reservedType
        vc.status = .History
        self.addViewControllerAsChild(childeViewController: vc)
        return vc
    }()

    
    
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = type?.title
        backButtonTitle("")
        listenNotificationCenter()
        SharedHandller.instance.setupSegmentFamousHeader(view: segmentView, segmentControl: segmentControl, numberOfSegments: CGFloat(segmentControl!.numberOfSegments))
        recentViewController.view.isHidden = false
    }
    
    func listenNotificationCenter() {
        _NC.addObserver(forName: .showAppointmentDetails, object: nil, queue: nil) { (notification) in
            if let info = notification.userInfo as? [String: Int] {
                if let id = info["id"], let type = info["type"] {
                    let vc = ReservationDetailsVC()
                    vc.id = id
                    vc.type = type
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    
    //MARK:- Actions
    @IBAction func segmentValueChanged(_ sender: UISegmentedControl) {
        self.setupViews()
    }
    


   

}

extension AllReservationsVC {
    
    private func addViewControllerAsChild(childeViewController: UIViewController) {
        childeViewController.view.frame = containerView.bounds
        childeViewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        containerView.addSubview(childeViewController.view)
    }
    
    private func setupViews() {
        recentViewController.view.isHidden = !(segmentControl.selectedSegmentIndex == 0 )
        historyViewController.view.isHidden = !(segmentControl.selectedSegmentIndex == 1 )
    }


    
}
