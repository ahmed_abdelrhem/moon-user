//
//  PrivacyPolicyVCPresenter.swift
//  MoonUser
//
//  Created by apple on 11/15/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation


class PrivacyPolicyVCPresenter {
    
    private weak var view: LoaderDelegate?
    private let interactor = PrivacyPolicyVCAPI()
    private var lists = [List]()
    
    
    init(view: LoaderDelegate) {
        self.view = view
    }
    
    var count: Int {
        return lists.count
    }
    
    func viewDidLoad() {
        view?.showProgress()
        
        interactor.getPrivacyPolicy(didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.lists = response.data ?? []
                if self?.lists.count != 0 {
                    self?.view?.onSuccess("")
                } else {
                    self?.view?.onEmpty(.no_data_image, NO_DATA_FOUND)
                }
            } else if response.status == StatusCode.ExpiredJWT.rawValue {
                Messages.instance.loginMessage()
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onEmpty(.no_connection_image, CONNECTION_ERROR)
        }
    }
    
    func cellConfigure(cell: ListCellView, index: Int) {
        let list = lists[index]
        cell.configure(title: list.title, message: list.message)
    }
    
    
}

