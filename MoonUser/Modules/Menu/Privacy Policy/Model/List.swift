//
//  List.swift
//  MoonUser
//
//  Created by apple on 11/15/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
struct List : Codable {
    let id : Int?
    let title : String?
    let message : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case title = "title"
        case message = "message"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }

}
