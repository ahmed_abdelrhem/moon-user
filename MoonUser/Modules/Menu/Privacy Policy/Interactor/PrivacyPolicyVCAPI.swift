//
//  PrivacyPolicyVCAPI.swift
//  MoonUser
//
//  Created by apple on 11/15/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class PrivacyPolicyVCAPI {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    
    typealias model = (ListModel)->()

    
    func getPrivacyPolicy( didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param = [
            "type": 13,
            "setting_type": 1
        ]
        
        let url = URLs.settings.url
        
        apiManager.contectToApiWith(url: url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(ListModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
        
}
