//
//  TechnicalSupportVC.swift
//  MoonUser
//
//  Created by Islam 3bRahiem on 6/30/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class TechnicalSupportVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var messageTxt: UITextView!
    
    
    //MARK:- Variables
    private var presenter: TechnicalSupportVCPresenter!
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        SharedHandller.instance.sideMenus(self)
        title = TECH_SUPPORT
        setPresenter()
    }
    
    
    //MARK:- Functions
    private func setPresenter() {
        presenter = TechnicalSupportVCPresenter(view: self)
    }
    
    //MARK:- Actions
    @IBAction func sendBtnClicked(_ sender: UIButton) {
        presenter.sendBtnClicked(message: messageTxt.text)
    }


}
