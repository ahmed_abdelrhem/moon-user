//
//  FavouritesVC.swift
//  MoonUser
//
//  Created by Islam 3bRahiem on 5/2/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class FavouritesVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView(frame: .zero)
            tableView.delegate = self
            tableView.dataSource = self
            tableView.separatorStyle = .singleLine
            tableView.register(UINib(nibName: "FavouriteCell", bundle: nil), forCellReuseIdentifier: "FavouriteCell")
        }
    }
    @IBOutlet weak var adsView: UIView!
    @IBOutlet weak var productsView: UIView!

    
    //MARK:- Variables
    internal var presenter: FavouriteVCPresenter!
    
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = FAVORITE
        SharedHandller.instance.sideMenus(self)
        setPresenter()
        addGestures()
    }



    //MARK:- Functions
    private func setPresenter() {
        presenter = FavouriteVCPresenter(view: self)
        presenter.viewDidLoad()
    }
    
    private func addGestures() {
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.adsViewClicked))
        adsView.addGestureRecognizer(tap1)
        
        let tap2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.productsViewClicked))
        productsView.addGestureRecognizer(tap2)
    }
    
    
    //MARK:- Actions
    @objc private func adsViewClicked() {
        presenter.adsViewClicked()
    }
    
    @objc private func productsViewClicked() {
        presenter.productsViewClicked()
    }
}
