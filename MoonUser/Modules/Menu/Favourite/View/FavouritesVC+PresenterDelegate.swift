//
//  FavouritesVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Islam 3bRahiem on 5/2/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension FavouritesVC: FavouriteVCView {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        tableView.restore()
        tableView.reloadData()
        
        if tableView.numberOfRows(inSection: 0) == 0 {
            tableView.setEmptyView(title: NO_DATA_FOUND, messageImage: .no_data_image)
        }
    }
    
    func onEmpty(_ img: UIImage, _ msg: String) {
        tableView.setEmptyView(title: msg, messageImage: img)
    }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }

    func setAds() {
        adsView.backgroundColor = .mainColor
        productsView.backgroundColor = .darkBlueColor
    }
    
    func setProducts() {
        adsView.backgroundColor = .darkBlueColor
        productsView.backgroundColor = .mainColor
    }
    
    func reloadRow(at indexPath: IndexPath) {
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }

    func navigateToProductDetailsVC(_ id: Int) {
        let vc = ProductDetailsVC()
        vc.productID = id
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func navigateToAdDetailsVC(_ id: Int, title: String?) {
        let vc = AdDetailsVC()
        vc.id = id
        vc.title = title
        navigationController?.pushViewController(vc, animated: true)
    }


}
