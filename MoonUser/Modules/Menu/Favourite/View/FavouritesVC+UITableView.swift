//
//  FavouritesVC+UITableView.swift
//  MoonUser
//
//  Created by Islam 3bRahiem on 5/2/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension FavouritesVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FavouriteCell") as? FavouriteCell else {
            return UITableViewCell()
        }
        presenter.favCellConfigure(cell: cell, at: indexPath.row)
        cell.favBtnActions = {
            self.presenter.favBtnActionClicked(at: indexPath)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRowAt(indexPath.row)
    }

}
