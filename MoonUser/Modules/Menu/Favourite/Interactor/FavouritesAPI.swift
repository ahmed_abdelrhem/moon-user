//
//  FavouritesAPI.swift
//  MoonUser
//
//  Created by Islam 3bRahiem on 5/2/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class FavouritesAPI: NSObject {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    
    typealias model = (FavouritesModel)->()
    
    
    func getFavourites(type: Int, didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param = [
            "type": type,
        ]
        
        let url = URLs.favorite.url.concatURL(param)
        
        apiManager.contectToApiWith(url: url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
                                        
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                if let result = try? self.decoder.decode(FavouritesModel.self, from: data) {
                    didDataReady(result)
                } else {
                    print("get Favourites error")
                }
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
    
    func addOrRemoveFavourite(productID: Int, type: Int) {
        
        let param = [
            "product_id": productID,
            "type": type,
            ]
        
        apiManager.contectToApiWith(url: URLs.addFavourite.url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in
            
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    _ = try self.decoder.decode(StatusModel.self, from: data)
                }catch{
                    print("error: \(error)")
                }
            }
        }) { (error, msg) in
            print(error, msg!)
        }
    }

    
    
}
