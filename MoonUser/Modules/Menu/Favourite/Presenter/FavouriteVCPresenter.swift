//
//  FavouriteVCPresenter.swift
//  MoonUser
//
//  Created by Islam 3bRahiem on 5/2/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol FavouriteVCView: LoaderDelegate {
    func setAds()
    func setProducts()
    func reloadRow(at indexPath: IndexPath)
    func navigateToProductDetailsVC(_ id: Int)
    func navigateToAdDetailsVC(_ id: Int, title: String?)
}


class FavouriteVCPresenter {
    
    private var view: FavouriteVCView?
    private let interactor = FavouritesAPI()
    private var ads = [FavouriteItem]()
    private var products = [FavouriteItem]()
    private var type: FavouriteType = .asAd
    
    
    init(view: FavouriteVCView) {
        self.view = view
    }
    
    
    func viewDidLoad() {

        if DEF.isLogin {

            view?.showProgress()
            
            let concurrentQueue = DispatchQueue(label: "favourites.queue", attributes: .concurrent)
            
            concurrentQueue.async {
                self.getAds()
            }
            
            concurrentQueue.async {
                self.getProducts()
            }
        } else {
            self.view?.onEmpty(.no_data_image, PLEASE_LOGIN)
        }
    }
    
    var count: Int {
        return type == .asAd ? ads.count : products.count
    }

    func favCellConfigure(cell: FavouriteCellView, at index: Int) {
        let item = type == .asAd ? ads[index] : products[index]
        cell.configure(type: self.type, img: item.image, name: item.name, seller: item.seller_name, price: item.price, isFav: item.isFav)
    }

    func favBtnActionClicked(at indexPath: IndexPath) {
        let index = indexPath.row
        let item = type == .asAd ? ads[index] : products[index]
        if type == .asAd {
            ads[index].isFav = !ads[index].isFav
        } else {
            products[index].isFav = !products[index].isFav
        }
        view?.reloadRow(at: indexPath)
        
        DispatchQueue.global(qos: .background).async {
            self.interactor.addOrRemoveFavourite(productID: item.id ?? 0, type: item.type ?? 0)
        }
    }

    func didSelectRowAt(_ index: Int) {
        if type == .asProduct {
            let item = products[index]
            DEF.type = item.type ?? 0
            DEF.flag = item.flag ?? 0
            view?.navigateToProductDetailsVC(item.id ?? 0)
        } else {
            let item = ads[index]
            let id = item.id ?? 0
            let title = item.name 
            view?.navigateToAdDetailsVC(id, title: title)
        }
    }
    
    func adsViewClicked() {
        type = .asAd
        view?.setAds()
        
        guard DEF.isLogin else {
            self.view?.onEmpty(.no_data_image, PLEASE_LOGIN)
            return
        }
        
        view?.onSuccess("")
    }
    
    func productsViewClicked() {
        type = .asProduct
        view?.setProducts()
        
        guard DEF.isLogin else {
            self.view?.onEmpty(.no_data_image, PLEASE_LOGIN)
            return
        }

        view?.onSuccess("")
    }



    
}


//MARK:- Private Functions
extension FavouriteVCPresenter {
    
    private func getAds() {
        
        interactor.getFavourites(type: 1, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            DispatchQueue.main.async {
                self?.view?.hideProgress()
            }
            if response.status == StatusCode.Success.rawValue {
                self?.ads = response.data ?? []
                DispatchQueue.main.async {
                    self?.view?.onSuccess("")
                }
            } else if response.status == StatusCode.ExpiredJWT.rawValue {
                DispatchQueue.main.async {
                    Messages.instance.loginMessage()
                }
            } else {
                DispatchQueue.main.async {
                    self?.view?.onFailure(response.message ?? "")
                }
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            DispatchQueue.main.async {
                self?.view?.hideProgress()
                self?.view?.onEmpty(.no_connection_image, CONNECTION_ERROR)
            }
        }
    }
    
    
    private func getProducts() {
        interactor.getFavourites(type: 2, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            if response.status == StatusCode.Success.rawValue {
                self?.products = response.data ?? []
            }
        }) { _ in }
    }

    
}
