//
//  AboutVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 3/3/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class AboutVCPresenter {
    
    private weak var view: LoaderDelegate?
    private let interactor = SignupVCAPI()
    
    
    init(view: LoaderDelegate) {
        self.view = view
    }
    
    func viewDidLoad(type: Int) {
        
        view?.showProgress()
        
        interactor.getSettings(type: type, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.view?.onSuccess(response.data ?? "")
            } else if response.status == StatusCode.ExpiredJWT.rawValue {
                Messages.instance.loginMessage()
            } else {
                self?.view?.warringMSG(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
        
    }
    
}
