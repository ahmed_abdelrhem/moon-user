//
//  AboutUsVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 3/3/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension AboutUsVC: LoaderDelegate {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        dataLbl.text = msg
    }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }

}
