//
//  AboutUsVC.swift
//  MoonUser
//
//  Created by Grand iMac on 3/3/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

enum SettingType: CaseIterable {
    
    case AboutUs
    case Privacy
    
    var type: Int {
        switch self {
            case .AboutUs: return 2
            case .Privacy: return 1
        }
    }
    
    var title: String {
        switch self {
            case .AboutUs: return ABOUT_US
            case .Privacy: return PRIVACY
        }
    }
    
}


class AboutUsVC: UIViewController {

    
    //MARK:- Outlets
    @IBOutlet weak var dataLbl: UILabel!
    
    
    //MARK:- Variabels
    internal var  presenter: AboutVCPresenter!
    var setting: SettingType!
    
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        SharedHandller.instance.sideMenus(self)
        title = setting.title
        setPresenter()
        listenNotificationCenter()
    }
    
    
    
    //MARK:- Fcuntions
    private func setPresenter() {
        presenter = AboutVCPresenter(view: self)
        presenter.viewDidLoad(type: setting.type)
    }
    
    private func listenNotificationCenter() {
       _NC.addObserver(forName: .chatNewMessageTapped, object: nil, queue: nil) { _ in
           let vc = ChatPageVC()
           vc.id = newMessage.id
           vc.reciverType = Int(newMessage.sender_type)!
           vc.hidesBottomBarWhenPushed = true
           self.navigationController?.pushViewController(vc, animated: true)
       }
   }


   

}
