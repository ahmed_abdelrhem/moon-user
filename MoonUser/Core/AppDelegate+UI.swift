//
//  AppDelegate+UI.swift
//  MoonUser
//
//  Created by Grand iMac on 1/26/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift
import MOLH


//MARK:- UI Helper Functions
extension AppDelegate {
    
    func setAuthentications() {
        //MARK:- User Default file
        let filePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("App.Plist")
        print("User Default : \(filePath!)\n")
        
        IQKeyboardManager.shared.enable = true
        

        //MARK:- MOLH


        //MARK:- Google Maps
        GMSServices.provideAPIKey(GOOGLE_API_KEY)
        GMSPlacesClient.provideAPIKey(GOOGLE_API_KEY)
    }

    
    func setAppUI() {
        //MARK:- Navigation Controller
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().tintColor = .mainColor
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        UINavigationBar.appearance().shadowImage = UIImage()

        
        //MARK:- Tabbar setting
        UITabBar.appearance().shadowImage = UIImage()
        UITabBar.appearance().unselectedItemTintColor = .black
        
        
        //MARK:- UIConstraintBasedLayoutLogUnsatisfiable
        UserDefaults.standard.set(false, forKey: "_UIConstraintBasedLayoutLogUnsatisfiable")
    }
    
    func reset() {
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        let storyboard = UIStoryboard(name: Storyboard.Main.name, bundle: nil)
        rootviewcontroller.rootViewController = storyboard.instantiateViewController(withIdentifier: Identifiers.Splash.id)
    }

}
