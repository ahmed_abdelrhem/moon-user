//
//  AppDelegate+FirebaseDelegate.swift
//  QApp Driver
//
//  Created by Islamic on 9/16/19.
//  Copyright © 2019 Organization. All rights reserved.
//

import Foundation
import Firebase
import FirebaseMessaging
import FirebaseInstanceID
import UserNotifications
import SwiftyJSON




var driverData = DriverData()
var newMessage = FirebaseMessage()

extension AppDelegate {
    
    //MARK:- Firebase
    func FirebaseConfigure(_ application: UIApplication)  {
        
        FirebaseApp.configure()
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (success, error) in
            if error == nil {
                print("Successful Authorization")
            }
        }
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        
        application.registerForRemoteNotifications()
        
        _NC.addObserver(self, selector: #selector(self.refreshToken(notification:)), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                DEF.DeviceToken = result.token
            }
        }
    }
    
    @objc func refreshToken (notification : NSNotification){
        
        //update device token method can be call here
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                DEF.DeviceToken = result.token
            }
        }
        FBHandler()
    }
    
    func FBHandler(){
        Messaging.messaging().shouldEstablishDirectChannel = true
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // print("Unable to register for remote notifications: \(error.localizedDescription)")
        print("APNs registration failed: \(error)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        print("APNs device token: \(deviceTokenString)")
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("FCM token: \(result.token)")
                DEF.DeviceToken = result.token
            }
        }
    }
    
}


@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo as! [String : Any]
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        print(userInfo)
        
        let myJson = JSON(userInfo)
        
        completionHandler([.alert, .badge, .sound])
        
        print("myJson : ",myJson)
        print("RECIVE")
        
        let state = UIApplication.shared.applicationState
        guard let type = myJson["gcm.notification.notification_type"].string else { return }
        
        
        switch type {
            
        case "2", "5": //Chat
            if let notiStr = userInfo["message"] as? String, let dict = convertToJson(text: notiStr) {
                let id =  dict["id"] as! Int
                let image =  dict["image"] as! String
                let type_data =  dict["type_data"] as! Int
                let sender_type =  dict["sender_type"] as! String
                let audio =  dict["audio"] as! String
                let body =  dict["body"] as! String
                let receiver_type =  dict["receiver_type"] as! String
                let receiver_id =  dict["receiver_id"] as! String
                DEF.userData.chats_count =  dict["chats_count"] as! Int
                
                newMessage = FirebaseMessage(id: id, body: body, image: image, audio: audio, type_data: type_data, sender_type: sender_type, receiver_type: receiver_type, receiver_id: receiver_id)

                if state == .background || state == .active {
                    perform(#selector(onNewMessage), with: nil, afterDelay: 0.0)
                }

            }
            break
            
        case "7": //Driver Accept Order
           guard let status = myJson["gcm.notification.status"].string else { return }
           guard let id = myJson["gcm.notification.order_id"].string else { return }

           if status == "2" || status == "5"  {
               if state == .background || state == .active {
                   perform(#selector(onDriverCancelOrder), with: nil, afterDelay: 0.0)
               }
           } else if status == "4" {
               if state == .background || state == .active {
                   perform(#selector(onDriverArriveLocation), with: nil, afterDelay: 0.0)
               }
           } else if status == "6" {
               guard let price = myJson["gcm.notification.price"].string else { return }
               guard let name = myJson["gcm.notification.name"].string else { return }
               guard let image = myJson["fcm_options"]["image"].string else { return }
               
               driverData = DriverData(tripID: id, price: price, name: name, image: image)

               if state == .background || state == .active {
                   perform(#selector(onRateDriver), with: nil, afterDelay: 0.0)
               }
           } else if status != "3" {
               DEF.tripID = Int(id)!
               if state == .background || state == .active {
                   perform(#selector(onDriverAcceptedOrder), with: nil, afterDelay: 0.0)
               }
           }
           break
        default:
            break
        }
        
    }

    
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        print(userInfo)
        
        let myJson = JSON(userInfo)
        print("myJson : ",myJson)
        print("TAPPED")
        
        let state = UIApplication.shared.applicationState
        guard let type = myJson["gcm.notification.notification_type"].string else { return }
        
        
        switch type {
        case "1": //Shop Order
            if let notiStr = userInfo["message"] as? String, let dict = convertToJson(text: notiStr) {
                let id =  dict["order_id"] as! Int
                DEF.orderID = id
                if state == .active {
                    perform(#selector(onNewOrderTapped), with: nil, afterDelay: 0.0)
                } else {
                    perform(#selector(onNewOrderTapped), with: nil, afterDelay: 2.5)
                }

            }
            break
            
        case "2": //Order Chat
            if let notiStr = userInfo["message"] as? String, let dict = convertToJson(text: notiStr) {
                let id =  dict["id"] as! Int
                let image =  dict["image"] as! String
                let type_data =  dict["type_data"] as! Int
                let sender_type =  dict["sender_type"] as! String
                let audio =  dict["audio"] as! String
                let body =  dict["body"] as! String
                let receiver_type =  dict["receiver_type"] as! String
                
                newMessage = FirebaseMessage(id: id, body: body, image: image, audio: audio, type_data: type_data, sender_type: sender_type, receiver_type: receiver_type, receiver_id: "0")

                if state == .active {
                    perform(#selector(onOrderNewMessageTapped), with: nil, afterDelay: 0.0)
                } else {
                    perform(#selector(onOrderNewMessageTapped), with: nil, afterDelay: 2.5)
                }
            }
            break
            
        case "5": //Private Chat
            if let notiStr = userInfo["message"] as? String, let dict = convertToJson(text: notiStr) {
                let id =  dict["id"] as! Int
                let image =  dict["image"] as! String
                let type_data =  dict["type_data"] as! Int
                let sender_type =  dict["sender_type"] as! String
                let audio =  dict["audio"] as! String
                let body =  dict["body"] as! String
                let receiver_type =  dict["receiver_type"] as! String
                let receiver_id =  dict["receiver_id"] as! String
                
                newMessage = FirebaseMessage(id: id, body: body, image: image, audio: audio, type_data: type_data, sender_type: sender_type, receiver_type: receiver_type, receiver_id: receiver_id)

                if state == .active {
                    perform(#selector(onNewMessageTapped), with: nil, afterDelay: 0.0)
                } else {
                    perform(#selector(onNewMessageTapped), with: nil, afterDelay: 2.5)
                }
            }
            break
            
        case "7": //Driver Accept Order
            guard let status = myJson["gcm.notification.status"].string else { return }
            guard let id = myJson["gcm.notification.order_id"].string else { return }

            if status == "2" || status == "5" {
                Messages.instance.showConfigMessage(title: "", body: "TRIP_CANCEL", state: .warning, layout: .statusLine, style: .top)
            } else if status == "6" {
                if DEF.tripID != 0 {
                    guard let price = myJson["gcm.notification.price"].string else { return }
                    guard let name = myJson["gcm.notification.name"].string else { return }
                    guard let image = myJson["fcm_options"]["image"].string else { return }

                    driverData = DriverData(tripID: id, price: price, name: name, image: image)

                    if state == .active {
                        perform(#selector(onRateDriver), with: nil, afterDelay: 0.0)
                    } else {
                        perform(#selector(onRateDriver), with: nil, afterDelay: 2.5)
                    }
                } else {
                    Messages.instance.showConfigMessage(title: "", body: TRIP_FINISHED, state: .warning, layout: .statusLine, style: .top)
                }
            } else {
                DEF.tripID = Int(id)!
                if state == .active {
                    perform(#selector(onDriverAcceptedOrder), with: nil, afterDelay: 0.0)
                } else {
                    perform(#selector(onDriverAcceptedOrder), with: nil, afterDelay: 2.5)
                }
            }
            break
            
        case "9": //Clinic Order
            if let notiStr = userInfo["message"] as? String, let dict = convertToJson(text: notiStr) {
                let id =  dict["order_id"] as! Int
                DEF.clinicID = id
                DEF.clinicType = 9

                if state == .active {
                    perform(#selector(onClinicOrderTapped), with: nil, afterDelay: 0.0)
                } else {
                    perform(#selector(onClinicOrderTapped), with: nil, afterDelay: 2.5)
                }
            }
            break
            
        case "10": //Beauty order
            if let notiStr = userInfo["message"] as? String, let dict = convertToJson(text: notiStr) {
                let id =  dict["order_id"] as! Int
                DEF.beautyID = id
                DEF.beautyType = 10

                if state == .active {
                    perform(#selector(onBeautyOrderTapped), with: nil, afterDelay: 0.0)
                } else {
                    perform(#selector(onBeautyOrderTapped), with: nil, afterDelay: 2.5)
                }
            }
            break
            
        default:
            break
        }

        
    }
    
    
    //TODO:- Helper Functions
    @objc func onDriverAcceptedOrder() {
        let vc = DriverDetailsVC()
        vc.modalPresentationStyle = .overFullScreen
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        rootviewcontroller.rootViewController?.present(vc, animated: true, completion: nil)
        _NC.post(name: .driverAcceptOrder, object: nil, userInfo: nil)
        self.clearNotifications()
    }

    @objc func onDriverCancelOrder() {
        _NC.post(name: .driverCancelOrder, object: nil, userInfo: nil)
    }
    
    @objc func onDriverArriveLocation() {
        _NC.post(name: .driverArrivedLocation, object: nil, userInfo: nil)
    }
    
    @objc func onRateDriver() {
        let vc = RateDriverVC()
        vc.modalPresentationStyle = .overFullScreen
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        rootviewcontroller.rootViewController?.present(vc, animated: true, completion: nil)
        _NC.post(name: .rateDriver, object: nil, userInfo: nil)
        self.clearNotifications()
    }

    @objc func onNewMessage() {
        _NC.post(name: .chatNewMessage, object: nil, userInfo: nil)
    }
    
    @objc func onNewMessageTapped() {
        _NC.post(name: .chatNewMessageTapped, object: nil, userInfo: nil)
    }
    
    @objc func onOrderNewMessageTapped() {
        _NC.post(name: .orderNewMessageTapped, object: nil, userInfo: nil)
    }
    
    @objc func onNewOrderTapped() {
        _NC.post(name: .newOrder, object: nil, userInfo: nil)
    }
    
    @objc func onClinicOrderTapped() {
        _NC.post(name: .clinicOrder, object: nil, userInfo: nil)
    }
    
    @objc func onBeautyOrderTapped() {
        _NC.post(name: .beautyOrder, object: nil, userInfo: nil)
    }


    
    
    private func clearNotifications() {
        //Clear older Firebase notifications
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
    }



    
}

// [END ios_10_message_handling]

extension AppDelegate : MessagingDelegate {
    
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        _NC.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        DEF.DeviceToken = fcmToken
        DEF.synchronize()
        
    }
    
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    
    func convertToJson(text: String) -> [String: Any]? {
            if let data = text.data(using: .utf8) {
                do {
                    return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                } catch {
                    print(error.localizedDescription)
                }
            }
            return nil
    }


    
}
