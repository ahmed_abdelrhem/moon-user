//
//  UILabelExt.swift
//
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation
import UIKit
import MOLH


//MARK:- Tranaslation
extension UILabel {
    @IBInspectable var localizedString: String {
        get {
            return ""
        }
        set(value) {
            self.text = NSLocalizedString(value, comment: "")
        }
    }
    
    func underline() {
        if let textString = self.text {
            let attributedString = NSMutableAttributedString(string: textString)
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: attributedString.length - 1))
            attributedText = attributedString
        }
    }

}


extension String {
    
        
    var localized: String {
        let lang = MOLHLanguage.currentAppleLanguage()
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }

    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }

    
    func findHashtageText() -> NSMutableAttributedString{
        let hashtagAttribute = [NSAttributedString.Key.font: UIFont.myRegularSystemFont(ofSize: 18), NSAttributedString.Key.foregroundColor: UIColor.blue]
        let normalAttribute = [NSAttributedString.Key.font: UIFont.myRegularSystemFont(ofSize: 18), NSAttributedString.Key.foregroundColor: UIColor.black]
        let mainAttributedString = NSMutableAttributedString(string: self, attributes: normalAttribute)
        let txtViewReviewText = self as NSString

        self.findMentionText().forEach { if self.contains($0) {
            mainAttributedString.addAttributes(hashtagAttribute, range: txtViewReviewText.range(of: $0))
            }
        }
        return mainAttributedString
    }

    
    func findMentionText() -> [String] {
        var arr_hasStrings:[String] = []
        let regex = try? NSRegularExpression(pattern: "(#[a-zA-Z0-9_\\p{Arabic}\\p{N}]*)", options: [])
        if let matches = regex?.matches(in: self, options:[], range:NSMakeRange(0, self.count)) {
            for match in matches {
                arr_hasStrings.append(NSString(string: self).substring(with: NSRange(location:match.range.location, length: match.range.length)))
            }
        }
        return arr_hasStrings
    }
    
    func strikeThrough() -> NSAttributedString {
        let attributeString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: NSMakeRange(0,attributeString.length))
        return attributeString
    }
    
    
    func concatURL(_ param: [String: Any?]) -> String {
        var newURL = self
        if param.count != 0 {
            newURL += "?"
        }
        for p in param {
            if p.value != nil {
                newURL += "&\(p.key)=\(p.value!)"
            }
        }
        return newURL
    }


    var fixedArabicURL: String  {
        return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics
            .union(CharacterSet.urlPathAllowed)
            .union(CharacterSet.urlFragmentAllowed))!
    }

    
}

