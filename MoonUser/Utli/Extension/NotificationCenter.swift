//
//  NotificationCenter.swift
//  RahtyWorker
//
//  Created by Islamic on 5/14/19.
//  Copyright © 2019 2grand. All rights reserved.
//

import Foundation

let _NC = NotificationCenter.default

//Notification name Ext.
extension Notification.Name {
    
    static let addReview = Notification.Name("AddReview")
    static let updateAd = Notification.Name("UpdateAd")
    static let deleteOrder = Notification.Name("DeleteOrder")
    static let showAppointmentDetails = Notification.Name("ShowAppointmentDetails")
    
    static let driverAcceptOrder = Notification.Name("DriverAcceptOrder")
    static let driverCancelOrder = Notification.Name("DriverCancelOrder")
    static let driverArrivedLocation = Notification.Name("DriverArriveLocation")
    static let rateDriver = Notification.Name("RateDriver")
    static let chatNewMessage = Notification.Name("ChatNewMessage")
    static let chatNewMessageTapped = Notification.Name("ChatNewMessageTapped")
    static let orderNewMessageTapped = Notification.Name("OrderNewMessageTapped")
    static let newOrder = Notification.Name("NewOrder")
    static let clinicOrder = Notification.Name("ClinicOrder")
    static let beautyOrder = Notification.Name("BeautyOrder")

}

