//
//  UIViewControllerExt.swift
//
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
//import Lottie


//let ANIMATION_VIEW = AnimationView(name: "Lottie")

//MARK:- UIViewController
extension UIViewController: NVActivityIndicatorViewable {

    //Indicator
//    func startAnimating(ponit:(x:CGFloat,y:CGFloat)) {
//        ANIMATION_VIEW.frame = CGRect(x: 0, y: 0, width: 250, height: 250)
//        ANIMATION_VIEW.center = CGPoint(x: ponit.x, y: ponit.y)
//        ANIMATION_VIEW.contentMode = .scaleAspectFit
//        ANIMATION_VIEW.loopMode = .loop
//        self.view.addSubview(ANIMATION_VIEW)
//        ANIMATION_VIEW.play()
//    }

//    func startAnimating() {
//        self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
//        self.view.alpha = 0.0;
//        UIView.animate(withDuration: 0.25) {
//            self.view.alpha = 1.0
//            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
//        }
//    }
//    
//    func stopAnimating() {
//        ANIMATION_VIEW.stop()
//        ANIMATION_VIEW.removeFromSuperview()
//    }
    
    func addViewControllerAsChild(view containerView: UIView, vc childeViewController: UIViewController) {
        childeViewController.view.frame = containerView.bounds
        childeViewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        containerView.addSubview(childeViewController.view)
    }

    //Hide Keyboard
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    //Hide back button title
    func backButtonTitle(_ title: String) {
        if let topItem = self.navigationController?.navigationBar.topItem {
            topItem.backBarButtonItem = UIBarButtonItem(title: title, style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        }
    }
    
    
    func setNavigationBarIsTransparent() {
        //Set navigationBar is transparent
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.isTranslucent = true
    }

    func setChatHistoryBadgeValue() {
        if let tabItems = self.tabBarController?.tabBar.items {
            let chatItem = tabItems[1]
            guard DEF.userData.chats_count > 0 else {
                chatItem.badgeValue = nil
                return
            }
            chatItem.badgeValue = "\(DEF.userData.chats_count)"
        }
    }
   
    
    
}


class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        _NC.addObserver(forName: .driverAcceptOrder, object: nil, queue: nil) { _ in
            let vc = DriverDetailsVC()
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: false, completion: nil)
        }
        
        _NC.addObserver(forName: .rateDriver, object: nil, queue: nil) { _ in
            let vc = RateDriverVC()
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: false, completion: nil)
        }

        _NC.addObserver(forName: .chatNewMessageTapped, object: nil, queue: nil) { _ in
            let vc = ChatPageVC()
            vc.id = newMessage.id
            vc.reciverType = Int(newMessage.sender_type)!
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        _NC.addObserver(forName: .orderNewMessageTapped, object: nil, queue: nil) { _ in
            let vc = ChatPageVC()
            vc.id = newMessage.id
            vc.reciverType = Int(newMessage.sender_type)!
            vc.type = .Order
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        _NC.addObserver(forName: .clinicOrder, object: nil, queue: nil) { _ in
            let vc = ReservationDetailsVC()
            vc.id = DEF.clinicID
            vc.type = DEF.clinicType
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
       }
        
        _NC.addObserver(forName: .beautyOrder, object: nil, queue: nil) { _ in
           let vc = ReservationDetailsVC()
           vc.id = DEF.beautyID
           vc.type = DEF.beautyType
           vc.hidesBottomBarWhenPushed = true
           self.navigationController?.pushViewController(vc, animated: true)
       }
        
        _NC.addObserver(forName: .chatNewMessage, object: nil, queue: nil) { _ in
            self.setChatHistoryBadgeValue()
        }
    }
    
}



final class OneTextFieldViewController: UIViewController {
    
    fileprivate lazy var textField: TextField = TextField()
    
    struct ui {
        static let height: CGFloat = 44
        static let hInset: CGFloat = 12
        static var vInset: CGFloat = 12
    }
    
    
    init(vInset: CGFloat = 12, configuration: TextField.Config?) {
        super.init(nibName: nil, bundle: nil)
        view.addSubview(textField)
        ui.vInset = vInset
        
        /// have to set textField frame width and height to apply cornerRadius
        textField.height = ui.height
        textField.width = view.width
        
        configuration?(textField)
        
        preferredContentSize.height = ui.height + ui.vInset
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("has deinitialized")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        textField.width = view.width - ui.hInset * 2
        textField.height = ui.height
        textField.center.x = view.center.x
        textField.center.y = view.center.y - ui.vInset / 2
    }
}
