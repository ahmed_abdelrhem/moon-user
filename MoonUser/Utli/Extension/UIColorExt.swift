//
//  UIColorExt.swift
//  MoonUser
//
//  Created by Grand iMac on 3/1/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension UIColor {
    class var mainColor : UIColor {
        return UIColor(named: "MainColor")!
    }
    class var cyanBlueColor : UIColor {
        return UIColor(named: "CyanBlue")!
    }
    class var darkBlueColor : UIColor {
        return UIColor(named: "DarkBlue")!
    }
    class var yellowColor : UIColor {
        return UIColor(named: "YellowColor")!
    }
}
