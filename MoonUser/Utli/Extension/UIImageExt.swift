//
//  UIImageExt.swift
//  MoonUser
//
//  Created by Grand iMac on 1/22/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation


//MARK:- constant Images
extension UIImage {
    
    class var placeHolder_image: UIImage {
        return UIImage(named: "no-image")!
    }
    class var avatar_image: UIImage {
        return UIImage(named: "Group1")!
    }
    class var no_data_image: UIImage {
        return UIImage(named: "no-data")!
    }
    class var no_connection_image: UIImage {
        return UIImage(named: "no-connection")!
    }
    class var back_image: UIImage {
        return UIImage(named: "arrow-left4")!
    }
    class var cancel_image: UIImage {
        return UIImage(named: "cancel")!
    }
    
    class var institution_image: UIImage {
        return UIImage(named: "institution")!
    }
    
    class var advertising_image: UIImage {
        return UIImage(named: "advertising")!
    }
    
    class var money_image: UIImage {
        return UIImage(named: "money")!
    }
    
    class var credit_image: UIImage {
        return UIImage(named: "credit")!
    }
    
    class var taxi_image: UIImage {
        return UIImage(named: "taxi intro")!
    }
    
    class var truck_image: UIImage {
        return UIImage(named: "truck intro")!
    }
    
    class var pinImage: UIImage {
        return UIImage(named: "pick-point")!
    }
    
    class var carImage: UIImage {
        return UIImage(named: "Vector")!
    }
    
    class var favImage: UIImage {
        return UIImage(named: "isFav")!
    }
    
    class var unFavImage: UIImage {
        return UIImage(named: "unFav")!
    }
    
    class var shareImage: UIImage {
        return UIImage(named: "share3")!
    }

    class var pickup: UIImage {
        return UIImage(named: "pickup2")!
    }

    
}

extension UIImage {
    
    /// Resizes an image to the specified size.
    ///
    /// - Parameters:
    ///     - size: the size we desire to resize the image to.
    ///
    /// - Returns: the resized image.
    ///
    func imageWithSize(size: CGSize) -> UIImage? {
        
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale);
        let rect = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height);
        draw(in: rect)
        
        let resultingImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return resultingImage
    }
    
    /// Resizes an image to the specified size and adds an extra transparent margin at all sides of
    /// the image.
    ///
    /// - Parameters:
    ///     - size: the size we desire to resize the image to.
    ///     - extraMargin: the extra transparent margin to add to all sides of the image.
    ///
    /// - Returns: the resized image.  The extra margin is added to the input image size.  So that
    ///         the final image's size will be equal to:
    ///         `CGSize(width: size.width + extraMargin * 2, height: size.height + extraMargin * 2)`
    ///
    func imageWithSize(size: CGSize, extraMargin: CGFloat) -> UIImage? {
        
        let imageSize = CGSize(width: size.width + extraMargin * 2, height: size.height + extraMargin * 2)
        
        UIGraphicsBeginImageContextWithOptions(imageSize, false, UIScreen.main.scale);
        let drawingRect = CGRect(x: extraMargin, y: extraMargin, width: size.width, height: size.height)
        draw(in: drawingRect)
        
        let resultingImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return resultingImage
    }
    
    /// Resizes an image to the specified size.
    ///
    /// - Parameters:
    ///     - size: the size we desire to resize the image to.
    ///     - roundedRadius: corner radius
    ///
    /// - Returns: the resized image with rounded corners.
    ///
    func imageWithSize(size: CGSize, roundedRadius radius: CGFloat) -> UIImage? {
        
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        if let currentContext = UIGraphicsGetCurrentContext() {
            let rect = CGRect(origin: .zero, size: size)
            currentContext.addPath(UIBezierPath(roundedRect: rect,
                                                byRoundingCorners: .allCorners,
                                                cornerRadii: CGSize(width: radius, height: radius)).cgPath)
            currentContext.clip()
            
            //Don't use CGContextDrawImage, coordinate system origin in UIKit and Core Graphics are vertical oppsite.
            draw(in: rect)
            currentContext.drawPath(using: .fillStroke)
            let roundedCornerImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return roundedCornerImage
        }
        return nil
    }
}


extension UIImageView {
    
    func setImageColor(color: UIColor) {
      let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
      self.image = templateImage
      self.tintColor = color
    }
    
    func setAlpha(_ alpha: CGFloat) {
        let tintView = UIView()
        tintView.backgroundColor = UIColor(white: 0, alpha: alpha) //change to your liking
        tintView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.frame.height)
        self.addSubview(tintView)
    }
        
    func fullScreenWhenTapped() {
        self.isUserInteractionEnabled = true
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.imageClicked))
        self.addGestureRecognizer(tap1)
    }
    
    @objc func imageClicked() {
        let vc = FullImageVC()
        vc.image = self.image
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        rootviewcontroller.rootViewController?.present(vc, animated: true, completion: nil)
    }
    
    
}


