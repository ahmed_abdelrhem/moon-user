//
//  UITextViewExt.swift
//
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation
import UIKit

extension UITextView {
    
    //IBInspectable
    @IBInspectable var localizedString: String {
       get {
           return ""
       }
       set(value) {
           self.text = NSLocalizedString(value, comment: "")
       }
    }

    @IBInspectable var PlaceholderLocalized: String {
       get {
           return ""
       }
       set(value) {
           self.PlaceholderLocalized = NSLocalizedString(value, comment: "")
       }
    }
    
    
    func didchange(constraints: NSLayoutConstraint){
        if self.contentSize.height != self.frame.size.height {
            
            let fixedWidth = self.frame.size.width
            self.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            
            var newFrame = self.frame
            let newSize = self.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            
            self.frame = newFrame;
            constraints.constant = CGFloat(newFrame.size.height)
        }
    }


    func numberOfCharactersThatFitTextView() -> Int {
        let fontRef = CTFontCreateWithName(font!.fontName as CFString, font!.pointSize, nil)
        let attributes = [kCTFontAttributeName : fontRef]
        let attributedString = NSAttributedString(string: text!, attributes: attributes as [NSAttributedString.Key : Any])
        let frameSetterRef = CTFramesetterCreateWithAttributedString(attributedString as CFAttributedString)
        
        var characterFitRange: CFRange = CFRange()
        
        CTFramesetterSuggestFrameSizeWithConstraints(frameSetterRef, CFRangeMake(0, 0), nil, CGSize(width: bounds.size.width, height: bounds.size.height), &characterFitRange)
        return Int(characterFitRange.length)
        
    }
    

}


public extension UITextView {
    
    /// Scroll to the bottom of text view
    func scrollToBottom() {
        let range = NSMakeRange((text as NSString).length - 1, 1)
        scrollRangeToVisible(range)
    }
    
    /// Scroll to the top of text view
    func scrollToTop() {
        let range = NSMakeRange(0, 1)
        scrollRangeToVisible(range)
    }
}


public func Log<T>(_ object: T?, filename: String = #file, line: Int = #line, funcname: String = #function) {
    #if DEBUG
        guard let object = object else { return }
        print("***** \(Date()) \(filename.components(separatedBy: "/").last ?? "") (line: \(line)) :: \(funcname) :: \(object)")
    #endif
}
