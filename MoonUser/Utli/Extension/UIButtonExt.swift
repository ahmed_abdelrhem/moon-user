//
//  UIButtonExt.swift
//
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation
import UIKit
import MOLH

extension UIButton {
    @IBInspectable var localizedString: String {
        get {
            return ""
        }
        set(value) {
            self.setTitle(NSLocalizedString(value, comment: ""), for: .normal)
        }
    }
    
    func underline() {
        guard let text = self.titleLabel?.text else { return }
        let attributedString = NSMutableAttributedString(string: text)
        //NSAttributedStringKey.foregroundColor : UIColor.blue
        attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: self.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: self.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: text.count))
        self.setAttributedTitle(attributedString, for: .normal)
    }
    
    func RotatIfNeeded() {
        if MOLHLanguage.isRTLLanguage() {
            self.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        }
    }
}



extension UIApplication {
    
    class func tryURL(urls: [String]) {
        let application = UIApplication.shared
        for url in urls {
            if url != "" {
                if application.canOpenURL(URL(string: url)!) {
                    application.open(URL(string: url)!)
                    return
                }
            }
        }
    }
}
