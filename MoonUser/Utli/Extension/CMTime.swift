//
//  CMTime.swift
//  MoonUser
//
//  Created by Islam 3bRahiem on 4/1/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import AVKit


extension CMTime{
    
    func toDisplayString( ) -> String {
        let totalSecond = Int(CMTimeGetSeconds(self))
        let seconds = totalSecond % 60
        let minutes = totalSecond / 60
        let timeformatString =  String(format: "%02d:%02d",minutes, seconds)
        return timeformatString
    }
}
