//
//  Constant Messages.swift
//  ByDeals
//
//  Created by Grand iMac on 11/27/19.
//  Copyright © 2019 Organization. All rights reserved.
//

import Foundation



//MARK:- MESSAGES
let CONNECTION_ERROR = "Connection Error".localized
let NO_DATA_FOUND = "No Data Found".localized
let PLEASE_LOGIN = "Please login First".localized
let ADD_FILE = "Add a File".localized
let CHOOSE_TYPE = "Choose a filetype to add...".localized
let CAMERA = "Camera".localized
let GALLARY = "Gallary".localized
let LIBRARY = "Images".localized
let VIDEO = "Video".localized
let CAMERA_ACCESS_MSG = "App does not have access to your camera. To enable access, tap settings and turn on Camera.".localized
let PHOTOS_ACCESS_MSG = "App does not have access to your photos. To enable access, tap settings and turn on Photo Library Access.".localized
let VIDEO_ACCESS_MSG = "App does not have access to your video. To enable access, tap settings and turn on Video Library Access.".localized
let SEETING = "Settings".localized
let CANCEL = "Cancel".localized
let OK = "OK".localized
let DONE = "Done".localized

let WELCOME = "Welcome".localized
let ENTER_VALIDATE_EMAIL = "Please enter a valid email".localized
let NOT_MATCHED = "Password not match".localized
let TERMS = "Terms and Conditions".localized
let CHOOSE_LANGUAGE = "Please Choose Your Language".localized
let CODE_SEND = "Code send successfully".localized
let COMPLETE_PROFILE = "Complete your profile".localized
let SELECT_PICTURE = "Please select a profile picture".localized
let WRONG_NUMBER = "Wrong Number".localized
let WRONG_CODE = "Wrong Code".localized
let LOGIN = "Login".localized
let LOGOUT = "Logout".localized
let FOTRGOT_PASSWORD = "Forgot Password".localized
let CODE = "Code".localized
let CHECK_PHONE = "Check Phone".localized
let SIGNUP = "Signup".localized
let MOON_MAP = "Moon Map".localized
let AVG = "Avg :".localized
let MIN = "Min".localized
let ALL_TAGS = "All Tags".localized
let SEARCH = "Search".localized
let DELIVERY = "Delivery".localized
let NO_RESULT = "No Result".localized
let ADDS = "Adds".localized
let REVIEWS = "Reviews".localized
let Add_REVIEW = "Add Review".localized
let FILL_ALL_DATA = "Please fill All data".localized
let REVIEW_DONE = "Review Done Successfully".localized
let ATTENTION = "Attention".localized
let Cart_Message = "If you add this to cart, the old cart will be cancel.".localized
let CONTINUE = "Continue".localized
let PLEASE_CHOOSE = "Please choose".localized
let SELECT_COLOR = "please select color".localized
let SELECT_SIZE = "please select size".localized
let USER_NAME = "User name".localized
let ADDRESS = "Address".localized
let PHONE_NUMBER = "Phone number".localized
let CHECKOUT = "Checkout".localized
let PAYMENT = "Payment".localized
let SHIPPING = "Shipping".localized
let CASH_DELIVERY = "Cash on deleivery".localized
let CREDIT = "Credit card".localized
let HOME_DELIVERY = "Home Deleivery".localized
let From_Shop = "From Shop".localized
let SELECT_PAYMENT = "Please select payment method".localized
let SELECT_SHIPPING = "Please select shipping method".localized
let ORDER_DETAILS = "Order Details".localized
let SHOP_CLOSED = "Sorry shop is cloed".localized
let DELETE = "Delete".localized
let SURE_TO_DELETE = "Are you sure to delete item?".localized
let LOCATION = "Location".localized
let HOTEL_INFO = "Hotel info :-".localized
let SERVICES = "Services :-".localized
let SELLER = "Seller".localized
let PRICE = "Price".localized
let TAGS = "Tags".localized
let CITIES = "Cities".localized
let SELECT_CITY = "Select city where should we place your ad?".localized
let CHOOSE_CATEGORY = "Choose Category...!".localized
let CHOOSE_SUBCATEGORY = "Choose subCategory...!".localized
let ADD_ADVERTISING = "Add Advertising".localized
let EDIT_ADVERTISING = "Edit Advertising".localized
let DONE_UPLOAD = "Done Upload ".localized
let MY_ADS = "My Ads".localized
let PHOTOGRAPHERS = "Photographers".localized
let FAMOUS = "Famous".localized
let FILTER = "Filter".localized
let HEALTH = "Health".localized
let BEAUTY = "Beauty".localized
let YEARS = "Years".localized
let CLINIC_PROFILE = "Clinic Profile".localized
let SALON_PROFILE = "Salon Profile".localized
let CALL = "Call".localized
let NO_PHONE = "No phone number".localized
let CLINIC_SPEC = "Clinic Specialists".localized
let BOOK_APPIONTMENT = "Book an appointment".localized
let NO_APPOINTMENTS = "No appointments".localized
let SELECT_TIME = "Please select book time".localized
let RESERVATIONS = "Reservations".localized
let HEALTH_RESERVATIONS = "Health Reservations".localized
let BEAUTY_RESERVATIONS = "Beauty Reservations".localized
let CONFIRMED = "Confirmed".localized
let FINISHED = "Finished".localized
let CANCELLED = "Cancelled".localized
let SURE_TO_CANCEL = "Are you sure to cancel appointment?".localized
let CHOOSE_SERVICE = "Choose Service".localized
let DOCTOR_FEES = "Doctor fees : ".localized
let TOTAL = "Total : ".localized
let CASH = "Cash".localized
let ONLINE = "Online".localized
let AWAITING_ACCEPT = "Awaiting acceptance".localized
let APPOINTMENT_DETAILS = "Appointment Details".localized
let NEW_PASSWORD = "New Password".localized
let MY_ORDERS = "My Orders".localized
let NOTIFICATIONS = "Notifications".localized
let FOLLOWING = "Following".localized
let TAXI = "Taxi".localized
let TRUCK = "Truck".localized
let TAXI_HISTORY = "Taxi History".localized
let TRUCK_HISTORY = "Truck History".localized
let TURN_LOCATION = "Trun location ON ?".localized
let MOON_TAXI = "Moon Taxi".localized
let CONFIRM_DESTINATION = "Confirm Destination".localized
let CONFIRM_NOW = "Confirm Now".localized
let GO_TO_YOUR_TRIPS_TO_CONTINUES = "Go to your trips to continues".localized
let CONTINUES = "Continues".localized
let RECENT_RIDES = "Showing Recent Rides".localized
let TRIP_DETAILS = "Ride Details".localized
let YOU_RATE = "You Rate".localized
let PER_HOUR = "per hour".localized
let MOON_TRNASPORTATIONS = "Moon Transportations".localized
let NO_DRIVERS = "No Drivers".localized
let NO_CHATS = "No Chats".localized
let TRIP_FINISHED = "Trip has Finished".localized
let ABOUT_US = "About US".localized
let PRIVACY = "Privacy Policy".localized
let CHOOSE_COUNTRY = "Please Choose Your Country".localized
let LANG_MSG = "Moon Will close your App to change language setting".localized
let CHAT = "Chat".localized

let FAVORITE = "Favorite".localized
let CONTACT_US = "Contact Us".localized
let DONE_SEND_MESSAGE = "Message send Successfully".localized
let TECH_SUPPORT = "Technical Support".localized

let FAQ = "FAQ".localized
