//
//  Constant.swift
//  DrWashBedrift
//
//  Created by Islamic on 2/4/19.
//  Copyright © 2019 2grand. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation


//MARK:- TypeaLias
typealias CompletionHandler = (_ success: Bool) -> ()




//MARK:- Show Login Message
func showLoginMessage() {
    Messages.instance.showConfigMessage(title: "", body: PLEASE_LOGIN, state: .warning, layout: .statusLine, style: .top)
}


//MARK:- Functions
func generateThumbnail(path: URL) -> UIImage? {
    do {
        let asset = AVURLAsset(url: path, options: nil)
        let imgGenerator = AVAssetImageGenerator(asset: asset)
        imgGenerator.appliesPreferredTrackTransform = true
        let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
        let thumbnail = UIImage(cgImage: cgImage)
        return thumbnail
    } catch let error {
        print("*** Error generating thumbnail: \(error.localizedDescription)")
        return nil
    }
}




//Go to Home Screen
func homeScreen(_ message: String) {
    let mainStorybord: UIStoryboard = UIStoryboard(name: Storyboard.Tabs.name, bundle: nil)
    let vc = mainStorybord.instantiateViewController(withIdentifier: Identifiers.SWReveal.id)
    UIApplication.shared.keyWindow?.rootViewController = vc
    
    if message != "" {
        Messages.instance.showConfigMessage(title: "", body: message, state: .success, layout: .centeredView, style: .center)
    }
}



//Share
func share(_ vc: UIViewController, data: String, completion: @escaping CompletionHandler) {
    let dataArray = [data]
    let share = UIActivityViewController(activityItems: dataArray, applicationActivities: [])
    share.popoverPresentationController?.sourceView = vc.view
    share.popoverPresentationController?.sourceRect = CGRect(x:0 , y: (vc.view.frame.height), width: 64, height: 64)
    vc.present(share, animated: true, completion: nil)

    share.completionWithItemsHandler = { activity, completed, items, error in
        if completed {
            completion(true)
        }
    }
}



//Sign out
func LogOut() {
    let storyboard = UIStoryboard(name: Storyboard.Auth.name, bundle: nil)
    let vc = storyboard.instantiateViewController(withIdentifier: Identifiers.Login.id)
    UIApplication.shared.keyWindow?.rootViewController = vc

    //Clear Default data
}
