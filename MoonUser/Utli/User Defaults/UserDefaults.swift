//
//  UserDefaults.swift
//
//  Copyright © 2018 2Grand. All rights reserved.
//

import Foundation



let DEF = UserDefaults.standard


extension UserDefaults {
    
    private(set) var DeviceID: String {
        get {
            return DEF.object(forKey: "DeviceID") as? String ?? ""
        } set {
            DEF.set(UIDevice.current.identifierForVendor!.uuidString, forKey: "DeviceID")
            DEF.synchronize()
        }
    }
    
    private(set) var Device_locale: Locale {
    get {
        return DEF.object(forKey: "Device_locale") as? Locale ?? Locale.autoupdatingCurrent
    } set {
        DEF.set(Locale.autoupdatingCurrent, forKey: "Device_locale")
       DEF.synchronize()
        }
    }
    
    private(set) var DeviceModel: String {
        get {
            return DEF.object(forKey: "DeviceModel") as? String ?? ""
        } set {
            DEF.set(UIDevice.current.model, forKey: "DeviceModel")
            DEF.synchronize()
        }
    }
    
    private(set) var systemVersion: String {
        get {
            return DEF.object(forKey: "buildNumber") as? String ?? ""
        } set {
            DEF.set(UIDevice.current.systemVersion, forKey: "buildNumber")
            DEF.synchronize()
        }
    }
    
    private(set) var buildNumber: String {
        get {
            return DEF.object(forKey: "buildNumber") as? String ?? ""
        } set {
            DEF.set(Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String, forKey: "buildNumber")
            DEF.synchronize()
        }
    }
    
    private(set) var APPversion: String {
        get {
            return DEF.object(forKey: "APPversion") as? String ?? ""
        } set {
            DEF.set(Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String, forKey: "APPversion")
            DEF.synchronize()
        }
    }
    
    var isFirstTime: Bool {
        get {
            return DEF.object(forKey: "isFirstTime") as? Bool ?? true
        } set {
            DEF.set(newValue, forKey: "isFirstTime")
            DEF.synchronize()
        }
    }
    
    var DeviceToken: String {
        get {
            return DEF.object(forKey: "device_token") as? String ?? ""
        } set {
            DEF.set(newValue, forKey: "device_token")
            DEF.synchronize()
        }
    }

    var isLogin: Bool {
        get {
            return DEF.object(forKey: "isLogin") as? Bool ?? false
        } set {
            DEF.set(newValue, forKey: "isLogin")
            DEF.synchronize()
        }
    }
    
    var language: String {
        get {
            return DEF.object(forKey: "lang") as? String ?? ""
        } set {
            DEF.set(newValue, forKey: "lang")
            DEF.synchronize()
        }
    }
    
    var allCountries: [CountryData] {
        get {
            if let def = DEF.data(forKey: "allCountries") {
                let data = try! JSONDecoder().decode([CountryData].self, from: def)
                return data
            }
            return []
        } set {
            let data = try! JSONEncoder().encode(newValue)
            DEF.set(data, forKey: "allCountries")
        }
    }

    
    var country: CountryData {
        get {
            if let def = DEF.data(forKey: "Country") {
                let data = try! JSONDecoder().decode(CountryData.self, from: def)
                return data
            }
            return CountryData()
        } set {
            let data = try! JSONEncoder().encode(newValue)
            DEF.set(data, forKey: "Country")
        }
    }
    
    var flag: Int {
        get {
            return DEF.object(forKey: "flag") as? Int ?? 0
        } set {
            DEF.set(newValue, forKey: "flag")
            DEF.synchronize()
        }
    }
    
    var currentLat: String {
        get {
            return DEF.object(forKey: "currentLat") as? String ?? ""
        } set {
            DEF.set(newValue, forKey: "currentLat")
            DEF.synchronize()
        }
    }
    
    var currentLng: String {
        get {
            return DEF.object(forKey: "currentLng") as? String ?? ""
        } set {
            DEF.set(newValue, forKey: "currentLng")
            DEF.synchronize()
        }
    }

    var currentAdd: String {
        get {
            return DEF.object(forKey: "currentAdd") as? String ?? ""
        } set {
            DEF.set(newValue, forKey: "currentAdd")
            DEF.synchronize()
        }
    }

    var type: Int {
        get {
            return DEF.object(forKey: "type") as? Int ?? 0
        } set {
            DEF.set(newValue, forKey: "type")
            DEF.synchronize()
        }
    }
        
    var userData: UserData {
        get {
            if let def = DEF.data(forKey: "UserData") {
                let data = try! JSONDecoder().decode(UserData.self, from: def)
                return data
            }
            return UserData()
        } set {
            let data = try! JSONEncoder().encode(newValue)
            DEF.set(data, forKey: "UserData")
        }
    }
    
    var tripID: Int {
        get {
            return DEF.object(forKey: "TripID") as? Int ?? 0
        } set {
            DEF.set(newValue, forKey: "TripID")
            DEF.synchronize()
        }
    }
    
    var orderID: Int {
        get {
            return DEF.object(forKey: "OrderID") as? Int ?? 0
        } set {
            DEF.set(newValue, forKey: "OrderID")
            DEF.synchronize()
        }
    }
    
    var clinicID: Int {
        get {
            return DEF.object(forKey: "clinicID") as? Int ?? 0
        } set {
            DEF.set(newValue, forKey: "clinicID")
            DEF.synchronize()
        }
    }
    
    var clinicType: Int {
        get {
            return DEF.object(forKey: "clinicType") as? Int ?? 0
        } set {
            DEF.set(newValue, forKey: "clinicType")
            DEF.synchronize()
        }
    }
    
    var beautyID: Int {
        get {
            return DEF.object(forKey: "beautyID") as? Int ?? 0
        } set {
            DEF.set(newValue, forKey: "beautyID")
            DEF.synchronize()
        }
    }

    var beautyType: Int {
        get {
            return DEF.object(forKey: "beautyType") as? Int ?? 0
        } set {
            DEF.set(newValue, forKey: "beautyType")
            DEF.synchronize()
        }
    }




    
}
