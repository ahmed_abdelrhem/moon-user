//
//  SharedHandller.swift
//  ByDeals
//
//  Created by Islamic on 5/28/19.
//  Copyright © 2019 Organization. All rights reserved.
//

import Foundation
import UIKit
import ImageSlideshow
import MOLH


class SharedHandller {
    
    static let instance = SharedHandller()
    
    
    //MARK:- side Menu Button
    private var sideButton : UIBarButtonItem!
    private var backButton : UIBarButtonItem!
    private var VC: UIViewController?
    
    
    private func setSideImage()  {
        let image = UIImage(named: "menu-black")
        sideButton = UIBarButtonItem(image: image,  style: .plain , target: self, action: Selector(("didTapEditButton:")))
    }
    
    func sideMenus(_ control: UIViewController) {
        setSideImage()
        if control.revealViewController() != nil {
            if MOLHLanguage.isRTLLanguage() {
                // right setting
                sideButton.target = control.revealViewController()
                sideButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
                control.navigationItem.leftBarButtonItem = sideButton
                control.revealViewController().rightViewRevealWidth = UIScreen.main.bounds.width - 80
                
            } else {
                // left setting
                sideButton.target = control.revealViewController()
                sideButton.action = #selector(SWRevealViewController.revealToggle(_:))
                control.navigationItem.leftBarButtonItem = sideButton
                control.revealViewController().rearViewRevealWidth = UIScreen.main.bounds.width - 80
            }
            control.revealViewController().frontViewController.view.addGestureRecognizer(control.revealViewController().tapGestureRecognizer())
            control.view.addGestureRecognizer(control.revealViewController().panGestureRecognizer())
        }
    }
    
    
    /*
    private func setBackImage()  {
        let image = UIImage(named: "arrow-left9")
        backButton = UIBarButtonItem(image: image,  style: .plain , target: self, action: Selector(("didTapEditButton:")))
    }
    
    func sideBack(_ control: UIViewController) {
        setBackImage()
        if control.revealViewController() != nil {
            backButton.action = #selector(back)
            control.navigationItem.leftBarButtonItem = backButton
        }
    }
    
    @objc func back() {
        homeScreen("TEST")
    }

 */
    
  
    
    //MARK:- UISegmentedControl
    private var segmentControl: UISegmentedControl?
    private let buttonBar = UIView()
    private var view: UIView?
    private var numberOfSegments: CGFloat?

    func setupSegment(view: UIView, segmentControl: UISegmentedControl, numberOfSegments: CGFloat) {
        
        self.view = view
        self.segmentControl = segmentControl
        self.numberOfSegments = numberOfSegments
        
        segmentControl.backgroundColor = .mainColor
        segmentControl.tintColor = .mainColor
        
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.lightGray], for: .normal)
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        
        segmentControl.addTarget(self, action: #selector(self.segmentedControlValueChanged(_:)), for: UIControl.Event.valueChanged)
        segmentControl.removeBorders()
        
        
        // This needs to be false since we are using auto layout constraints
        buttonBar.translatesAutoresizingMaskIntoConstraints = false
        buttonBar.backgroundColor = UIColor.white
        
        view.addSubview(buttonBar)

        buttonBar.topAnchor.constraint(equalTo: segmentControl.bottomAnchor).isActive = true
        buttonBar.heightAnchor.constraint(equalToConstant: 3.5).isActive = true
        // Constrain the button bar to the width of the segmented control divided by the number of segments
        buttonBar.widthAnchor.constraint(equalTo: segmentControl.widthAnchor, multiplier: CGFloat(1 / numberOfSegments)).isActive = true

        if MOLHLanguage.isRTLLanguage() {
            // Constrain the button bar to the left side of the segmented control
            buttonBar.rightAnchor.constraint(equalTo: segmentControl.rightAnchor).isActive = true
        }else{
            // Constrain the button bar to the left side of the segmented control
            buttonBar.leftAnchor.constraint(equalTo: segmentControl.leftAnchor).isActive = true
        }
    
    }
    
    @objc func segmentedControlValueChanged(_ sender: UISegmentedControl) {
        DispatchQueue.main.async { [unowned self] in
            UIView.animate(withDuration: 0.3) {
                if MOLHLanguage.isRTLLanguage() {
                    self.buttonBar.frame.origin.x = self.view!.width - (self.view!.width / self.numberOfSegments!)  - ( (self.view!.width / self.numberOfSegments!) * CGFloat(self.segmentControl!.selectedSegmentIndex))
                    
                } else {
                    self.buttonBar.frame.origin.x = (self.view!.width / self.numberOfSegments!) * CGFloat(self.segmentControl!.selectedSegmentIndex)
                }
            }
        }
    }
    
    
    
    func setupSegmentFamousHeader(view: UIView, segmentControl: UISegmentedControl, numberOfSegments: CGFloat) {

       self.view = view
       self.segmentControl = segmentControl
       self.numberOfSegments = numberOfSegments
       
       segmentControl.backgroundColor = .clear
       segmentControl.tintColor = .clear
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkText,NSAttributedString.Key.font: UIFont.myBoldSystemFont(ofSize: 18)], for: .normal)
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkBlueColor,NSAttributedString.Key.font: UIFont.myBoldSystemFont(ofSize: 18)], for: .selected)
        
        segmentControl.addTarget(self, action: #selector(self.segmentedControlValueChanged(_:)), for: UIControl.Event.valueChanged)
        segmentControl.removeBorders()


        // This needs to be false since we are using auto layout constraints
        buttonBar.translatesAutoresizingMaskIntoConstraints = false
        buttonBar.backgroundColor = .darkBlueColor

        view.addSubview(buttonBar)
        
        buttonBar.topAnchor.constraint(equalTo: segmentControl.bottomAnchor).isActive = true
        buttonBar.heightAnchor.constraint(equalToConstant: 5).isActive = true
        // Constrain the button bar to the width of the segmented control divided by the number of segments
        buttonBar.widthAnchor.constraint(equalTo: segmentControl.widthAnchor, multiplier: CGFloat(1 / numberOfSegments)).isActive = true

        if MOLHLanguage.isRTLLanguage() {
            // Constrain the button bar to the left side of the segmented control
            buttonBar.rightAnchor.constraint(equalTo: segmentControl.rightAnchor).isActive = true
        }else{
            // Constrain the button bar to the left side of the segmented control
            buttonBar.leftAnchor.constraint(equalTo: segmentControl.leftAnchor).isActive = true
        }

    }

    
    
    
    //MARK:- ImageSlideshow
    private var mySlideShow: ImageSlideshow!
    private var myVC: UIViewController!
    private var sdWebImageSource = [InputSource]()
    
    
    func setSlider(_ vc: UIViewController, slideShow: ImageSlideshow!, images: [String]) {
        self.mySlideShow = slideShow
        self.myVC = vc
        slideShow.slideshowInterval = 3.0
        slideShow.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
        slideShow.contentScaleMode = UIView.ContentMode.scaleAspectFill
        
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = .white
        pageControl.pageIndicatorTintColor = .darkBlueColor
        slideShow.pageIndicator = pageControl
        slideShow.pageIndicatorPosition = PageIndicatorPosition(horizontal: .center, vertical: .bottom)
        slideShow.zoomEnabled = true
        slideShow.setImageInputs(sdWebImageSource)
        sdWebImageSource = [] //Clear old data
        
        //Slider images
        for img in images {
            self.sdWebImageSource.append(SDWebImageSource(urlString: img)!)
        }
        slideShow.setImageInputs(self.sdWebImageSource)

        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTap))
        slideShow.addGestureRecognizer(recognizer)
        
    }
    
    //Slider Tap Action
    @objc private func didTap() {
        let fullScreenController = mySlideShow.presentFullScreenController(from: myVC)
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }

    
}
