//
//  Enum.swift
//  MoonUser
//
//  Created by Islam 3bRahiem on 3/29/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import MOLH

enum Storyboard  {
    case Main
    case Auth
    case Tabs
    
    var name: String {
        switch self {
        case .Main:
            return "Main"
        case .Auth:
            return "Auth"
        case .Tabs:
            return "Tabs"
        }
    }
}


enum Identifiers {
    case Splash
    case Menu
    case SWReveal
    case Login
    case Language
    case Country
    case TurnLocation
    case ForgotPassword
    case CheckPhone
    case Code
    case Signup
    case NewPassword
    case Home
    case Pages
    case Discovers
    
    var id: String {
        switch self {
        case .Splash:
            return "SplashVC"
        case .Menu:
            return "MenuTableViewController"
        case .SWReveal:
            return MOLHLanguage.isRTLLanguage() ? "SWRevealViewControllerRTL" : "SWRevealViewControllerLTR"
        case .Login:
            return "LoginNC"
        case .Language:
            return "LanguageVC"
        case .Country:
            return "CountryVC"
        case .TurnLocation:
            return "TurnOnLocationVC"
        case .ForgotPassword:
            return "ForgotPasswordVC"
        case .CheckPhone:
            return "CheckPhoneVC"
        case .Code:
            return "CodeVC"
        case .Signup:
            return "SignupVC"
        case .NewPassword:
            return "NewPasswordVC"
        case .Home:
            return "HomeVC"
        case .Pages:
            return "PageViewController"
        case .Discovers:
            return "DiscoversVC"
        }
    }
    
}
