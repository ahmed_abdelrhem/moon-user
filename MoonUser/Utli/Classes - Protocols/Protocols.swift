//
//  Protocols.swift
//  MoonUser
//
//  Created by Grand iMac on 1/22/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation
import SwiftMessages

// Presenter protocal
protocol LoaderDelegate: class {
    func showProgress()
    func hideProgress()
    func onSuccess(_ msg: String)
    func warringMSG(_ msg: String)
    func onFailure(_ msg: String)
    func onEmpty(_ img: UIImage,_ msg: String)
}


extension LoaderDelegate {
    func warringMSG(_ msg: String) {}
    func onEmpty(_ img: UIImage,_ msg: String) {}
}


//Service Location Protocol
protocol LocationDelegate: class {
    func RetriveLocation(lat: Double, lng: Double, add: String)
    func dismiss()
}

extension LocationDelegate {
    func dismiss() {}
}




