//
//  AttachmentHandler.swift
//  Tabadul
//
//  Created by Islamic on 3/5/19.
//  Copyright © 2019 2grand. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices
import AVFoundation
import Photos
import BSImagePicker
import Photos

var MAX = 6

class AttachmentHandler: NSObject{
    
    static let shared = AttachmentHandler()
    fileprivate var currentVC: UIViewController?
    
    //MARK: - Internal Properties
    var singleImagePickedBlock: ((UIImage,NSData) -> Void)?
    var imagesPickedBlock: (([UIImage]) -> Void)?
    var videoPickedBlock: ((NSURL,NSData, String) -> Void)?
    
    
    enum AttachmentType: String{
        case camera, video, photoLibrary, Gallary
    }
    
    
    //MARK: - Constants
    struct Constants {
        static let actionFileTypeHeading = ADD_FILE
        static let actionFileTypeDescription = CHOOSE_TYPE
        static let camera = CAMERA
        static let gallary = GALLARY
        static let phoneLibrary = LIBRARY
        static let video = VIDEO
        
        static let alertForCameraAccessMessage = CAMERA_ACCESS_MSG
        static let alertForPhotoLibraryMessage = PHOTOS_ACCESS_MSG
        static let alertForVideoLibraryMessage = VIDEO_ACCESS_MSG
        
        static let settingsBtnTitle = SEETING
        static let cancelBtnTitle = CANCEL
    }
    
    //MARK: - showAttachmentActionSheet
    // This function is used to show the attachment sheet for image, video, photo and file.
    func showAttachmentActionSheet(_ vc: UIViewController,_ sender: AnyObject) {
        
//        MAX = max
        
        currentVC = vc
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.popoverPresentationController?.sourceView = sender as? UIView
        actionSheet.popoverPresentationController?.sourceRect = sender.bounds
        
        actionSheet.addAction(UIAlertAction(title: Constants.phoneLibrary, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .photoLibrary, vc: self.currentVC!)
        }))
        
        actionSheet.addAction(UIAlertAction(title: Constants.video, style: .default, handler: { (action) -> Void in
        self.authorisationStatus(attachmentTypeEnum: .video, vc: self.currentVC!)
        }))
        
        actionSheet.addAction(UIAlertAction(title: Constants.cancelBtnTitle, style: .cancel, handler: nil))
        vc.present(actionSheet, animated: true, completion: nil)
    }
    
    func showPostAttachmentActionSheet(_ vc: UIViewController,_ sender: AnyObject) {
                
        currentVC = vc
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.popoverPresentationController?.sourceView = sender as? UIView
        actionSheet.popoverPresentationController?.sourceRect = sender.bounds
        
        actionSheet.addAction(UIAlertAction(title: Constants.camera, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .camera, vc: self.currentVC!)
        }))
        
        actionSheet.addAction(UIAlertAction(title: Constants.gallary, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .Gallary, vc: self.currentVC!)
        }))
        
        actionSheet.addAction(UIAlertAction(title: Constants.video, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .video, vc: self.currentVC!)
        }))
        
        actionSheet.addAction(UIAlertAction(title: Constants.cancelBtnTitle, style: .cancel, handler: nil))
        vc.present(actionSheet, animated: true, completion: nil)
    }
    
    func showAttachmentImage(_ vc: UIViewController,_ sender: AnyObject ) {
        currentVC = vc
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.popoverPresentationController?.sourceView = sender as? UIView
        actionSheet.popoverPresentationController?.sourceRect = sender.bounds
        
        actionSheet.addAction(UIAlertAction(title: Constants.camera, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .camera, vc: self.currentVC!)
        }))
        
        actionSheet.addAction(UIAlertAction(title: Constants.gallary, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .Gallary, vc: self.currentVC!)
        }))
        
        actionSheet.addAction(UIAlertAction(title: Constants.cancelBtnTitle, style: .cancel, handler: nil))
        
        vc.present(actionSheet, animated: true, completion: nil)
    }
    
    
    //MARK: - Authorisation Status
    // This is used to check the authorisation status whether user gives access to import the image, photo library, video.
    // if the user gives access, then we can import the data safely
    // if not show them alert to access from settings.
    func authorisationStatus(attachmentTypeEnum: AttachmentType, vc: UIViewController){
        currentVC = vc
        
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            if attachmentTypeEnum == AttachmentType.camera {
                openCamera()
            }
            if attachmentTypeEnum == AttachmentType.Gallary {
                photoLibrary()
            }
            
            if attachmentTypeEnum == AttachmentType.photoLibrary {
                imagesLibrary()
            }
            if attachmentTypeEnum == AttachmentType.video {
                videoLibrary()
            }
        case .denied:
            print("permission denied")
            self.addAlertForSettings(attachmentTypeEnum)
        case .notDetermined:
            print("Permission Not Determined")
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == PHAuthorizationStatus.authorized{
                    // photo library access given
                    print("access given")
                    if attachmentTypeEnum == AttachmentType.photoLibrary{
                        self.imagesLibrary()
                    }
                    if attachmentTypeEnum == AttachmentType.video{
                        self.videoLibrary()
                    }
                }else{
                    print("restriced manually")
                    self.addAlertForSettings(attachmentTypeEnum)
                }
            })
        case .restricted:
            print("permission restricted")
            self.addAlertForSettings(attachmentTypeEnum)
        default:
            break
        }
    }
    
    //MARK: - PHOTO PICKER
    //This function is used to open camera from the iphone and take a photo
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .camera
            myPickerController.mediaTypes = [kUTTypeImage as String]
            myPickerController.cameraCaptureMode = .photo
            
            currentVC?.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    
    //MARK: - PHOTO PICKER
    func photoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .photoLibrary
            currentVC?.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    
    //MARK:- Multi Images Picker
    func imagesLibrary(){
        var SelectedAssets = [PHAsset]()
        
        // create an instance
        let vc = BSImagePickerViewController()
        vc.maxNumberOfSelections = MAX
        //display picture gallery
        currentVC?.bs_presentImagePickerController(vc, animated: true, select: { (asset: PHAsset) -> Void in
            
        }, deselect: { (asset: PHAsset) -> Void in
            // User deselected an assets.
        }, cancel: { (assets: [PHAsset]) -> Void in
            // User cancelled. And this where the assets currently selected.
        }, finish: { (assets: [PHAsset]) -> Void in
            // User finished with these assets
            for i in 0..<assets.count {
                SelectedAssets.append(assets[i])
            }
            self.convertAssetToImages(SelectedAssets)
        }, completion: nil)
        
    }
    
    func convertAssetToImages(_ SelectedAssets: [PHAsset]) -> Void {
        var images = [UIImage]()
        if SelectedAssets.count != 0 {
            for i in 0..<SelectedAssets.count{
                let manager = PHImageManager.default()
                let option = PHImageRequestOptions()
                var thumbnail = UIImage()
                option.isSynchronous = true
                
                manager.requestImage(for: SelectedAssets[i], targetSize: CGSize(width: 200, height: 200), contentMode: .aspectFill, options: option, resultHandler: {(result, info)->Void in
                    thumbnail = result!
                })
                let data = thumbnail.jpegData(compressionQuality: 0.7)
                let newImage = UIImage(data: data!)
                images.append(newImage! as UIImage)
            }
            self.imagesPickedBlock?(images)
        }
    }
    
    
    //MARK: - VIDEO PICKER
    func videoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .photoLibrary
            myPickerController.mediaTypes = [kUTTypeMovie as String, kUTTypeVideo as String]
            currentVC?.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    //MARK: - SETTINGS ALERT
    func addAlertForSettings(_ attachmentTypeEnum: AttachmentType){
        var alertTitle: String = ""
        
        if attachmentTypeEnum == AttachmentType.camera{
            alertTitle = Constants.alertForCameraAccessMessage
        }
        
        if attachmentTypeEnum == AttachmentType.Gallary{
            alertTitle = Constants.alertForPhotoLibraryMessage
        }
        
        if attachmentTypeEnum == AttachmentType.photoLibrary{
            alertTitle = Constants.alertForPhotoLibraryMessage
        }
        if attachmentTypeEnum == AttachmentType.video{
            alertTitle = Constants.alertForVideoLibraryMessage
        }
        
        let cameraUnavailableAlertController = UIAlertController (title: alertTitle , message: nil, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: Constants.settingsBtnTitle, style: .destructive) { (_) -> Void in
            let settingsUrl = NSURL(string:UIApplication.openSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            }
        }
        let cancelAction = UIAlertAction(title: Constants.cancelBtnTitle, style: .default, handler: nil)
        cameraUnavailableAlertController .addAction(cancelAction)
        cameraUnavailableAlertController .addAction(settingsAction)
        currentVC?.present(cameraUnavailableAlertController , animated: true, completion: nil)
    }
    
}


//MARK: - IMAGE PICKER DELEGATE
// This is responsible for image picker interface to access image, video and then responsibel for canceling the picker
extension AttachmentHandler: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        currentVC?.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            DispatchQueue.main.async {
                print("correct img")
                if picker.sourceType == UIImagePickerController.SourceType.camera {
                    print("camera")
                    let imgData = image.jpegData(compressionQuality: 0.25)! as NSData
                    if info[UIImagePickerController.InfoKey.imageURL] != nil {
                        self.singleImagePickedBlock?(image, imgData)
                        
                    }else{
                        
                        self.singleImagePickedBlock?(image, imgData)
                    }
                    
                } else {
                    print("photo lib")
                    
                    print("imageurl == > \(String(describing: info[UIImagePickerController.InfoKey.imageURL]))")
                    let data = NSData(contentsOf: info[UIImagePickerController.InfoKey.imageURL] as! URL)!
                    self.singleImagePickedBlock?(image, data)
                    
                }
            }
            currentVC?.dismiss(animated: true, completion: nil)
        } else{
            print("Something went wrong in  image")
        }
        
        if let videoUrl = info[UIImagePickerController.InfoKey.mediaURL] as? NSURL{
            DispatchQueue.main.async {
                print("videourl: ", videoUrl)
                //trying compression of video
                let data = NSData(contentsOf: videoUrl as URL)!
                print("File size before compression: \(Double(data.length / 1048576)) mb")
                if picker.sourceType == UIImagePickerController.SourceType.camera {
                    self.compressWithSessionStatusFunc(videoUrl, status: "live")
                } else {
                    self.compressWithSessionStatusFunc(videoUrl, status: "uploaded")
                }
            }
            currentVC?.dismiss(animated: true, completion: nil)
        } else{
            print("Something went wrong in  video")
        }
        
    }
    
    //MARK: Video Compressing technique
    fileprivate func compressWithSessionStatusFunc(_ videoUrl: NSURL, status: String) {
        let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".MOV")
        compressVideo(inputURL: videoUrl as URL, outputURL: compressedURL) { (exportSession) in
            guard let session = exportSession else {
                return
            }
            
            switch session.status {
            case .unknown:
                break
            case .waiting:
                break
            case .exporting:
                break
            case .completed:
                guard let compressedData = NSData(contentsOf: compressedURL) else {
                    return
                }
                print("File size after compression: \(Double(compressedData.length / 1048576)) mb")
                
                DispatchQueue.main.async {
                    self.videoPickedBlock?(compressedURL as NSURL, compressedData, status)
                }
                
            case .failed:
                break
            case .cancelled:
                break
            default:
                break
            }
        }
    }
    
    // Now compression is happening with medium quality, we can change when ever it is needed
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPreset1280x720) else {
            handler(nil)
            
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mov
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
    
    
}
