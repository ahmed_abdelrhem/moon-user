//
//  TagsCell.swift
//  MoonUser
//
//  Created by Grand iMac on 1/29/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class TagsCell: UITableViewCell, TagsCellView {

    @IBOutlet weak fileprivate var tagLbl: UILabel!
    @IBOutlet weak fileprivate var selectedImg: UIImageView!

    var emptyBox = UIImage(named: "empty-box")
    var checkBox = UIImage(named: "check-box")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            selectedImg.image = checkBox
        } else {
            selectedImg.image = emptyBox
        }
    }
    
    func configure(_ tag: String?) {
        tagLbl.text = tag
    }


}
