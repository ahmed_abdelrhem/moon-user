//
//  InstituationCell.swift
//  MoonUser
//
//  Created by Grand iMac on 2/17/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import SDWebImage


class InstituationCell: UITableViewCell, InstituationCellView {
    
    @IBOutlet weak fileprivate var instituationNameLbl: UILabel!
    @IBOutlet weak fileprivate var addressLbl: UILabel!
    @IBOutlet weak fileprivate var instituationImg: UIImageView!
    @IBOutlet weak fileprivate var rateView: CosmosView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        instituationImg.setAlpha(0.5)
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func configure(image: String?, name: String?, address: String?, rate: String?) {
        instituationImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        let imgURL = URL(string: image ?? "")
        instituationImg.sd_setImage(with: imgURL, placeholderImage: .placeHolder_image)
        rateView.rating = Double(rate ?? "0")!
        instituationNameLbl.text = name
        addressLbl.text = address
        
        if address == "" {
            addressLbl.isHidden = true
        }
    }
    

    
}
