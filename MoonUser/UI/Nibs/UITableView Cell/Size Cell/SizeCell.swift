//
//  SizeCell.swift
//  MoonUser
//
//  Created by Grand iMac on 2/5/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class SizeCell: UITableViewCell, SizeCellView {
    

     @IBOutlet weak fileprivate var selectedImg: UIImageView!
     @IBOutlet weak fileprivate var SizeNameLbl: UILabel!
     @IBOutlet weak fileprivate var SizePriceLbl: UILabel!
     
     
     override func awakeFromNib() {
         super.awakeFromNib()
         selectionStyle = .none
     }
     
     var radioEmpty = UIImage(named: "radio-empty")
     var radioFull = UIImage(named: "radio-full")


     override func setSelected(_ selected: Bool, animated: Bool) {
         super.setSelected(selected, animated: animated)
         if selected {
             selectedImg.image = radioFull
            selectedImg.tintColor = .mainColor
             SizeNameLbl.textColor = .mainColor
             SizePriceLbl.textColor = .mainColor
         } else {
             selectedImg.image = radioEmpty
             selectedImg.tintColor = .black
             SizeNameLbl.textColor = .black
             SizePriceLbl.textColor = .black
         }
     }
    
    
    func cellConfigure(name: String?, price: String?) {
        SizeNameLbl.text = name
        SizePriceLbl.text = "\(price ?? "0") \(DEF.country.currency)"
    }

    
}
