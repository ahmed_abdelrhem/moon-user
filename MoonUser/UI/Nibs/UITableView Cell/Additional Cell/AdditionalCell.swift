//
//  AdditionalCell.swift
//  MoonUser
//
//  Created by Grand iMac on 2/5/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class AdditionalCell: UITableViewCell, AdditionalCellView {

    @IBOutlet weak fileprivate var selectedImg: UIImageView!
    @IBOutlet weak fileprivate var AdditionalNameLbl: UILabel!
    @IBOutlet weak fileprivate var AdditionalPriceLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    var emptyBox = UIImage(named: "empty-box")
    var checkBox = UIImage(named: "check-box2")

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            selectedImg.image = checkBox
            selectedImg.tintColor = .mainColor
            AdditionalNameLbl.textColor = .mainColor
            AdditionalPriceLbl.textColor = .mainColor
        } else {
            selectedImg.image = emptyBox
            selectedImg.tintColor = .black
            AdditionalNameLbl.textColor = .black
            AdditionalPriceLbl.textColor = .black
        }
    }
    
    func cellConfigure(name: String?, price: String?) {
        AdditionalNameLbl.text = name
        AdditionalPriceLbl.text = "\(price ?? "0") \(DEF.country.currency)"
    }


    
}
