//
//  ReciverChatCell.swift
//  MoonUser
//
//  Created by Grand iMac on 3/9/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import SDWebImage

class ReciverChatCell: UITableViewCell {

    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var textView: UIView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var messagImg: UIImageView!
    @IBOutlet weak var dateLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        messagImg.fullScreenWhenTapped()
    }
    
    
    func configure(userImg: String?, name: String?, text: String?, image: String?, date: String?) {
        let imgURL = URL(string: userImg ?? "")
        let messageURL = URL(string: image ?? "")
        self.userImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.userImg.sd_setImage(with: imgURL, placeholderImage: .avatar_image)
        self.messagImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.messagImg.sd_setImage(with: messageURL, placeholderImage: .placeHolder_image)
        nameLbl.text = name
        messageLbl.text = text
        dateLbl.text = date
    }
    
    func setIsText() {
        textView.isHidden = false
        messagImg.isHidden = true
    }
    
    func setIsImage() {
        textView.isHidden = true
        messagImg.isHidden = false
    }

    
}
