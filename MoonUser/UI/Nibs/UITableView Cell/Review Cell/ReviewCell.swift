//
//  ReviewCell.swift
//  MoonUser
//
//  Created by Grand iMac on 2/3/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class ReviewCell: UITableViewCell, ReviewCellView {
    
    @IBOutlet weak fileprivate var nameLbl: UILabel!
    @IBOutlet weak fileprivate var dateLbl: UILabel!
    @IBOutlet weak fileprivate var commentLbl: UILabel!
    @IBOutlet weak fileprivate var rateView: CosmosView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    func configure(name: String?, date: String?, comment: String?, rate: String?) {
        nameLbl.text = name
        dateLbl.text = date
        commentLbl.text = comment
        rateView.rating = Double(rate ?? "0")!
    }
    

    
}
