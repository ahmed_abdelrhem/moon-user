//
//  OrderCell.swift
//  MoonUser
//
//  Created by Grand iMac on 2/13/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class OrderCell: UITableViewCell, OrderCellView {

    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var orderIDLbl: UILabel!
    @IBOutlet weak var orderStatusLbl: UILabel!
    @IBOutlet weak var deletaBtn: UIButton!

    
    var deleteBtnAction: (() -> Void)? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    func configure(date: String?, id: String?, status: String?) {
        dateLbl.text = date
        orderIDLbl.text = id
        orderStatusLbl.text = status
    }
    
    func hideDeleteOrderBtn() {
        deletaBtn.isHidden = true
    }

    @IBAction func deleteBtnClicked(sender: UIButton) {
        if let deleteAction = self.deleteBtnAction {
            deleteAction()
        }
    }

 
    
}
