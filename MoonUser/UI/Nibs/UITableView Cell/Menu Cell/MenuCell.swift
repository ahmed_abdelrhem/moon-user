//
//  MenuCell.swift
//  Cham
//
//  Created by iMac on 5/12/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import SDWebImage

 protocol MenuCellView {
    func setName(name: String)
    func setPhoto(with icon: UIImage)
    func setPhoto(with Url: String)
}

class MenuCell: UITableViewCell, MenuCellView {
   
    
    @IBOutlet weak fileprivate var menuIcon: UIImageView!
    @IBOutlet weak fileprivate var menuTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        accessoryType = selected ? .checkmark : .none
    }
   
    func setName(name: String) {
        menuTitle.text = name
    }
       
    func setPhoto(with icon: UIImage) {
        menuIcon.image = icon
    }
    
    func setPhoto(with Url: String) {
        let logo_url = URL(string: Url)
        menuIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
        menuIcon.sd_setImage(with: logo_url, placeholderImage: .placeHolder_image)
        
        if Url == "" {
            menuIcon.isHidden = true
        }
    }
       
    
}
