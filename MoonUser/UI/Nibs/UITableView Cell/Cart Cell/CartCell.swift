//
//  CartCell.swift
//  MoonUser
//
//  Created by Grand iMac on 2/10/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit


class CartCell: UITableViewCell, CartCellView {
    

    @IBOutlet weak fileprivate var nameLbl: UILabel!
    @IBOutlet weak fileprivate var priceLbl: UILabel!
    @IBOutlet weak fileprivate var descritionLbl: UILabel!
    @IBOutlet weak fileprivate var sizeLbl: UILabel!
    @IBOutlet weak fileprivate var colorView: UIView!
    @IBOutlet weak fileprivate var extraLbl: UILabel!
    @IBOutlet weak fileprivate var quantity2Lbl: UILabel!
    @IBOutlet weak fileprivate var quantityLbl: UILabel!
    @IBOutlet weak fileprivate var additionsPriceLbl: UILabel!
    @IBOutlet weak fileprivate var specialRequestLbl: UILabel!
    @IBOutlet weak fileprivate var sizeStack: UIStackView!
    @IBOutlet weak fileprivate var colorStack: UIStackView!
    @IBOutlet weak fileprivate var extraStack: UIStackView!
    @IBOutlet weak fileprivate var specialStack: UIStackView!
    @IBOutlet weak var actionsHeight: NSLayoutConstraint!
    
    
    var deleteBtn: (() -> Void)? = nil
    var increaseBtn: (() -> Void)? = nil
    var decreaseBtn: (() -> Void)? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    func configure(name: String, price: String, desc: String, size: String, color: String, extra: String, qty: String, additionsPrice: String, specialRequest: String) {
        nameLbl.text = name
        priceLbl.text = "\(price) \(DEF.country.currency)"
        descritionLbl.text = desc
        sizeLbl.text = size
        extraLbl.text = extra
        quantityLbl.text = qty
        quantity2Lbl.text = qty
        additionsPriceLbl.text = "\(additionsPrice) \(DEF.country.currency)"
        specialRequestLbl.text = specialRequest

        if color == "" {
            colorStack.isHidden = true
        } else {
            colorView.backgroundColor = HexStringToUIColor.hexStringToUIColor(hex: color)
            extraStack.isHidden = true
        }
        if extra == "" {
            extraStack.isHidden = true
        }
        if specialRequest == "" {
            specialStack.isHidden = true
        }
    }

    func hideActions() {
        actionsHeight.constant = 0
    }
    
    
    @IBAction func deleteAction(sender: UIButton) {
        if let deleteAction = self.deleteBtn {
            deleteAction()
        }
    }
    
    @IBAction func increaseAction(sender: UIButton) {
        if let increaseAction = self.increaseBtn {
            increaseAction()
        }
    }
    
    @IBAction func decreaseAction(sender: UIButton) {
        if let decreaseAction = self.decreaseBtn {
            decreaseAction()
        }
    }

    
}
