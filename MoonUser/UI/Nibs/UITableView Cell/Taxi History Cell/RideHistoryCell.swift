//
//  TaxiHistoryCell.swift
//  MoonUser
//
//  Created by Grand iMac on 3/8/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

protocol RideHistoryCellView {
    func setRideHistory(_ ride: SingleTrip?)
}


class RideHistoryCell: UITableViewCell, RideHistoryCellView {

    @IBOutlet weak fileprivate var pickupLbl: UILabel!
    @IBOutlet weak fileprivate var destinationLbl: UILabel!
    @IBOutlet weak fileprivate var dateLbl: UILabel!
    @IBOutlet weak fileprivate var priceLbl: UILabel!
    @IBOutlet weak fileprivate var rateView: CosmosView!

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    func setRideHistory(_ ride: SingleTrip?) {
        let total = ride?.price ?? 0
        pickupLbl.text = ride?.start_address ?? "Unknown"
        destinationLbl.text = ride?.end_address ?? "Unknown"
        dateLbl.text = ride?.trip_date ?? " "
        priceLbl.text = "\(total) \(DEF.country.currency)"
        rateView.rating = Double(ride?.rate ?? "0")!
    }

    
    
}
