//
//  SenderAudioCell.swift
//  Moon Shop
//
//  Created by a7med on 3/30/20.
//  Copyright © 2020 2grand. All rights reserved.
//

import AVKit
import SDWebImage


class SenderAudioCell: UITableViewCell{

    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var totalTimeLbl: UILabel!
    @IBOutlet weak var currentTimeLbl: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var dateLbl: UILabel!

    
    private var player : AVPlayer?
    private var audio_url: URL?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    
    @objc func playerDidFinishPlaying(note: NSNotification) {
        player?.seek(to: .zero)
        slider.value = 0
        playBtn.isSelected = false
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        totalTimeLbl.text = "--:--"
        currentTimeLbl.text = "00:00"
        slider.value = 0
        playBtn.isSelected = false
        audio_url = nil
        player = nil
    }
    
    func setAudioPlayer(_ url:String, userImg: String?, date: String?) {
        DispatchQueue.main.async {
            let imgURL = URL(string: userImg ?? "")
            self.userImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.userImg.sd_setImage(with: imgURL, placeholderImage: .avatar_image)
            self.dateLbl.text = date
        
            self.playBtn.isSelected = false
            guard  let audioUrl =  URL(string: url) else {
                return
            }
            let avPlayer = AVPlayer()
            avPlayer.automaticallyWaitsToMinimizeStalling = false
            self.player = avPlayer
            self.audio_url = audioUrl
            let playerItem = AVPlayerItem(url: audioUrl)
            self.player?.replaceCurrentItem(with: playerItem)
            let interval = CMTimeMake(value: 1,timescale: 2)
            self.player?.addPeriodicTimeObserver(forInterval: interval, queue: .main) { [weak self] (time) in
                if  self?.player?.currentItem?.status ==  AVPlayerItem.Status.readyToPlay{
                    let duration = self?.player?.currentItem?.duration
                    self?.totalTimeLbl.text = duration?.toDisplayString()
                }
                self?.currentTimeLbl.text = time.toDisplayString()
                self?.updateSlider()
            }
        }
    }
    
    @IBAction func sliderChanged(_ sender: UISlider) {
        let percentage = sender.value
        guard let duration = player?.currentItem?.duration else {
            return
        }
        let durationInSeconds = CMTimeGetSeconds(duration)
        let seekTimeInSeconds = Float64(percentage) * durationInSeconds
        let seekTime = CMTimeMakeWithSeconds(seekTimeInSeconds, preferredTimescale: 1)
        player?.seek(to: seekTime)
    }
    
    @IBAction func palyBtnPressed(_ sender: UIButton) {
        if player?.timeControlStatus == .paused {
            player?.play()
        }else{
            player?.pause()
        }
        sender.isSelected  = !sender.isSelected
    }
    
}


extension SenderAudioCell{
    fileprivate func updateSlider(){
        let currentTimeSeconds = CMTimeGetSeconds((player?.currentTime())!)
        let durationTimeSeconds = CMTimeGetSeconds(player?.currentItem?.duration ?? CMTimeMake(value: 1, timescale: 1))
        let percentage = currentTimeSeconds / durationTimeSeconds
        slider.value = Float(percentage)
    }
}
