//
//  ReservationCell.swift
//  MoonUser
//
//  Created by Grand iMac on 2/27/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import SDWebImage

protocol ReservationCellView {
    func configure(image: String?, name: String?, date: String?, day: String?, time: String?, actiontitle: String?)
    func preventAction()
}

class ReservationCell: UITableViewCell, ReservationCellView {

    @IBOutlet weak fileprivate var clinicImg: UIImageView!
    @IBOutlet weak fileprivate var nameLbl: UILabel!
    @IBOutlet weak fileprivate var dateLbl: UILabel!
    @IBOutlet weak fileprivate var dayLbl: UILabel!
    @IBOutlet weak fileprivate var timeLbl: UILabel!
    @IBOutlet weak fileprivate var actionBtn: UIButton!
    
    var cancelBtnAction: (() -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    func configure(image: String?, name: String?, date: String?, day: String?, time: String?, actiontitle: String?) {
        clinicImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        let imgURL = URL(string: image ?? "")
        clinicImg.sd_setImage(with: imgURL, placeholderImage: .placeHolder_image)
        nameLbl.text = name
        dateLbl.text = date
        dayLbl.text = day
        timeLbl.text = time
        actionBtn.setTitle(actiontitle, for: .normal)
    }
    
    func preventAction() {
        actionBtn.isUserInteractionEnabled = false
    }
    
    @IBAction func cancelAction(sender: UIButton) {
        if let cancelAction = self.cancelBtnAction {
            cancelAction()
        }
    }


}
