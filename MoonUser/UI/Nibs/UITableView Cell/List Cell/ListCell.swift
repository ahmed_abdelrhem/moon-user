//
//  ListCell.swift
//  MoonUser
//
//  Created by apple on 11/15/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

protocol ListCellView {
    func configure(title: String?, message: String?)
}

class ListCell: UITableViewCell, ListCellView {

    @IBOutlet weak fileprivate var titleLbl: UILabel!
    @IBOutlet weak fileprivate var messageLbl: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    
    func configure(title: String?, message: String?) {
        titleLbl.text = title
        messageLbl.text = message
    }
   
    
}
