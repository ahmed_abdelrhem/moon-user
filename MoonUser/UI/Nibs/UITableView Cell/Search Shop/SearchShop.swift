//
//  SearchShop.swift
//  MoonUser
//
//  Created by Grand iMac on 1/30/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import SDWebImage

class SearchShop: UITableViewCell, ShopCellView {
    
    @IBOutlet weak fileprivate var shopImg: UIImageView!
    @IBOutlet weak fileprivate var shopNameLbl: UILabel!
    @IBOutlet weak fileprivate var tagsLbl: UILabel!
    @IBOutlet weak fileprivate var rateView: CosmosView!
    @IBOutlet weak fileprivate var avgLbl: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    func configure(image: String?, name: String?, tags: String?, rate: String?, avg: String?) {
        shopImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        let imgURL = URL(string: image ?? "")
        shopImg.sd_setImage(with: imgURL, placeholderImage: .placeHolder_image)
        shopNameLbl.text = name ?? ""
        tagsLbl.text = tags ?? ""
        rateView.rating = Double(rate ?? "0")!
        avgLbl.text = "\(AVG) \(avg ?? "0") \(MIN)"
    }
    
    func setIsInstitution(address: String?) {
        tagsLbl.text = "\(LOCATION) (\(address ?? ""))"
        rateView.isHidden = true
        avgLbl.isHidden = true
    }
    
    func setIsCommercial(seller: String?, price: String?) {
        tagsLbl.text = "\(SELLER) (\(seller ?? ""))"
        avgLbl.text = "\(PRICE) (\(price ?? "0") \(DEF.country.currency))"
        rateView.isHidden = true
    }
    
    func setIsFamous() {
        avgLbl.isHidden = true
        tagsLbl.isHidden = true
        rateView.isHidden = true
    }

    func setIsClinic(address: String?) {
        tagsLbl.text = address
        avgLbl.isHidden = true
    }

    
    func setIsOffer() { }
    
    func setIsVoucher() { }
    
    func setIsClosed() { }

}
