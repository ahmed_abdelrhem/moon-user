//
//  MyAdsCell.swift
//  MoonUser
//
//  Created by Grand iMac on 2/23/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import SDWebImage

class MyAdsCell: UITableViewCell, MyAdsCellView {

    @IBOutlet weak fileprivate var advertisingImg: UIImageView!
    @IBOutlet weak fileprivate var nameLbl: UILabel!
    @IBOutlet weak fileprivate var priceLbl: UILabel!
    
    
    var deleteBtn: (() -> Void)? = nil
    var editBtn: (() -> Void)? = nil

    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        advertisingImg.layer.cornerRadius = 10
        advertisingImg.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]

    }
    
    func configure(image: String?, name: String?, price: String?) {
        advertisingImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        let imgURL = URL(string: image ?? "")
        advertisingImg.sd_setImage(with: imgURL, placeholderImage: .placeHolder_image)
        nameLbl.text = name
        priceLbl.text = "\(price ?? "0") \(DEF.country.currency)"
    }

    
    
    @IBAction func deleteAction(sender: UIButton) {
        if let deleteAction = self.deleteBtn {
            deleteAction()
        }
    }
    
    @IBAction func editBtn(sender: UIButton) {
        if let editAction = self.editBtn {
            editAction()
        }
    }
    
    
}
