//
//  HeaderCell.swift
//  ByDeals
//
//  Created by Islamic on 6/19/19.
//  Copyright © 2019 Organization. All rights reserved.
//

import UIKit
import Foundation

class HeaderCell: UITableViewCell, headerCellView {

    @IBOutlet weak fileprivate var userImg: UIImageView!
    @IBOutlet weak fileprivate var userNameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    func setUserImage() {
        let imgURL = DEF.userData.image
        userImg.sd_setImage(with: URL(string: imgURL), placeholderImage: .avatar_image)
    }
    
    func setUserName() {
        let name = DEF.userData.name
        userNameLbl.text = name
    }

}
