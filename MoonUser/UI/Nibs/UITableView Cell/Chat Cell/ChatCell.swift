//
//  ChatCell.swift
//  MoonUser
//
//  Created by Grand iMac on 3/9/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import SDWebImage

protocol ChatCellView {
    func configure(image: String?, name: String?, unReadMessages: Int?)
}

class ChatCell: UITableViewCell, ChatCellView {

    @IBOutlet weak fileprivate var userImg: UIImageView!
    @IBOutlet weak fileprivate var nameLbl: UILabel!
    @IBOutlet weak fileprivate var unReadMessagesCountLbl: UILabel!
    @IBOutlet weak fileprivate var unReadMessageView: UIView!

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(image: String?, name: String?, unReadMessages: Int?) {
        let imgURL = URL(string: image ?? "")
        userImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        userImg.sd_setImage(with: imgURL, placeholderImage: .placeHolder_image)
        nameLbl.text = name
        unReadMessagesCountLbl.text = "\(unReadMessages ?? 0)"
        if unReadMessages == 0 {
            unReadMessageView.isHidden = true
        }
    }

    
}
