//
//  ClinicCell.swift
//  MoonUser
//
//  Created by Grand iMac on 2/26/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import SDWebImage

protocol ClinicCellView {
    func configure(image: String?, name: String?, address: String?, desc: String?, rate: String?, isFollow: Bool?)
}

class ClinicCell: UITableViewCell, ClinicCellView {

    @IBOutlet weak fileprivate var clinicImg: UIImageView!
    @IBOutlet weak fileprivate var nameLbl: UILabel!
    @IBOutlet weak fileprivate var addressLbl: UILabel!
    @IBOutlet weak fileprivate var descLbl: UILabel!
    @IBOutlet weak fileprivate var rateView: CosmosView!
    @IBOutlet weak fileprivate var followBtn: UIButton!
    
    var detailsBtnActions: (() -> Void)? = nil
    var followBtnActions: (() -> Void)? = nil

    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        clinicImg.layer.cornerRadius = 12
        clinicImg.layer.maskedCorners = [.layerMinXMinYCorner]
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(image: String?, name: String?, address: String?, desc: String?, rate: String?, isFollow: Bool?) {
        clinicImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        let imgURL = URL(string: image ?? "")
        clinicImg.sd_setImage(with: imgURL, placeholderImage: .placeHolder_image)
        nameLbl.text = name
        addressLbl.text = address
        descLbl.text = desc
        rateView.rating = Double(rate ?? "0")!
        followBtn.isSelected = isFollow ?? false
    }
    
    
    @IBAction func detailsAction(sender: UIButton) {
        if let detailsAction = self.detailsBtnActions {
            detailsAction()
        }
    }
    
    @IBAction func followAction(sender: UIButton) {
        followBtn.isSelected = !followBtn.isSelected
        if let followAction = self.followBtnActions {
            followAction()
        }
    }

}


