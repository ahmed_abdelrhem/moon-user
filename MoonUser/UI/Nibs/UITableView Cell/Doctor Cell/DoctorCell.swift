//
//  DoctorCell.swift
//  MoonUser
//
//  Created by Grand iMac on 2/27/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import SDWebImage

class DoctorCell: UITableViewCell, ClinicCellView {

    @IBOutlet weak fileprivate var doctorImg: UIImageView!
    @IBOutlet weak fileprivate var nameLbl: UILabel!
    @IBOutlet weak fileprivate var addressLbl: UILabel!
    @IBOutlet weak fileprivate var descLbl: UILabel!
    @IBOutlet weak fileprivate var bookBtn: UIButton!
    @IBOutlet weak fileprivate var rateView: CosmosView!

    var bookBtnActions: (() -> Void)? = nil

    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        bookBtn.layer.cornerRadius = 12
        bookBtn.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }

    func configure(image: String?, name: String?, address: String?, desc: String?, rate: String?, isFollow: Bool?) {
        doctorImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        let imgURL = URL(string: image ?? "")
        doctorImg.sd_setImage(with: imgURL, placeholderImage: .avatar_image)
        nameLbl.text = name
        addressLbl.text = address
        descLbl.text = desc
        rateView.rating = Double(rate ?? "0")!
    }
    
    
    @IBAction func bookAction(sender: UIButton) {
        if let detailsAction = self.bookBtnActions {
            detailsAction()
        }
    }

}
