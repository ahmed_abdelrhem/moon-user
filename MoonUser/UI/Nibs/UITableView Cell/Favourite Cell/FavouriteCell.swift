//
//  FavouriteCell.swift
//  MoonUser
//
//  Created by Islam 3bRahiem on 5/2/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import SDWebImage

enum FavouriteType {
    case asAd
    case asProduct
    
    var id: Int {
        switch self {
        case .asAd:
            return 1
        case .asProduct:
            return 2
        }
    }
}

protocol FavouriteCellView {
    func configure(type: FavouriteType, img: String?, name: String?, seller: String?, price: String?, isFav: Bool)
}

class FavouriteCell: UITableViewCell, FavouriteCellView {

    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var sellerLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var favBtn: UIButton!
    
    var favBtnActions: (() -> Void)? = nil

    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    func configure(type: FavouriteType, img: String?, name: String?, seller: String?, price: String?, isFav: Bool) {
        productImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        let imgURL = URL(string: img ?? "")
        productImg.sd_setImage(with: imgURL, placeholderImage: .placeHolder_image)
        nameLbl.text = name
        sellerLbl.text = type == .asAd ? seller : "\(SELLER) (\(seller ?? ""))"
        priceLbl.text = price
        favBtn.isSelected = isFav
    }

    @IBAction func favAction(sender: UIButton) {
        favBtn.isSelected = !favBtn.isSelected
        if let favAction = self.favBtnActions {
            favAction()
        }
    }

    
}
