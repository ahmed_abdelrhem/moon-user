//
//  FollowingCell.swift
//  MoonUser
//
//  Created by Grand iMac on 3/4/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import SDWebImage


class FollowingCell: UITableViewCell, FollowingCellView {

    @IBOutlet weak fileprivate var followerImg: UIImageView!
    @IBOutlet weak fileprivate var nameLbl: UILabel!
    @IBOutlet weak fileprivate var rateView: CosmosView!
    @IBOutlet weak fileprivate var folowBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    var followBtnAction : (() -> Void)? = nil

    func configure(image: String?, name: String?, rate: String?, isFollow: Bool) {
        followerImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        let imgURL = URL(string: image ?? "")
        followerImg.sd_setImage(with: imgURL, placeholderImage: .placeHolder_image)
        nameLbl.text = name
        rateView.rating = Double(rate ?? "0")!
        folowBtn.isSelected = !isFollow
        folowBtn.backgroundColor = isFollow ? .mainColor : .white
    }
    
    
    @IBAction func followBtnClicked(sender: UIButton) {
        if let followAction = self.followBtnAction {
            followAction()
        }
        sender.isSelected = !sender.isSelected
        sender.backgroundColor = sender.isSelected ? .white : .mainColor
    }

    
}
