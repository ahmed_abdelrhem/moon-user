//
//  ShopCell.swift
//  MoonUser
//
//  Created by Grand iMac on 1/27/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import SDWebImage

class ShopCell: UITableViewCell, ShopCellView {
    

    @IBOutlet weak fileprivate var shopImg: UIImageView!
    @IBOutlet weak fileprivate var saleImg: UIImageView!
    @IBOutlet weak fileprivate var voucherImg: UIImageView!
    @IBOutlet weak fileprivate var closedImg: UIImageView!
    @IBOutlet weak fileprivate var shopNameLbl: UILabel!
    @IBOutlet weak fileprivate var tagsLbl: UILabel!
    @IBOutlet weak fileprivate var rateView: CosmosView!
    @IBOutlet weak fileprivate var avgLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        shopImg.setAlpha(0.5)
        saleImg.isHidden = true
        voucherImg.isHidden = true
        closedImg.isHidden = true
    }
    
    func configure(image: String?, name: String?, tags: String?, rate: String?, avg: String?) {
        shopImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        let imgURL = URL(string: image ?? "")
        shopImg.sd_setImage(with: imgURL, placeholderImage: .placeHolder_image)
        shopNameLbl.text = name ?? ""
        tagsLbl.text = tags ?? ""
        rateView.rating = Double(rate ?? "0")!
        avgLbl.text = "\(AVG) \(avg ?? "0") \(MIN)"
        
    }
    
    func setIsOffer() {
        saleImg.isHidden = false
//        voucherImg.isHidden = true
//        closedImg.isHidden = true
        avgLbl.isHidden = false
    }
    
    func setIsVoucher() {
//        saleImg.isHidden = true
        voucherImg.isHidden = false
//        closedImg.isHidden = true
        avgLbl.isHidden = false
    }
    
    func setIsClosed() {
//        saleImg.isHidden = true
//        voucherImg.isHidden = true
        closedImg.isHidden = false
        avgLbl.isHidden = true
        shopImg.setAlpha(0.7)
    }
    
    func setIsInstitution(address: String?) { }
    
    func setIsCommercial(seller: String?, price: String?) { }

    func setIsFamous() { }

    func setIsClinic(address: String?) { }



}
