//
//  DriverCell.swift
//  MoonUser
//
//  Created by Grand iMac on 3/9/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import SDWebImage

protocol DriverCellView {
    func configure(image: String?, name: String?, price: Int?)
}

class DriverCell: UITableViewCell, DriverCellView {

    @IBOutlet weak fileprivate var driverImg: UIImageView!
    @IBOutlet weak fileprivate var driverName: UILabel!
    @IBOutlet weak fileprivate var priceLbl: UILabel!
    
    var applyBtnAction : (() -> Void)? = nil

    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configure(image: String?, name: String?, price: Int?) {
        let imgURL = URL(string: image ?? "")
        driverImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        driverImg.sd_setImage(with: imgURL, placeholderImage: .avatar_image)
        driverName.text = name
        priceLbl.text = "\(price ?? 0) \(DEF.country.currency) \(PER_HOUR)"
    }
    
    
    @IBAction func applyBtnClicked(sender: UIButton) {
        if let applyAction = self.applyBtnAction {
            applyAction()
        }
    }

    
}
