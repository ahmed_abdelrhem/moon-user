//
//  NotificationCell.swift
//  MoonUser
//
//  Created by Grand iMac on 3/4/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

protocol NotificationCellView {
    func configure(title: String?, date: String?, notification: String?)
}

class NotificationCell: UITableViewCell, NotificationCellView {

    @IBOutlet weak fileprivate var titleLbl: UILabel!
    @IBOutlet weak fileprivate var dateLbl: UILabel!
    @IBOutlet weak fileprivate var notificationLbl: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    
    func configure(title: String?, date: String?, notification: String?) {
        titleLbl.text = title
        dateLbl.text = date
        notificationLbl.text = notification
    }
   
    
}
