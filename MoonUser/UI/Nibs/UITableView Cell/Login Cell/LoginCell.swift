//
//  loginCell.swift
//  ByDeals
//
//  Created by Grand iMac on 11/20/19.
//  Copyright © 2019 Organization. All rights reserved.
//

import UIKit

class LoginCell: UITableViewCell {

    @IBOutlet weak fileprivate var loginImg: UIImageView!
    @IBOutlet weak fileprivate var titleLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    func configure() {
        let imgLogin = UIImage(named: "login")
        let imglogout = UIImage(named: "logout1")
        titleLbl.text = DEF.isLogin ? LOGOUT : LOGIN
        loginImg.image = DEF.isLogin ? imglogout : imgLogin
    }

    
    
}
