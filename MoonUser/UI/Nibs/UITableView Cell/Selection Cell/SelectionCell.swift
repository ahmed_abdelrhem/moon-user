//
//  SelectionCell.swift
//  MoonUser
//
//  Created by Grand iMac on 2/11/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class SelectionCell: UITableViewCell, SelectionCellView {

    @IBOutlet weak fileprivate var itemImg: UIImageView!
    @IBOutlet weak fileprivate var selectionImg: UIImageView!
    @IBOutlet weak fileprivate var selectionLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    var radioEmpty = UIImage(named: "radio-empty")
    var radioFull = UIImage(named: "radio-full")


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            selectionImg.image = radioFull
            itemImg.tintColor = .mainColor
            selectionImg.tintColor = .mainColor
            selectionLbl.textColor = .mainColor
        } else {
            selectionImg.image = radioEmpty
            itemImg.tintColor = .black
            selectionImg.tintColor = .black
            selectionLbl.textColor = .black
        }
    }
    

    func configure(title: String?, img: UIImage?) {
        selectionLbl.text = title
        itemImg.image = img
    }
    
    func hideImage() {
        itemImg.isHidden = true
    }

//    func setSelected() {
//        selectionImg.image = radioFull
//        itemImg.tintColor = .mainColor
//        selectionImg.tintColor = .mainColor
//        selectionLbl.textColor = .mainColor
//    }
//
//    func setDeSelected() {
//        selectionImg.image = radioEmpty
//        itemImg.tintColor = .black
//        selectionImg.tintColor = .black
//        selectionLbl.textColor = .black
//    }

}
