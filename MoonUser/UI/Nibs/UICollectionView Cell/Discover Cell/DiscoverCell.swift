//
//  DiscoverCell.swift
//  MoonUser
//
//  Created by Grand iMac on 1/29/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import SDWebImage

class DiscoverCell: UICollectionViewCell, DiscoverCellView {

    @IBOutlet weak fileprivate var dicoverImg: UIImageView!
    @IBOutlet weak fileprivate var shadowView: UIView!
    @IBOutlet weak fileprivate var videoImg: UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        dicoverImg.fullScreenWhenTapped()
    }
    
    func configure(img: String?) {
        let imgURL = URL(string: img ?? "")
        dicoverImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        dicoverImg.sd_setImage(with: imgURL, placeholderImage: .placeHolder_image)
    }
    
    func isVideo(_ isVideo: Bool) {
        if isVideo {
            shadowView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
            videoImg.isHidden = false
        } else {
            shadowView.backgroundColor = .clear
            shadowView.isHidden = true
            videoImg.isHidden = true
        }
    }


}
