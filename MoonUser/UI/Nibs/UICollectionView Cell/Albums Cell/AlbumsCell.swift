//
//  AlbumsCell.swift
//  MoonUser
//
//  Created by Grand iMac on 2/25/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import SDWebImage


class AlbumsCell: UICollectionViewCell, AlbumsCellView  {
    
    @IBOutlet weak var adsTitleLbl: UILabel!
    @IBOutlet weak var adsTitleHeight: NSLayoutConstraint!
    @IBOutlet weak var albumImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var albumStack: UIStackView!
    @IBOutlet weak var albumStackHeight: NSLayoutConstraint!
    @IBOutlet weak fileprivate var shadowView: UIView!
    @IBOutlet weak fileprivate var videoImg: UIImageView!

    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setPhoto(with Url: String?) {
        let url = URL(string: Url ?? "")
        albumImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        albumImg.sd_setImage(with: url, placeholderImage: .placeHolder_image)
    }
    
    func setName(_ name: String?) {
        nameLbl.text = name
        adsTitleLbl.text = name
    }
    
    func setdesc(_ desc: String?) {
        descLbl.text = desc
    }
    
    func setIsAlbum() {
        albumStack.isHidden = false
        albumStackHeight.constant = 40

        adsTitleLbl.isHidden = true
        adsTitleHeight.constant = 0
    }
    
    func setIsAds() {
        adsTitleLbl.isHidden = false
        adsTitleHeight.constant = 20

        albumStack.isHidden = true
        albumStackHeight.constant = 0
    }
    
    func isVideo(_ isVideo: Bool) {
        if isVideo {
            shadowView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
            videoImg.isHidden = false
        } else {
            shadowView.backgroundColor = .clear
            shadowView.isHidden = true
            videoImg.isHidden = true
        }
    }




}
