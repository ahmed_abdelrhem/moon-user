//
//  ProductCell.swift
//  MoonUser
//
//  Created by Grand iMac on 2/2/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import SDWebImage

class ProductCell: UICollectionViewCell, ProductCellView {
    
    @IBOutlet weak fileprivate var productImg: UIImageView!
    @IBOutlet weak fileprivate var productNameLbl: UILabel!
    @IBOutlet weak fileprivate var productPriceLbl: UILabel!
    @IBOutlet weak fileprivate var productDescriptionLbl: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure(image: String?, name: String?, price: String?, desc: String?) {
        productImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        let imgURL = URL(string: image ?? "")
        productImg.sd_setImage(with: imgURL, placeholderImage: .placeHolder_image)
        productNameLbl.text = name
        productPriceLbl.text = "\(price ?? "0") \(DEF.country.currency)"
        productDescriptionLbl.text = desc
    }
    

}
