//
//  ColorCell.swift
//  MoonUser
//
//  Created by Grand iMac on 2/9/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class ColorCell: UICollectionViewCell, ColorCellView {

    @IBOutlet weak fileprivate var borderView: UIViewX!
    @IBOutlet weak fileprivate var colorView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override var isSelected: Bool {
        get {
             return super.isSelected
        } set {
             super.isSelected = newValue
            if (newValue) {
                borderView.borderWidth = 1.5
            } else {
                borderView.borderWidth = 0
            }
        }
    }
    
    func cellConfigure(color: String?) {
        colorView.backgroundColor = HexStringToUIColor.hexStringToUIColor(hex: color!)
    }

    
}
