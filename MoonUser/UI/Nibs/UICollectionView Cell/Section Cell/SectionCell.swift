//
//  SectionCell.swift
//  MoonUser
//
//  Created by Grand iMac on 2/2/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class SectionCell: UICollectionViewCell, SectionCellView {

    @IBOutlet weak fileprivate var sectionTitleLbl: UILabel!
    @IBOutlet weak fileprivate var footerView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override var isSelected: Bool {
        get {
             return super.isSelected
        } set {
             super.isSelected = newValue
            if (newValue) {
                footerView.backgroundColor = .white
            } else {
                footerView.backgroundColor = .clear
            }
        }
    }
    
    func setName(name: String?) {
        sectionTitleLbl.text = name
    }

}
