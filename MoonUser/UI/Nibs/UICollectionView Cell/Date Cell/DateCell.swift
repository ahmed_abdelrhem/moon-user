//
//  DateCell.swift
//  MoonUser
//
//  Created by Grand iMac on 3/1/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

protocol DateCellDelegate {
    func configure(_ num: String?, _ day: String?, _ mon: String?)
}


class DateCell: UICollectionViewCell, DateCellDelegate {

    @IBOutlet weak fileprivate var selectView: UIView!
    @IBOutlet weak fileprivate var monthLbl: UILabel!
    @IBOutlet weak fileprivate var DateLbl: UILabel!
    @IBOutlet weak fileprivate var DayLbl: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override var isSelected: Bool {
        get {
             return super.isSelected
        } set {
             super.isSelected = newValue
            if (newValue) {
                selectView.backgroundColor = .mainColor
                monthLbl.textColor = .mainColor
                DateLbl.textColor = .mainColor
                DayLbl.textColor = .mainColor
            } else {
                selectView.backgroundColor = .white
                monthLbl.textColor = .black
                DateLbl.textColor = .black
                DayLbl.textColor = .black
            }
        }
    }
    
    
    func configure(_ num: String?, _ day: String?, _ mon: String?) {
        DateLbl.text = num
        DayLbl.text = day
        monthLbl.text = mon
    }
    
    
}
