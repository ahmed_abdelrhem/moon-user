//
//  FamousPeopleCell.swift
//  Moon Shop
//
//  Created by apple on 2/9/20.
//  Copyright © 2020 2grand. All rights reserved.
//

import UIKit
import SDWebImage

protocol FamousPeopleCellView:class {
    func setPhoto(with Url: String)
    func setName(_ name:String)
    
}


class FamousPeopleCell: UICollectionViewCell, FamousPeopleCellView {

    @IBOutlet weak var namelbl: UILabel!
    @IBOutlet weak var photo: UIImageViewX!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    
    func setPhoto(with Url: String) {
        let logo_url = URL(string: Url)
        photo.sd_imageIndicator = SDWebImageActivityIndicator.gray
        photo.sd_setImage(with: logo_url, placeholderImage: .avatar_image)
    }
    
    func setName(_ name: String) {
        namelbl.text = name
    }
    
}
