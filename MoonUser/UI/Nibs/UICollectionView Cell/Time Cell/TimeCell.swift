//
//  TimeCell.swift
//  MoonUser
//
//  Created by Grand iMac on 3/1/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

protocol TimeCellView {
    func cellConfigue(_ time: String?)
    func setIsReserved()
}

class TimeCell: UICollectionViewCell, TimeCellView {
    
    @IBOutlet weak fileprivate var colorView: UIView!
    @IBOutlet weak fileprivate var timeLbl: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override var isSelected: Bool {
        get {
             return super.isSelected
        } set {
             super.isSelected = newValue
            if (newValue) {
                colorView.backgroundColor = .mainColor
                timeLbl.textColor = .white
            } else {
                colorView.backgroundColor = .white
                timeLbl.textColor = .black
            }
        }
    }

    func cellConfigue(_ time: String?) {
        timeLbl.text = time
    }
    
    func setIsReserved() {
        colorView.backgroundColor = .red
        timeLbl.textColor = .white
    }
}
