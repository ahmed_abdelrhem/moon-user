//
//  FamousHeader.swift
//  MoonUser
//
//  Created by Grand iMac on 2/25/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import SDWebImage

protocol FamousHeaderDelegate : class{
    func chatBtnPressed()
    func followBtnPressed()
    func shopViewPressed()
    func segmentControlChange(_ index: Int)
    func filterBtnPressed()
}



class FamousHeader: UICollectionReusableView, FamousHeaderView {
    
    weak var delegate : FamousHeaderDelegate?
    private let buttonBar = UIView()

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var famousImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var followersLbl: UILabel!
    @IBOutlet weak var shopView: UIView!
    @IBOutlet weak var segmentView: UIView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var followBtn: UIButton!
    @IBOutlet weak var chatBtn: UIButton!
    @IBOutlet weak var filterStack: UIStackView!
    @IBOutlet weak var filterStackHeight: NSLayoutConstraint!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        addGestures()
        SharedHandller.instance.setupSegmentFamousHeader(view: segmentView, segmentControl: segmentControl, numberOfSegments: CGFloat(segmentControl!.numberOfSegments))
    }
    
    
    @objc private func segmentedControlValueChanged(_ sender: UISegmentedControl) {
        delegate?.segmentControlChange(sender.selectedSegmentIndex)
        UIView.animate(withDuration: 0.3) {
            self.buttonBar.frame.origin.x = (self.segmentView.frame.width / 2) * CGFloat(sender.selectedSegmentIndex)
        }
    }
    
    private func addGestures() {
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.shopViewClicked))
        shopView.addGestureRecognizer(tap1)
    }

    @objc func shopViewClicked() {
        delegate?.shopViewPressed()
    }
    
    
    func setPhoto(with Url: String?) {
        let url = URL(string: Url ?? "")
        famousImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        famousImg.sd_setImage(with: url, placeholderImage: .avatar_image)
    }
    
    func setName(_ name: String?) {
        nameLbl.text = name
    }
    
    func setIsShop(_ isShop: Bool) {
        shopView.isHidden = !isShop
    }
    
    func setFollower(_ followers: String?) {
        followersLbl.text = followers ?? "0"
    }
    
    func setIsFollow(_ isFollow: Bool) {
        followBtn.isSelected = isFollow
    }
    
    func hideFilter(_ hidden: Bool) {
        filterStack.isHidden = hidden
        filterStackHeight.constant = hidden ? 0 :  35
    }

    func hideChat(_ hidden: Bool) {
        chatBtn.isHidden = hidden
    }
    
    
    
    @IBAction func chatBtnClicked(_ sender: UIButton) {
        delegate?.chatBtnPressed()
    }
    
    @IBAction func followBtnClicked(_ sender: UIButton) {
        followBtn.isSelected = !followBtn.isSelected
        delegate?.followBtnPressed()
    }
    
    
    @IBAction func filterBtnPressed(_ sender: UIButton) {
        delegate?.filterBtnPressed()
    }
    

    @IBAction func segmentValueChanged(_ sender: UISegmentedControl) {
        delegate?.segmentControlChange(sender.selectedSegmentIndex)
    }
    
    


}
