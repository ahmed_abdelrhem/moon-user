//
//  TruckCell.swift
//  MoonUser
//
//  Created by Grand iMac on 3/9/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import SDWebImage

protocol TruckCellView {
    func configure(image: String?, name: String?)
}

class TruckCell: UICollectionViewCell, TruckCellView {

    @IBOutlet weak fileprivate var containerView: UIViewX!
    @IBOutlet weak fileprivate var carImg: UIImageView!
    @IBOutlet weak fileprivate var carNameLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override var isSelected: Bool {
        get {
             return super.isSelected
        } set {
             super.isSelected = newValue
            if (newValue) {
                containerView.borderColor = .mainColor
            } else {
                containerView.borderColor = .clear
            }
        }
    }
    
    func configure(image: String?, name: String?) {
        let imgURL = URL(string: image ?? "")
        carImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        carImg.sd_setImage(with: imgURL, placeholderImage: .placeHolder_image)
        carNameLbl.text = name
    }

}
