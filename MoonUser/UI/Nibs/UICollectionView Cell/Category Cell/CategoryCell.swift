//
//  CategoryCell.swift
//  MoonUser
//
//  Created by Grand iMac on 1/26/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import SDWebImage

class CategoryCell: UICollectionViewCell, StoryCellConfigure {

    @IBOutlet weak fileprivate var categoryImg: UIImageView!
    @IBOutlet weak fileprivate var categoryNameLbl: UILabel!

    override func awakeFromNib() {
       super.awakeFromNib()
    }

    func configure(image: String?, name: String?) {
        let imgURL = URL(string: image ?? "")
        categoryImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        categoryImg.sd_setImage(with: imgURL, placeholderImage: .placeHolder_image)
        categoryNameLbl.text = name
    }

}
