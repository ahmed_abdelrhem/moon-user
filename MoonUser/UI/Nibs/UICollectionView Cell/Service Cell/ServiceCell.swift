//
//  ServiceCell.swift
//  MoonUser
//
//  Created by Grand iMac on 1/27/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import SDWebImage

class ServiceCell: UICollectionViewCell, ServiceCellView {

    @IBOutlet weak fileprivate var containerView: UIViewX!
    @IBOutlet weak fileprivate var categoryImg: UIImageView!
    @IBOutlet weak fileprivate var categoryNameLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override var isSelected: Bool {
        get {
             return super.isSelected
        } set {
             super.isSelected = newValue
            if (newValue) {
                containerView.borderColor = .mainColor
                categoryNameLbl.textColor = .mainColor
                categoryImg.setImageColor(color: .mainColor)
            } else {
                containerView.borderColor = .white
                categoryNameLbl.textColor = .black
                categoryImg.setImageColor(color: .black)
            }
        }
    }

    
    func configure(image: String?, name: String?) {
        let imgURL = URL(string: image ?? "")
        categoryImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        categoryImg.sd_setImage(with: imgURL, placeholderImage: .placeHolder_image)
        categoryNameLbl.text = name
    }
    
    func setIsSelected(_ isSelected: Bool) {
        if isSelected {
            containerView.borderColor = .mainColor
            categoryNameLbl.textColor = .mainColor
            categoryImg.setImageColor(color: .mainColor)
        } else {
            containerView.borderColor = .white
            categoryNameLbl.textColor = .black
            categoryImg.setImageColor(color: .black)
        }
        
    }


}

