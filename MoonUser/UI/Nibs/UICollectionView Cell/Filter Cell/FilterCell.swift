//
//  FilterCell.swift
//  MoonUser
//
//  Created by Grand iMac on 2/26/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

class FilterCell: UICollectionViewCell, FilterCellView {

    @IBOutlet weak fileprivate var containerView: UIViewX!
    @IBOutlet weak fileprivate var shopNameLbl: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override var isSelected: Bool {
        get {
             return super.isSelected
        } set {
             super.isSelected = newValue
            if (newValue) {
                containerView.backgroundColor = .darkBlueColor
                shopNameLbl.textColor = .white
            } else {
                containerView.backgroundColor = .white
                shopNameLbl.textColor = .black
            }
        }
    }
    
    
    func setShopName(_ name: String?) {
        shopNameLbl.text = name
    }

}
