//
//  ShowLocationVC.swift
//  Tabadul
//
//  Created by Islamic on 3/5/19.
//  Copyright © 2019 2grand. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit

class ShowLocationVC: UIViewController {

    //MARK:- Outelts
    @IBOutlet weak var myMapView: UIView!
    
    //MARK: Variables
    var mapView: GMSMapView!
    var lat: Double? = 0.0
    var lng: Double? = 0.0

    //MARK:- Storyboard
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMapView()
    }

    
    //MARK:- Actions
    @IBAction func okBtnPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    


}


//MARK:- Maps Helper Funcs
extension ShowLocationVC: CLLocationManagerDelegate, GMSMapViewDelegate {
    
    func setupMapView(){
        mapView = GMSMapView()
        self.MapSetup(lat:"\(self.lat!)", long: "\(self.lng!)")
    }
    
    func MapSetup(lat: String , long: String) {
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(lat)!, longitude: CLLocationDegrees(long)!, zoom: 16)
        let f: CGRect = view.frame
        let mapFrame = CGRect(x: f.origin.x, y: 0, width: f.size.width, height: f.size.height)
        mapView = GMSMapView.map(withFrame: mapFrame, camera: camera)
        mapView.settings.compassButton = true
        mapView.delegate = self
        self.myMapView.addSubview(mapView)
        MapMarkerSetup()
    }
    
    func MapMarkerSetup() {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: self.lat!, longitude: self.lng!)
        marker.icon = .pickup
        marker.map = mapView
        mapView.selectedMarker = marker
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        //TODO:- Goolge map Navigaion
        let destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: CLLocationDegrees(self.lat!), longitude: CLLocationDegrees(self.lng!))))
        MKMapItem.openMaps(with: [destination], launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
        return true
    }

    
}
