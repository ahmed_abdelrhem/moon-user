//
//  FilterVC.swift
//  MoonUser
//
//  Created by Grand iMac on 1/28/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

protocol FilterVCDelegate: class {
    func retriveID(_ id: Int)
}

class FilterVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var ratingBtn: UIButton! {
        didSet {
            ratingBtn.tag = 1
        }
    }
    @IBOutlet weak var newestBtn: UIButton!{
        didSet {
            newestBtn.tag = 2
        }
    }
    @IBOutlet weak var AToZBtn: UIButton!{
        didSet {
            AToZBtn.tag = 3
        }
    }
    @IBOutlet weak var minOrderBtn: UIButton!{
        didSet {
            minOrderBtn.tag = 4
        }
    }
    @IBOutlet weak var backBtn: UIButton!

    
    //MARK:- Variables
    weak var delegate: FilterVCDelegate?
    private var selectedFilterID: Int = 1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ratingBtn.isSelected = true
        minOrderBtn.isHidden = !(DEF.type == 1 && DEF.flag == 1)
        backBtn.RotatIfNeeded()
    }

    @IBAction func filterBtnClicked(_ sender: UIButton) {
        selectedFilterID = sender.tag
        ratingBtn.isSelected = sender.tag == 1
        newestBtn.isSelected = sender.tag == 2
        AToZBtn.isSelected = sender.tag == 3
        minOrderBtn.isSelected = sender.tag == 4
    }
    
    
    @IBAction func submitBtnClicked(_ sender: UIButton) {
        delegate?.retriveID(self.selectedFilterID)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }


    
  

}
