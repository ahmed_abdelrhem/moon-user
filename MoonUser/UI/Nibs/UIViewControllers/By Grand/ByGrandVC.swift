//
//  ByGrandVC.swift
//  DrWash
//
//  Created by Islamic on 1/20/19.
//  Copyright © 2019 2grand. All rights reserved.
//

import UIKit

class ByGrandVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    
    
    @IBAction func closeButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func openSite(_ sender: Any) {
        if let url = URL(string: "http://2grand.net") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func call(_ sender: Any) {
        let url:NSURL = NSURL(string: "tel://00201157771069")!
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    }
    
    @IBAction func whatsApp(_ sender: Any) {
        let date = Date()
        let msg = "Hello \(date)"
        let urlWhats = "whatsapp://send?phone=+201157771069&abid=00201157771069&text=\(msg)"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = NSURL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                    UIApplication.shared.open(whatsappURL as URL, options: [:], completionHandler: nil)
                } else {
                    Messages.instance.showConfigMessage(title: "", body:  "Whatsapp is not installed", state: .warning, layout: .cardView, style: .top)
                }
            }
        }
    }
    
}
