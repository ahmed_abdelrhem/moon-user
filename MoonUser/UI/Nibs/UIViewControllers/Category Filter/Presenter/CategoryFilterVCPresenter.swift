//
//  CategoryFilterVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 2/19/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol CategoryFilterVCView: LoaderDelegate {
    func retriveDate(catID: Int, subCat: Int)
}

class CategoryFilterVCPresenter {
    
    private weak var view: CategoryFilterVCView?
    private let interactor = CategoryFilterVCAPI()
    private var categories = [CategoryFilter]()
    private var subCategories = [CategoryService]()
    private var selectedCategoryID: Int = 0
    private var selectedSubCategoryID: Int = 0
    
    
    init(view: CategoryFilterVCView) {
        self.view = view
    }
    
    var count: Int {
        return selectedCategoryID == 0 ? categories.count : subCategories.count
    }
    
    var headerTitle: String {
        return selectedCategoryID == 0 ? CHOOSE_CATEGORY : CHOOSE_SUBCATEGORY
    }
    
    func viewDidLoad(serviceID: Int) {
                
        if selectedCategoryID == 0 {
            view?.showProgress()
            
            interactor.getAllCategories(serviceID: serviceID, didDataReady: { [weak self](response) in
                guard self != nil else { return }
                self?.view?.hideProgress()
                if response.status == StatusCode.Success.rawValue {
                    self?.categories = response.data ?? []
                    self?.view?.onSuccess("")
                } else if response.status == StatusCode.ExpiredJWT.rawValue {
                    Messages.instance.loginMessage()
                } else {
                    self?.view?.onFailure(response.message ?? "")
                }
            }) { [weak self](error) in
                guard self != nil else { return }
                self?.view?.hideProgress()
                self?.view?.onEmpty(.no_connection_image, CONNECTION_ERROR)
            }
        }
    }
    
    func cellConfigure(cell: MenuCellView, index: Int) {
        if selectedCategoryID == 0 {
            let item = categories[index]
            cell.setName(name: item.name ?? "")
            cell.setPhoto(with: item.image ?? "")
        } else {
            let item = subCategories[index]
            cell.setName(name: item.name ?? "")
            cell.setPhoto(with: item.image ?? "")
        }
    }
    
    func didselect(at index: Int) {
        if selectedCategoryID == 0 {
            let cat = categories[index]
            self.selectedCategoryID = cat.id ?? 0
            self.subCategories = cat.sub_category ?? []
            view?.onSuccess("")
        } else {
            let cat = subCategories[index]
            self.selectedSubCategoryID = cat.id ?? 0
            view?.retriveDate(catID: selectedCategoryID, subCat: selectedSubCategoryID)
        }
    }
        
    
}
