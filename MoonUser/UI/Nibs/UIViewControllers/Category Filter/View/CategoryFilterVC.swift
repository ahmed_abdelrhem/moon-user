//
//  CategoryFilterVC.swift
//  MoonUser
//
//  Created by Grand iMac on 2/18/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit

protocol CategoryFilterDelegate: class {
    func retriveDate(catID: Int, subCat: Int)
}

class CategoryFilterVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(UINib(nibName: "MenuCell", bundle: nil), forCellReuseIdentifier: "MenuCell")
            tableView.tableFooterView = UIView(frame: .zero)
            tableView.dataSource = self
            tableView.delegate = self
            tableView.separatorStyle = .singleLine
            tableView.allowsMultipleSelection = false
        }
    }
    @IBOutlet weak var backBtn: UIButton!

    

    //MARK:- Variables
    internal var presenter: CategoryFilterVCPresenter!
    weak var delegate: CategoryFilterDelegate?
    var serviceID: Int?
    
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setPresenter()
        backBtn.RotatIfNeeded()
    }

    
    //MARK:- Functions
    private func setPresenter() {
        presenter = CategoryFilterVCPresenter(view: self)
        presenter.viewDidLoad(serviceID: serviceID ?? 0)
    }

    
    
    //MARK:- Actions
    @IBAction func backBtnClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }


}



