//
//  CategoryFilterVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 2/19/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension CategoryFilterVC: CategoryFilterVCView {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        tableView.reloadData()
        if tableView.numberOfRows(inSection: 0) == 0 {
            tableView.setEmptyView(title: NO_DATA_FOUND, messageImage: .no_data_image)
        }
    }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func onEmpty(_ img: UIImage, _ msg: String) {
        tableView.setEmptyView(title: msg, messageImage: img)
    }
    
    func retriveDate(catID: Int, subCat: Int) {
        delegate?.retriveDate(catID: catID, subCat: subCat)
        dismiss(animated: true, completion: nil)
    }
}

