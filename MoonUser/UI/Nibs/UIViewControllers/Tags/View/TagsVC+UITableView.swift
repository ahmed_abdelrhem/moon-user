//
//  TagsVC+UITableView.swift
//  MoonUser
//
//  Created by Grand iMac on 2/19/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension TagsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return serviceID != nil ? presenter.count : cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TagsCell") as? TagsCell else {
            return UITableViewCell()
        }
        if serviceID != nil {
            presenter.cellConfigure(cell: cell, index: indexPath.row)
        } else {
            let name = cities[indexPath.row].city_name
            presenter.cityConfigure(cell: cell, name: name)
        }
        return cell
    }
        
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
        
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel()
        label.backgroundColor = .groupTableViewBackground
        label.font = UIFont.myMeduimSystemFont(ofSize: 17)
        label.text = serviceID != nil ? ALL_TAGS : SELECT_CITY
        label.textColor = .black
        return label
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 48
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let id = serviceID != nil ? presenter.tags[indexPath.row].id ?? 0 : cities[indexPath.row].id ?? 0
        selectedTags.append(id)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let id = serviceID != nil ? presenter.tags[indexPath.row].id ?? 0  : cities[indexPath.row].id ?? 0
        let index = selectedTags.firstIndex(where: { $0 == id})
        self.selectedTags.remove(at: index!)
    }
    
    
}

