//
//  TagsVC.swift
//  MoonUser
//
//  Created by Grand iMac on 1/28/20.
//  Copyright © 2020 Organization. All rights reserved.
//
 
import UIKit

protocol TagsVCDelegate: class {
    func retriveIDs(_ ids: [Int])
}

class TagsVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView(frame: .zero)
            tableView.dataSource = self
            tableView.delegate = self
            tableView.separatorStyle = .none
            tableView.estimatedRowHeight = 100
            tableView.allowsMultipleSelection = serviceID != nil
            tableView.register(UINib(nibName: "TagsCell", bundle: nil), forCellReuseIdentifier: "TagsCell")
        }
    }
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    
    
    //MARK:- Variables
    internal var presenter: TagsVCPresenter!
    var serviceID: Int?
    weak var delegate: TagsVCDelegate?
    internal var selectedTags = [Int]()
    internal var cities: [Cities] = DEF.country.cities
    
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setPresenter()
        titleLbl.text = serviceID != nil ? TAGS : CITIES
        backBtn.RotatIfNeeded()
    }
    
    
    //MARK:- Functions
    private func setPresenter() {
        presenter = TagsVCPresenter(view: self)
        if serviceID != nil {
            presenter.viewDidLoad(serviceID: serviceID!)
        }
    }

    
    //MARK:- Actions
    @IBAction func submitBtnClicked(_ sender: UIButton) {
        if serviceID != nil {
            //Handle Multiple Selection
            delegate?.retriveIDs(self.selectedTags)
        } else {
            //Handle Single Selection
            delegate?.retriveIDs([self.selectedTags.last ?? 0])
        }
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }



   
}


