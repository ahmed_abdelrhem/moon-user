//
//  TagsVC+PresenterDelegate.swift
//  MoonUser
//
//  Created by Grand iMac on 2/19/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

extension TagsVC: LoaderDelegate {
    
    func showProgress() {
        self.startAnimating()
    }
    
    func hideProgress() {
        self.stopAnimating()
    }
    
    func onSuccess(_ msg: String) {
        tableView.reloadData()
    }
    
    func onFailure(_ msg: String) {
        Messages.instance.showConfigMessage(title: "", body: msg, state: .error, layout: .messageView, style: .top)
    }
    
    func onEmpty(_ img: UIImage, _ msg: String) {
        tableView.setEmptyView(title: msg, messageImage: img)
    }
}
