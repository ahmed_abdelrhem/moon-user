//
//  TagsVCAPI.swift
//  MoonUser
//
//  Created by Grand iMac on 1/28/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

class TagsVCAPI {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias model = (TagsModel)->()
    
    func getAllTags(serviceID: Int, didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorCompletionType) {
        
        let param = [
            "type": DEF.type,
            "service_id": serviceID
        ]
        
        let url = URLs.tags.url.concatURL(param)
        
        apiManager.contectToApiWith(url: url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
             
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                do {
                    let result = try self.decoder.decode(TagsModel.self, from: data)
                    didDataReady(result)
                }catch{
                    print("error\(error)")
                }
                
            }
        }) { (error, msg) in
            print(error, msg!)
            errorCompletion(error)
        }
    }
    
}
