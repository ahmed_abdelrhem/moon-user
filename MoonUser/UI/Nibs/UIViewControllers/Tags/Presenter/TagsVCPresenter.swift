//
//  TagsVCPresenter.swift
//  MoonUser
//
//  Created by Grand iMac on 1/29/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import Foundation

protocol TagsCellView {
    func configure(_ tag: String?)
}

class TagsVCPresenter {
    
    private weak var view: LoaderDelegate?
    private let interactor = TagsVCAPI()
    private (set) var tags = [Tag]()
    
    
    init(view: LoaderDelegate) {
        self.view = view
    }
    
    var count: Int {
        return tags.count
    }
    
    func viewDidLoad(serviceID: Int) {
        view?.showProgress()
        
        interactor.getAllTags(serviceID: serviceID, didDataReady: { [weak self](response) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            if response.status == StatusCode.Success.rawValue {
                self?.tags = response.data ?? []
                if self?.tags.count == 0 {
                    self?.view?.onEmpty(.no_data_image, NO_DATA_FOUND)
                } else {
                    self?.view?.onSuccess("")
                }
            } else {
                self?.view?.onFailure(response.message ?? "")
            }
        }) { [weak self](error) in
            guard self != nil else { return }
            self?.view?.hideProgress()
            self?.view?.onEmpty(.no_connection_image, CONNECTION_ERROR)
        }
    }
    
    func cellConfigure(cell: TagsCellView, index: Int) {
        cell.configure(tags[index].name)
    }
    
    func cityConfigure(cell: TagsCellView, name: String?) {
        cell.configure(name)
    }
    
    
}
