//
//  FullImageVC.swift
//  ByDeals
//
//  Created by Grand iMac on 1/7/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit


class FullImageVC: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var displayImg: UIImageView!
    @IBOutlet weak var backBtn: UIButton!
    
    var image: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setScrollView()
        displayImg.image = image
        backBtn.RotatIfNeeded()
    }
    
    
    func setScrollView() {
        scrollView.delegate = self
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 6.0
    }
    
    
    @IBAction func closeBtnClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
}

    
extension FullImageVC: UIScrollViewDelegate {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return displayImg
    }


}
